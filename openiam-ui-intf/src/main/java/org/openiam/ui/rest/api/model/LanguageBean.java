package org.openiam.ui.rest.api.model;

public class LanguageBean extends KeyNameAndDisplayNameBean {

    private String languageCode;
    private boolean isDefault;
    private boolean isUsed;

    public LanguageBean() {
        super();
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public boolean getIsDefault() {
        return isDefault;
    }

    public boolean getIsUsed() {
        return isUsed;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public void setIsUsed(boolean isUsed) {
        this.isUsed = isUsed;
    }

}
