package org.openiam.ui.rest.api.model;

import org.openiam.idm.srvc.user.dto.UserNote;
import org.openiam.ui.web.model.AbstractBean;
import java.util.Date;


public class UserNoteBean extends AbstractBean {

    private Date createDate;
    private String createdBy;
    private String description;
    private String noteType;
    private String userId;

    public UserNoteBean(UserNote userNote) {
        this.createDate = userNote.getCreateDate();
        this.createdBy = userNote.getCreatedBy();
        this.description = userNote.getDescription();
        this.noteType = userNote.getNoteType();
        this.userId = userNote.getUserId();
        this.setId(userNote.getUserNoteId());
    }

    public UserNoteBean() {

    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserNote getUserNote() {
        UserNote userNote = new UserNote();
        userNote.setCreateDate(this.getCreateDate());
        userNote.setCreatedBy(this.getCreatedBy());
        userNote.setDescription(this.getDescription());
        userNote.setNoteType(this.getNoteType());
        userNote.setUserId(this.getUserId());
        userNote.setUserNoteId(this.getId());
        return userNote;
    }
}
