package org.openiam.ui.rest.api.model;

public class KeyNameAndDisplayNameBean extends KeyNameBean {

    private String displayName;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

}