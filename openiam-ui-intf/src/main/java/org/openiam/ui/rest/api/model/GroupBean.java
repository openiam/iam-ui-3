package org.openiam.ui.rest.api.model;

import org.openiam.base.AttributeOperationEnum;
import org.openiam.idm.srvc.grp.dto.Group;

public class GroupBean extends AbstractEntitlementsBean {

	
	public GroupBean() {}

	public GroupBean(Group grp) {
		this.setId(grp.getId());
		this.setName(grp.getName());
		this.setManagedSysId(grp.getManagedSysId());
		this.setManagedSysName(grp.getManagedSysName());
		this.setOperation(grp.getOperation());
		this.setMdTypeId(grp.getMdTypeId());
		this.setMetadataTypeName(grp.getMetadataTypeName());
		this.setDescription(grp.getDescription());
	}

	public Group getGroup() {
		Group grp = new Group();
		grp.setId(this.getId());
		grp.setName(this.getName());
		grp.setManagedSysId(this.getManagedSysId());
		grp.setManagedSysName(this.getManagedSysName());
		grp.setOperation(this.getOperation());
		grp.setMdTypeId(this.getMdTypeId());
		grp.setMetadataTypeName(this.getMetadataTypeName());
		grp.setDescription(this.getDescription());
		return grp;
	}
	
	private String managedSysId;
    private String managedSysName;
	public String getManagedSysId() {
		return managedSysId;
	}
	public void setManagedSysId(String managedSysId) {
		this.managedSysId = managedSysId;
	}
	public String getManagedSysName() {
		return managedSysName;
	}
	public void setManagedSysName(String managedSysName) {
		this.managedSysName = managedSysName;
	}
}
