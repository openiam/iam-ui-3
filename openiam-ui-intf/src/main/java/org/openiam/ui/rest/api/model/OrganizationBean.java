package org.openiam.ui.rest.api.model;

import org.openiam.idm.srvc.org.dto.Organization;
import org.openiam.idm.srvc.org.dto.OrganizationType;
import org.openiam.ui.rest.api.model.KeyNameBean;
import org.openiam.ui.web.model.AbstractBean;

public class OrganizationBean extends KeyNameBean {

    public OrganizationBean() {
    }

    private String type;
    private String organizationTypeId;
    private String organizationTypeName;
    private String organizationType;
    private String organizationUser;
    private boolean selectable = true;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSelectable() {
        return selectable;
    }

    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    public String getOrganizationUser() {
        return organizationUser;
    }

    public void setOrganizationUser(String organizationUser) {
        this.organizationUser = organizationUser;
    }

    public String getOrganizationTypeId() {
        return organizationTypeId;
    }

    public void setOrganizationTypeId(String organizationTypeId) {
        this.organizationTypeId = organizationTypeId;
    }

    public String getOrganizationTypeName() {
        return organizationTypeName;
    }

    public void setOrganizationTypeName(String organizationTypeName) {
        this.organizationTypeName = organizationTypeName;
    }

    public String getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(String organizationType) {
        this.organizationType = organizationType;
    }
}
