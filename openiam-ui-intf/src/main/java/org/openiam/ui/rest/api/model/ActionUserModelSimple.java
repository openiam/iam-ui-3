package org.openiam.ui.rest.api.model;

import org.openiam.idm.srvc.user.dto.UserAttribute;
import org.openiam.ui.web.util.DateFormatStr;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class ActionUserModelSimple implements Serializable {
    private String id;
    private List<UserAttribute> userAttributes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<UserAttribute> getUserAttributes() {
        return userAttributes;
    }

    public void setUserAttributes(List<UserAttribute> userAttributes) {
        this.userAttributes = userAttributes;
    }
}
