package org.openiam.ui.rest.api.model;

import org.openiam.ui.web.model.AbstractBean;

public class SyncPasswordFromSource extends AbstractBean {

    private String serviceAccountLogin;
    private String serviceAccountPassword;
    private String userLogin;
    private String userPassword;
    private String managedSystemId;
    private boolean preventChangeCountIncrement;
    private boolean forceChange;

    public String getServiceAccountLogin() {
        return serviceAccountLogin;
    }

    public void setServiceAccountLogin(String serviceAccountLogin) {
        this.serviceAccountLogin = serviceAccountLogin;
    }

    public String getServiceAccountPassword() {
        return serviceAccountPassword;
    }

    public void setServiceAccountPassword(String serviceAccountPassword) {
        this.serviceAccountPassword = serviceAccountPassword;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getManagedSystemId() {
        return managedSystemId;
    }

    public void setManagedSystemId(String managedSystemId) {
        this.managedSystemId = managedSystemId;
    }

    public boolean isPreventChangeCountIncrement() {
        return preventChangeCountIncrement;
    }

    public void setPreventChangeCountIncrement(boolean preventChangeCountIncrement) {
        this.preventChangeCountIncrement = preventChangeCountIncrement;
    }

    public boolean isForceChange() {
        return forceChange;
    }

    public void setForceChange(boolean forceChange) {
        this.forceChange = forceChange;
    }
}
