package org.openiam.ui.rest.api.model;

import org.openiam.base.AttributeOperationEnum;
import org.openiam.idm.srvc.role.dto.Role;

/**
 * Created with IntelliJ IDEA.
 * User: alexander
 * Date: 10/26/13
 * Time: 12:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class RoleBean extends AbstractEntitlementsBean {

    private String managedSysId;
    private String managedSysName;

    public RoleBean(){}

    public RoleBean(Role role){
        this.setId(role.getId());
        this.setName(role.getName());
        this.setManagedSysId(role.getManagedSysId());
        this.setManagedSysName(role.getManagedSysName());
        this.setAccessRightIds(role.getAccessRightIds());
        this.setDescription(role.getDescription());
        this.setMdTypeId(role.getMdTypeId());
        this.setMetadataTypeName(role.getMetadataTypeName());
        this.setOperation(role.getOperation());
    }

    public Role getRole() {
        Role role = new Role();
        role.setId(this.getId());
        role.setName(this.getName());
        role.setManagedSysId(this.getManagedSysId());
        role.setManagedSysName(this.getManagedSysName());
        role.setAccessRightIds(this.getAccessRightIds());
        role.setDescription(this.getDescription());
        role.setMdTypeId(this.getMdTypeId());
        role.setMetadataTypeName(this.getMetadataTypeName());
        role.setOperation(this.getOperation());
        return role;
    }


    public String getManagedSysId() {
        return managedSysId;
    }
    public void setManagedSysId(String managedSysId) {
        this.managedSysId = managedSysId;
    }
    public String getManagedSysName() {
        return managedSysName;
    }
    public void setManagedSysName(String managedSysName) {
        this.managedSysName = managedSysName;
    }

}
