package org.openiam.ui.rest.api.model;

import org.openiam.base.AttributeOperationEnum;

import java.util.Set;

public abstract class AbstractEntitlementsBean extends KeyNameDescriptionBean {
    private AttributeOperationEnum operation = AttributeOperationEnum.NO_CHANGE;
    private Set<String> accessRightIds;
    private String mdTypeId;
    private String metadataTypeName;

    public Set<String> getAccessRightIds() {
        return accessRightIds;
    }

    public void setAccessRightIds(Set<String> accessRightIds) {
        this.accessRightIds = accessRightIds;
    }

    public String getMdTypeId() {
        return mdTypeId;
    }

    public void setMdTypeId(String mdTypeId) {
        this.mdTypeId = mdTypeId;
    }

    public String getMetadataTypeName() {
        return metadataTypeName;
    }

    public void setMetadataTypeName(String metadataTypeName) {
        this.metadataTypeName = metadataTypeName;
    }

    public AttributeOperationEnum getOperation() {
        return operation;
    }

    public void setOperation(AttributeOperationEnum operation) {
        this.operation = operation;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((accessRightIds == null) ? 0 : accessRightIds.hashCode());
        result = prime * result
                + ((mdTypeId == null) ? 0 : mdTypeId.hashCode());
        result = prime
                * result
                + ((metadataTypeName == null) ? 0 : metadataTypeName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractEntitlementsBean other = (AbstractEntitlementsBean) obj;
        if (accessRightIds == null) {
            if (other.accessRightIds != null)
                return false;
        } else if (!accessRightIds.equals(other.accessRightIds))
            return false;
        if (mdTypeId == null) {
            if (other.mdTypeId != null)
                return false;
        } else if (!mdTypeId.equals(other.mdTypeId))
            return false;
        if (metadataTypeName == null) {
            if (other.metadataTypeName != null)
                return false;
        } else if (!metadataTypeName.equals(other.metadataTypeName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "AbstractEntitlementsBean [accessRightIds=" + accessRightIds
                + ", mdTypeId=" + mdTypeId + ", metadataTypeName=" + metadataTypeName + "]";
    }


}
