package org.openiam.ui.rest.api.mvc;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.am.srvc.dto.URIPattern;
import org.openiam.am.srvc.searchbeans.URIPatternSearchBean;
import org.openiam.authmanager.service.AuthorizationManagerMenuWebService;
import org.openiam.base.AttributeOperationEnum;
import org.openiam.base.BaseConstants;
import org.openiam.base.ws.Response;
import org.openiam.base.ws.ResponseCode;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.connector.type.constant.ErrorCode;
import org.openiam.exception.EsbErrorToken;
import org.openiam.idm.searchbeans.PolicySearchBean;
import org.openiam.idm.searchbeans.ResourceSearchBean;
import org.openiam.idm.searchbeans.UserSearchBean;
import org.openiam.idm.srvc.audit.constant.AuditAction;
import org.openiam.idm.srvc.audit.dto.IdmAuditLog;
import org.openiam.idm.srvc.auth.dto.Login;
import org.openiam.idm.srvc.auth.ws.LoginDataWebService;
import org.openiam.idm.srvc.auth.ws.LoginListResponse;
import org.openiam.idm.srvc.mngsys.dto.ManagedSysDto;
import org.openiam.idm.srvc.policy.dto.Policy;
import org.openiam.idm.srvc.policy.dto.PolicyConstants;
import org.openiam.idm.srvc.policy.service.PolicyDataService;
import org.openiam.idm.srvc.pswd.dto.PasswordValidationResponse;
import org.openiam.idm.srvc.pswd.service.PasswordGenerator;
import org.openiam.idm.srvc.pswd.ws.PasswordWebService;
import org.openiam.idm.srvc.user.dto.ProfilePicture;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.idm.srvc.user.dto.UserAttribute;
import org.openiam.idm.srvc.user.dto.UserStatusEnum;
import org.openiam.provision.dto.*;
import org.openiam.provision.service.ActionEventBuilder;
import org.openiam.provision.service.ProvisionServiceEventProcessor;
import org.openiam.ui.exception.ErrorMessageException;
import org.openiam.ui.login.LoginActionToken;
import org.openiam.ui.rest.api.model.*;
import org.openiam.ui.util.WSUtils;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.model.SetPasswordToken;
import org.openiam.ui.web.mvc.AbstractUserController;
import org.openiam.ui.web.util.LoginProvider;
import org.openiam.ui.web.validator.UserInfoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.List;

@RestController
@RequestMapping("/prov")
public class ProvisioningRestController extends AbstractUserController {

    @Value("${org.openiam.ui.user.profile.picture.autoResize}")
    private boolean autoResize;
    @Value("${org.openiam.ui.user.profile.picture.formats}")
    private String formats;
    @Value("${org.openiam.ui.user.profile.picture.maxWidth}")
    private int maxWidth;
    @Value("${org.openiam.ui.user.profile.picture.maxHeight}")
    private int maxHeight;
    @Value("${org.openiam.ui.user.profile.picture.maxSize}")
    private long maxSize;

    @Autowired
    private UserInfoValidator userInfoValidator;

    @Resource(name = "passwordServiceClient")
    private PasswordWebService passwordService;

    @Resource(name = "loginServiceClient")
    private LoginDataWebService loginDataWebService;
    @Autowired
    private LoginProvider loginProvider;

    @Resource(name = "policyServiceClient")
    protected PolicyDataService policyServiceClient;

    @Resource(name = "authorizationManagerMenuServiceClient")
    private AuthorizationManagerMenuWebService authorizationManagerMenuServiceClient;

    private Map<ResponseCode, PasswordResponseHandler> passwordErrormap = new HashMap<ResponseCode, PasswordResponseHandler>();

    /**
     * Deletes user by userId
     *
     * @param request         the current HttpServletRequest
     * @param actionUserModel request to perform action DELETE
     * @return ajax response
     * @see BasicAjaxResponse
     */
    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    public BasicAjaxResponse deleteUser(final HttpServletRequest request, final @RequestBody ActionUserModel actionUserModel) {
        Date performDate = null;
        try {
            performDate = actionUserModel.getPerformDate();
        } catch (ParseException e) {
            log.error("Wrong date format");
        }
        if (actionUserModel.getUserAttributes() != null) {
            for (UserAttribute ua : actionUserModel.getUserAttributes()) {
                addUserAttributes(ua, actionUserModel.getUserId());
            }
        }
        if (actionUserModel.getPerformNow() || performDate == null)
            return handleUserOperation(request, actionUserModel.getUserId(), UserOperation.DELETE);
        else {
            return handleOperationsWithDelay(request, actionUserModel.getUserId(), performDate, UserStatusEnum.PENDING_DEACTIVATION);
        }
    }

    /**
     * Removes user by userId
     *
     * @param request         the current HttpServletRequest
     * @param actionUserModel request to perform action REMOVE
     * @return ajax response
     * @see BasicAjaxResponse
     */
    @RequestMapping(value = "/removeUser", method = RequestMethod.POST)
    public BasicAjaxResponse removeUser(final HttpServletRequest request, final @RequestBody ActionUserModel actionUserModel) {
        Date performDate = null;
        try {
            performDate = actionUserModel.getPerformDate();
        } catch (ParseException e) {
            log.error("Wrong date format");
        }
        if (actionUserModel.getPerformNow() || performDate == null)
            return handleUserOperation(request, actionUserModel.getUserId(), UserOperation.REMOVE);
        else {
            return handleOperationsWithDelay(request, actionUserModel.getUserId(), performDate, UserStatusEnum.PENDING_DELETE);
        }
    }

    /**
     * @param request         the current HttpServletRequest
     * @param actionUserModel model of the user to be enabled
     * @return ajax response
     * @see BasicAjaxResponse
     */
    @RequestMapping(value = "/enableUser", method = RequestMethod.POST)
    public BasicAjaxResponse enableUser(final HttpServletRequest request, final @RequestBody ActionUserModelSimple actionUserModel) {
        if (actionUserModel.getUserAttributes() != null) {
            for (UserAttribute ua : actionUserModel.getUserAttributes()) {
                addUserAttributes(ua, actionUserModel.getId());
            }
        }
        return handleUserOperation(request, actionUserModel.getId(), UserOperation.ENABLE);
    }

    /**
     * Disables user by userId
     *
     * @param request         the current HttpServletRequest
     * @param actionUserModel model of the user to be disabled
     * @return ajax response
     * @see BasicAjaxResponse
     */
    @RequestMapping(value = "/disableUser", method = RequestMethod.POST)
    public BasicAjaxResponse disableUser(final HttpServletRequest request, final @RequestBody ActionUserModelSimple actionUserModel) {
        if (actionUserModel.getUserAttributes() != null) {
            for (UserAttribute ua : actionUserModel.getUserAttributes()) {
                addUserAttributes(ua, actionUserModel.getId());
            }
        }
        return handleUserOperation(request, actionUserModel.getId(), UserOperation.DISABLE);
    }

    /**
     * Activates user by userId
     *
     * @param request         the current HttpServletRequest
     * @param actionUserModel model of the user to be activated
     * @return ajax response
     * @see BasicAjaxResponse
     */
    @RequestMapping(value = "/activateUser", method = RequestMethod.POST)
    public BasicAjaxResponse activateUser(final HttpServletRequest request, final @RequestBody ActionUserModelSimple actionUserModel) {
        ErrorToken errorToken = null;
        SuccessToken successToken = null;
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        Response wsResponse = null;

        ActionEventBuilder eventBuilder = new ActionEventBuilder(
                actionUserModel.getId(),
                getRequesterId(request),
                ProvisionActionEnum.ACTIVATE);
        ProvisionActionEvent actionEvent = eventBuilder.build();
        Response response = provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.PRE);
        if (ProvisionServiceEventProcessor.BREAK.equalsIgnoreCase((String) response.getResponseValue())) {
            return genAbortAjaxResponse(response, localeResolver.resolveLocale(request));
        }

        try {
            if (actionUserModel.getUserAttributes() != null) {
                for (UserAttribute ua : actionUserModel.getUserAttributes()) {
                    addUserAttributes(ua, actionUserModel.getId());
                }
            }
            if (provisionServiceFlag) {
                final User user = userDataWebService.getUserWithDependent(actionUserModel.getId(), null, true);
                final ProvisionUser pUser = new ProvisionUser(user);
                pUser.setRequestorUserId(getRequesterId(request));
                //IDMAPPS-3226
                if (UserStatusEnum.PENDING_DEACTIVATION.equals(pUser.getStatus()) || UserStatusEnum.PENDING_DELETE.equals(pUser.getStatus())) {
                    pUser.setLastDate(BaseConstants.NULL_DATE);
                }
                pUser.setStatus(UserStatusEnum.ACTIVE);
                wsResponse = provisionService.modifyUser(pUser);

                IdmAuditLog idmAuditLog = new IdmAuditLog();
                idmAuditLog.setRequestorUserId(getRequesterId(request));
                idmAuditLog.setTargetUser(user.getId(), user.getLogin());
                idmAuditLog.setAction(AuditAction.USER_ACTIVE.value());
                idmAuditLog.setAuditDescription("User activation");
                auditLogService.addLog(idmAuditLog);
            } else {
                wsResponse = userDataWebService.activateUser(actionUserModel.getId());
            }

            if (wsResponse.getStatus() == ResponseStatus.SUCCESS) {
                successToken = new SuccessToken(SuccessMessage.USER_ACTIVATED);
            } else {
                errorToken = new ErrorToken(Errors.CANNOT_ACTIVATE_USER);
            }

        } catch (Exception e) {
            errorToken = new ErrorToken(Errors.INTERNAL_ERROR);
            log.error("Exception while saving custom field", e);

        } finally {
            if (errorToken != null) {
                ajaxResponse.setStatus(500);
                errorToken.setMessage(messageSource.getMessage(
                        errorToken.getError().getMessageName(),
                        errorToken.getParams(),
                        localeResolver.resolveLocale(request)));
                ajaxResponse.addError(errorToken);
            } else if (successToken != null) {
                provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.POST);
                ajaxResponse.setSuccessToken(successToken);
                ajaxResponse.setSuccessMessage(messageSource.getMessage(
                        successToken.getMessage().getMessageName(),
                        null,
                        localeResolver.resolveLocale(request)));
                ajaxResponse.setStatus(200);
            }
        }
        return ajaxResponse;
    }

    /**
     * Creates new or updates existing user
     *
     * @param request   the current HttpServletRequest
     * @param userModel Ajax object representing user
     * @return ajax response
     * @see EditUserModel
     * @see BasicAjaxResponse
     */
    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public BasicAjaxResponse saveUser(final HttpServletRequest request, @RequestBody @Valid final EditUserModel userModel) {
        ErrorToken errorToken = null;
        SuccessToken successToken = null;
        BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        Response wsResponse = null;

        ActionEventBuilder eventBuilder = new ActionEventBuilder(
                userModel.getId(),
                getRequesterId(request),
                ProvisionActionEnum.SAVE);
        ProvisionActionEvent actionEvent = eventBuilder.build();
        Response response = provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.PRE);
        if (ProvisionServiceEventProcessor.BREAK.equalsIgnoreCase((String) response.getResponseValue())) {
            return genAbortAjaxResponse(response, localeResolver.resolveLocale(request));
        }

        try {
            wsResponse = saveUserInfo(request, userModel);
            if (wsResponse.getStatus() == ResponseStatus.SUCCESS) {
                successToken = new SuccessToken(SuccessMessage.USER_INFO_SAVED);
            } else {
                Errors error = Errors.CANNOT_SAVE_USER_INFO;
                if (wsResponse.getErrorCode() != null) {
                    switch (wsResponse.getErrorCode()) {
                        case SEND_EMAIL_FAILED:
                            error = Errors.USER_CREATED_BUT_EMAIL_NOT_SENT;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_1:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_1;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_2:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_2;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_3:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_3;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_4:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_4;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_5:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_5;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_6:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_6;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_7:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_7;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_8:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_8;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_9:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_9;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_10:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_10;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_11:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_11;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_12:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_12;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_13:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_13;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_14:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_14;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_15:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_15;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_16:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_16;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_17:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_17;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_18:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_18;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_19:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_19;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_20:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_20;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_1:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_1;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_2:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_2;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_3:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_3;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_4:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_4;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_5:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_5;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_6:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_6;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_7:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_7;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_8:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_8;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_9:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_9;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_10:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_10;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_11:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_11;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_12:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_12;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_13:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_13;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_14:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_14;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_15:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_15;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_16:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_16;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_17:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_17;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_18:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_18;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_19:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_19;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_20:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_20;
                            break;
                        default:
                            error = Errors.INTERNAL_ERROR;
                            break;
                    }
                }
                errorToken = new ErrorToken(error);
            }

        } catch (Exception e) {
            errorToken = new ErrorToken(Errors.INTERNAL_ERROR);
            log.error("Exception while saving custom field", e);

        } finally {
            if (errorToken != null) {
                ajaxResponse.setStatus(500);
                errorToken.setMessage(messageSource.getMessage(
                        errorToken.getError().getMessageName(),
                        errorToken.getParams(),
                        localeResolver.resolveLocale(request)));
                ajaxResponse.addError(errorToken);
            } else {
                if (wsResponse != null && wsResponse.getResponseValue() != null) {
                    if (wsResponse.getResponseValue() instanceof String) {
                        final String userId = (String) wsResponse.getResponseValue();
                        if (StringUtils.isBlank(userModel.getId())) {
                            ajaxResponse.setRedirectURL(new StringBuilder("editUser.html?id=").append(userId).toString());
                        }
                    }
                }
                if (successToken != null) {
                    provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.POST);
                    ajaxResponse.setSuccessToken(successToken);
                    final String userId = (String) wsResponse.getResponseValue();
                    ajaxResponse = setUserStatusInfoBuilder(request, ajaxResponse, userId, successToken);
                }
                ajaxResponse.addContextValues("checkStatusInProgress", true);
                ajaxResponse.setStatus(200);
            }
        }

        return ajaxResponse;
    }

    @RequestMapping(value = "/resyncPassword", method = RequestMethod.POST)
    public BasicAjaxResponse resyncPassword(final HttpServletRequest request, @RequestBody ResetPasswordBean passwordBean) throws IOException {
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        List<ErrorToken> errorTokenList = new LinkedList<>();
        Response wsResponse = null;
        SetPasswordToken token = null;
        if (CollectionUtils.isEmpty(passwordBean.getManagedSystem())
                && StringUtils.isEmpty(passwordBean.getManagedSystemId())) {
            ajaxResponse.setStatus(500);
            errorTokenList.add(new ErrorToken("Managed system must be set."));
            ajaxResponse.addErrors(errorTokenList);
            return ajaxResponse;
        }
        ActionEventBuilder eventBuilder = new ActionEventBuilder(
                passwordBean.getUserId(),
                getRequesterId(request),
                ProvisionActionEnum.RESYNC_PASSWORD);
        ProvisionActionEvent actionEvent = eventBuilder.build();
        Response response = provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.PRE);
        if (ProvisionServiceEventProcessor.BREAK.equalsIgnoreCase((String) response.getResponseValue())) {
            return genAbortAjaxResponse(response, localeResolver.resolveLocale(request));
        }

        try {
            UserSearchBean usb = new UserSearchBean();
            usb.setKey(passwordBean.getUserId());
            List<User> u = userDataWebService.findBeans(usb, 0, 1);
            Map<String, Login> loginBeManagedSys = new HashMap<String, Login>();
            for (Login l : u.get(0).getPrincipalList()) {
                loginBeManagedSys.put(l.getManagedSysId(), l);
            }
            for (String managedSystemId : passwordBean.getManagedSystem()) {
                if (provisionServiceFlag) {
                    Login l = loginBeManagedSys.get(managedSystemId);
                    String mangedSysLoginDecryptPassword = l.getPassword();
                    Response res = loginDataWebService.decryptPassword(l.getUserId(), l.getPassword());

                    if (res.isSuccess()) {
                        mangedSysLoginDecryptPassword = (String) res.getResponseValue();
                    } else {
                        errorTokenList.add(new ErrorToken(Errors.INVALID_LOGIN));
                    }
                    if (StringUtils.isNotBlank(mangedSysLoginDecryptPassword)) {
                        final PasswordSync pswdSync = new PasswordSync();
                        pswdSync.setManagedSystemId(managedSystemId);
                        pswdSync.setPassword(mangedSysLoginDecryptPassword);
                        pswdSync.setUserId(passwordBean.getUserId());
                        pswdSync.setRequestClientIP(request.getRemoteHost());
                        pswdSync.setRequestorLogin(cookieProvider.getPrincipal(request));
                        pswdSync.setRequestorId(getRequesterId(request));
                        pswdSync.setResyncMode(true);
                        pswdSync.setSendPasswordToUser(passwordBean.getNotifyUserViaEmail());
                        WSUtils.setWSClientTimeout(provisionService, 360000L);
                        final Response setPasswordResponse = provisionService.setPassword(pswdSync);
                        if (ResponseStatus.SUCCESS != setPasswordResponse.getStatus()) {
                            errorTokenList.add(new ErrorToken(Errors.RESET_PASSWORD_PASSWORD_NOT_SET));
                        }
                    }
                }
            }
        } catch (Throwable e) {
            errorTokenList.add(new ErrorToken(Errors.INTERNAL_ERROR));
            log.error("Exception while resyncing the password", e);
        } finally {
            if (CollectionUtils.isNotEmpty(errorTokenList)) {
                ajaxResponse.setStatus(500);
                ajaxResponse.addErrors(errorTokenList);
            } else {
                provisionService.addEvent(eventBuilder.build(), ProvisionActionTypeEnum.POST);
                ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.PASSWORD_RESYNC));
                ajaxResponse.setStatus(200);
                final boolean isAuthenticated = authorizationManagerMenuServiceClient.isUserAuthenticatedToMenuWithURL(
                        getRequesterId(request), "/webconsole/editUser.html", null, true);
                if (isAuthenticated) {
                    ajaxResponse.setRedirectURL(String.format("editUser.html?id=%s", passwordBean.getUserId()));
                }
            }
            ajaxResponse.process(localeResolver, messageSource, request);
        }
        return ajaxResponse;
    }

    @RequestMapping(value = "/changePasswordForManagedSystem", method = RequestMethod.POST)
    public BasicAjaxResponse resetPasswordSelfservice(final HttpServletRequest request, @RequestBody @Valid ResetPasswordBean passwordBean) throws Exception {
        BasicAjaxResponse ajaxResponse = null;
        ajaxResponse = new BasicAjaxResponse();
        ajaxResponse.addError(new ErrorToken(Errors.INVALID_LOGIN));
        ajaxResponse.setStatus(500);
        if (passwordBean.getPrincipal() == null || passwordBean.getManagedSystem() == null || passwordBean.getCurrentPassword() == null) {
            return ajaxResponse;
        }
        Response response = loginDataWebService.isPasswordEq(passwordBean.getPrincipal(), passwordBean.getManagedSystemId(), passwordBean.getCurrentPassword());
        if (ResponseStatus.SUCCESS.equals(response.getStatus())) {
            SetPasswordToken token = attemptResetPassword(request, passwordBean.getPassword(), passwordBean.getUserId(), passwordBean.getManagedSystemId(), true, false);
            if (token != null && token.hasErrors()) {
                ajaxResponse.setStatus(500);
                ajaxResponse.setPossibleErrors(token.getRules());
            } else {
                ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.PASSWORD_CHANGED));
                ajaxResponse.setStatus(200);
                ajaxResponse.setRedirectURL("/selfservice/myInfo.html");
            }
        }
        ajaxResponse.process(localeResolver, messageSource, request);
        return ajaxResponse;
    }


    /**
     * @param request       - http sesstion request
     * @param sourceRequest - request body
     * @return org.openiam.ui.web.model.BasicAjaxResponse
     * @throws IOException
     */
    @RequestMapping(value = "/syncPasswordFromSrc", method = RequestMethod.POST)
    public BasicAjaxResponse syncPasswordFromSrc(final HttpServletRequest request, final HttpServletResponse response, @RequestBody SyncPasswordFromSource sourceRequest) throws IOException {
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        if (sourceRequest == null) {
            ajaxResponse.setStatus(500);
            ajaxResponse.addError(new ErrorToken(Errors.INVALID_REQUEST));
            return ajaxResponse;
        }

        if (StringUtils.isBlank(sourceRequest.getServiceAccountLogin()) || StringUtils.isBlank(sourceRequest.getServiceAccountPassword())) {
            ajaxResponse.setStatus(500);
            ajaxResponse.addError(new ErrorToken(Errors.INVALID_LOGIN));
            return ajaxResponse;
        }

        if (StringUtils.isBlank(sourceRequest.getUserLogin()) || StringUtils.isBlank(sourceRequest.getUserPassword())) {
            ajaxResponse.setStatus(500);
            ajaxResponse.addError(new ErrorToken(Errors.USER_NOT_FOUND));
            return ajaxResponse;
        }

        if (StringUtils.isBlank(sourceRequest.getManagedSystemId())) {
            ajaxResponse.setStatus(500);
            ajaxResponse.addError(new ErrorToken(Errors.MANAGED_SYSTEM_NOT_SET));
            return ajaxResponse;
        }
        LoginActionToken loginActionToken = null;
        try {
            loginActionToken = loginProvider.getLoginActionToken(request, response, sourceRequest.getServiceAccountLogin(), sourceRequest.getServiceAccountPassword());
        } catch (Exception e) {
            ajaxResponse.setStatus(500);
            ajaxResponse.addError(new ErrorToken(Errors.INTERNAL_ERROR));
            return ajaxResponse;
        }
        if (loginActionToken == null) {
            ajaxResponse.setStatus(500);
            ajaxResponse.addError(new ErrorToken(Errors.INTERNAL_ERROR));
            return ajaxResponse;
        }


        //check that service account has access to this URI
        URIPatternSearchBean searchBean = new URIPatternSearchBean();
        searchBean.setPattern("/prov/syncPasswordFromSrc");
        searchBean.setDeepCopy(true);
        List<URIPattern> patterns = contentProviderServiceClient.findUriPatterns(searchBean, 0, 5);

        if (CollectionUtils.isEmpty(patterns)) {
            ajaxResponse.setStatus(500);
            ajaxResponse.addError(new ErrorToken(Errors.URI_PATERN_NOT_SET));
            return ajaxResponse;
        }
        //Collect resourceIds for all patters that give access to /prov/syncPasswordFromSrc
        boolean isMemberOfUri = false;
        for (URIPattern pattern : patterns) {
            if (ResponseCode.RELATIONSHIP_EXISTS.equals(
                    resourceDataService.canAddUserToResource(
                            loginActionToken.getUserId(),
                            pattern.getResourceId()).getErrorCode())) {
                isMemberOfUri = true;
                break;
            }
        }
        if (!isMemberOfUri) {
            ajaxResponse.setStatus(500);
            ajaxResponse.addError(new ErrorToken(Errors.INVALID_URI_PATTERN));
            return ajaxResponse;
        }

        ResourceSearchBean resourceSearchBean = new ResourceSearchBean();
        resourceSearchBean.setResourceTypeId("URL_PATTERN");
        resourceSearchBean.addUserId(loginActionToken.getUserId());
        resourceSearchBean.setDeepCopy(false);
        List<org.openiam.idm.srvc.res.dto.Resource> userResources = resourceDataService.findBeans(resourceSearchBean,
                0, Integer.MAX_VALUE, null);

        if (loginActionToken.isSuccess() || loginActionToken.isSuccessWithWarning()) {
            PasswordSync passwordSync = new PasswordSync();
            passwordSync.setPrincipal(sourceRequest.getUserLogin());
            passwordSync.setPassword(sourceRequest.getUserPassword());
            passwordSync.setManagedSystemId(sourceRequest.getManagedSystemId());
            passwordSync.setPreventChangeCountIncrement(sourceRequest.isPreventChangeCountIncrement());
            passwordSync.setForceChange(sourceRequest.isForceChange());
            passwordSync.setRequestorId(loginActionToken.getUserId());
            Response serviceResponse = provisionService.syncPasswordFromSrc(passwordSync);

            if (serviceResponse == null) {
                ajaxResponse.setStatus(500);
                ajaxResponse.addError(new ErrorToken(Errors.INTERNAL_ERROR));
                return ajaxResponse;
            }
            if (serviceResponse.isFailure()) {
                ajaxResponse.setStatus(500);
                if (serviceResponse.getErrorCode() != null) {
                    switch (serviceResponse.getErrorCode()) {
                        case PRINCIPAL_NOT_FOUND:
                            ajaxResponse.addError(new ErrorToken(Errors.USER_NOT_FOUND));
                            break;
                        case USER_NOT_FOUND:
                            ajaxResponse.addError(new ErrorToken(Errors.USER_NOT_FOUND));
                            break;
                        case FAIL_ENCRYPTION:
                            ajaxResponse.addError(new ErrorToken(Errors.PASSWORD_ENCRYPTION_FAIL));
                            break;
                        default:
                            break;
                    }
                } else {
                    ajaxResponse.addError(new ErrorToken(Errors.INTERNAL_ERROR));
                }
                return ajaxResponse;
            }
            ajaxResponse.setStatus(200);
            ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.PASSWORD_RESETED));
            return ajaxResponse;
        } else {
            ajaxResponse.setStatus(500);
            final Errors error = getErrorFromCode(loginActionToken.getErrCode());
            if (error != null) {
                ajaxResponse.addError(new ErrorToken(error));
            }
            return ajaxResponse;
        }
    }


    /**
     * Resets password for user
     *
     * @param request      - the current HttpServletRequest
     * @param passwordBean - Ajax password object
     * @return ajax response
     * @throws IOException
     * @see ResetPasswordBean
     * @see BasicAjaxResponse
     */
    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    public BasicAjaxResponse resetPassword(final HttpServletRequest request, @RequestBody @Valid ResetPasswordBean passwordBean) throws IOException {
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();

        Set<ErrorToken> errorTokenSet = new LinkedHashSet<>();
        Response wsResponse = null;
        SetPasswordToken token = null;

        if (CollectionUtils.isEmpty(passwordBean.getManagedSystem())
                && StringUtils.isEmpty(passwordBean.getManagedSystemId())) {
            ajaxResponse.setStatus(500);
            ErrorToken errorToken = new ErrorToken(Errors.MANAGED_SYSTEM_NOT_SELECT);
            errorToken.setMessage(messageSource.getMessage(
                    errorToken.getError().getMessageName(),
                    errorToken.getParams(),
                    localeResolver.resolveLocale(request)));
            errorTokenSet.add(errorToken);

            ajaxResponse.addErrors(errorTokenSet);
            return ajaxResponse;
        }

        if (passwordBean.getAutoGeneratePassword() && !passwordBean.getNotifyUserViaEmail()) {
            ajaxResponse.setStatus(500);
            ErrorToken errorToken = new ErrorToken(Errors.INCORRECTLY_CHECKBOXES_GENERATE_PASS);
            errorToken.setMessage(messageSource.getMessage(
                    errorToken.getError().getMessageName(),
                    errorToken.getParams(),
                    localeResolver.resolveLocale(request)));
            errorTokenSet.add(errorToken);
            ajaxResponse.addErrors(errorTokenSet);
            return ajaxResponse;
        }

        ActionEventBuilder eventBuilder = new ActionEventBuilder(
                passwordBean.getUserId(),
                getRequesterId(request),
                ProvisionActionEnum.RESET_PASSWORD);
        ProvisionActionEvent actionEvent = eventBuilder.build();
        Response response = provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.PRE);
        if (ProvisionServiceEventProcessor.BREAK.equalsIgnoreCase((String) response.getResponseValue())) {
            return genAbortAjaxResponse(response, localeResolver.resolveLocale(request));
        }

        // Rest password for selected managed system passwordBean.getManagedSystemId()
        // We have to get Password Policy for selected managed system and apply
        // If many managed systems should be used => authManagedSystemId = "ALL" we should to do reset password for every manged system
        // and apply policy according selected ManagedSystem

        // default authentification policy
        Policy defaultPolicy = this.getAuthentificationPolicy();
        String defaultManagedSysLogin = this.buildLoginAccordingAuthPolicy(defaultPolicy, passwordBean.getPrincipal());

        Map<String, String> loginByManagedSysId = new HashMap<String, String>();
        final LoginListResponse loginResponce = loginDataWebService.getLoginByUser(passwordBean.getUserId());
        if (loginResponce.isSuccess()) {
            for (Login login : loginResponce.getPrincipalList()) {
                loginByManagedSysId.put(login.getManagedSysId(), login.getLogin());
            }
        } else {
            loginByManagedSysId.put(defaultManagedSysId, defaultManagedSysLogin);
        }

        Map<String, Boolean> results = new LinkedHashMap<>(passwordBean.getManagedSystem().size());
        if (passwordBean.getAutoGeneratePassword()) {
            Policy pp = passwordService.getPasswordPolicy(defaultManagedSysLogin, defaultManagedSysId);
            passwordBean.setPassword(PasswordGenerator.generatePassword(pp));
        }
        //if you change this code ak VYakunin for review
        for (String managedSysId : passwordBean.getManagedSystem()) {
            try {
                PolicySearchBean policySearchBean = new PolicySearchBean();
                policySearchBean.setPolicyDefId(PolicyConstants.AUTHENTICATION_POLICY);
                policySearchBean.addAttribute("MANAGED_SYS_ID", managedSysId);
                List<Policy> policies = policyServiceClient.findBeans(policySearchBean, 0, 1); // search policy by managed system
                String targetSystemId;
                if (CollectionUtils.isNotEmpty(policies)) {
                    targetSystemId = managedSysId;
                } else {
                    targetSystemId = defaultManagedSysId;
                }

                String managedSysLogin = loginByManagedSysId.get(targetSystemId);

                token = validatePassword(managedSysLogin, targetSystemId, passwordBean.getPassword(), false);
                if (token.hasErrors()) {
                    errorTokenSet.addAll(token.getErrorList());
                } else {
                    // try to reset
                    if (provisionServiceFlag) {
                        final PasswordSync pswdSync = new PasswordSync();
                        pswdSync.setManagedSystemId(managedSysId);
                        pswdSync.setPassword(passwordBean.getPassword());
                        pswdSync.setUserId(passwordBean.getUserId());
                        pswdSync.setRequestClientIP(request.getRemoteHost());
                        pswdSync.setRequestorLogin(cookieProvider.getPrincipal(request));
                        pswdSync.setRequestorId(getRequesterId(request));
                        pswdSync.setUserActivateFlag(passwordBean.getUserActivateFlag());
                        pswdSync.setSendPasswordToUser(passwordBean.getNotifyUserViaEmail());
                        pswdSync.setForceChange(passwordBean.getForceChange());
                        pswdSync.setActivateSecUserStatus(passwordBean.getActivateSecUserStatus());

                        WSUtils.setWSClientTimeout(provisionService, 360000L);
                        final Response setPasswordResponse = provisionService.resetPassword(pswdSync);

                        results.put(managedSysId, setPasswordResponse.getStatus() == ResponseStatus.SUCCESS);
                        if (ResponseStatus.SUCCESS != setPasswordResponse.getStatus()) {
                            // Don't fail if target system does not support password reset
                            if (!ErrorCode.UNSUPPORTED_OPERATION.value().equals(setPasswordResponse.getErrorText())
                                    && !ErrorCode.OPERATION_NOT_SUPPORTED_EXCEPTION.value().equals(setPasswordResponse.getErrorText())
                                    ) {
                                errorTokenSet.add(new ErrorToken(Errors.CHANGE_PASSWORD_FAILED));
                            }
                        }

                    } else {

                        String encPassword = null;
                        wsResponse = loginDataWebService.encryptPassword(passwordBean.getUserId(), passwordBean.getPassword());

                        if (wsResponse.getStatus() != ResponseStatus.SUCCESS) {
                            errorTokenSet.add(new ErrorToken(Errors.PASSWORD_ENCRYPTION_FAIL));
                        }
                        encPassword = (String) wsResponse.getResponseValue();

                        wsResponse = loginDataWebService.resetPasswordAndNotifyUser(managedSysLogin, targetSystemId,
                                encPassword, passwordBean.getNotifyUserViaEmail());

                        IdmAuditLog idmAuditLog = new IdmAuditLog();
                        idmAuditLog.setRequestorUserId(getRequesterId(request));
                        idmAuditLog.setTargetUser(passwordBean.getUserId(), passwordBean.getPrincipal());
                        idmAuditLog.setAction(AuditAction.USER_RESETPASSWORD.value());
                        idmAuditLog.setAuditDescription("User reset password");
                        results.put(managedSysId, wsResponse.getStatus() == ResponseStatus.SUCCESS);
                        if (wsResponse.getStatus() == ResponseStatus.SUCCESS) {
                            idmAuditLog.succeed();
                        } else {
                            idmAuditLog.fail();
                            idmAuditLog.setFailureReason(wsResponse.getErrorText());
                        }
                        auditLogService.addLog(idmAuditLog);
                    }
                }
            } catch (Throwable e) {
                results.put(managedSysId, Boolean.FALSE);
                log.error("Exception while resetting the password", e);
            }
        }

        if (token != null && token.hasErrors()) { // password validation failed
            ajaxResponse.setPossibleErrors(token.getRules());
            ajaxResponse.setStatus(500);
            ajaxResponse.addErrors(errorTokenSet);
            ajaxResponse.process(localeResolver, messageSource, request);
        } else if (!results.containsValue(Boolean.TRUE)) { // No single successful operation
            ajaxResponse.setStatus(500);
            ajaxResponse.addError(new ErrorToken(Errors.CHANGE_PASSWORD_FAILED));
            ajaxResponse.process(localeResolver, messageSource, request);
        } else if (!results.containsValue(Boolean.FALSE)) { // No failures
            provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.POST);
            ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.PASSWORD_RESETED));
            ajaxResponse.setStatus(200);
            ajaxResponse.process(localeResolver, messageSource, request);

            if (displayPasswordAfterReset && CollectionUtils.isNotEmpty(passwordBean.getManagedSystem())) {
                String url = String.format("/webconsole/resetPassword.html?id=%s", passwordBean.getUserId());
                for (String id : passwordBean.getManagedSystem()) {
                    url += "&ms=" + id;
                }
                ajaxResponse.setRedirectURL(url);
            } else {
                final boolean isAuthenticated = authorizationManagerMenuServiceClient.isUserAuthenticatedToMenuWithURL(
                        getRequesterId(request), "/webconsole/editUser.html", null, true);
                if (isAuthenticated) {
                    ajaxResponse.setRedirectURL(String.format("editUser.html?id=%s", passwordBean.getUserId()));
                }
            }

        } else { // some failed, others - not
            provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.POST);

            final String resetPasswordLabel = messageSource.getMessage(
                    SuccessMessage.PASSWORD_RESETED.getMessageName(),
                    null,
                    localeResolver.resolveLocale(request));

            StringBuilder sb = new StringBuilder(resetPasswordLabel);
            final String success = messageSource.getMessage("openiam.ui.common.success", null, localeResolver.resolveLocale(request));
            final String failure = messageSource.getMessage("openiam.ui.common.error", null, localeResolver.resolveLocale(request));
            for (Map.Entry<String, Boolean> entry : results.entrySet()) {
                final String managedSysId = entry.getKey();
                final ManagedSysDto managedSys = managedSysServiceClient.getManagedSys(managedSysId);
                sb.append("<br/>").append(managedSys != null ? managedSys.getName() : "ID " + managedSysId);
                sb.append(": ").append(entry.getValue() ? success : failure);
            }
            ajaxResponse.setSuccessMessage(sb.toString());
            ajaxResponse.setStatus(200);
        }
        return ajaxResponse;
    }

    @RequestMapping(value = "/checkStatus", method = RequestMethod.POST)
    public BasicAjaxResponse checkStatus(final HttpServletRequest request, final @RequestParam(value = "id", required = true) String userId) {
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        SuccessToken successToken = new SuccessToken(SuccessMessage.USER_INFO_SAVED);
        ajaxResponse.setSuccessToken(successToken);
        setUserStatusInfoBuilder(request, ajaxResponse, userId, successToken);

        ajaxResponse.setStatus(200);
        return ajaxResponse;
    }


    /**
     * Resets user account by userId
     *
     * @param request - the current HttpServletRequest
     * @param userId  userId of the user to be reset
     * @return ajax response
     * @see ResetPasswordBean
     * @see BasicAjaxResponse
     */
    @RequestMapping(value = "/resetUser", method = RequestMethod.POST)
    public BasicAjaxResponse resetUser(final HttpServletRequest request, final @RequestParam(value = "id", required = true) String userId) {
        ErrorToken errorToken = null;
        SuccessToken successToken = null;
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        Response wsResponse = null;

        ActionEventBuilder eventBuilder = new ActionEventBuilder(
                userId,
                getRequesterId(request),
                ProvisionActionEnum.RESET_ACCOUNT);

        ProvisionActionEvent actionEvent = eventBuilder.build();
        Response response = provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.PRE);
        if (ProvisionServiceEventProcessor.BREAK.equalsIgnoreCase((String) response.getResponseValue())) {
            return genAbortAjaxResponse(response, localeResolver.resolveLocale(request));
        }

        try {
            if (provisionServiceFlag) {
                final User user = userDataWebService.getUserWithDependent(userId, null, true);
                final ProvisionUser pUser = new ProvisionUser(user);
                pUser.setRequestorUserId(getRequesterId(request));
                pUser.setDateITPolicyApproved(BaseConstants.NULL_DATE);
                pUser.setClaimDate(BaseConstants.NULL_DATE);
                pUser.setStatus(UserStatusEnum.PENDING_INITIAL_LOGIN);
                pUser.setSecondaryStatus(null);
                wsResponse = provisionService.modifyUser(pUser);

                if (ResponseStatus.SUCCESS.equals(wsResponse.getStatus())) {
                    Response responseQuestions = challengeResponseService.resetQuestionsForUser(pUser.getId());
                    if (ResponseStatus.FAILURE.equals(responseQuestions.getStatus())) {
                        errorToken = new ErrorToken(Errors.CANNOT_RESET_QUESTIONS);
                    }
                }

 /*               if (wsResponse.isSuccess()) {
                    final PasswordSync pswdSync = new PasswordSync();
                    pswdSync.setManagedSystemId(null);
                    pswdSync.setPassword(PasswordGenerator.generatePassword(16));
                    pswdSync.setUserId(userId);
                    pswdSync.setRequestClientIP(request.getRemoteHost());
                    pswdSync.setRequestorLogin(cookieProvider.getPrincipal(request));
                    pswdSync.setRequestorId(getRequesterId(request));
                    WSUtils.setWSClientTimeout(provisionService, 360000L);
                    final Response setPasswordResponse = provisionService.resetPassword(pswdSync);
                    final Response resetQuestionsResponse = challengeResponseService.resetQuestionsForUser(userId);
                }

     **/
            } else {

//                String login = loginDataWebService.getPrimaryIdentity(userId).getPrincipal().getLogin();
                // Generate random password
//                wsResponse = loginDataWebService.encryptPassword(userId, PasswordGenerator.generatePassword(16));
//                if (wsResponse.getStatus() == ResponseStatus.SUCCESS) {
                wsResponse = userDataWebService.resetUser(userId);
//                    String encPassword = (String) wsResponse.getResponseValue();
//                    if (wsResponse.getStatus() == ResponseStatus.SUCCESS) {
//                        wsResponse = loginDataWebService.resetPassword(login, defaultManagedSysId, encPassword);
//                    }
//                }
            }

            if (wsResponse.getStatus() == ResponseStatus.SUCCESS) {
                successToken = new SuccessToken(SuccessMessage.USER_ACCOUNT_RESET);
            } else {
                errorToken = new ErrorToken(Errors.CANNOT_RESET_USER);
            }

        } catch (Exception e) {
            errorToken = new ErrorToken(Errors.INTERNAL_ERROR);
            log.error("Exception while resetting user account", e);

        } finally {
            if (errorToken != null) {
                ajaxResponse.setStatus(500);
                errorToken.setMessage(messageSource.getMessage(
                        errorToken.getError().getMessageName(),
                        errorToken.getParams(),
                        localeResolver.resolveLocale(request)));
                ajaxResponse.addError(errorToken);
            } else if (successToken != null) {
                provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.POST);
                ajaxResponse.setSuccessToken(successToken);
                ajaxResponse.setSuccessMessage(messageSource.getMessage(
                        successToken.getMessage().getMessageName(),
                        null,
                        localeResolver.resolveLocale(request)));
                ajaxResponse.setStatus(200);
            }
        }
        return ajaxResponse;
    }


    @RequestMapping("/images/{id}")
    public void getImage(HttpServletRequest request,
                         final HttpServletResponse response,
                         @PathVariable String id) throws IOException {
        if (StringUtils.isNotBlank(id)) {
            ProfilePicture profilePicture = userDataWebService.getProfilePictureByUserId(id, getRequesterId(request));
            if (profilePicture != null) {
                ImageInputStream iis = ImageIO.createImageInputStream(new ByteArrayInputStream(profilePicture.getPicture()));
                Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);
                if (iter.hasNext()) {
                    ImageReader ir = iter.next();
                    String format = ir.getFormatName();
                    List<String> contentTypes = Arrays.asList("jpeg", "jpg", "png", "gif");
                    if (StringUtils.isNotBlank(formats)) {
                        contentTypes = Arrays.asList(formats.split("\\s*,\\s*"));
                    }
                    if (contentTypes.contains(format.toLowerCase())) {
                        response.setContentType("image/" + format.toLowerCase());
                    }
                    BufferedImage bufferedImage = ImageIO.read(iis);
                    ImageIO.write(bufferedImage, format, response.getOutputStream());
                }
            }
        }
    }

    @RequestMapping(value = "/deleteProfilePic", method = RequestMethod.POST)
    public BasicAjaxResponse deleteProfilePic(HttpServletRequest request, HttpServletResponse response,
                                       @RequestParam(value = "id", required = true) final String userId) throws IOException {

        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();

        Response res = userDataWebService.deleteProfilePictureByUserId(userId, getRequesterId(request));

        if (res.isSuccess()) {
            User user = null;
            String requesterId = getRequesterId(request);
            if (requesterId.equals(userId)) {
                user = userDataWebService.getUserWithDependent(userId, null, false);
            } else {
                user = userDataWebService.getUserWithDependent(userId, requesterId, false);
            }
            user.setRequestorUserId(requesterId);
            res = provisionService.modifyUser(new ProvisionUser(user));
        }

        if (res.isSuccess()) {
            ajaxResponse.setStatus(200);
            SuccessToken successToken = new SuccessToken(SuccessMessage.IMAGE_DELETE_SUCCESS);
            ajaxResponse.setSuccessToken(successToken);
            ajaxResponse.setSuccessMessage(messageSource.getMessage(
                    successToken.getMessage().getMessageName(),
                    null,
                    localeResolver.resolveLocale(request)));
        } else {
            ajaxResponse.setStatus(500);
            if (CollectionUtils.isNotEmpty(res.getErrorTokenList())) {
                for (EsbErrorToken e : res.getErrorTokenList()) {
                    ErrorToken errorToken = new ErrorToken();
                    errorToken.setValidationError(e.getMessage());
                    if (e.getLengthConstraint() != null) {
                        errorToken.setParams(new Object[]{e.getLengthConstraint()});
                    }
                    ajaxResponse.addError(errorToken);
                }
            } else {
                ErrorToken errorToken = new ErrorToken(Errors.INTERNAL_ERROR);
                errorToken.setMessage(messageSource.getMessage(
                        errorToken.getError().getMessageName(),
                        errorToken.getParams(),
                        localeResolver.resolveLocale(request)));
                ajaxResponse.addError(errorToken);
            }
        }

        return ajaxResponse;
    }

    @RequestMapping(value = "/addProfilePic", method = RequestMethod.POST)
    public BasicAjaxResponse addProfilePic(MultipartHttpServletRequest request, HttpServletResponse response,
                                    @RequestParam(value = "id", required = true) final String userId,
                                    @RequestPart(value = "pic", required = false) final MultipartFile pic) throws IOException {

        String requesterId = getRequesterId(request);

        User user = null;

        if (requesterId.equals(userId)) {
            user = userDataWebService.getUserWithDependent(userId, null, true);
        } else {
            user = userDataWebService.getUserWithDependent(userId, requesterId, true);
        }

        log.warn("Upload user picture with deep copy true");

        //test
        if (requesterId.equals(userId)) {
            user.setRequestorUserId("3000");
        } else {
            user.setRequestorUserId(requesterId);
        }


        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse(500);

        if (user != null) {
            try {
                byte[] imageData = getValidAndResizedProfilePicData(pic, autoResize, request);
                ProfilePicture profilePic = userDataWebService.getProfilePictureByUserId(user.getId(), requesterId);
                if (profilePic == null) {
                    profilePic = new ProfilePicture();
                }
                profilePic.setUser(user);
                profilePic.setName(pic.getName());
                profilePic.setPicture(imageData);
                Response res = userDataWebService.saveProfilePicture(profilePic, requesterId);

                if (res.isSuccess()) {
                    res = provisionService.modifyUser(new ProvisionUser(user));
                }

                if (res.isSuccess()) {
                    ajaxResponse.setStatus(200);
                    SuccessToken successToken = new SuccessToken(SuccessMessage.IMAGE_UPLOAD_SUCCESS);
                    ajaxResponse.setSuccessToken(successToken);
                    ajaxResponse.setSuccessMessage(messageSource.getMessage(
                            successToken.getMessage().getMessageName(),
                            null,
                            localeResolver.resolveLocale(request)));
                } else {
                    ajaxResponse.setStatus(500);
                    if (CollectionUtils.isNotEmpty(res.getErrorTokenList())) {
                        for (EsbErrorToken e : res.getErrorTokenList()) {
                            ErrorToken errorToken = new ErrorToken();
                            errorToken.setValidationError(e.getMessage());
                            if (e.getLengthConstraint() != null) {
                                errorToken.setParams(new Object[]{e.getLengthConstraint()});
                            }
                            ajaxResponse.addError(errorToken);
                        }
                    } else {
                        ErrorToken errorToken = new ErrorToken(Errors.INTERNAL_ERROR);
                        errorToken.setMessage(messageSource.getMessage(
                                errorToken.getError().getMessageName(),
                                errorToken.getParams(),
                                localeResolver.resolveLocale(request)));
                        ajaxResponse.addError(errorToken);
                    }
                }
            } catch (ErrorMessageException emex) {
                ErrorToken errorToken = new ErrorToken(emex.getError());
                if (Errors.IMAGE_INVALID_TYPE.equals(emex.getError())) {
                    errorToken.setParams(new Object[]{formats});

                } else if (Errors.IMAGE_INVALID_DIMENSIONS.equals(emex.getError())) {
                    errorToken.setParams(new Object[]{maxWidth, maxHeight});

                } else if (Errors.IMAGE_INVALID_SIZE.equals(emex.getError())) {
                    errorToken.setParams(new Object[]{maxSize});
                }
                errorToken.setMessage(messageSource.getMessage(
                        errorToken.getError().getMessageName(),
                        errorToken.getParams(),
                        localeResolver.resolveLocale(request)));
                ajaxResponse.addError(errorToken);
            }
        } else {
            ErrorToken errorToken = new ErrorToken(Errors.USER_NOT_SET);
            errorToken.setMessage(messageSource.getMessage(
                    errorToken.getError().getMessageName(),
                    errorToken.getParams(),
                    localeResolver.resolveLocale(request)));
            ajaxResponse.addError(errorToken);
        }

        return ajaxResponse;
    }

    private byte[] getValidAndResizedProfilePicData(MultipartFile pic, boolean autoResize, HttpServletRequest request) throws IOException, ErrorMessageException {

        byte[] result = null;
        ImageInputStream iis = ImageIO.createImageInputStream(new ByteArrayInputStream(pic.getBytes()));
        Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);
        if (iter.hasNext()) {
            ImageReader ir = iter.next();
            String format = ir.getFormatName();
            BufferedImage bufferedImage = ImageIO.read(iis);

            List<String> contentTypes = Arrays.asList("jpeg", "jpg", "png", "gif");
            if (StringUtils.isNotBlank(formats)) {
                contentTypes = Arrays.asList(formats.split("\\s*,\\s*"));
            }
            if (!contentTypes.contains(format.toLowerCase())) {
                throw new ErrorMessageException(Errors.IMAGE_INVALID_TYPE);
            }
            if ((pic.getSize() > maxSize * 1024)) {
                throw new ErrorMessageException(Errors.IMAGE_INVALID_SIZE);
            }
            if (!autoResize) {
                if ((bufferedImage.getWidth() > maxWidth) || (bufferedImage.getHeight() > maxHeight)) {
                    throw new ErrorMessageException(Errors.IMAGE_INVALID_DIMENSIONS);
                }
                result = pic.getBytes();

            } else {
                double wScale = 1;
                if (bufferedImage.getWidth() > maxWidth) {
                    wScale = (double) (bufferedImage.getWidth()) / maxWidth;
                }
                double hScale = 1;
                if (bufferedImage.getHeight() > maxHeight) {
                    hScale = (double) (bufferedImage.getHeight()) / maxHeight;
                }
                double scale = Math.max(wScale, hScale);
                if (scale > 1) {
                    int width = (int) (bufferedImage.getWidth() / scale);
                    int height = (int) (bufferedImage.getHeight() / scale);
                    int type = bufferedImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : bufferedImage.getType();
                    BufferedImage resizedImage = new BufferedImage(width, height, type);
                    Graphics2D g = resizedImage.createGraphics();
                    g.drawImage(bufferedImage, 0, 0, width, height, null);
                    g.dispose();
                    g.setComposite(AlphaComposite.Src);
                    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                    g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageIO.write(resizedImage, format, baos);
                    baos.flush();
                    result = baos.toByteArray();
                    baos.close();
                } else {
                    result = pic.getBytes();
                }
            }
            return result;

        } else {
            throw new ErrorMessageException(Errors.IMAGE_INVALID);
        }
    }

    private BasicAjaxResponse handleOperationsWithDelay(HttpServletRequest request, String userId, Date lastDate, UserStatusEnum status) {
        final User user = userDataWebService.getUserWithDependent(userId,
                getRequesterId(request), false);
        if (user == null) {
            BasicAjaxResponse response = new BasicAjaxResponse();
            response.addError(new ErrorToken(Errors.USER_NOT_FOUND));
            return response;
        }
        //FIXME kostil'!
        user.setEmail(null);
        EditUserModel userModel = mapper.mapToObject(user, EditUserModel.class);
        userModel.setUserStatus(status);
        userModel.setLastDate(lastDate);
        return this.saveUser(request, userModel);
    }


    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(userInfoValidator);
    }


    private interface PasswordResponseHandler {
        public ErrorToken handle(final PasswordValidationResponse response);
    }

    private void addUserAttributes(UserAttribute uaSave, String userId) {
        if ((uaSave != null) && (StringUtils.isNotBlank(uaSave.getName()))) {
            List<UserAttribute> ual = userDataWebService.getUserAttributes(userId);
            UserAttribute uAttribute = null;
            if (ual != null) {
                for (UserAttribute ua : ual) {
                    if (StringUtils.isNotBlank(uaSave.getName()) && uaSave.getName().equals(ua.getName())) {
                        uAttribute = ua;
                        break;
                    }
                }
            }
            if (uAttribute == null) {
                uAttribute = new UserAttribute();
                uAttribute.setOperation(AttributeOperationEnum.ADD);
                uAttribute.setName(uaSave.getName());
                uAttribute.setUserId(userId);
            } else {
                uAttribute.setOperation(AttributeOperationEnum.REPLACE);
            }
            uAttribute.setValue(uaSave.getValue());
            userDataWebService.addAttribute(uAttribute);
        }
    }
}
