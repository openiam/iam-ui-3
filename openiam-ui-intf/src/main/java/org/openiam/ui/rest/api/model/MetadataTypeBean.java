package org.openiam.ui.rest.api.model;

import org.openiam.idm.srvc.meta.domain.MetadataTypeGrouping;

public class MetadataTypeBean extends KeyNameAndDisplayNameBean {

	private MetadataTypeGrouping grouping;

	public MetadataTypeBean() {
		super();
	}

	public MetadataTypeGrouping getGrouping() {
		return grouping;
	}

	public void setGrouping(MetadataTypeGrouping grouping) {
		this.grouping = grouping;
	}

}
