package org.openiam.ui.rest.api.model;

import org.openiam.idm.srvc.policy.dto.ResetPasswordTypeEnum;
import org.openiam.idm.srvc.user.dto.UserStatusEnum;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.util.DateFormatStr;

import java.text.SimpleDateFormat;
import java.util.*;

public class EditUserModel extends BasicAjaxResponse  {
    private String id;
    private UserStatusEnum userStatus;
    private UserStatusEnum secondaryStatus;

    private String firstName;
    private String middleInit;
    private String lastName;
    private String nickname;
    private String maidenName;
    private String suffix;
    private String prefix;
    private String birthdateAsStr;
    private Date birthdate;
    private String sex;
    private Integer showInSearch;
    private String title;
    private String jobCodeId;
    private String classification;
    private String employeeId;
    private String userTypeInd;
    private String employeeTypeId;
    private String startDateAsStr;
    private Date startDate;
    private String lastDateAsStr;
    private Date lastDate;
    private String claimDateAsStr;
    private Date claimDate;
    //for Primary supervisors
    private String supervisorId;
    private String supervisorName;
    private List<KeyNameOperationBean> supervisors  = new ArrayList<>();
    private String alternateContactId;
    private String alternateContactName;
    private String mailCode;
    private String costCenter;
    private String metadataTypeId;

    // these fields are used only when userWS is used directly without provision
    private String login;
    private String password;
    private String confirmPassword;
    private List<EmailBean> emails  = new ArrayList<>();
    private List<AddressBean> addresses  = new ArrayList<>();
    private List<PhoneBean> phones  = new ArrayList<>();
    private Boolean notifyUserViaEmail=true;
    private Boolean notifySupervisorViaEmail = false;
    private Boolean provisionOnStartDate = false;
    
    private List<KeyNameBean> organizationIds  = new ArrayList<>();
    private String userSubTypeId;
    private String partnerName;
    private String prefixPartnerName;
    private String prefixLastName;

    // ************ NEW
    private Date createDate;
    private Date lastUpdate;
    private Date datePasswordChanged;
    private Date dateChallengeRespChanged;
    private Date dateITPolicyApproved;

    private String displayNameFormat;

    private String name;
    private String companyOwnerId;
    private String createdBy;
    private String lastUpdatedBy;
    private String locationCd;
    private String locationName;
    private String passwordTheme;
    private String userOwnerId;

    private boolean fromActivitiCreation = false;
    private ResetPasswordTypeEnum resetPasswordType;

    private List<KeyNameOperationBean> roleList = new ArrayList<>();
    private List<KeyNameOperationBean> groupList = new ArrayList<>();
    private HashMap<String, UserAttributeBean> userAttributes = new LinkedHashMap<>();
    private List<ResourceBean> resources = new ArrayList<>();
    private List<LoginBean> principalList = new ArrayList<>();

    //************ ProvisionUser
    private List<String> notProvisioninResourcesIds;
    private boolean emailCredentialsToNewUsers = false;
    private boolean addInitialPasswordToHistory = false;
    private boolean skipPreprocessor = false;
    private boolean skipPostProcessor = false;
    private String srcSystemId;
    private boolean notifyTargetSystems = true;

    public EditUserModel() {};

    public List<KeyNameBean> getOrganizationIds() {
        return organizationIds;
    }

    public void setOrganizationIds(List<KeyNameBean> organizationIds) {
        this.organizationIds = organizationIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserStatusEnum getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatusEnum userStatus) {
        this.userStatus = userStatus;
    }

    public UserStatusEnum getSecondaryStatus() {
        return secondaryStatus;
    }

    public void setSecondaryStatus(UserStatusEnum secondaryStatus) {
        this.secondaryStatus = secondaryStatus;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleInit() {
        return middleInit;
    }

    public void setMiddleInit(String middleInit) {
        this.middleInit = middleInit;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMaidenName() {
        return maidenName;
    }

    public void setMaidenName(String maidenName) {
        this.maidenName = maidenName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getShowInSearch() {
        return showInSearch;
    }

    public void setShowInSearch(Integer showInSearch) {
        this.showInSearch = showInSearch;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJobCodeId() {
        return jobCodeId;
    }

    public void setJobCodeId(String jobCodeId) {
        this.jobCodeId = jobCodeId;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getUserTypeInd() {
        return userTypeInd;
    }

    public void setUserTypeInd(String userTypeInd) {
        this.userTypeInd = userTypeInd;
    }

    public String getEmployeeTypeId() {
        return employeeTypeId;
    }

    public void setEmployeeTypeId(String employeeTypeId) {
        this.employeeTypeId = employeeTypeId;
    }

    public String getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }

    public String getSupervisorName() {
        return supervisorName;
    }

    public void setSupervisorName(String supervisorName) {
        this.supervisorName = supervisorName;
    }

    public List<KeyNameOperationBean> getSupervisors() {
        return supervisors;
    }

    public void setSupervisors(List<KeyNameOperationBean> supervisors) {
        this.supervisors = supervisors;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getLastDate() {
        return lastDate;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }

    public String getBirthdateAsStr() {
        return birthdateAsStr;
    }

    public void setBirthdateAsStr(String birthdateAsStr) {
        this.birthdateAsStr = birthdateAsStr;
    }

    public String getStartDateAsStr() {
        return startDateAsStr;
    }

    public void setStartDateAsStr(String startDateAsStr) {
        this.startDateAsStr = startDateAsStr;
    }

    public String getLastDateAsStr() {
        return lastDateAsStr;
    }

    public void setLastDateAsStr(String lastDateAsStr) {
        this.lastDateAsStr = lastDateAsStr;
    }

    public String getClaimDateAsStr() {
        return claimDateAsStr;
    }

    public void setClaimDateAsStr(String claimDateAsStr) {
        this.claimDateAsStr = claimDateAsStr;
    }

    public Date getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(Date claimDate) {
        this.claimDate = claimDate;
    }

    public String getMailCode() {
        return mailCode;
    }

    public void setMailCode(String mailCode) {
        this.mailCode = mailCode;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public List<EmailBean> getEmails() {
        return emails;
    }

    public void setEmails(List<EmailBean> emails) {
        this.emails = emails;
    }

    public List<AddressBean> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressBean> addresses) {
        this.addresses = addresses;
    }

    public List<PhoneBean> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneBean> phones) {
        this.phones = phones;
    }

    public Boolean getNotifyUserViaEmail() {
        return notifyUserViaEmail;
    }

    public void setNotifyUserViaEmail(Boolean notifyUserViaEmail) {
        this.notifyUserViaEmail = notifyUserViaEmail;
    }

    public Boolean getNotifySupervisorViaEmail() {
        return notifySupervisorViaEmail;
    }

    public void setNotifySupervisorViaEmail(Boolean notifySupervisorViaEmail) {
        this.notifySupervisorViaEmail = notifySupervisorViaEmail;
    }

    public Boolean getProvisionOnStartDate() {
        return provisionOnStartDate;
    }

    public void setProvisionOnStartDate(Boolean provisionOnStartDate) {
        this.provisionOnStartDate = provisionOnStartDate;
    }

    public String getAlternateContactId() {
        return alternateContactId;
    }

    public void setAlternateContactId(String alternateContactId) {
        this.alternateContactId = alternateContactId;
    }

    public String getAlternateContactName() {
        return alternateContactName;
    }

    public void setAlternateContactName(String alternateContactName) {
        this.alternateContactName = alternateContactName;
    }

    public String getMetadataTypeId() {
        return metadataTypeId;
    }

    public void setMetadataTypeId(String metadataTypeId) {
        this.metadataTypeId = metadataTypeId;
    }

    public void formatDates(){
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormatStr.getSdfDate());

        if(this.birthdate!=null){
            this.birthdateAsStr = sdf.format(this.birthdate);
        }
        if(this.startDate!=null){
            this.startDateAsStr = sdf.format(this.startDate);
        }
        if(this.lastDate!=null){
            this.lastDateAsStr = sdf.format(this.lastDate);
        }
        if(this.claimDate!=null){
            this.claimDateAsStr = sdf.format(this.claimDate);
        }
    }

    public String getUserSubTypeId() {
        return userSubTypeId;
    }

    public void setUserSubTypeId(String userSubTypeId) {
        this.userSubTypeId = userSubTypeId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPrefixPartnerName() {
        return prefixPartnerName;
    }

    public void setPrefixPartnerName(String prefixPartnerName) {
        this.prefixPartnerName = prefixPartnerName;
    }

    public String getPrefixLastName() {
        return prefixLastName;
    }

    public void setPrefixLastName(String prefixLastName) {
        this.prefixLastName = prefixLastName;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    //**************************************



    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }


    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Date getDatePasswordChanged() {
        return datePasswordChanged;
    }

    public void setDatePasswordChanged(Date datePasswordChanged) {
        this.datePasswordChanged = datePasswordChanged;
    }

    public Date getDateChallengeRespChanged() {
        return dateChallengeRespChanged;
    }

    public void setDateChallengeRespChanged(Date dateChallengeRespChanged) {
        this.dateChallengeRespChanged = dateChallengeRespChanged;
    }

    public Date getDateITPolicyApproved() {
        return dateITPolicyApproved;
    }

    public void setDateITPolicyApproved(Date dateITPolicyApproved) {
        this.dateITPolicyApproved = dateITPolicyApproved;
    }

    public String getDisplayNameFormat() {
        return displayNameFormat;
    }

    public void setDisplayNameFormat(String displayNameFormat) {
        this.displayNameFormat = displayNameFormat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyOwnerId() {
        return companyOwnerId;
    }

    public void setCompanyOwnerId(String companyOwnerId) {
        this.companyOwnerId = companyOwnerId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getLocationCd() {
        return locationCd;
    }

    public void setLocationCd(String locationCd) {
        this.locationCd = locationCd;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getPasswordTheme() {
        return passwordTheme;
    }

    public void setPasswordTheme(String passwordTheme) {
        this.passwordTheme = passwordTheme;
    }

    public String getUserOwnerId() {
        return userOwnerId;
    }

    public void setUserOwnerId(String userOwnerId) {
        this.userOwnerId = userOwnerId;
    }

    public boolean getFromActivitiCreation() {
        return fromActivitiCreation;
    }

    public void setFromActivitiCreation(boolean fromActivitiCreation) {
        this.fromActivitiCreation = fromActivitiCreation;
    }

    public ResetPasswordTypeEnum getResetPasswordType() {
        return resetPasswordType;
    }

    public void setResetPasswordType(ResetPasswordTypeEnum resetPasswordType) {
        this.resetPasswordType = resetPasswordType;
    }

    public List<LoginBean> getPrincipalList() {
        return principalList;
    }

    public void setPrincipalList(List<LoginBean> principalList) {
        this.principalList = principalList;
    }

    public List<String> getNotProvisioninResourcesIds() {
        return notProvisioninResourcesIds;
    }

    public void setNotProvisioninResourcesIds(List<String> notProvisioninResourcesIds) {
        this.notProvisioninResourcesIds = notProvisioninResourcesIds;
    }

    public boolean isEmailCredentialsToNewUsers() {
        return emailCredentialsToNewUsers;
    }

    public void setEmailCredentialsToNewUsers(boolean emailCredentialsToNewUsers) {
        this.emailCredentialsToNewUsers = emailCredentialsToNewUsers;
    }

    public boolean isAddInitialPasswordToHistory() {
        return addInitialPasswordToHistory;
    }

    public void setAddInitialPasswordToHistory(boolean addInitialPasswordToHistory) {
        this.addInitialPasswordToHistory = addInitialPasswordToHistory;
    }

    public boolean isSkipPreprocessor() {
        return skipPreprocessor;
    }

    public void setSkipPreprocessor(boolean skipPreprocessor) {
        this.skipPreprocessor = skipPreprocessor;
    }

    public boolean isSkipPostProcessor() {
        return skipPostProcessor;
    }

    public void setSkipPostProcessor(boolean skipPostProcessor) {
        this.skipPostProcessor = skipPostProcessor;
    }

    public String getSrcSystemId() {
        return srcSystemId;
    }

    public void setSrcSystemId(String srcSystemId) {
        this.srcSystemId = srcSystemId;
    }

    public boolean isNotifyTargetSystems() {
        return notifyTargetSystems;
    }

    public void setNotifyTargetSystems(boolean notifyTargetSystems) {
        this.notifyTargetSystems = notifyTargetSystems;
    }

    public List<KeyNameOperationBean> getRoleList() {
        return roleList;
    }

    public void addToRoleList (KeyNameOperationBean role) {
        if (roleList == null) {
            roleList = new ArrayList<>();
        }
        roleList.add(role);
    }

    public void setRoleList(List<KeyNameOperationBean> roleList) {
        this.roleList = roleList;
    }

    public List<KeyNameOperationBean> getGroupList() {
        return groupList;
    }

    public void addToGroupList (KeyNameOperationBean group) {
        if (groupList == null) {
            groupList = new ArrayList<>();
        }
        groupList.add(group);
    }

    public void setGroupList(List<KeyNameOperationBean> groupList) {
        this.groupList = groupList;
    }

    public HashMap<String, UserAttributeBean> getUserAttributes() {
        return userAttributes;
    }

    public void setUserAttributes(HashMap<String, UserAttributeBean> userAttributes) {
        this.userAttributes = userAttributes;
    }

    public List<ResourceBean> getResources() {
        return resources;
    }

    public void addToResourceList (ResourceBean res) {
        if (resources == null) {
            resources = new ArrayList<>();
        }
        resources.add(res);
    }

    public void setResources(List<ResourceBean> resources) {
        this.resources = resources;
    }

}
