package org.openiam.ui.rest.api.model;


public class IdentityQuestionBean extends KeyNameAndDisplayNameBean {

	private Boolean active;

	public Boolean getActive() {
		return active;
	}
	
	public void setActive(Boolean status) {
		this.active = status;
	}

}
