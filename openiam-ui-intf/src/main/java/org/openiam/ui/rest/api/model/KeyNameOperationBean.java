package org.openiam.ui.rest.api.model;

import org.openiam.base.AttributeOperationEnum;
import org.openiam.ui.web.model.AbstractBean;

public class KeyNameOperationBean extends KeyNameBean{

	private AttributeOperationEnum operation = AttributeOperationEnum.NO_CHANGE;


	public KeyNameOperationBean() {
	}
	public KeyNameOperationBean(String id, String name) {
		setId(id);
		setName(name);
	}
	public AttributeOperationEnum getOperation() {
		return operation;
	}

	public void setOperation(AttributeOperationEnum operation) {
		this.operation = operation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((operation == null) ? 0 : operation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KeyNameOperationBean other = (KeyNameOperationBean) obj;
		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		if (operation == null) {
			if (other.operation != null)
				return false;
		} else if (!operation.equals(other.operation))
			return false;
		return true;
	}
}
