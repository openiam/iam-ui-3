package org.openiam.ui.rest.api.model;

import org.openiam.base.AttributeOperationEnum;
import org.openiam.idm.srvc.user.dto.UserAttribute;

import java.util.List;

public class UserAttributeBean extends MetadataAttributeBean {

	private AttributeOperationEnum operation = AttributeOperationEnum.NO_CHANGE;

	public UserAttributeBean() {

	}

	public UserAttributeBean(UserAttribute userAttr) {
		this.setId(userAttr.getId());
		this.setName(userAttr.getName());
		this.setValue(userAttr.getValue());
		this.setIsMultivalued(userAttr.getIsMultivalued());
		this.setValues(userAttr.getValues());
		this.setMetadataId(userAttr.getMetadataId());
		this.setMetadataName(userAttr.getMetadataName());
		this.setMetadataDescription(userAttr.getMetadataDescription());
		this.setParentId(userAttr.getUserId());
		this.setOperation(userAttr.getOperation());
	}

	public UserAttribute updateUserAttribute(UserAttribute userAttr) {
		userAttr.setId(this.getId());
		userAttr.setName(this.getName());
		userAttr.setValue(this.getValue());
		userAttr.setIsMultivalued(this.getIsMultivalued());
		userAttr.setValues(this.getValues());
		userAttr.setMetadataId(this.getMetadataId());
		userAttr.setMetadataName(this.getMetadataName());
		userAttr.setMetadataDescription(this.getMetadataDescription());
		userAttr.setUserId(this.getParentId());
		userAttr.setOperation(this.getOperation());

		return userAttr;
	}

	public AttributeOperationEnum getOperation() {
		return operation;
	}

	public void setOperation(AttributeOperationEnum operation) {
		this.operation = operation;
	}

}
