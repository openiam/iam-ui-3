package org.openiam.ui.rest.api.model;

import org.openiam.idm.srvc.res.dto.Resource;
import org.openiam.idm.srvc.res.dto.ResourceRisk;
import org.openiam.idm.srvc.res.dto.ResourceType;

public class ResourceBean extends AbstractEntitlementsBean {

	private String resourceType;
	private String resourceTypeId;
    private ResourceRisk risk;

	public ResourceBean() {};

	public ResourceBean(Resource res) {
		this.setId(res.getId());
		this.setName(res.getName());
		this.setDescription(res.getDescription());
		this.setMdTypeId(res.getMdTypeId());
		this.setMetadataTypeName(res.getMetadataTypeName());
		this.setOperation(res.getOperation());
		this.setResourceType(res.getResourceType().getDescription());
		this.setResourceTypeId(res.getResourceType().getId());
		this.setRisk(res.getRisk());
	}

	public void updateResource(Resource res) {
		res.setId(this.getId());
		res.setName(this.getName());
		res.setDescription(this.getDescription());
		res.setMdTypeId(this.getMdTypeId());
		res.setMetadataTypeName(this.getMetadataTypeName());
		res.setOperation(this.getOperation());
		res.setRisk(this.getRisk());
		ResourceType resType = new ResourceType();
		resType.setId(this.getId());
		resType.setDisplayName(this.getResourceType());
		res.setResourceType(resType);
	}

	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	public String getResourceTypeId() {
		return resourceTypeId;
	}
	public void setResourceTypeId(String resourceTypeId) {
		this.resourceTypeId = resourceTypeId;
	}

    public ResourceRisk getRisk() {
        return risk;
    }

    public void setRisk(ResourceRisk risk) {
        this.risk = risk;
    }
}
