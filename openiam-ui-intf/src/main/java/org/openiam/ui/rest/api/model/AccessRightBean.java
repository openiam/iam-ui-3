package org.openiam.ui.rest.api.model;

public class AccessRightBean extends KeyNameBean {

    private String displayName;
    private String metadataType1;
    private String metadataType2;

    private String metadataTypeDisplayName1;
    private String metadataTypeDisplayName2;

    public String getMetadataType1() {
        return metadataType1;
    }

    public void setMetadataType1(String metadataType1) {
        this.metadataType1 = metadataType1;
    }

    public String getMetadataType2() {
        return metadataType2;
    }

    public void setMetadataType2(String metadataType2) {
        this.metadataType2 = metadataType2;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getMetadataTypeDisplayName1() {
        return metadataTypeDisplayName1;
    }

    public void setMetadataTypeDisplayName1(String metadataTypeDisplayName1) {
        this.metadataTypeDisplayName1 = metadataTypeDisplayName1;
    }

    public String getMetadataTypeDisplayName2() {
        return metadataTypeDisplayName2;
    }

    public void setMetadataTypeDisplayName2(String metadataTypeDisplayName2) {
        this.metadataTypeDisplayName2 = metadataTypeDisplayName2;
    }
}
