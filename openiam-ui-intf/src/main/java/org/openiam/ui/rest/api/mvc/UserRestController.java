package org.openiam.ui.rest.api.mvc;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.base.AttributeOperationEnum;
import org.openiam.base.ExtendController;
import org.openiam.base.ws.*;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.idm.searchbeans.PotentialSupSubSearchBean;
import org.openiam.idm.searchbeans.UserSearchBean;
import org.openiam.idm.srvc.auth.dto.Login;
import org.openiam.idm.srvc.auth.dto.ProvLoginStatusEnum;
import org.openiam.idm.srvc.auth.ws.LoginListResponse;
import org.openiam.idm.srvc.grp.dto.Group;
import org.openiam.idm.srvc.org.dto.OrganizationType;
import org.openiam.idm.srvc.org.dto.OrganizationUserDTO;
import org.openiam.idm.srvc.policy.dto.Policy;
import org.openiam.idm.srvc.role.dto.Role;
import org.openiam.idm.srvc.sysprop.dto.SystemPropertyDto;
import org.openiam.idm.srvc.sysprop.ws.SystemPropertyWebService;
import org.openiam.idm.srvc.user.dto.*;
import org.openiam.idm.srvc.user.ws.UserDataWebService;
import org.openiam.idm.srvc.user.ws.UserResponse;
import org.openiam.provision.dto.ProvisionActionEnum;
import org.openiam.provision.dto.ProvisionActionEvent;
import org.openiam.provision.dto.ProvisionActionTypeEnum;
import org.openiam.provision.dto.ProvisionUser;
import org.openiam.provision.dto.accessmodel.UserAccessControlFilter;
import org.openiam.provision.dto.accessmodel.UserAccessControlRequest;
import org.openiam.provision.dto.accessmodel.UserAccessControlResponse;
import org.openiam.provision.dto.srcadapter.UserSearchAttributeRequest;
import org.openiam.provision.dto.srcadapter.UserSearchKey;
import org.openiam.provision.dto.srcadapter.UserSearchKeyEnum;
import org.openiam.provision.resp.ProvisionUserResponse;
import org.openiam.provision.service.ActionEventBuilder;
import org.openiam.provision.service.ProvisionServiceEventProcessor;
import org.openiam.provision.service.UserAccessControlService;
import org.openiam.ui.rest.api.model.*;
import org.openiam.ui.util.HeaderUtils;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.model.BeanResponse;
import org.openiam.ui.web.mvc.AbstractController;
import org.openiam.ui.web.mvc.AbstractUserController;
import org.openiam.ui.web.util.ApplicationsProvider;
import org.openiam.ui.web.util.UserBeanPropertiesParser;
import org.openiam.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

@Controller
public class UserRestController extends AbstractUserController {
    private static final Integer defaultPageSize = 10;
    private static final Integer defaultPageNumber = 0;

    private static final String SYSTEM_ADMIN_ID = "3000";

    @Autowired
    private UserBeanPropertiesParser userSearchParser;

    @Resource(name = "userAccessControlServiceClient")
    private UserAccessControlService userAccessControlServiceClient;

    @Resource(name = "userServiceClient")
    private UserDataWebService userServiceClient;

    @Resource(name = "systemPropertyClient")
    protected SystemPropertyWebService systemPropertyClient;

    @Value("${org.openiam.ui.selfservice.directory.lookup.skip.delegation.filter}")
    private boolean skipDeletagionFilterForDirectoryLookup;

    @Value("${org.openiam.ui.selfservice.directorylookup.user.status.not.show}")
    private String statusNotShow;

    @Autowired
    private ApplicationsProvider applicationProvider;

    @Value("${org.openiam.ui.search.light.mode}")
    private boolean ligthSearchMode;

    @Value("${org.openiam.ui.supervisor.search.status.show}")
    private String statusSupervisorsShow;
    private HttpServletRequest request;
    private EditUserModel userModel;

    @RequestMapping("/users/userStatuses")
    public
    @ResponseBody
    BeanResponse getUserStatuses() {
        final List<KeyNameBean> beans = getUserStatusList();
        return new BeanResponse(beans, beans.size());
    }

    @RequestMapping("/users/jobCodes")
    public
    @ResponseBody
    BeanResponse getJobCodes() {
        final List<KeyNameBean> beans = getJobCodeList();
        return new BeanResponse(beans, beans.size());
    }

    @RequestMapping("/users/userSecondaryStatuses")
    public
    @ResponseBody
    BeanResponse getUserSecondaryStatuses() {
        final List<KeyNameBean> beans = getUserSecondaryStatusList();
        return new BeanResponse(beans, beans.size());
    }

    @RequestMapping("/users/attributeList")
    public
    @ResponseBody
    BeanResponse getUserAttributeList() {
        final List<KeyNameBean> beans = getCompleteMetadataElementList();
        return new BeanResponse(beans, beans.size());
    }

    @RequestMapping("/users/employeeTypes")
    public
    @ResponseBody
    BeanResponse getEmployeeTypes() {
        final List<KeyNameBean> beans = getUserTypeList();
        return new BeanResponse(beans, beans.size());
    }

    @RequestMapping("/users/applications")
    public
    @ResponseBody
    BeanResponse getApplications(final HttpServletRequest request,
                                 @RequestParam(required = false, value = "ignoreUser") final Boolean ignoreUser) {
        final List<Application> beans = applicationProvider.getAppliationsForUser((Boolean.TRUE.equals(ignoreUser)) ? null : getRequesterId(request), request);
        return new BeanResponse(beans, beans.size());
    }

    @RequestMapping(value = "/users/getUserFormAttributes", method = RequestMethod.GET)
    public
    @ResponseBody
    UserSearchFormBean populateSearchForm(final HttpServletRequest request, String requesterId) {
        if (StringUtils.isBlank(requesterId)) {
            requesterId = getRequesterId(request);
        }

        final UserSearchFormBean userSearchFormBean = new UserSearchFormBean();
        userSearchFormBean.setRequesterId(requesterId);
        userSearchFormBean.setUserSearchModel(new UserSearchModel());
        //userSearchFormBean.setStatusList(getUserStatusList());
        //userSearchFormBean.setUserSecondaryStatusList(getUserSecondaryStatusList());
        //userSearchFormBean.setAttributeList(getCompleteMetadataElementList());
        //userSearchFormBean.setOrganizationList(getOrganizationBeanList(requesterId));
        //userSearchFormBean.setGroupList(getGroupList(requesterId));
        //userSearchFormBean.setRoleList(getRoleList(requesterId));
        //userSearchFormBean.setEmployeeTypes(getUserTypeList());
        userSearchFormBean.setAdditionalSearchCriteria(userSearchParser.getAdditionalSearchCriteriaList());
        //addAdditionalSearchCriteria("userStatus");
//        userSearchFormBean.addAdditionalSearchCriteria("organizationId");
        userSearchFormBean.setSize(defaultPageSize);
        userSearchFormBean.setPageNumber(defaultPageNumber);
        return userSearchFormBean;
    }

    @RequestMapping(value = "/users/searchSuperiors", method = RequestMethod.POST)
    public
    @ResponseBody
    BeanResponse searchSuperiors(final HttpServletRequest request, final @RequestBody UserSearchModel searchModel) {

        if (searchModel.isEmpty()) {
            return BeanResponse.EMPTY_RESPONSE;
        }

        MatchType defMatchType = MatchType.STARTS_WITH;
        List<SystemPropertyDto> propList = systemPropertyClient.getByType("USER_SEARCH_PROP");
        if (CollectionUtils.isNotEmpty(propList)) {
            for (SystemPropertyDto sysProp : propList) {
                if ("DEFAULT_MATCH_TYPE".equalsIgnoreCase(sysProp.getName())) {
                    try {
                        defMatchType = MatchType.valueOf(sysProp.getValue());
                    } catch (Exception e) {
                        log.error("Cann't parse system property : DEFAULT_MATCH_TYPE = " + sysProp.getValue());
                    }
                    continue;
                }
            }
        }

        PotentialSupSubSearchBean searchBean = searchModel.buildSearchBean(this.getRequesterId(request), PotentialSupSubSearchBean.class, defMatchType);
        searchBean.setInitDefaulLogin(true);
        //buildSearchBeanFromModel(searchModel, request);

        final int from = searchModel.getFrom();
        final int size = searchModel.getSize();
        final List<User> userList = userServiceClient.findPotentialSupSubs(searchBean, from, size);
        final Integer count = (from == 0) ? userServiceClient.findPotentialSupSubsCount(searchBean) : null;
        final List<UserBean> beanList = new LinkedList<UserBean>();
        String userStatus;
        if (CollectionUtils.isNotEmpty(userList)) {
            for (final User user : userList) {
                if (!statusSupervisorsShow.isEmpty()) {
                    userStatus = user.getStatus().toString();
                    if (statusSupervisorsShow.contains(userStatus)) {
                        beanList.add(UserBean.getInstance(user));
                    }
                } else {
                    beanList.add(UserBean.getInstance(user));
                }
            }
        }
        return new BeanResponse(beanList, count);
    }

    @RequestMapping(value = "/users/searchSubordinates", method = RequestMethod.POST)
    public
    @ResponseBody
    BeanResponse searchSubordinates(final HttpServletRequest request, final @RequestBody UserSearchModel searchModel) {

        if (searchModel.isEmpty()) {
            return BeanResponse.EMPTY_RESPONSE;
        }

        MatchType defMatchType = MatchType.STARTS_WITH;
        List<SystemPropertyDto> propList = systemPropertyClient.getByType("USER_SEARCH_PROP");
        if (CollectionUtils.isNotEmpty(propList)) {
            for (SystemPropertyDto sysProp : propList) {
                if ("DEFAULT_MATCH_TYPE".equalsIgnoreCase(sysProp.getName())) {
                    try {
                        defMatchType = MatchType.valueOf(sysProp.getValue());
                    } catch (Exception e) {
                        log.error("Cann't parse system property : DEFAULT_MATCH_TYPE = " + sysProp.getValue());
                    }
                    continue;
                }
            }
        }

        PotentialSupSubSearchBean searchBean = searchModel.buildSearchBean(this.getRequesterId(request), PotentialSupSubSearchBean.class, defMatchType);
        searchBean.setInitDefaulLogin(true);
        //buildSearchBeanFromModel(searchModel, request);

        final int from = searchModel.getFrom();
        final int size = searchModel.getSize();
        final List<User> userList = userServiceClient.findPotentialSupSubs(searchBean, from, size);
        final Integer count = (from == 0) ? userServiceClient.findPotentialSupSubsCount(searchBean) : null;
        final List<UserBean> beanList = new LinkedList<UserBean>();
        if (CollectionUtils.isNotEmpty(userList)) {
            for (final User user : userList) {
                beanList.add(UserBean.getInstance(user));
            }
        }
        return new BeanResponse(beanList, count);
    }


    @RequestMapping(value = "/users/getSuperiors", method = RequestMethod.GET)
    public
    @ResponseBody
    BeanResponse getSuperiors(final @RequestParam(required = false, value = "id") String userId,
                              final @RequestParam(required = true, value = "size") Integer size,
                              final @RequestParam(required = true, value = "from") Integer from) {

        List<User> superiors = userServiceClient.getSuperiors(userId, from, size);
        Integer count = userServiceClient.getSuperiorsCount(userId);
        final List<UserBean> beanList = new LinkedList<UserBean>();
        if (CollectionUtils.isNotEmpty(superiors)) {
            for (final User user : superiors) {
                beanList.add(UserBean.getInstance(user));
            }
        }
        return new BeanResponse(beanList, count);
    }

    @RequestMapping(value = "/users/getAllSuperiors", method = RequestMethod.GET)
    public
    @ResponseBody
    BeanResponse getAllSuperiors(final @RequestParam(required = true, value = "size") Integer size,
                                 final @RequestParam(required = true, value = "from") Integer from) {

        List<User> superiors = userServiceClient.getAllSuperiors(from, size);
        Integer count = userServiceClient.getAllSuperiorsCount();
        final List<UserBean> beanList = new LinkedList<UserBean>();
        if (CollectionUtils.isNotEmpty(superiors)) {
            for (final User user : superiors) {
                beanList.add(UserBean.getInstance(user));
            }
        }
        return new BeanResponse(beanList, count);
    }

    //New Method

    @RequestMapping(value = "/users/getAllSuperiors", method = RequestMethod.POST)
    public
    @ResponseBody
    BeanResponse getAllSupervisor(final HttpServletRequest request, final @RequestBody UserSearchModel searchModel,
                                  final @RequestParam(required = false, value = "size") Integer sizes) {

        long startTime = System.currentTimeMillis();
        if (searchModel.isEmpty()) {
            return BeanResponse.EMPTY_RESPONSE;
        }
        BeanResponse beanResponse = new BeanResponse();

        String requestorId = (skipDeletagionFilterForDirectoryLookup && searchModel.getFromDirectoryLookup()) ? SYSTEM_ADMIN_ID : this.getRequesterId(request);
        if(log.isDebugEnabled()) {
            log.debug("Original search mode!");
        }
        MatchType defMatchType = MatchType.STARTS_WITH;
        List<SystemPropertyDto> propList = systemPropertyClient.getByType("USER_SEARCH_PROP");
        if (CollectionUtils.isNotEmpty(propList)) {
            for (SystemPropertyDto sysProp : propList) {
                if ("DEFAULT_MATCH_TYPE".equalsIgnoreCase(sysProp.getName())) {
                    try {
                        defMatchType = MatchType.valueOf(sysProp.getValue());
                    } catch (Exception e) {
                        log.error("Cann't parse system property : DEFAULT_MATCH_TYPE = " + sysProp.getValue());
                    }
                    continue;
                }
            }
        }


        UserSearchBean searchBean = searchModel.buildSearchBean(requestorId, UserSearchBean.class, defMatchType);

        List<String> columnList = userSearchParser.getUserSearchResultColumnList();

        if (CollectionUtils.isNotEmpty(columnList) && columnList.contains("principal")) {
            searchBean.setInitDefaulLogin(true);
        }


        String validationError = validateSearchRequest(searchBean, request);
        final List<UserBean> beanList = new LinkedList<UserBean>();
        Integer count = 0;

        if (StringUtils.isNotBlank(validationError)) {
            beanResponse.setError(validationError);
        } else {
            final int from = searchModel.getFrom();
            final int size = searchModel.getSize();




            final List<User> userList = userServiceClient.findBeans(searchBean, from, size);
            count = (from == 0) ? userServiceClient.count(searchBean) : null;

            String userStatus;

            if (CollectionUtils.isNotEmpty(userList)) {

                    for (final User user : userList) {
                        beanList.add(UserBean.getInstance(user));
                    }

            }
        }
        beanResponse.setBeans(beanList);
        beanResponse.setSize(count);

        return beanResponse;
    }

    @RequestMapping(value = "/users/getSubordinates", method = RequestMethod.GET)
    public
    @ResponseBody
    BeanResponse getSubordinates(final @RequestParam(required = false, value = "id") String userId,
                                 final @RequestParam(required = true, value = "size") Integer size,
                                 final @RequestParam(required = true, value = "from") Integer from) {

        List<User> subordinates = userServiceClient.getSubordinates(userId, from, size);
        Integer count = userServiceClient.getSubordinatesCount(userId);
        final List<UserBean> beanList = new LinkedList<UserBean>();
        if (CollectionUtils.isNotEmpty(subordinates)) {
            for (final User user : subordinates) {
                beanList.add(UserBean.getInstance(user));
            }
        }
        return new BeanResponse(beanList, count);
    }

    @RequestMapping(value = "/users/attributes", method = RequestMethod.GET)
    public
    @ResponseBody
    BeanResponse userAttributes(final HttpServletRequest request,
                                final HttpServletResponse response,
                                final @RequestParam(required = true, value = "id") String id) {
        final List<UserAttribute> attributes = userServiceClient.getUserAttributesInternationalized(id, getCurrentLanguage());

        //IDMAPPS-2375
        final List<UserAttribute> attributesValidated = new LinkedList<UserAttribute>();
        if (CollectionUtils.isNotEmpty(attributes)) {
            for (final UserAttribute attribute : attributes) {
                if (StringUtils.equals(attribute.getMetadataName(), "password")) {
                    attribute.setValue("*****");
                    attribute.setValues(null);
                }
                if (!attribute.getName().contains("DLG_FLT")) {
                    attributesValidated.add(attribute);
                }
            }
        }
        final List<MetadataAttributeBean> beans = mapper.mapToList(attributesValidated, MetadataAttributeBean.class);
        return new BeanResponse(beans, beans.size());
    }

    @RequestMapping(value = "/users/search", method = RequestMethod.POST)
    public
    @ResponseBody
    BeanResponse searchUsers(final HttpServletRequest request, final @RequestBody UserSearchModel searchModel,
                             @RequestParam(required = false, value = "deepCopy") Boolean deepCopy) {
        long startTime = System.currentTimeMillis();
        if (searchModel.isEmpty()) {
            return BeanResponse.EMPTY_RESPONSE;
        }
        BeanResponse beanResponse = new BeanResponse();
        deepCopy = deepCopy == null ? false : deepCopy;
        String requestorId = (skipDeletagionFilterForDirectoryLookup && searchModel.getFromDirectoryLookup()) ? SYSTEM_ADMIN_ID : this.getRequesterId(request);
        try {
            if (ligthSearchMode && searchModel.isLightSearch() && Boolean.FALSE.equals(deepCopy)) {
                log.debug("Light search mode!");
                //NEW====================================
                final int from = searchModel.getFrom();
                final int size = searchModel.getSize();
                final List<UserBean> beanList = new LinkedList<UserBean>();
                LightSearchRequest lightSearchRequest = new LightSearchRequest();
                lightSearchRequest.setFrom(from);
                lightSearchRequest.setSize(size);
                lightSearchRequest.setEmailAddress(searchModel.getEmail());
                lightSearchRequest.setEmployeeId(searchModel.getEmployeeId());
                lightSearchRequest.setLastName(searchModel.getLastName());
                lightSearchRequest.setLogin(searchModel.getPrincipal());
                if (StringUtils.isNotBlank(searchModel.getUserStatus())) {
                    lightSearchRequest.setStatus(UserStatusEnum.valueOf(searchModel.getUserStatus().toUpperCase()));
                }
                if (StringUtils.isNotBlank(searchModel.getAccountStatus())) {
                    lightSearchRequest.setSecondaryStatus(UserStatusEnum.valueOf(searchModel.getAccountStatus().toUpperCase()));
                }
                if (searchModel.getOrderBy() != null && StringUtils.isNotBlank(searchModel.getSortBy())) {
                    lightSearchRequest.setSortParam(Arrays.asList(new SortParam(searchModel.getOrderBy(), searchModel.getSortBy())));
                }
                lightSearchRequest.setRequesterId(requestorId);
                LightSearchResponse lightSearchResponse = userServiceClient.getLightSearchResult(lightSearchRequest);
                if (org.openiam.base.ws.ResponseStatus.SUCCESS.equals(lightSearchResponse.getStatus())) {
                    List<LightUserSearchModel> userList = lightSearchResponse.getLightUserSearchModels();

                    if (CollectionUtils.isNotEmpty(userList)) {
                        for (final LightUserSearchModel user : userList) {
                            beanList.add(UserBean.getInstance(user));
                        }
                    }

                    beanResponse.setBeans(beanList);
                    beanResponse.setSize(lightSearchResponse.getCount());
                } else {
                    return BeanResponse.EMPTY_RESPONSE;
                }
            } else {
                log.debug("Original search mode!");
                MatchType defMatchType = MatchType.STARTS_WITH;
                List<SystemPropertyDto> propList = systemPropertyClient.getByType("USER_SEARCH_PROP");
                if (CollectionUtils.isNotEmpty(propList)) {
                    for (SystemPropertyDto sysProp : propList) {
                        if ("DEFAULT_MATCH_TYPE".equalsIgnoreCase(sysProp.getName())) {
                            try {
                                defMatchType = MatchType.valueOf(sysProp.getValue());
                            } catch (Exception e) {
                                log.error("Cann't parse system property : DEFAULT_MATCH_TYPE = " + sysProp.getValue());
                            }
                            continue;
                        }
                    }
                }

                UserSearchBean searchBean = searchModel.buildSearchBean(requestorId, UserSearchBean.class, defMatchType);
                List<String> columnList = userSearchParser.getUserSearchResultColumnList();
                if (CollectionUtils.isNotEmpty(columnList) && columnList.contains("principal")) {
                    searchBean.setInitDefaulLogin(true);
                }

                String validationError = validateSearchRequest(searchBean, request);
                final List<UserBean> beanList = new LinkedList<UserBean>();
                Integer count = 0;

                if (StringUtils.isNotBlank(validationError)) {
                    beanResponse.setError(validationError);
                } else {
                    final int from = searchModel.getFrom();
                    final int size = searchModel.getSize();
                    if (deepCopy != null)
                        searchBean.setDeepCopy(deepCopy);

                    if (searchModel.getFromDirectoryLookup()) {
                        searchBean.setDeepCopy(true);
                    }

                    final List<User> userList = userServiceClient.findBeans(searchBean, from, size);
                    count = (from == 0) ? userServiceClient.count(searchBean) : null;

                    String userStatus;

                    if (CollectionUtils.isNotEmpty(userList)) {
                        if (searchModel.getFromDirectoryLookup()) {
                            for (final User user : userList) {
                                userStatus = user.getStatus().toString();
                                if (!statusNotShow.contains(userStatus)) {
                                    beanList.add(UserBean.getInstance(user));
                                }
                            }
                        } else {
                            for (final User user : userList) {
                                beanList.add(UserBean.getInstance(user));
                            }
                        }
                    }
                }
                beanResponse.setBeans(beanList);
                beanResponse.setSize(count);
                return beanResponse;
            }
        } catch (Throwable e) {
            log.error("Search user exception", e);
            if ("Read timed out".equals(e.getCause().getCause().getMessage())) {
                beanResponse.setError(getMessage(request, Errors.TIME_OUT_ERROR));
            } else {
                beanResponse.setError(e.getMessage());
            }
        }
        log.debug("Search time=" + (System.currentTimeMillis() - startTime));
        log.debug("Results amount=" + beanResponse.getSize());
        return beanResponse;
    }

    @RequestMapping(value = "/users/search/metadata", method = RequestMethod.POST)
    public
    @ResponseBody
    List<String> searchUsersMetadata(final HttpServletRequest request) {
        return userSearchParser.getUserSearchResultColumnList();
    }

    //New Method



    private String validateSearchRequest(UserSearchBean searchBean, final HttpServletRequest request) {
        Response wsResponse = userDataWebService.validateUserSearchRequest(searchBean);
        String errorText = null;

        if (wsResponse.getStatus() != org.openiam.base.ws.ResponseStatus.SUCCESS) {
            if (wsResponse.getErrorCode() != null) {
                switch (wsResponse.getErrorCode()) {
                    case NOT_ALLOWED_ORGANIZATION_IN_SEARCH:
                        errorText = getMessage(request, Errors.NOT_ALLOWED_ORGANIZATION_IN_SEARCH);
                        break;
                    case NOT_ALLOWED_GROUP_IN_SEARCH:
                        errorText = getMessage(request, Errors.NOT_ALLOWED_GROUP_IN_SEARCH);
                        break;
                    case NOT_ALLOWED_ROLE_IN_SEARCH:
                        errorText = getMessage(request, Errors.NOT_ALLOWED_ROLE_IN_SEARCH);
                        break;
                    default:
                        errorText = getMessage(request, Errors.INVALID_USER_SEARCH_REQUEST);
                        break;
                }
            }
        }
        return errorText;
    }


    @RequestMapping(value = "/access/get/{type}/{value}", method = RequestMethod.GET)
    public
    @ResponseBody
    String getAccess(final HttpServletRequest httpServletRequest,
                     @PathVariable(value = "type") String type,
                     @PathVariable(value = "value") String value,
                     @RequestParam(value = "named", required = false, defaultValue = "false") boolean namedTypes,
                     @RequestParam(value = "mngSysF", required = false) List<String> managedSystems,
                     @RequestParam(value = "roleMTF", required = false) List<String> roleMetadataTypes,
                     @RequestParam(value = "groupMTF", required = false) List<String> groupMetadataTypes,
                     @RequestParam(value = "resMTF", required = false) List<String> resourceMetadataTypes,
                     @RequestParam(value = "resTF", required = false) List<String> resourceTypes,
                     @RequestParam(value = "groupNameF", required = false) String groupNameF,
                     @RequestParam(value = "roleNameF", required = false) String roleNameF,
                     @RequestParam(value = "resourceNameF", required = false) String resourceNameF,
                     @RequestParam(value = "commonNameF", required = false) String commonNameF,
                     @RequestParam(value = "tags", required = false) List<String> tags
    ) throws IOException {
        UserAccessControlRequest userAccessControlRequest = new UserAccessControlRequest();
        UserAccessControlFilter userAccessControlFilter = new UserAccessControlFilter();
        userAccessControlFilter.setManagedSystemNames(managedSystems);
        userAccessControlFilter.setRoleMetadataTypes(roleMetadataTypes);
        userAccessControlFilter.setGroupMetadataTypes(groupMetadataTypes);
        userAccessControlFilter.setResourceMetadataTypes(resourceMetadataTypes);
        userAccessControlFilter.setResourceTypes(resourceTypes);
        userAccessControlFilter.setGroupFIlter(groupNameF);
        userAccessControlFilter.setRoleFilter(roleNameF);
        userAccessControlFilter.setResourceFilter(resourceNameF);
        userAccessControlFilter.setCommonNameFilter(commonNameF);
        if (CollectionUtils.isNotEmpty(tags)) {
            for (String tag : tags) {
                String[] splittedTag = tag.split(":");
                if (splittedTag != null && splittedTag.length == 2) {
                    UserSearchAttributeRequest attributeRequest = new UserSearchAttributeRequest();
                    attributeRequest.setName(splittedTag[0]);
                    attributeRequest.setValue(splittedTag[1]);
                    if (userAccessControlFilter.getSearchAttributes() == null) {
                        userAccessControlFilter.setSearchAttributes(new ArrayList<UserSearchAttributeRequest>());
                    }
                    userAccessControlFilter.getSearchAttributes().add(attributeRequest);
                }
            }
        }
        userAccessControlRequest.setFilter(userAccessControlFilter);
        userAccessControlRequest.setNamedTypes(namedTypes);
        userAccessControlRequest.setTreeRepresentation(false);
        userAccessControlRequest.setIpAdress(HeaderUtils.getClientIP(httpServletRequest));
        userAccessControlRequest.setRequesterId(this.getRequesterId(httpServletRequest));
        userAccessControlRequest.setRequesterLogin(this.getRequesterPrincipal(httpServletRequest));
        try {
            if (StringUtils.isBlank(type) || StringUtils.isBlank(value)) {
                throw new Exception("Identifier can't be black");
            }
            UserSearchKey userSearchKey = new UserSearchKey();
            userSearchKey.setName(UserSearchKeyEnum.valueOf(type.toUpperCase()));
            userSearchKey.setValue(value);
            userAccessControlRequest.setKey(userSearchKey);
        } catch (Exception e) {
            UserAccessControlResponse response = new UserAccessControlResponse();
            response.setStatus(org.openiam.base.ws.ResponseStatus.FAILURE);
            response.setError(e.getMessage());
            return jacksonMapper.writeValueAsString(response);
        }
        return jacksonMapper.writeValueAsString(userAccessControlServiceClient.getAccessControl(userAccessControlRequest));
    }





    @RequestMapping(value = "/users/user/search", method = RequestMethod.POST)
    public
    @ResponseBody
    List<EditUserModel> searchUserList(final HttpServletRequest request,
                                    @RequestBody UserSearchModel searchModel) throws IOException {
        String requestorId = (skipDeletagionFilterForDirectoryLookup && searchModel.getFromDirectoryLookup()) ? SYSTEM_ADMIN_ID : this.getRequesterId(request);
        List<EditUserModel> resultUserList = new ArrayList<>();

        MatchType defMatchType = MatchType.STARTS_WITH;
        List<SystemPropertyDto> propList = systemPropertyClient.getByType("USER_SEARCH_PROP");
        if (CollectionUtils.isNotEmpty(propList)) {
            for (SystemPropertyDto sysProp : propList) {
                if ("DEFAULT_MATCH_TYPE".equalsIgnoreCase(sysProp.getName())) {
                    try {
                        defMatchType = MatchType.valueOf(sysProp.getValue());
                    } catch (Exception e) {
                        log.error("Cann't parse system property : DEFAULT_MATCH_TYPE = " + sysProp.getValue());
                    }
                    continue;
                }
            }
        }

        UserSearchBean searchBean = searchModel.buildSearchBean(requestorId, UserSearchBean.class, defMatchType);
        List<String> columnList = userSearchParser.getUserSearchResultColumnList();
        if (CollectionUtils.isNotEmpty(columnList) && columnList.contains("principal")) {
            searchBean.setInitDefaulLogin(true);
        }

        String validationError = validateSearchRequest(searchBean, request);
        final List<UserBean> beanList = new LinkedList<UserBean>();
        Integer count = 0;

        if (StringUtils.isBlank(validationError)) {
            final int from = searchModel.getFrom();
            final int size = searchModel.getSize();
            searchBean.setDeepCopy(searchModel.isDeepCopy());
            final List<User> userList = userServiceClient.findBeans(searchBean, from, size);
            if (userList != null && userList.size() > 0) {
                for (User usr : userList) {
                    EditUserModel eum = new EditUserModel();
                    eum = mapper.mapToObject(usr, EditUserModel.class);
                    eum.formatDates();
                    setUserModelOrg(usr, eum);
                    setUserModelAttr(usr, eum);
                    setUserModelRes(usr, eum);
                    setModelSupervisors(usr, eum);
                    eum.setStatus(200);
                    resultUserList.add(eum);
                }
                return resultUserList;
            }
        }
        EditUserModel eumError = new EditUserModel();
        eumError.setStatus(500);
        ErrorToken errorToken = new ErrorToken(Errors.INVALID_USER_SEARCH_REQUEST);
        errorToken.setMessage(validationError);
        eumError.addError(errorToken);
        resultUserList.add(eumError);

        return resultUserList;
    }

    @RequestMapping(value = "/users/get/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    EditUserModel getUser(final HttpServletRequest request,
                      @PathVariable(value = "id") String userId) throws IOException {
        EditUserModel userModel = new EditUserModel();
        if (StringUtils.isNotBlank(userId)) {
            final User user = userDataWebService.getUserWithDependent(userId,
                    getRequesterId(request), true);
            if (user == null) {
                userModel.setStatus(500);
                ErrorToken errorToken = new ErrorToken(Errors.USER_ID_WRONG);
                errorToken.setMessage(messageSource.getMessage(
                        errorToken.getError().getMessageName(),
                        errorToken.getParams(), localeResolver.resolveLocale(request)));
                userModel.addError(errorToken);
                return userModel;
            }

            userModel = mapper.mapToObject(user, EditUserModel.class);
            userModel.formatDates();
            setUserModelOrg(user, userModel);
            setUserModelAttr(user, userModel);
            setUserModelRes(user, userModel);
            setModelSupervisors(user, userModel);
            userModel.setStatus(200);
        } else {
            userModel.setStatus(500);
            ErrorToken errorToken = new ErrorToken(Errors.USER_ID_WRONG);
            errorToken.setMessage(messageSource.getMessage(
                    errorToken.getError().getMessageName(),
                    errorToken.getParams(), localeResolver.resolveLocale(request)));
            userModel.addError(errorToken);
        }
        return userModel;
    }

    @RequestMapping(value = "/users/login/get/{login}", method = RequestMethod.GET)
    public
    @ResponseBody
    EditUserModel getUserByLogin(final HttpServletRequest request,
                      @PathVariable(value = "login") String login) {
        EditUserModel userModel = new EditUserModel();
        if (StringUtils.isNotBlank(login)) {

            Policy policy = this.getAuthentificationPolicy();
            User user = userDataWebService.getUserByPrincipal(login, this.getAuthentificationManagedSystem(policy), true);
            if (user == null) {
                userModel.setStatus(500);
                ErrorToken errorToken = new ErrorToken(Errors.USER_ID_WRONG);
                errorToken.setMessage(messageSource.getMessage(
                        errorToken.getError().getMessageName(),
                        errorToken.getParams(), localeResolver.resolveLocale(request)));
                userModel.addError(errorToken);
                return userModel;
            }
            userModel = mapper.mapToObject(user, EditUserModel.class);
            userModel.formatDates();
            setUserModelOrg(user, userModel);
            setUserModelAttr(user, userModel);
            setUserModelRes(user, userModel);
            setModelSupervisors(user, userModel);
            userModel.setStatus(200);
        } else {
            userModel.setStatus(500);
            ErrorToken errorToken = new ErrorToken(Errors.USER_ID_WRONG);
            errorToken.setMessage(messageSource.getMessage(
                    errorToken.getError().getMessageName(),
                    errorToken.getParams(), localeResolver.resolveLocale(request)));
            userModel.addError(errorToken);
        }
        return userModel;

    }

    @RequestMapping(value = "/users/user", method = RequestMethod.PUT)
    public
    @ResponseBody
    EditUserModel createUser(final HttpServletRequest request, @RequestBody EditUserModel userModel) throws Exception {
        if (userModel != null)
            userModel.setId(null);
        return saveUser(request, userModel);
    }

    @RequestMapping(value = "/users/user/{id}", method = RequestMethod.POST)
    public
    @ResponseBody
    EditUserModel updateUser(final HttpServletRequest request, @RequestBody EditUserModel userModel,
                      @PathVariable(value = "id") String id) throws Exception {
        if (StringUtils.isNotBlank(id)) {
            userModel.setId(id);
            return saveUser(request, userModel);
        }
        EditUserModel user = new EditUserModel();
        user.setStatus(500);
        ErrorToken errorToken = new ErrorToken(Errors.USER_ID_WRONG);
        errorToken.setMessage(messageSource.getMessage(
                errorToken.getError().getMessageName(),
                errorToken.getParams(), localeResolver.resolveLocale(request)));
        user.addError(errorToken);
        return user;
    }

    @RequestMapping(value = "/users/user/{operation}/{id}", method = RequestMethod.DELETE)
    public
    @ResponseBody
    BasicAjaxResponse deleteUser(final HttpServletRequest request,
                             @PathVariable(value = "id") String userId,
                             @PathVariable(value = "operation") String operation) {

        if ("DELETE".equals(operation)) {
            return handleUserOperation(request, userId, UserOperation.DELETE);
        } else if ("REMOVE".equals(operation)) {
            return handleUserOperation(request, userId, UserOperation.REMOVE);
        } else if ("DISABLE".equals(operation)) {
            return handleUserOperation(request, userId, UserOperation.DISABLE);
        }
        BasicAjaxResponse response = new BasicAjaxResponse();
        response.setStatus(500);
        ErrorToken errorToken = new ErrorToken(Errors.OPERATION_ABORTED);
        errorToken.setMessage("Can't parse operation : " + operation);
        response.addError(errorToken);
        return response;
    }

    private EditUserModel saveUser(final HttpServletRequest request, final EditUserModel userModel) {
        ErrorToken errorToken = null;
        SuccessToken successToken = null;

        EditUserModel newModUser = new EditUserModel();
        Response wsResponse = null;

        ActionEventBuilder eventBuilder = new ActionEventBuilder(
                userModel.getId(),
                getRequesterId(request),
                ProvisionActionEnum.SAVE);
        ProvisionActionEvent actionEvent = eventBuilder.build();
        Response response = provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.PRE);
        if (ProvisionServiceEventProcessor.BREAK.equalsIgnoreCase((String) response.getResponseValue())) {
            if (response.isSuccess()) {
                successToken = new SuccessToken(SuccessMessage.OPERATION_ABORTED);
                newModUser.setStatus(200);
                newModUser.setSuccessToken(successToken);
                newModUser.setSuccessMessage(messageSource.getMessage(
                        successToken.getMessage().getMessageName(),
                        null, localeResolver.resolveLocale(request)));
            } else {
                newModUser.setStatus(500);
                errorToken = new ErrorToken(Errors.OPERATION_ABORTED);
                errorToken.setMessage(messageSource.getMessage(
                        errorToken.getError().getMessageName(),
                        errorToken.getParams(), localeResolver.resolveLocale(request)));
                newModUser.addError(errorToken);
            }
            return newModUser;
        }

        try {
            wsResponse = saveUserInfo(request, userModel);
            if (wsResponse.getStatus() == ResponseStatus.SUCCESS) {
                successToken = new SuccessToken(SuccessMessage.USER_INFO_SAVED);
            } else {
                Errors error = Errors.CANNOT_SAVE_USER_INFO;
                if (wsResponse.getErrorCode() != null) {
                    switch (wsResponse.getErrorCode()) {
                        case SEND_EMAIL_FAILED:
                            error = Errors.USER_CREATED_BUT_EMAIL_NOT_SENT;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_1:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_1;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_2:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_2;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_3:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_3;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_4:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_4;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_5:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_5;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_6:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_6;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_7:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_7;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_8:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_8;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_9:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_9;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_10:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_10;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_11:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_11;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_12:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_12;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_13:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_13;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_14:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_14;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_15:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_15;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_16:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_16;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_17:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_17;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_18:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_18;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_19:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_19;
                            break;
                        case FAIL_PREPROCESSOR_CUSTOM_ERROR_20:
                            error = Errors.FAIL_PREPROCESSOR_CUSTOM_ERROR_20;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_1:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_1;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_2:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_2;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_3:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_3;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_4:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_4;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_5:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_5;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_6:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_6;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_7:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_7;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_8:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_8;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_9:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_9;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_10:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_10;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_11:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_11;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_12:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_12;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_13:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_13;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_14:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_14;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_15:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_15;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_16:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_16;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_17:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_17;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_18:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_18;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_19:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_19;
                            break;
                        case FAIL_POSTPROCESSOR_CUSTOM_ERROR_20:
                            error = Errors.FAIL_POSTPROCESSOR_CUSTOM_ERROR_20;
                            break;
                        default:
                            error = Errors.INTERNAL_ERROR;
                            break;
                    }
                }
                errorToken = new ErrorToken(error);
            }

        } catch (Exception e) {
            errorToken = new ErrorToken(Errors.INTERNAL_ERROR);
            log.error("Exception while saving custom field", e);

        } finally {
            if (errorToken != null) {
                newModUser.setStatus(500);
                errorToken.setMessage(messageSource.getMessage(
                        errorToken.getError().getMessageName(),
                        errorToken.getParams(),
                        localeResolver.resolveLocale(request)));
                newModUser.addError(errorToken);
                ErrorToken er = new ErrorToken();
                er.setMessage(wsResponse.getErrorCode().toString());
                newModUser.addError(er);
            } else {
                User resultUser = null;
                if (wsResponse != null && wsResponse.getResponseValue() != null) {
                    if (wsResponse.getResponseValue() instanceof String) {
                        final String userId = (String) wsResponse.getResponseValue();
                        resultUser = userDataWebService.getUserWithDependent(userId,
                                getRequesterId(request), true);
                    }
                }
                if (successToken != null) {
                    provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.POST);
                    if (resultUser != null) {
                        newModUser = mapper.mapToObject(resultUser, EditUserModel.class);
                        newModUser.formatDates();
                        setUserModelOrg(resultUser, newModUser);
                        setUserModelAttr(resultUser, newModUser);
                        setUserModelRes(resultUser, newModUser);
                        setModelSupervisors(resultUser, newModUser);
                    }
                    newModUser.setStatus(200);
                    newModUser.setSuccessToken(successToken);
                }
            }
        }

        return newModUser;
    }

    private void setUserModelOrg(User user, EditUserModel userModel) {
        if (user != null && user.getOrganizationUserDTOs() != null && user.getOrganizationUserDTOs().size() > 0) {
            for (OrganizationUserDTO org : user.getOrganizationUserDTOs()) {
                userModel.getOrganizationIds().add(new KeyNameBean(org.getOrganization().getId(), org.getOrganization().getName()));
            }
        }
    }
    private void setUserModelAttr(User user, EditUserModel userModel) {
        if (user != null && user.getUserAttributes() != null && user.getUserAttributes().size() > 0) {
            for (UserAttribute userAttr : user.getUserAttributes().values()) {
                userModel.getUserAttributes().put(userAttr.getName(), new UserAttributeBean(userAttr));
            }
        }
    }

    private void setUserModelRes(User user, EditUserModel userModel) {
        if (user != null && user.getResources() != null && user.getResources().size() > 0) {
            for (org.openiam.idm.srvc.res.dto.Resource userRes : user.getResources()) {
                userModel.getResources().add(new ResourceBean(userRes));
            }
        }
    }

    private void setModelSupervisors(User user, EditUserModel userModel) {
        List<User> supervisorList = userDataWebService.getSuperiors(user.getId(), -1, -1);
        if (supervisorList != null) {
            for (User supervisor : supervisorList) {
                userModel.getSupervisors().add(new KeyNameOperationBean(supervisor.getId(), supervisor.getFirstName() + " " + supervisor.getLastName()));
            }
        }
    }

}
