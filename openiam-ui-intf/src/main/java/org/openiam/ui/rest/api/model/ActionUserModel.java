package org.openiam.ui.rest.api.model;

import org.openiam.idm.srvc.user.dto.UserAttribute;
import org.openiam.ui.web.util.DateFormatStr;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by zaporozhec on 8/17/15.
 */
public class ActionUserModel implements Serializable {
    private String userId;
    private String performDataString;
    private String description;
    private List<UserAttribute> userAttributes;
    private Boolean performNow = Boolean.FALSE;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPerformDataString() {
        return performDataString;
    }

    public void setPerformDataString(String performDataString) {
        this.performDataString = performDataString;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPerformNow() {
        return performNow;
    }

    public void setPerformNow(Boolean performNow) {
        this.performNow = performNow;
    }

    public Date getPerformDate() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormatStr.getSdfDate());
        return sdf.parse(performDataString);
    }

    public List<UserAttribute> getUserAttributes() {
        return userAttributes;
    }

    public void setUserAttributes(List<UserAttribute> userAttributes) {
        this.userAttributes = userAttributes;
    }
}
