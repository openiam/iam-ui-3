package org.openiam.ui.web.model;

import org.apache.cxf.common.util.StringUtils;

public class SelectResetTypeFormRequest {

    private String userId;
    private String resetTypeSelect;
    private String postbackURL;

    public boolean hasEmptyField() {
        return StringUtils.isEmpty(resetTypeSelect) ||
                StringUtils.isEmpty(userId);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getResetTypeSelect() {
        return resetTypeSelect;
    }

    public void setResetTypeSelect(String resetTypeSelect) {
        this.resetTypeSelect = resetTypeSelect;
    }

    public String getPostbackURL() {
        return postbackURL;
    }

    public void setPostbackURL(String postbackURL) {
        this.postbackURL = postbackURL;
    }
}
