package org.openiam.ui.web.mvc;

import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openiam.idm.srvc.auth.ws.LoginDataWebService;
import org.openiam.idm.srvc.auth.ws.LoginResponse;
import org.openiam.idm.srvc.policy.dto.Policy;
import org.openiam.idm.srvc.policy.dto.PolicyAttribute;
import org.openiam.idm.srvc.policy.dto.ResetPasswordTypeEnum;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.idm.srvc.user.ws.UserResponse;
import org.openiam.ui.exception.ErrorTokenException;
import org.openiam.ui.security.OpeniamWebAuthenticationDetails;
import org.openiam.ui.util.URIUtils;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.model.SelectResetTypeFormRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@Controller
public class SelectUserTypeController extends AbstractPasswordController {

    private static Logger log = Logger.getLogger(SelectUserTypeController.class);

    @RequestMapping(value = "/selectResetPasswordType", method = RequestMethod.GET)
    public String getResetTypePasswordScreen(final HttpServletRequest request,
                                             final HttpServletResponse response, @RequestParam(value = "postbackURL", required = false) String postbackURL) throws Exception {

        if (!URIUtils.isValidPostbackURL(postbackURL)) {
            log.warn(String.format("Postback URL '%s' not valid - doesn't start with a '/' - XSS detected.  Returning 401", postbackURL));
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }

        String userId = getRequesterIdFromCookie(request);
        if (userId == null) {
            String currentLogin = getRequesterPrincipal(request);
            if (currentLogin == null) {
                currentLogin = getTemporaryLoginCookie(request);
                if (StringUtils.isNotBlank(currentLogin)) {
                    Policy policy = this.getAuthentificationPolicy();
                    String internalLogin = this.buildLoginAccordingAuthPolicy(policy, currentLogin);
                    LoginResponse res = loginServiceClient.getLoginByManagedSys(internalLogin, this.getAuthentificationManagedSystem(policy));
                    if (res.isSuccess()) {
                        userId = res.getPrincipal().getUserId();
                    }
                }
            }
        }

        Policy policy = getPasswordPolicy(userId);
        boolean disabledSelect = true;
        if (policy != null) {
            PolicyAttribute policyAttribute = policy.getAttribute("USER_DRIVEN_RESET");
            if (policyAttribute != null && StringUtils.isNotBlank(policyAttribute.getValue1())
                    && "true".equalsIgnoreCase(policyAttribute.getValue1())) {
                disabledSelect = false;
            }
        }
        request.setAttribute("disabledSelectResetPassword", disabledSelect);

        User u = userDataWebService.getUserWithDependent(userId, userId, false);
        if (u != null) {
            request.setAttribute("resetPasswordType", u.getResetPasswordType() == null ? null : u.getResetPasswordType().name());
        }
        request.setAttribute("currentUserId", userId);
        request.setAttribute("postbackURL", postbackURL);

        return "jar:user/selectResetPasswordType";
    }

    @ResponseBody
    @RequestMapping(value = "/selectResetPasswordType", method = RequestMethod.POST)
    public BasicAjaxResponse postResetTypePasswordScreen(final HttpServletRequest request,
                                                         final HttpServletResponse response,
                                                         @RequestBody final SelectResetTypeFormRequest formRequest) throws Exception {

        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();

        if (!URIUtils.isValidPostbackURL(formRequest.getPostbackURL())) {
            log.warn(String.format("Postback URL '%s' not valid - doesn't start with a '/' - XSS detected.  Returning 401", formRequest.getPostbackURL()));
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            ajaxResponse.addError(new ErrorToken(Errors.UNAUTHORIZED));
            return ajaxResponse;
        }

        try {
            if (formRequest.hasEmptyField()) {
                throw new ErrorTokenException(new ErrorToken(Errors.CHANGE_PASSWORD_FAILED));
            }
            User u = userDataWebService.getUserWithDependent(formRequest.getUserId(), formRequest.getUserId(), false);
            if (u == null) {
                throw new ErrorTokenException(new ErrorToken(Errors.CHANGE_PASSWORD_FAILED));
            }
            u.setResetPasswordType(ResetPasswordTypeEnum.valueOf(formRequest.getResetTypeSelect()));
            u.setNotifyUserViaEmail(false);
            UserResponse userResponse = userDataWebService.saveUserInfo(u, null);
            if (userResponse.isSuccess()) {
                final SecurityContext ctx = SecurityContextHolder.getContext();
                final Authentication authentication = (ctx != null) ? ctx.getAuthentication() : null;
                if (authentication != null && authentication.getDetails() != null) {
                    ((OpeniamWebAuthenticationDetails) authentication.getDetails()).setResetPasswordTypeSelected(true);
                }

                ajaxResponse.setStatus(HttpServletResponse.SC_OK);
                ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.RESET_PASS_TYPE_SAVED_SUCCESS));

                if (StringUtils.isNotBlank(formRequest.getPostbackURL())) {
                    ajaxResponse.setRedirectURL(URIUtil.encodeQuery(formRequest.getPostbackURL()));
                } else {
                    ajaxResponse.setRedirectURL(URIUtil.encodeQuery("/selfservice/"));
                }
            } else {
                throw new Exception(String.format("Can't save changes"));
            }
        } catch (ErrorTokenException e) {
            ajaxResponse.setStatus(500);
            ajaxResponse.addErrors(e.getTokenList());
        } catch (Throwable e) {
            log.error("Can't save reset password type", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            ajaxResponse.addError(new ErrorToken(Errors.INTERNAL_ERROR));
        }

        ajaxResponse.process(localeResolver, messageSource, request);
        return ajaxResponse;
    }
}
