package org.openiam.ui.web.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.idm.srvc.auth.dto.AuthenticationRequest;
import org.openiam.idm.srvc.auth.dto.LogoutRequest;
import org.openiam.idm.srvc.auth.dto.Subject;
import org.openiam.idm.srvc.auth.service.AuthenticationConstants;
import org.openiam.idm.srvc.auth.service.AuthenticationService;
import org.openiam.idm.srvc.auth.ws.AuthenticationResponse;
import org.openiam.ui.login.LoginActionToken;
import org.openiam.ui.security.OpenIAMCookieProvider;
import org.openiam.ui.util.HeaderUtils;
import org.openiam.ui.util.URIUtils;
import org.openiam.ui.web.filter.ContentProviderFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Set;

@Component
public class LoginProvider {

	@Resource(name="authServiceClient")
	private AuthenticationService authServiceClient;
	
	@Value("${org.openiam.password.expiration.warnDays}")
	private int minNumOfDaysToWarnUserBeforePasswordExpiration;
	

	@Value("${org.openiam.ui.auth.logout.unsetCookieOnLogout}")
	private boolean unsetCookieOnLogout;

	@Value("${org.openiam.idp.provider.saml.idp.logout.global}")
	private String globalLogoutURL;

	@Value("${org.openiam.idp.provider.saml.sp.logout}")
	private String samlServiceProviderLogoutURL;

    @Autowired
    protected OpenIAMCookieProvider cookieProvider;
    
    /**
     * Attempst to login using the given login/password
     * @param request
     * @param response
     * @param login
     * @param password
     * @return
     * @throws Exception
     */
	public LoginActionToken getLoginActionToken(final HttpServletRequest request, final HttpServletResponse response, final String login, final String password) throws Exception {
		final AuthenticationRequest authenticatedRequest = getAuthenticationRequest(request, password, login, false, false);
		return getLoginActionToken(authenticatedRequest);
	}

	/**
	 * Attempts to login, but skips checking the password and skips checking the user status
	 * @param request
	 * @param response
	 * @param login
	 * @return
	 */
    public LoginActionToken getLoginActionToken(final HttpServletRequest request, final HttpServletResponse response, final String login) {
    	final AuthenticationRequest authenticatedRequest = getAuthenticationRequest(request, null, login, true, true);
    	return getLoginActionToken(authenticatedRequest);
    }
    
    private AuthenticationRequest getAuthenticationRequest(final HttpServletRequest request, final String password, final String login, final boolean skipPasswordCheck, final boolean skipUserStatusCheck) {
    	final AuthenticationRequest authenticatedRequest = new AuthenticationRequest();
        authenticatedRequest.setClientIP(HeaderUtils.getClientIP(request));
		authenticatedRequest.setPassword(password);
		authenticatedRequest.setPrincipal(login);
		authenticatedRequest.setSkipUserStatusCheck(skipUserStatusCheck);
		
		/*
		 * Comment by Lev Bornovalov
		 * 
		 * HACK Alert!
		 * 
		 * We are setting this flag to true only to skip checking the password.  We would have created a different flag for this, but this is a prod issue, and
		 * setting the kerbAuth flag will give us teh same affect.
		 * 
		 * DO NOT merge this into V4.
		 */
		if(skipPasswordCheck) {
			authenticatedRequest.setKerberosAuth(true);
		}
		try {
			authenticatedRequest.setNodeIP(InetAddress.getLocalHost().getHostAddress());
		} catch (UnknownHostException e) {
			
		}
		return authenticatedRequest;
    }
    
    private LoginActionToken getLoginActionToken(final AuthenticationRequest authenticatedRequest) {
    	final LoginActionToken token = new LoginActionToken();
    	final AuthenticationResponse authenticationResponse = authServiceClient.login(authenticatedRequest);
		if(authenticationResponse == null || authenticationResponse.getStatus() == null) {
			token.setHttpErrorCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} else {
			int errCode = authenticationResponse.getAuthErrorCode();
			final Subject subject = authenticationResponse.getSubject();
			final boolean isPasswordExpirationWarning = (subject != null && subject.getResultCode() == AuthenticationConstants.RESULT_SUCCESS_PASSWORD_EXP);
			
			Integer numOfDaysUntilPasswordExpiration = null;
			boolean isSuccessfulLogin = (isPasswordExpirationWarning)|| (ResponseStatus.SUCCESS == authenticationResponse.getStatus());
			if (isSuccessfulLogin) {
				token.setUserId(subject.getUserId());
				
				/* redirect to change password screen in case it's time to reset password, but the login is still valid */
				numOfDaysUntilPasswordExpiration = authenticationResponse.getSubject().getDaysToPwdExp();
				if(numOfDaysUntilPasswordExpiration != null && numOfDaysUntilPasswordExpiration.intValue() <= minNumOfDaysToWarnUserBeforePasswordExpiration) {
					errCode = AuthenticationConstants.RESULT_SUCCESS_PASSWORD_EXP;
					token.setSuccessWithWarning(true);
				} else {
					token.setSuccess(true);
					token.setDoRedirect(true);
				}
				token.setNumOfDaysUntilPasswordExpiration(numOfDaysUntilPasswordExpiration);
			}
			
			token.setAuthResponse(authenticationResponse);
			token.setErrCode(errCode);
		}
		return token;
    }

	public boolean doLogout(final HttpServletRequest request, final HttpServletResponse response, final boolean doSAMLLogout) throws IOException, URISyntaxException {
		final String userId = cookieProvider.getUserId(request);
		if(doSAMLLogout) {
			final Set<String> identityProvidersForCurrentSession = (Set<String>)request.getSession(true).getAttribute(SAMLConstants.SAML_IDENETITY_PROVIDERS_FOR_SESSION);
			if(CollectionUtils.isNotEmpty(identityProvidersForCurrentSession)) {
				response.sendRedirect(new StringBuilder(URIUtils.getBaseURI(request)).append(globalLogoutURL).toString());
				return false;
			}

			final Set<String> serviceProvidersForCurrentSession = (Set<String>)request.getSession(true).getAttribute(SAMLConstants.SAML_SERVICE_PROVIDERS_FOR_SESSION);
			if(CollectionUtils.isNotEmpty(serviceProvidersForCurrentSession)) {
				/* assume that there will always be one Service Provider for any single login session, so just redirect
				 * to the first SP that's identified for the session */
				final StringBuilder url = new StringBuilder(URIUtils.getBaseURI(request)).append(samlServiceProviderLogoutURL).append("/").append(serviceProvidersForCurrentSession.iterator().next());
				response.sendRedirect(url.toString());
				return false;
			}
		}

		if(userId != null) {
			final LogoutRequest logoutRequest = new LogoutRequest();
			logoutRequest.setUserId(userId);
			logoutRequest.setPatternId(ContentProviderFilter.getURIPatternForRequest(request));
			authServiceClient.globalLogoutRequest(logoutRequest);
		}

		cookieProvider.invalidate(request, response);

		forceSecurityContextReloadOnNextRequest(request);
		return true;
	}

	public void forceSecurityContextReloadOnNextRequest(final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);
		if(session != null) {
			session.invalidate();
		}
		SecurityContextHolder.clearContext();
	}
/*
    public void doLogout(final HttpServletRequest request, final HttpServletResponse response, final boolean doSAMLLogout) throws IOException {
        final String userId = cookieProvider.getUserIdFromCookie(request);
        if (StringUtils.isNotEmpty(userId)) {
            final AuthStateSearchBean searchBean = new AuthStateSearchBean();
            final AuthStateId id = new AuthStateId();
            id.setUserId(userId);
            id.setTokenType("SAML_SP");
            searchBean.setKey(id);
            searchBean.setOnlyActive(true);
            final List<AuthStateEntity> authStateList = authServiceClient.findBeans(searchBean, 0, Integer.MAX_VALUE);
            if (doSAMLLogout && authStateList != null && CollectionUtils.size(authStateList) == 1) {
                final AuthStateEntity serviceProviderState = authStateList.get(0);
                response.sendRedirect(new StringBuilder("/idp/sp/logout/").append(serviceProviderState.getToken()).toString());
                return;
            } else {
                try {
                    authServiceClient.globalLogout(userId);
                } catch (Throwable e) {
                    //ignore for now
                }

            }
        }
        if (unsetCookieOnLogout) {
            cookieProvider.invalidate(request, response);
        }

        final HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        SecurityContextHolder.clearContext();
    }
    */
}
