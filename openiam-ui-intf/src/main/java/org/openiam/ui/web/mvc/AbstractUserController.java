package org.openiam.ui.web.mvc;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.base.AttributeOperationEnum;
import org.openiam.base.ExtendController;
import org.openiam.base.ws.Response;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.idm.searchbeans.*;
import org.openiam.idm.srvc.audit.constant.AuditAction;
import org.openiam.idm.srvc.audit.dto.IdmAuditLog;
import org.openiam.idm.srvc.auth.dto.Login;
import org.openiam.idm.srvc.auth.dto.ProvLoginStatusEnum;
import org.openiam.idm.srvc.auth.ws.LoginListResponse;
import org.openiam.idm.srvc.continfo.dto.Address;
import org.openiam.idm.srvc.continfo.dto.EmailAddress;
import org.openiam.idm.srvc.continfo.dto.Phone;

import org.openiam.idm.srvc.org.dto.Organization;
import org.openiam.idm.srvc.org.dto.OrganizationUserDTO;
import org.openiam.idm.srvc.res.dto.Resource;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.idm.srvc.user.dto.UserAttribute;
import org.openiam.idm.srvc.user.dto.UserStatusEnum;
import org.openiam.idm.srvc.user.ws.UserResponse;
import org.openiam.provision.dto.ProvisionActionEnum;
import org.openiam.provision.dto.ProvisionActionEvent;
import org.openiam.provision.dto.ProvisionActionTypeEnum;
import org.openiam.provision.dto.ProvisionUser;
import org.openiam.provision.resp.ProvisionUserResponse;
import org.openiam.provision.service.ActionEventBuilder;
import org.openiam.provision.service.ProvisionServiceEventProcessor;
import org.openiam.ui.exception.ErrorMessageException;
import org.openiam.ui.rest.api.model.*;
import org.openiam.ui.util.WSUtils;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

public abstract class AbstractUserController extends AbstractPasswordController {

    protected static final String extendController = "/webconsole/EditUserController.groovy";

    protected enum UserOperation {
        DELETE, REMOVE, DISABLE, ENABLE;
    }

    protected BasicAjaxResponse handleUserOperation(HttpServletRequest request, String userId, UserOperation operation) {
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        ErrorToken errorToken = null;
        SuccessToken successToken = null;
        ActionEventBuilder eventBuilder = new ActionEventBuilder(userId, getRequesterId(request), null);
        switch (operation) {
            case DELETE:
                eventBuilder.setActionType(ProvisionActionEnum.DEACTIVATE);
                successToken = new SuccessToken(SuccessMessage.USER_DELETED);
                break;
            case REMOVE:
                eventBuilder.setActionType(ProvisionActionEnum.DELETE);
                successToken = new SuccessToken(SuccessMessage.USER_REMOVED);
                break;
            case ENABLE:
                eventBuilder.setActionType(ProvisionActionEnum.ENABLE);
                successToken = new SuccessToken(SuccessMessage.USER_ENABLED);
                break;
            case DISABLE:
                eventBuilder.setActionType(ProvisionActionEnum.DISABLE);
                successToken = new SuccessToken(SuccessMessage.USER_DISABLED);
                break;
        }
        ProvisionActionEvent actionEvent = eventBuilder.build();
        Response response = provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.PRE);
        if (ProvisionServiceEventProcessor.BREAK.equalsIgnoreCase((String) response.getResponseValue())) {
            return genAbortAjaxResponse(response, localeResolver.resolveLocale(request));
        }

        try {
            Errors error = performUserOperation(request, userId, operation);
            if (error != null) {
                throw new ErrorMessageException(error);
            }
        } catch (ErrorMessageException e) {
            errorToken = new ErrorToken(e.getError());
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            errorToken = new ErrorToken(Errors.INTERNAL_ERROR);
            log.error(e.getMessage(), e);

        } finally {
            if (errorToken == null) {
                provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.POST);
                ajaxResponse.setStatus(200);
                ajaxResponse.setSuccessToken(successToken);
                ajaxResponse.setSuccessMessage(messageSource.getMessage(
                        successToken.getMessage().getMessageName(),
                        null,
                        localeResolver.resolveLocale(request)));
                if (operation == UserOperation.DELETE || operation == UserOperation.REMOVE) {
                    ajaxResponse.setRedirectURL("users.html");
                }
            } else {
                ajaxResponse.setStatus(500);
                errorToken.setMessage(messageSource.getMessage(
                        errorToken.getError().getMessageName(),
                        errorToken.getParams(),
                        localeResolver.resolveLocale(request)));
                ajaxResponse.addError(errorToken);
            }
        }
        return ajaxResponse;
    }

    private Errors performUserOperation(HttpServletRequest request, String userId, UserOperation operation) throws Exception {
        Errors retVal = null;
        Response wsResponse = null;
        String requesterId = getRequesterId(request);
        final User user = userDataWebService.getUserWithDependent(userId, requesterId, true);
        if (user != null) {
            final ProvisionUser pUser = new ProvisionUser(user);
            IdmAuditLog idmAuditLog = new IdmAuditLog();
            idmAuditLog.setRequestorUserId(requesterId);
            idmAuditLog.setTargetUser(pUser.getId(), pUser.getLogin());

            if (provisionServiceFlag) {
                pUser.setRequestorUserId(requesterId);
                setAuditInfo(pUser, request);

                Map<String, Object> controllerObj = new HashMap<String, Object>();
                controllerObj.put("user", pUser);

                ExtendController extCmd = (ExtendController) scriptRunner.instantiateClass(null, extendController);

                if (extCmd.pre(operation.name(), controllerObj, null) == ExtendController.SUCCESS_CONTINUE) {
                    switch (operation) {
                        case DELETE:
                            wsResponse = provisionService.deleteByUserId(userId, UserStatusEnum.DELETED, requesterId);
                            break;
                        case REMOVE:
                            wsResponse = provisionService.deleteByUserId(userId, UserStatusEnum.REMOVE, requesterId);
                            break;
                        case ENABLE:
                            WSUtils.setWSClientTimeout(provisionService, 360000L);
                            wsResponse = provisionService.disableUser(userId, false, requesterId);
                            idmAuditLog.setAction(AuditAction.USER_ENABLE.value());
                            idmAuditLog.setAuditDescription("User enable");
                            auditLogService.addLog(idmAuditLog);
                            break;
                        case DISABLE:
                            WSUtils.setWSClientTimeout(provisionService, 360000L);
                            wsResponse = provisionService.disableUser(userId, true, requesterId);
                            idmAuditLog.setAction(AuditAction.USER_DISABLE.value());
                            idmAuditLog.setAuditDescription("User disable");
                            auditLogService.addLog(idmAuditLog);
                            break;
                    }
                }
            } else {

                switch (operation) {
                    case DELETE:
                        wsResponse = userDataWebService.deleteUser(userId);
                        break;
                    case REMOVE:
                        wsResponse = userDataWebService.removeUser(userId);
                        break;
                    case ENABLE:
                        wsResponse = userDataWebService.setSecondaryStatus(userId, null);
                        idmAuditLog.setAction(AuditAction.USER_ENABLE.value());
                        idmAuditLog.setAuditDescription("User enable");
                        auditLogService.addLog(idmAuditLog);
                        break;
                    case DISABLE:
                        wsResponse = userDataWebService.setSecondaryStatus(userId, UserStatusEnum.DISABLED);
                        idmAuditLog.setAction(AuditAction.USER_DISABLE.value());
                        idmAuditLog.setAuditDescription("User disable");
                        auditLogService.addLog(idmAuditLog);
                        break;
                }
            }
        } else {
            wsResponse = new Response();
            wsResponse.setStatus(ResponseStatus.FAILURE);
            wsResponse.setErrorText("User don't exist");
        }
        if (!ResponseStatus.SUCCESS.equals(wsResponse.getStatus())) {
            switch (operation) {
                case DELETE:
                    retVal = Errors.CANNOT_DELETE_USER;
                    break;
                case REMOVE:
                    retVal = Errors.CANNOT_REMOVE_USER;
                    break;
                case ENABLE:
                    retVal = Errors.CANNOT_ENABLE_USER;
                    break;
                case DISABLE:
                    retVal = Errors.CANNOT_DISABLE_USER;
                    break;
            }

        }
        return retVal;
    }

    protected Response saveUserInfo(HttpServletRequest request, EditUserModel userModel) throws Exception {
        String requesterId = getRequesterId(request);
        Response wsResult = new Response();
        boolean isNew = StringUtils.isBlank(userModel.getId());
        User user = mapper.mapToObject(userModel, User.class);
        //md must set by mapper
        setContactInfo(user, isNew);
        setOrganizationIds(user, userModel, requesterId, isNew);
        setUserAttribute(user, userModel);
        setUserRes(user, userModel);

        if (isNew) {
            user.setId(null);
            user.setCreateDate(new Date());
            user.setCreatedBy(requesterId);
            user.setStatus(UserStatusEnum.PENDING_INITIAL_LOGIN);

        } else {

        }

        wsResult.setStatus(ResponseStatus.FAILURE);
        User returnedUser;
        if (provisionServiceFlag) {
            ExtendController extCmd = (ExtendController) scriptRunner.instantiateClass(null, extendController);
            Map<String, Object> controllerObj = new HashMap<String, Object>();

            ProvisionUser pUser = new ProvisionUser(user);
            pUser.setRequestorUserId(getRequesterId(request));

            setSupervisors(pUser, userModel, requesterId, isNew);
            updateProvisionUserFields(pUser, userModel);

            setAuditInfo(pUser, request);
            controllerObj.put("user", pUser);
            if (isNew) {
                pUser.setProvisionOnStartDate(userModel.getProvisionOnStartDate());
                pUser.setEmailCredentialsToNewUsers(userModel.getNotifyUserViaEmail());
                pUser.setEmailCredentialsToSupervisor(userModel.getNotifySupervisorViaEmail());
                // provision user
                if (extCmd != null) {
                    extCmd.pre("ADD", controllerObj, null);
                }
                wsResult = provisionService.addUser(pUser);
                returnedUser = ((ProvisionUserResponse) wsResult).getUser();
            } else {
                if (extCmd != null) {
                    extCmd.pre("MODIFY", controllerObj, null);
                }

                wsResult = provisionService.modifyUser(pUser);
                returnedUser = ((ProvisionUserResponse) wsResult).getUser();

                //returnedUser = pUser;
            }
        } else {
            String supervisorId = null;
            if (CollectionUtils.isNotEmpty(userModel.getSupervisors())) {
                supervisorId = userModel.getSupervisors().get(0).getId();
            }
            wsResult = userDataWebService.saveUserInfo(user, supervisorId);
            returnedUser = ((UserResponse) wsResult).getUser();
        }
        if (returnedUser != null && wsResult.getStatus() == ResponseStatus.SUCCESS) {
            wsResult.setResponseValue(returnedUser.getId());
        }
        return wsResult;
    }

    private void setOrganizationIds(User user, EditUserModel userModel, String requesterId, boolean isNewUser) {
        List<Organization> orgs = new ArrayList<>();
        Map<String, String> ids = new HashMap<>();
        User fullUser = new User();
        if (!isNewUser) {
            fullUser = userDataWebService.getUserWithDependent(user.getId(), null, true);
        }

        if (CollectionUtils.isNotEmpty(userModel.getOrganizationIds())) {
            OrganizationSearchBean searchBean = new OrganizationSearchBean();

            if (CollectionUtils.isNotEmpty(userModel.getOrganizationIds())) {

                for (KeyNameBean bean : userModel.getOrganizationIds()) {
                    ids.put(bean.getId(), bean.getName());
                }
                searchBean.setKeys(ids.keySet());
                orgs = organizationDataService.findBeansLocalized(searchBean, requesterId, -1, -1, getCurrentLanguage());
            }
        }
        if (CollectionUtils.isNotEmpty(orgs)) {
            for (Organization o : orgs) {
                OrganizationUserDTO organizationUserDTO = fullUser.containOrganization(o);
                if (isNewUser || organizationUserDTO == null) {
                    user.getOrganizationUserDTOs().add(new OrganizationUserDTO(user.getId(), o.getId(), "DEFAULT_AFFILIATION", AttributeOperationEnum.ADD));
                }
            }
        }
        if (!isNewUser) {
            for (OrganizationUserDTO o : fullUser.getOrganizationUserDTOs()) {
                boolean isFound = false;
                for (Organization internalOrg : orgs) {
                    if (internalOrg.getId().equals(o.getOrganization().getId())) {
                        isFound = true;
                    }
                }
                if (!isFound) {
                    o.setOperation(AttributeOperationEnum.DELETE);
                    user.getOrganizationUserDTOs().add(o);
                }
            }
        }
    }

    private void setUserAttribute(User user, EditUserModel userModel) {
        if (!userModel.getUserAttributes().isEmpty()) {
            for (UserAttributeBean userAttr : userModel.getUserAttributes().values()) {

                UserAttribute attr = new UserAttribute();
                userAttr.updateUserAttribute(attr);
                user.getUserAttributes().put(attr.getName(), attr);

            }
        }
    }

    private void setUserRes(User user, EditUserModel userModel) {
        if (!userModel.getResources().isEmpty()) {
            for (ResourceBean userRes : userModel.getResources()) {
                Resource res = new Resource();
                userRes.updateResource(res);
                user.getResources().add(res);
            }
        }
    }


    private void setContactInfo(User user, boolean isNew) {
        if (isNew) {
            if (user.getEmailAddresses() != null) {
                for (EmailAddress email : user.getEmailAddresses()) {
                    email.setOperation(AttributeOperationEnum.ADD);
                }
            }
            if (user.getAddresses() != null) {
                for (Address address : user.getAddresses()) {
                    address.setOperation(AttributeOperationEnum.ADD);
                }
            }

            if (user.getPhones() != null) {
                for (Phone phone : user.getPhones()) {
                    phone.setOperation(AttributeOperationEnum.ADD);
                }
            }
        }
    }


    private void setSupervisors(ProvisionUser pUser, EditUserModel userModel, String requesterId, boolean isNew) {
        if (CollectionUtils.isNotEmpty(userModel.getSupervisors())) {
            for (KeyNameOperationBean bean : userModel.getSupervisors()) {
                if (!AttributeOperationEnum.NO_CHANGE.equals(bean.getOperation())) {
                    User supervisor = new User();
                    supervisor.setId(bean.getId());
                    supervisor.setOperation(bean.getOperation());
                    pUser.getSuperiors().add(supervisor);
                }
            }
        }
    }

    private void updateProvisionUserFields(ProvisionUser pUser, EditUserModel userModel) {
        if (CollectionUtils.isNotEmpty(userModel.getNotProvisioninResourcesIds())) {
            if (CollectionUtils.isEmpty(pUser.getNotProvisioninResourcesIds())) {
                pUser.setNotProvisioninResourcesIds(new HashSet<String>());
            }
            pUser.getNotProvisioninResourcesIds().addAll(userModel.getNotProvisioninResourcesIds());
        }
        pUser.setEmailCredentialsToNewUsers(userModel.isEmailCredentialsToNewUsers());
        pUser.setAddInitialPasswordToHistory(userModel.isAddInitialPasswordToHistory());
        pUser.setSkipPreprocessor(userModel.isSkipPreprocessor());
        pUser.setSkipPostProcessor(userModel.isSkipPostProcessor());
        pUser.setSrcSystemId(userModel.getSrcSystemId());
        pUser.setNotifyTargetSystems(userModel.isNotifyTargetSystems());
    }

    protected void setAuditInfo(ProvisionUser pUser, HttpServletRequest request) {
        pUser.setRequestClientIP(request.getRemoteHost());
        pUser.setRequestorLogin(cookieProvider.getPrincipal(request));
    }

    protected BasicAjaxResponse genAbortAjaxResponse(Response response, Locale locale) {
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        if (response.isSuccess()) {
            SuccessToken successToken = new SuccessToken(SuccessMessage.OPERATION_ABORTED);
            ajaxResponse.setStatus(200);
            ajaxResponse.setSuccessToken(successToken);
            ajaxResponse.setSuccessMessage(messageSource.getMessage(
                    successToken.getMessage().getMessageName(),
                    null, locale));
        } else {
            ajaxResponse.setStatus(500);
            ErrorToken errorToken = new ErrorToken(Errors.OPERATION_ABORTED);
            errorToken.setMessage(messageSource.getMessage(
                    errorToken.getError().getMessageName(),
                    errorToken.getParams(), locale));
            ajaxResponse.addError(errorToken);
        }
        return ajaxResponse;
    }

    protected BasicAjaxResponse setUserStatusInfoBuilder(HttpServletRequest request, BasicAjaxResponse ajaxResponse, String userId, SuccessToken successToken) {
        String saveUserLabel = messageSource.getMessage(
                successToken.getMessage().getMessageName(),
                null,
                localeResolver.resolveLocale(request));
        String provMessage = messageSource.getMessage(
                SuccessMessage.USER_INFOPROV_SAVED.getMessageName(),
                null,
                localeResolver.resolveLocale(request));

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(saveUserLabel);
        stringBuilder.append("<br/>");
        stringBuilder.append(provMessage);
        stringBuilder.append(":");
        LoginListResponse loginListResponse = loginServiceClient.getLoginByUser(userId);
        int successCounter = 0;
        for (Login login : loginListResponse.getPrincipalList()) {
            String mngSysName = managedSysServiceClient.getManagedSys(login.getManagedSysId()).getName();

            stringBuilder.append("<br/>").append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").append(mngSysName).append(": ").append(login.getProvStatus() != null ? login.getProvStatus().getValue() : "NONE");
            if (login.getProvStatus() != ProvLoginStatusEnum.PENDING_CREATE
                    && login.getProvStatus() != ProvLoginStatusEnum.PENDING_UPDATE
                    && login.getProvStatus() != ProvLoginStatusEnum.PENDING_DELETE
                    && login.getProvStatus() != ProvLoginStatusEnum.PENDING_DISABLE
                    && login.getProvStatus() != ProvLoginStatusEnum.PENDING_ENABLE
                    && login.getProvStatus() != null) {
                successCounter++;
            }
        }
        ajaxResponse.setSuccessMessage(stringBuilder.toString());
        // We have to define the stop behaviour
        //  ajaxResponse.addContextValues("checkStatusInProgress", successCounter==loginListResponse.getPrincipalList().size());
        ajaxResponse.addContextValues("checkStatusInProgress", true);
        ajaxResponse.addContextValues("userId", userId);
        return ajaxResponse;
    }
}
