package org.openiam.ui.web.util;

public class SAMLConstants {

	public static final String SAML_IDENETITY_PROVIDERS_FOR_SESSION = "SAML_IDENETITY_PROVIDERS_FOR_SESSION";
	
	public static final String SAML_SERVICE_PROVIDERS_FOR_SESSION = "SAML_SERVICE_PROVIDERS_FOR_SESSION";

	public static final String SAML_SESSION_INDEX_FOR_PROVIDERS = "SAML_SESSION_INDEX_FOR_PROVIDERS";
}
