package org.openiam.ui.web.mvc;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.base.ws.Response;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.idm.searchbeans.IdentityAnswerSearchBean;
import org.openiam.idm.searchbeans.IdentityQuestionSearchBean;
import org.openiam.idm.srvc.pswd.dto.IdentityQuestion;
import org.openiam.idm.srvc.pswd.dto.UserIdentityAnswer;
import org.openiam.idm.srvc.pswd.service.PasswordGenerator;
import org.openiam.ui.security.OpeniamWebAuthenticationDetails;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.model.ChallengeResponseModel;
import org.openiam.ui.web.model.ChallengeResponseRequest;
import org.openiam.ui.web.model.IdentityAnswerBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public abstract class AbstractChallengeResponseController extends AbstractController {

    @Value("${org.openiam.selfservice.challenge.response.group}")
    private String challengeResponseGroup;

    @Value("${org.openiam.ui.challenge.answers.secured}")
    private Boolean secureAnswers;

    @RequestMapping(value = "/challengeResponse", method = RequestMethod.GET)
    public String challengeResponse(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    @RequestParam(value = "postbackUrl", required = false) String postbackUrl,
                                    @RequestParam(value = "auth", required = false) boolean auth) throws Exception {

        if (StringUtils.isNotBlank(postbackUrl)) {
            request.setAttribute("postbackUrl", postbackUrl);
        }
        request.setAttribute("readOnly", false);
        return challengeResponse(request, cookieProvider.getUserId(request), auth);
    }

    protected String challengeResponse(final HttpServletRequest request, final String userId, final boolean auth) throws Exception {

        final IdentityQuestionSearchBean questionSearchBean = new IdentityQuestionSearchBean();
        questionSearchBean.setDeepCopy(false);
        questionSearchBean.setGroupId(challengeResponseGroup);
        questionSearchBean.setActive(true);
        //all questions
        final List<IdentityQuestion> questionList = challengeResponseService.findQuestionBeans(questionSearchBean, 0, Integer.MAX_VALUE, getCurrentLanguage());
        // questions as a MAP
        Map<String, IdentityQuestion> questionMap = null;
        if (CollectionUtils.isNotEmpty(questionList)) {
            questionMap = new HashMap<String, IdentityQuestion>();
            for (IdentityQuestion identityQuestion : questionList) {
                questionMap.put(identityQuestion.getId(), identityQuestion);
            }
        }

        final IdentityAnswerSearchBean answerSearchBean = new IdentityAnswerSearchBean();
        answerSearchBean.setDeepCopy(true);
        answerSearchBean.setUserId(userId);
        //all answers
        final List<UserIdentityAnswer> answerList = challengeResponseService.findAnswerBeans(answerSearchBean, userId, 0, Integer.MAX_VALUE);
        //get number of Required Questions Enterprise
        Integer numOfRequiredQuestionsEnterprise = challengeResponseService.getNumOfRequiredQuestions(userId, true);
        numOfRequiredQuestionsEnterprise = (numOfRequiredQuestionsEnterprise == null) ? 0 : numOfRequiredQuestionsEnterprise;
        //get number of Required Questions Custom
        Integer numOfRequiredQuestionsCustom = challengeResponseService.getNumOfRequiredQuestions(userId, false);
        numOfRequiredQuestionsCustom = (numOfRequiredQuestionsCustom == null) ? 0 : numOfRequiredQuestionsCustom;
        final List<ChallengeResponseModel> modelListEnterprise = new ArrayList<ChallengeResponseModel>(numOfRequiredQuestionsEnterprise.intValue());
        final List<ChallengeResponseModel> modelListCustom = new ArrayList<ChallengeResponseModel>(numOfRequiredQuestionsCustom.intValue());

        if (CollectionUtils.isNotEmpty(answerList)) {
            int counterEnterprise = 0;
            int counterCustom = 0;
            ChallengeResponseModel model = null;
            for (UserIdentityAnswer userIdentityAnswer : answerList) {
                if (userIdentityAnswer.getQuestionId() != null) {
                    model = new ChallengeResponseModel(questionList, userIdentityAnswer);
                    if (counterEnterprise < numOfRequiredQuestionsEnterprise) {
                        if (questionMap.get(userIdentityAnswer.getQuestionId()) != null) {
                            counterEnterprise++;
                        }
                    } else {
                        model.setMarkedAsDeleted(true);
                    }
                    modelListEnterprise.add(model);
                } else {
                    model = new ChallengeResponseModel(null, userIdentityAnswer);
                    if (counterCustom < numOfRequiredQuestionsCustom) {
                        counterCustom++;
                    } else {
                        model.setMarkedAsDeleted(true);
                    }
                    modelListCustom.add(model);
                }
            }
            //add more if required
            if (counterCustom < numOfRequiredQuestionsCustom) {
                this.addEmptyChallengeResponseQuestions(numOfRequiredQuestionsCustom - counterCustom, modelListCustom,
                        null, userId);
            }
            if (counterEnterprise < numOfRequiredQuestionsEnterprise) {
                this.addEmptyChallengeResponseQuestions(numOfRequiredQuestionsEnterprise - counterEnterprise,
                        modelListEnterprise, questionList, userId);
            }
        } else {
            this.addEmptyChallengeResponseQuestions(numOfRequiredQuestionsEnterprise, modelListEnterprise, questionList, userId);
            this.addEmptyChallengeResponseQuestions(numOfRequiredQuestionsCustom, modelListCustom, null, userId);
        }
        modelListEnterprise.addAll(modelListCustom);
        request.setAttribute("modelList", modelListEnterprise);
        request.setAttribute("secureAnswers", secureAnswers);
        request.setAttribute("unchangedValue", PasswordGenerator.generatePassword(16));
        request.setAttribute("auth", auth);
        return "jar:users/challengeResponse";
    }

    private void addEmptyChallengeResponseQuestions(int number, List<ChallengeResponseModel> targetList,
                                                    List<IdentityQuestion> questions, String userId) {
        if (targetList == null) {
            targetList = new ArrayList<ChallengeResponseModel>();
        }
        for (int i = 0; i < number; i++) {
            UserIdentityAnswer userIdentityAnswer = new UserIdentityAnswer();
            userIdentityAnswer.setUserId(userId);
            targetList.add(new ChallengeResponseModel(questions, userIdentityAnswer));
        }

    }

    @RequestMapping(value = "/challengeResponse", method = RequestMethod.POST)
    public String challengeResponsePost(final HttpServletRequest request, final @RequestBody ChallengeResponseRequest challengeRequest) {
        final String userId = cookieProvider.getUserId(request);

        Errors error = null;
        final List<UserIdentityAnswer> answerList = new ArrayList<UserIdentityAnswer>();
        if (CollectionUtils.isNotEmpty(challengeRequest.getAnswerList())) {
            for (final IdentityAnswerBean answer : challengeRequest.getAnswerList()) {
                if (secureAnswers) {
                    if (!StringUtils.equals(answer.getQuestionAnswer(), answer.getConfirmAnswer())) {
                        error = Errors.ANSWERS_NOT_EQUAL;
                        break;
                    }
                    if (StringUtils.equals(answer.getQuestionAnswer(), challengeRequest.getUnchangedValue())) {
                        continue;
                    }
                }
                UserIdentityAnswer answerDTO = new UserIdentityAnswer();
                answerDTO.setUserId(userId);
                answerDTO.setId(StringUtils.trimToNull(answer.getId()));
                answerDTO.setQuestionId(answer.getQuestionId());
                answerDTO.setQuestionAnswer(answer.getQuestionAnswer());
                answerDTO.setQuestionText(answer.getQuestionText());
                answerList.add(answerDTO);
            }
        }

        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        if (CollectionUtils.isNotEmpty(challengeRequest.getDeleteList())) {
            for (final String answerId : challengeRequest.getDeleteList()) {
                challengeResponseService.deleteAnswer(answerId);
            }
        }

        if (error == null && CollectionUtils.isNotEmpty(answerList)) {
            final Response wsResponse = challengeResponseService.saveAnswers(answerList);
            if (wsResponse.getStatus() != ResponseStatus.SUCCESS) {
                error = Errors.INTERNAL_ERROR;
                if (wsResponse.getErrorCode() != null) {
                    switch (wsResponse.getErrorCode()) {
                        case NO_ANSWER_TO_QUESTION:
                            error = Errors.NO_QUESTION_TO_ANSWER;
                            break;
                        case IDENTICAL_QUESTIONS:
                            error = Errors.IDENTICAL_QUESTION;
                            break;

                        case QUEST_NOT_SELECTED:
                            error = Errors.QUESTION_NOT_TAKEN;
                            break;

                        case ANSWER_NOT_TAKEN:
                            error = Errors.ANSWER_NOT_TAKEN;
                            break;

                        case ANSWER_IS_TOO_LONG:
                            error = Errors.ANSWER_IS_TOO_LONG;
                            break;


                        default:
                            error = Errors.CHALLENGE_RESPONSES_NOT_SAVED;
                    }
                }
            }
        }

        if (error == null) {
            final SecurityContext ctx = SecurityContextHolder.getContext();
            final Authentication authentication = (ctx != null) ? ctx.getAuthentication() : null;
            if (authentication != null && authentication.getDetails() != null) {
                ((OpeniamWebAuthenticationDetails) authentication.getDetails()).setIdentityQuestionsAnswered(true);
            }
            if (StringUtils.isNotBlank(challengeRequest.getPostbackUrl())) {
                ajaxResponse.setRedirectURL(challengeRequest.getPostbackUrl());
            }
            ajaxResponse.setStatus(200);
            ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.CHALLENGE_RESPONSE_SAVED));
        } else {
            ajaxResponse.addError(new ErrorToken(error));
            ajaxResponse.setStatus(500);
        }
        request.setAttribute("response", ajaxResponse);
        return "common/basic.ajax.response";
    }

}
