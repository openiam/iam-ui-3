package org.openiam.ui.web.mvc.provider;

import org.apache.log4j.Logger;
import org.openiam.idm.srvc.meta.dto.PageTempate;
import org.openiam.idm.srvc.meta.dto.TemplateRequest;
import org.openiam.idm.srvc.meta.ws.MetadataElementTemplateWebService;
import org.openiam.ui.util.URIUtils;
import org.openiam.ui.web.filter.ContentProviderFilter;
import org.openiam.ui.web.filter.OpeniamFilter;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Component
public class TemplateProvider {
	
	@Resource(name="metadataElementTemplateServiceClient")
	private MetadataElementTemplateWebService templateService;
	
	private static Logger LOG = Logger.getLogger(TemplateProvider.class);

	public PageTempate getPageTemplate(final String targetObjectId, final String requesterId,  final HttpServletRequest request) {
		final String patternId = ContentProviderFilter.getURIPatternForRequest(request);
		
		final TemplateRequest templateRequest = new TemplateRequest();
		templateRequest.setLanguageId(OpeniamFilter.getCurrentLangauge(request).getId());
		templateRequest.setRequestURI(URIUtils.getRequestURL(request));
		templateRequest.setPatternId(patternId);
		templateRequest.setTargetObjectId(targetObjectId);
		templateRequest.setRequesterId(requesterId);
		final PageTempate template = templateService.getTemplate(templateRequest);
		return template;
	}
}
