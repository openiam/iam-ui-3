package org.openiam.ui.web.mvc;

import org.openiam.base.ws.Response;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.bpm.activiti.ActivitiService;
import org.openiam.bpm.request.ActivitiClaimRequest;
import org.openiam.bpm.request.ActivitiRequestDecision;
import org.openiam.ui.util.HeaderUtils;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by: Alexander Duckardt
 * Date: 8/27/14.
 */
public class AbstractActivitiController extends AbstractController {
    @Resource(name = "activitiClient")
    protected ActivitiService activitiService;

    protected final static String AFFILIATION_TYPE = "AFFILIATION_TYPE";

    protected BasicAjaxResponse doDecision(final HttpServletRequest request,
                                           final HttpServletResponse response,
                                           final ActivitiRequestDecision decision) {

        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();

        decision.setRequestorUserId(cookieProvider.getUserId(request));
        final Response wsResponse = activitiService.makeDecision(decision);
        Errors error = null;
        try {
            if (wsResponse == null) {
                error = Errors.INTERNAL_ERROR;
            }
            if (wsResponse.getStatus() == ResponseStatus.FAILURE) {
                error = Errors.INTERNAL_ERROR;
                if (wsResponse.getErrorCode() != null) {
                    switch (wsResponse.getErrorCode()) {
                        default:
                            break;
                    }
                }
            }
        } finally {
            if (error != null) {
                ajaxResponse.addError(new ErrorToken(error));
                ajaxResponse.setStatus(500);
            } else {
                ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.TASK_COMPLETED));
                ajaxResponse.setRedirectURL("myTasks.html");
                ajaxResponse.setStatus(200);
            }
            ajaxResponse.process(localeResolver, messageSource, request);
        }
        return ajaxResponse;
    }

    protected BasicAjaxResponse doDecisionList(final HttpServletRequest request,
                                           final HttpServletResponse response,
                                           final List<String> decision,
                                               final boolean isAccept) {

        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        Errors error = null;
        try {
            for(String id : decision){
                ActivitiRequestDecision decis = new ActivitiRequestDecision();
                decis.setTaskId(id);
                decis.setAccepted(isAccept);

                decis.setRequestorUserId(cookieProvider.getUserId(request));
                final Response wsResponse = activitiService.makeDecision(decis);

                if (wsResponse == null) {
                    error = Errors.INTERNAL_ERROR;
                }
                if (wsResponse.getStatus() == ResponseStatus.FAILURE) {
                    error = Errors.INTERNAL_ERROR;
                    if (wsResponse.getErrorCode() != null) {
                        switch (wsResponse.getErrorCode()) {
                            default:
                                break;
                        }
                    }
                }
            }
        } finally {
            if (error != null) {
                ajaxResponse.addError(new ErrorToken(error));
                ajaxResponse.setStatus(500);
            } else {
                ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.TASK_COMPLETED));
                ajaxResponse.setRedirectURL("myTasks.html");
                ajaxResponse.setStatus(200);
            }
            ajaxResponse.process(localeResolver, messageSource, request);
        }
        return ajaxResponse;
    }

    protected BasicAjaxResponse doClaimDecisionList(final HttpServletRequest request, final HttpServletResponse response, List<String> ids){

        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        Errors error = null;
        try {
            for (String taskId : ids) {
                final ActivitiClaimRequest claimRequest = new ActivitiClaimRequest();
                claimRequest.setTaskId(taskId);
                claimRequest.setRequestorUserId(cookieProvider.getUserId(request));
                claimRequest.setRequestClientIP(HeaderUtils.getClientIP(request));
                final Response wsResponse = activitiService.claimRequest(claimRequest);

                if (wsResponse == null || wsResponse.getStatus() == ResponseStatus.FAILURE) {
                        error = Errors.COULD_NOT_CLAIM_TASK;
                }
            }
        } finally {
                if (error != null) {
                    ajaxResponse.addError(new ErrorToken(error));
                    ajaxResponse.setStatus(500);
                } else {
                    ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.TASK_CLAIMED));
                    ajaxResponse.setStatus(200);
                }
                ajaxResponse.process(localeResolver, messageSource, request);
            }
        return ajaxResponse;
    }

}
