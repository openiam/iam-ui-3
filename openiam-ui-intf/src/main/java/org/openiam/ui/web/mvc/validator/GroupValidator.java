package org.openiam.ui.web.mvc.validator;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.idm.searchbeans.OrganizationSearchBean;
import org.openiam.idm.srvc.grp.dto.GroupRequestModel;
import org.openiam.idm.srvc.grp.ws.GroupDataWebService;
import org.openiam.idm.srvc.meta.dto.PageTempate;
import org.openiam.idm.srvc.meta.dto.TemplateUIField;
import org.openiam.idm.srvc.org.dto.Organization;
import org.openiam.idm.srvc.org.dto.OrganizationType;
import org.openiam.ui.security.OpenIAMCookieProvider;
import org.openiam.ui.web.mvc.provider.TemplateProvider;
import org.openiam.ui.web.util.OpeniamCookieLocaleResolver;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.openiam.idm.srvc.grp.dto.Group;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by alexander on 05/01/16.
 */
@Component
public class GroupValidator implements Validator, ApplicationContextAware {
    private ApplicationContext ctx;

    @Autowired
    private TemplateProvider templateProvider;

    @Autowired
    private OpenIAMCookieProvider cookieProvider;

    @Autowired
    protected MessageSource messageSource;

    @Autowired
    @Qualifier("localeResolver")
    protected OpeniamCookieLocaleResolver localeResolver;

    @Resource(name="groupServiceClient")
    protected GroupDataWebService groupServiceClient;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

    }

    @Override
    public boolean supports(Class<?> clazz) {
        return GroupRequestModel.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors err) {
        final ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        final HttpServletRequest request = sra.getRequest();
        final String requestorId = cookieProvider.getUserId(request);
        final PageTempate template = templateProvider.getPageTemplate(null, requestorId, request);

        if (template == null) {
            err.rejectValue("pageTemplate", "required", org.openiam.ui.util.messages.Errors.MISCONFIGURED_PAGE.getMessageName());
        } else {
            final GroupRequestModel requestModel = (GroupRequestModel) target;
            final Group group = requestModel.getTargetObject();

            final boolean isNewGroup = StringUtils.isBlank(group.getId());
            final Group oldGroup = (!isNewGroup) ? groupServiceClient.getGroup(group.getId(), null) : null;
            if (!isNewGroup && oldGroup == null) {
                err.rejectValue("pageTemplate", "required", org.openiam.ui.util.messages.Errors.INVALID_REQUEST.getMessageName());
                return;
            }

            TemplateUIField field = template.getUIField("GROUP_NAME");
            if (field != null) {
                if (field.isRequired() && StringUtils.isBlank(group.getName())) {
                    err.rejectValue("group.firstName", "required", org.openiam.ui.util.messages.Errors.INVALID_GROUP_NAME.getMessageName());
                } else if (oldGroup != null && !field.isEditable()) {
                    group.setName(oldGroup.getName());
                }
            }
            field = template.getUIField("GROUP_DESCRIPTION");
            if (field != null) {
                if (field.isRequired() && StringUtils.isBlank(group.getDescription())) {
                    err.rejectValue("group.description", "required", org.openiam.ui.util.messages.Errors.REQUIRED_GROUP_DESCRIPTION.getMessageName());
                } else if (oldGroup != null && !field.isEditable()) {
                    group.setDescription(oldGroup.getDescription());
                }
            }
            field = template.getUIField("GROUP_MANAGED_SYS");
            if (field != null) {
                if (field.isRequired() && StringUtils.isBlank(group.getManagedSysId())) {
                    err.rejectValue("group.managedsys", "required", org.openiam.ui.util.messages.Errors.REQUIRED_MANAGED_SYS.getMessageName());
                } else if (oldGroup != null && !field.isEditable()) {
                    group.setManagedSysId(oldGroup.getManagedSysId());
                }
            }
            field = template.getUIField("GROUP_CLASSIFICATION");
            if (field != null) {
                if (field.isRequired() && StringUtils.isBlank(group.getClassificationId())) {
                    err.rejectValue("group.classification", "required", org.openiam.ui.util.messages.Errors.REQUIRED_CLASSIFICATION.getMessageName());
                } else if (oldGroup != null && !field.isEditable()) {
                    group.setClassificationId(oldGroup.getClassificationId());
                }
            }

            if("AD_GROUP".equals(group.getMdTypeId())){
                field = template.getUIField("GROUP_AD_GROUP_TYPE");
                if (field != null) {
                    if (field.isRequired() && StringUtils.isBlank(group.getAdGroupTypeId())) {
                        err.rejectValue("group.adGroupType", "required", org.openiam.ui.util.messages.Errors.REQUIRED_AD_GROUP_TYPE.getMessageName());
                    } else if (oldGroup != null && !field.isEditable()) {
                        group.setAdGroupTypeId(oldGroup.getAdGroupTypeId());
                    }
                }
                field = template.getUIField("GROUP_AD_SCOPE");
                if (field != null) {
                    if (field.isRequired() && StringUtils.isBlank(group.getAdGroupScopeId())) {
                        err.rejectValue("group.adGroupScope", "required", org.openiam.ui.util.messages.Errors.REQUIRED_AD_GROUP_SCOPE.getMessageName());
                    } else if (oldGroup != null && !field.isEditable()) {
                        group.setAdGroupScopeId(oldGroup.getAdGroupScopeId());
                    }
                }
            }


            field = template.getUIField("GROUP_RISK");
            if (field != null) {
                if (field.isRequired() && StringUtils.isBlank(group.getRiskId())) {
                    err.rejectValue("group.risk", "required", org.openiam.ui.util.messages.Errors.REQUIRED_RISK.getMessageName());
                } else if (oldGroup != null && !field.isEditable()) {
                    group.setRiskId(oldGroup.getRiskId());
                }
            }
            field = template.getUIField("GROUP_MAX_USERS");
            if (field != null) {
                if (field.isRequired() && group.getMaxUserNumber()==null) {
                    err.rejectValue("group.maxUserNumber", "required", org.openiam.ui.util.messages.Errors.REQUIRED_MAX_USER_COUNT.getMessageName());
                } else if (oldGroup != null && !field.isEditable()) {
                    group.setMaxUserNumber(oldGroup.getMaxUserNumber());
                }
            }
            field = template.getUIField("GROUP_MEMBERSHIP_DURATION");
            if (field != null) {
                if (field.isRequired() && group.getMembershipDuration()==null) {
                    err.rejectValue("group.membershipDuration", "required", org.openiam.ui.util.messages.Errors.REQUIRED_MEMBERSHIP_DURATION.getMessageName());
                } else if (oldGroup != null && !field.isEditable()) {
                    group.setMembershipDuration(oldGroup.getMembershipDuration());
                }
            }
            field = template.getUIField("GROUP_PARENT");
            if (field != null) {
                if (field.isRequired() && CollectionUtils.isEmpty(group.getParentGroups())) {
                    err.rejectValue("group.parent", "required",
                            org.openiam.ui.util.messages.Errors.REQUIRED_GROUP_PARENT.getMessageName());
                }
            }

            if(isNewGroup){
                field = template.getUIField("GROUP_OWNER");
                if (field != null) {
                    if (field.isRequired() && (group.getOwner()==null || StringUtils.isBlank(group.getOwner().getId()))) {
                        err.rejectValue("group.groupOwner", "required", org.openiam.ui.util.messages.Errors.REQUIRED_GROUP_OWNER.getMessageName());
                    }
                }
            }

            field = template.getUIField("GROUP_ORGANIZATION");
            if (field != null) {
                if (field.isRequired()  && CollectionUtils.isEmpty(group.getOrganizations())) {
                    err.rejectValue("group.organizations", "required", org.openiam.ui.util.messages.Errors.REQUIRED_GROUP_ORGANIZATION.getMessageName());
                }
            }


        }
    }
}
