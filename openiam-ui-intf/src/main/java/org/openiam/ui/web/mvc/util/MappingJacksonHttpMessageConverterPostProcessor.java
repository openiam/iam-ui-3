package org.openiam.ui.web.mvc.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

@Component
public class MappingJacksonHttpMessageConverterPostProcessor implements BeanPostProcessor {
	
	@Autowired
	@Qualifier("jacksonMapper")
	private ObjectMapper jacksonMapper;

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName)
			throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName)
			throws BeansException {
		if(bean instanceof RequestMappingHandlerAdapter) {
			final RequestMappingHandlerAdapter adapter = (RequestMappingHandlerAdapter)bean;
			if(adapter.getMessageConverters() != null) {
				for(final HttpMessageConverter<?> converter : adapter.getMessageConverters()) {
					if(converter instanceof MappingJackson2HttpMessageConverter) {
						final MappingJackson2HttpMessageConverter messageConverter = (MappingJackson2HttpMessageConverter)converter;
						messageConverter.setObjectMapper(jacksonMapper);
					}
				}
			}
		}
		return bean;
	}
}
