package org.openiam.ui.web.mvc.entitlements;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.base.ws.Response;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.idm.srvc.audit.constant.AuditAction;
import org.openiam.idm.srvc.audit.dto.IdmAuditLog;
import org.openiam.idm.srvc.auth.dto.IdentityDto;
import org.openiam.idm.srvc.auth.ws.IdentityWebService;
import org.openiam.idm.srvc.grp.dto.Group;
import org.openiam.idm.srvc.grp.dto.GroupOwner;
import org.openiam.idm.srvc.grp.dto.GroupRequestModel;
import org.openiam.idm.srvc.grp.ws.GroupDataWebService;
import org.openiam.idm.srvc.meta.domain.MetadataTypeGrouping;
import org.openiam.idm.srvc.meta.dto.MetadataType;
import org.openiam.idm.srvc.meta.dto.PageTempate;
import org.openiam.idm.srvc.meta.dto.SaveTemplateProfileResponse;
import org.openiam.idm.srvc.org.dto.Organization;
import org.openiam.idm.srvc.org.dto.OrganizationType;
import org.openiam.idm.srvc.org.service.OrganizationDataService;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.provision.dto.ProvisionGroup;
import org.openiam.provision.service.ObjectProvisionService;
import org.openiam.ui.rest.api.model.OrganizationBean;
import org.openiam.ui.rest.api.model.UserBean;
import org.openiam.ui.util.ErrorUtils;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.mvc.provider.TemplateProvider;
import org.openiam.ui.web.mvc.validator.GroupValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class AbstractGroupController extends AbstractEntityEntitlementsController<GroupRequestModel> {
	@Resource(name = "groupProvisionServiceClient")
	protected ObjectProvisionService<ProvisionGroup> groupProvisionService;

	@Resource(name = "identityServiceClient")
	protected IdentityWebService identityService;

	@Resource(name="groupServiceClient")
	protected GroupDataWebService groupServiceClient;

	@Resource(name="organizationServiceClient")
	protected OrganizationDataService organizationServiceClient;

	@Autowired
	protected TemplateProvider templateProvider;

	@Autowired
	private GroupValidator groupValidator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(groupValidator);
	}

	@RequestMapping(value="/groupEntitlements", method=RequestMethod.GET)
	public String roleEntitlements(final HttpServletRequest request,
								   final HttpServletResponse response,
								   final @RequestParam(value="id", required=true) String groupId,
								   @RequestParam(value="type", required=false) String type) throws IOException {

        String requesterId = getRequesterId(request);

		if(StringUtils.isBlank(type)) {
			type = "childgroups";
		}
		
		final Group group = groupServiceClient.getGroup(groupId, requesterId);
		if(group == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, String.format("Group '%s' does not exist", groupId));
			return null;
		}
		
		request.setAttribute("group", group);
		request.setAttribute("type", type);
		setMenuTree(request, getEditMenu());
		return "jar:groups/entitlements";
	}
	
	@RequestMapping(value="/deleteGroup", method=RequestMethod.POST)
	public String deleteGroup(final HttpServletRequest request,
							  final HttpServletResponse response,
							  final @RequestBody String groupId) throws Exception {
		final GroupRequestModel group = getEntity(groupId, request);
		final BasicAjaxResponse ajaxResponse = doDelete(request, response, group);
		request.setAttribute("response", ajaxResponse);
		return "common/basic.ajax.response";
	}
	
	@RequestMapping(value="/editGroup", method=RequestMethod.POST)
	public String saveGroup(final HttpServletRequest request,
							final HttpServletResponse response,
						    @Valid @RequestBody final GroupRequestModel group) throws Exception {
		final BasicAjaxResponse ajaxResponse = doEdit(request, response, group);
		request.setAttribute("response", ajaxResponse);
		return "common/basic.ajax.response";
	}
	
	@RequestMapping(value="/editGroup", method=RequestMethod.GET)
	public String editGroup(final HttpServletRequest request,
						    final HttpServletResponse response,
						    final @RequestParam(required=false,value="id") String groupId,
							final @RequestParam(required=false,value="groupTypeId") String groupTypeId) throws IOException {
        final String requesterId = getRequesterId(request);

        Group group = new Group();
		if(StringUtils.isNotBlank(groupId)) {
			// edit group
			group = groupServiceClient.getGroupLocalize(groupId,requesterId, getCurrentLanguage());
			if(group == null) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, String.format("Group '%s' does not exist", groupId));
				return null;
			}
			List<GroupOwner> groupOwners = groupServiceClient.getOwnersBeansForGroup(groupId);
			if(CollectionUtils.isNotEmpty(groupOwners)){
				GroupOwner firstOwner = groupOwners.get(0);
				if(firstOwner!=null && "user".equals(firstOwner.getType())){
					User user = userDataWebService.getUserWithDependent(firstOwner.getId(), requesterId, false);
					if (user==null){
						user = userDataWebService.getUserWithDependent(firstOwner.getId(), null, false);
						request.setAttribute("notPermitToChangeGroupOwner", "Y");
					}
					UserBean usrBean = UserBean.getInstance(user);
					firstOwner.setName(usrBean.getName());
				}
				request.setAttribute("groupOwner", firstOwner);
			}

		} else if(StringUtils.isBlank(groupId) && StringUtils.isBlank(groupTypeId)){
			final List<MetadataType> groupTypes = this.getMetadataTypesByGrouping(MetadataTypeGrouping.GROUP_TYPE);
			request.setAttribute("groupTypes", groupTypes);
			setMenuTree(request, getRootMenu());
			return "jar:groups/selectGroupType";
		} else if(StringUtils.isBlank(groupId) && StringUtils.isNotBlank(groupTypeId)){
			MetadataType type = this.getMetadataTypeById(groupTypeId);
			group.setMdTypeId(type.getId());
			group.setMetadataTypeName(type.getDisplayName());
		}

		if(StringUtils.isNotBlank(groupId)) {
			setMenuTree(request, getEditMenu());
		} else {
			setMenuTree(request, getRootMenu());
		}
		final List<List<OrganizationType>> orgTypes = getOrgTypeList();

		PageTempate template = templateProvider.getPageTemplate(group.getId(), getRequesterId(request), request);
		if(template != null && template.getJsonDataModel()!=null){
			template.setObjectDataModel(jacksonMapper.readValue(template.getJsonDataModel(), Map.class));
			template.setJsonDataModel(null);
		}

		request.setAttribute("template", (template != null) ? jacksonMapper.writeValueAsString(template) : null);
		request.setAttribute("pageTemplate", (template != null) ? template : new PageTempate());

		request.setAttribute("isNew", StringUtils.isBlank(group.getId()));
		request.setAttribute("groupAsJSON", jacksonMapper.writeValueAsString(group));
		request.setAttribute("orgHierarchy", (orgTypes != null) ? jacksonMapper.writeValueAsString(orgTypes) : null);
		request.setAttribute("group", group);

		request.setAttribute("managedSystems", getManagedSystemsAsKeyNameBeans());

		request.setAttribute("groupClassification", getMetaDataTypeByGrouping("GROUP_CLASSIFICATION"));
		request.setAttribute("adGroupType", getMetaDataTypeByGrouping("AD_GROUP_TYPE"));
		request.setAttribute("adGroupScope", getMetaDataTypeByGrouping("AD_GROUP_SCOPE"));
		request.setAttribute("risk", getMetaDataTypeByGrouping("RISK"));

		final List<OrganizationBean> orgList = new LinkedList<OrganizationBean>();
		for (Organization org : group.getOrganizations()) {
			OrganizationBean orgBean = new OrganizationBean();
			orgBean.setId(org.getId());
			orgBean.setName(org.getName());
			orgBean.setOrganizationTypeId(org.getOrganizationTypeId());
			orgList.add(orgBean);
		}
		request.setAttribute("orgList", (orgList.size() > 0) ? jacksonMapper.writeValueAsString(orgList) : null);


		return "jar:groups/editGroup";
	}
	
	@RequestMapping(value="/groups", method=RequestMethod.GET)
	public String searchGroups(final HttpServletRequest request, 
							   final HttpServletResponse response) throws Exception {
		setMenuTree(request, getRootMenu());
		request.setAttribute("managedSystems", jacksonMapper.writeValueAsString(getManagedSystemsAsKeyNameBeans()));
		return "jar:groups/search";
	}

    @RequestMapping(value = "/addChildGroup", method = RequestMethod.POST)
    public String addChildGroup(final HttpServletRequest request, final @RequestParam(required = true, value = "groupId") String groupId,
                                final @RequestParam(required = true, value = "childGroupId") String childGroupId) {
        final BasicAjaxResponse ajaxResponse = addGroup2Group(request, groupId, childGroupId);
        request.setAttribute("response", ajaxResponse);
        return "common/basic.ajax.response";
    }

    @RequestMapping(value = "/removeChildGroup", method = RequestMethod.POST)
    public String removeChildGroup(final HttpServletRequest request, final @RequestParam(required = true, value = "groupId") String groupId,
                                   final @RequestParam(required = true, value = "childGroupId") String childGroupId) {
        final BasicAjaxResponse ajaxResponse = removeGroupFromGroup(request, groupId, childGroupId);
        request.setAttribute("response", ajaxResponse);
        return "common/basic.ajax.response";
    }
    
    @Override
    protected GroupRequestModel getEntity(final String id, final HttpServletRequest request) {
		GroupRequestModel model = new GroupRequestModel();
		model.setTargetObject(groupServiceClient.getGroup(id, getRequesterId(request)));
    	return model;
    }
	
	@Override
	protected final List<ErrorToken> getEditErrors(final Response wsResponse, final HttpServletRequest request, final GroupRequestModel entity) {
		SaveTemplateProfileResponse groupResponse = (SaveTemplateProfileResponse)wsResponse;
		final List<ErrorToken> retVal = new LinkedList<ErrorToken>();
		if(groupResponse != null && groupResponse.isFailure()) {
			if(groupResponse.getErrorCode() != null) {
				switch(groupResponse.getErrorCode()) {
					case NO_NAME:
						retVal.add(new ErrorToken(Errors.INVALID_GROUP_NAME));
						break;
					case NAME_TAKEN:
						retVal.add(new ErrorToken(Errors.GROUP_NAME_TAKEN));
						break;
					case VALIDATION_ERROR:
						retVal.addAll(ErrorUtils.getESBErrorTokens(wsResponse));
						break;
					case INVALID_VALUE:
						retVal.add(new ErrorToken(Errors.INVALID_META_VALUE, new Object[]{groupResponse.getCurrentValue(), groupResponse.getElementName()}));
						break;
					case REQUIRED:
						retVal.add(new ErrorToken(Errors.META_VALUE_REQUIRED, new Object[]{groupResponse.getElementName()}));
						break;
					default:
						retVal.add(new ErrorToken(Errors.INTERNAL_ERROR));
						break;
				}
			} else {
				retVal.add(new ErrorToken(Errors.INTERNAL_ERROR));
			}
		}
		return retVal;
	}

	protected BasicAjaxResponse doEdit(final HttpServletRequest request, final HttpServletResponse response, final GroupRequestModel group, String redirectUrl) throws Exception {
		final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();

		String requesterId = getRequesterId(request);
		group.setRequesterId(requesterId);
		IdmAuditLog idmAuditLog = new IdmAuditLog();
		idmAuditLog.setRequestorUserId(getRequesterId(request));
		idmAuditLog.setRequestorPrincipal(getRequesterPrincipal(request));
		boolean isNew = group.getTargetObject().getId() == null;
		if (isNew) {
			idmAuditLog.setAction(AuditAction.ADD_GROUP.value());
			idmAuditLog.setAuditDescription("Create new group");
		} else {
			idmAuditLog.setAction(AuditAction.EDIT_GROUP.value());
			idmAuditLog.setAuditDescription(getLogValuesForEditGroup(group.getTargetObject(), request));
		}
		final SaveTemplateProfileResponse wsResponse = groupServiceClient.saveGroupRequest(group);

		if (wsResponse.isSuccess()) {
			String groupId = (String) wsResponse.getResponseValue();
			Group createdGroup = groupServiceClient.getGroup(groupId, requesterId);
			ProvisionGroup provisionGroup = new ProvisionGroup(createdGroup);
			Response groupResponse = isNew ? groupProvisionService.add(provisionGroup) :
					groupProvisionService.modify(provisionGroup);
			idmAuditLog.setTargetGroup(groupId, group.getTargetObject().getName());

			//TODO group provisioning processing the result

			ajaxResponse.setStatus(200);
			ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.GROUP_SAVED));
			if (StringUtils.isBlank(redirectUrl)) {
				if (StringUtils.isBlank(group.getId())) {
					ajaxResponse.setRedirectURL(new StringBuilder("editGroup.html?id=").append(wsResponse.getResponseValue()).toString());
				}
			} else {
				ajaxResponse.setRedirectURL(redirectUrl);
			}

			idmAuditLog.succeed();
		} else {
			ajaxResponse.addErrors(getEditErrors(wsResponse, request, group));
			idmAuditLog.fail();
			idmAuditLog.setFailureReason(wsResponse.getErrorCode());
			idmAuditLog.setFailureReason(wsResponse.getErrorText());
			idmAuditLog.setTargetGroup(group.getId(), group.getTargetObject().getName());
		}
		auditLogService.addLog(idmAuditLog);

		return ajaxResponse;
	}



	//To fill auditLog with the Updated Values

	public String getLogValuesForEditGroup(Group group, HttpServletRequest request){

		StringBuilder logValue= new StringBuilder("");
		final Group gp = getEntity(group.getId(), request).getTargetObject();


		if(gp.getName()!=null) {
			if (group.getName() != null )
				if(!gp.getName().equals(group.getName())) {
					logValue.append("Name : [Previous Value:  " + gp.getName() + " Current Value: " + group.getName() + " ]  ");
				}} else {
			if (group.getName() != null) {
				logValue.append("Name : [Previous Value:  " + gp.getName() + " Current Value: " + group.getName() + " ]  ");
			}
		}



		if(gp.getDescription()!=null) {
			if (group.getDescription() != null )
				if(!gp.getDescription().equals(group.getDescription())) {
					logValue.append("Description : [Previous Value:  " + gp.getDescription() + " Current Value: " + group.getDescription() + " ]  ");
				}} else {
			if (group.getDescription() != null) {
				logValue.append("Description : [Previous Value:  " + gp.getDescription() + " Current Value: " + group.getDescription() + " ]  ");
			}
		}

		if(gp.getManagedSysId()!=null) {
			if (group.getManagedSysId() != null )
				if(!gp.getManagedSysId().equals(group.getManagedSysId())) {
					logValue.append("Managed System : [Previous Value:  " + gp.getManagedSysId() + " Current Value: " + group.getManagedSysId() + " ]  ");
				}} else {
			if (group.getManagedSysId() != null) {
				logValue.append("Managed System : [Previous Value:  " + gp.getManagedSysId() + " Current Value: " + group.getManagedSysId() + " ]  ");
			}
		}

		if(gp.getMaxUserNumber()!=null) {
			if (group.getMaxUserNumber() != null )
				if(!gp.getMaxUserNumber().equals(group.getMaxUserNumber())) {
					logValue.append("Maximum User Number : [Previous Value:  " + gp.getMaxUserNumber() + " Current Value: " + group.getMaxUserNumber() + " ]  ");
				}} else {
			if (group.getMaxUserNumber() != null) {
				logValue.append("Maximum User Number : [Previous Value:  " + gp.getMaxUserNumber() + " Current Value: " + group.getMaxUserNumber() + " ]  ");
			}
		}

		if(gp.getMembershipDuration()!=null) {
			if (group.getMembershipDuration() != null )
				if(!gp.getMembershipDuration().equals(group.getMembershipDuration())) {
					logValue.append("Membership Duration : [Previous Value:  " + gp.getMembershipDuration() + " Current Value: " + group.getMembershipDuration() + " ]  ");
				}} else {
			if (group.getMembershipDuration() != null) {
				logValue.append("Membership Duration : [Previous Value:  " + gp.getMembershipDuration() + " Current Value: " + group.getMembershipDuration() + " ]  ");
			}
		}

		if(gp.getClassificationId()!=null) {
			if (group.getClassificationId() != null )
				if(!gp.getClassificationId().equals(group.getClassificationId())) {
					logValue.append("Classification : [Previous Value:  " + gp.getClassificationId() + " Current Value: " + group.getClassificationId() + " ]  ");
				}} else {
			if (group.getClassificationId() != null) {
				logValue.append("Classification : [Previous Value:  " + gp.getClassificationId() + " Current Value: " + group.getClassificationId() + " ]  ");
			}
		}

		if(gp.getAdGroupTypeId()!=null) {
			if (group.getAdGroupTypeId() != null )
				if(!gp.getAdGroupTypeId().equals(group.getAdGroupTypeId())) {
					logValue.append("AD Group Type : [Previous Value:  " + gp.getAdGroupTypeId() + " Current Value: " + group.getAdGroupTypeId() + " ]  ");
				}} else {
			if (group.getAdGroupTypeId() != null) {
				logValue.append("AD Group Type : [Previous Value:  " + gp.getAdGroupTypeId() + " Current Value: " + group.getAdGroupTypeId() + " ]  ");
			}
		}


		if(gp.getAdGroupScopeId()!=null) {
			if (group.getAdGroupScopeId() != null)
				if(!gp.getAdGroupScopeId().equals(group.getAdGroupScopeId())) {
					logValue.append("AD Group Scope : [Previous Value:  " + gp.getAdGroupScopeId() + " Current Value: " + group.getAdGroupScopeId() + " ]  ");
				}} else {
			if (group.getAdGroupScopeId() != null) {
				logValue.append("AD Group Scope : [Previous Value:  " + gp.getAdGroupScopeId() + " Current Value: " + group.getAdGroupScopeId() + " ]  ");
			}
		}

		if(gp.getRiskId()!=null) {
			if (group.getRiskId() != null)
				if(!gp.getRiskId().equals(group.getRiskId())) {
					logValue.append("Risk : [Previous Value:  " + gp.getRiskId() + " Current Value: " + group.getRiskId() + " ]  ");
				}}else{
			if (group.getRiskId() != null){
				logValue.append("Risk : [Previous Value:  " + gp.getRiskId() + " Current Value: " + group.getRiskId() + " ]  ");
			}
		}

		//	For Organizations
		StringBuilder oldValue=new StringBuilder();
		StringBuilder newValue=new StringBuilder();

		for(Organization org : gp.getOrganizations()){
			oldValue.append(org.getName());
		}
		for(Organization org : group.getOrganizations()){
			Organization newOrg=getOrgEntity(org.getId(), request);
			newValue.append(newOrg.getName());
		}
		if(!(oldValue.toString().equals(newValue.toString()))) {
			if(oldValue.toString().equals(""))
				logValue.append("Organization : [Previous Value: " + "NULL" + " Current Value: " + newValue+" ]  ");
			else if(newValue.toString().equals(""))
				logValue.append("Organization : [Previous Value: " + oldValue + " Current Value: " + "NULL"+" ]  ");
			else
				logValue.append("Organization : [Previous Value: " + oldValue + " Current Value: " + newValue+" ]  ");

		}

		return (logValue.toString());

	}

	protected Organization getOrgEntity(final String id, final HttpServletRequest request) {
		return organizationServiceClient.getOrganization(id, getRequesterId(request));
	}

	@Override
	protected BasicAjaxResponse doDelete(final HttpServletRequest request, final HttpServletResponse response, final GroupRequestModel entity) throws Exception {
		final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
		final Group group = entity.getTargetObject();
		final String callerId = getRequesterId(request);
		IdmAuditLog idmAuditLog = new IdmAuditLog();
		idmAuditLog.setRequestorUserId(callerId);
		idmAuditLog.setAction(AuditAction.DELETE_GROUP.value());
		idmAuditLog.setAuditDescription("Delete group");
		idmAuditLog.setTargetGroup(group.getId(), group.getName());

		List<IdentityDto> identityDtos = identityService.getIdentities(group.getId());
		Response wsResponse = null;
		if(identityDtos != null && identityDtos.size() > 0) {
			wsResponse = groupProvisionService.remove(group.getId(), callerId);
		}
        wsResponse = groupServiceClient.deleteGroup(group.getId(), callerId);


		if(wsResponse.isSuccess()) {
			new Response(ResponseStatus.SUCCESS);
			ajaxResponse.setStatus(200);
			ajaxResponse.setRedirectURL("groups.html");
			ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.GROUP_DELETED));
			idmAuditLog.succeed();
		} else {
			ajaxResponse.addErrors(getDeleteErrors(wsResponse, request, entity));
			idmAuditLog.fail();
			idmAuditLog.setFailureReason(wsResponse.getErrorCode());
			idmAuditLog.setFailureReason(wsResponse.getErrorText());
		}
		auditLogService.addLog(idmAuditLog);

		return ajaxResponse;
	}

//	@Override
	protected BasicAjaxResponse addGroup2Group(HttpServletRequest request,
											   String groupId, String childGroupId) {
		final String callerId = getRequesterId(request);
		final Response wsResponse = groupServiceClient.addChildGroup(groupId, childGroupId, callerId);
		return getResponseAfterEntity2EntityAddition(wsResponse, false);
	}

//	@Override
	protected BasicAjaxResponse removeGroupFromGroup(
			HttpServletRequest request, String groupId, String childGroupId) {
		final String callerId = getRequesterId(request);
		final Response wsResponse = groupServiceClient.removeChildGroup(groupId, childGroupId.toLowerCase(), callerId);
		return getResponseAfterEntity2EntityAddition(wsResponse, true);
	}
	
//	protected abstract BasicAjaxResponse addGroup2Group(final HttpServletRequest request, final String groupId, final String childGroupId);
//	protected abstract BasicAjaxResponse removeGroupFromGroup(final HttpServletRequest request, final String groupId, final String childGroupId);
}
