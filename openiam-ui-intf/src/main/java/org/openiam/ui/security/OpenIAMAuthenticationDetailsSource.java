package org.openiam.ui.security;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openiam.idm.srvc.auth.ws.LoginDataWebService;
import org.openiam.idm.srvc.auth.ws.LoginResponse;
import org.openiam.idm.srvc.policy.dto.ITPolicy;
import org.openiam.idm.srvc.policy.dto.Policy;
import org.openiam.idm.srvc.policy.dto.PolicyAttribute;
import org.openiam.idm.srvc.policy.service.PolicyDataService;
import org.openiam.idm.srvc.pswd.service.ChallengeResponseWebService;
import org.openiam.idm.srvc.pswd.ws.PasswordWebService;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.idm.srvc.user.ws.UserDataWebService;
import org.openiam.ui.web.util.UsePolicyHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

public class OpenIAMAuthenticationDetailsSource extends WebAuthenticationDetailsSource {

    private static Logger LOG = Logger.getLogger(OpenIAMAuthenticationDetailsSource.class);

    @Autowired
    private OpenIAMCookieProvider cookieProvider;

    @Resource(name = "challengeResponseServiceClient")
    private ChallengeResponseWebService challengeResponseServiceClient;

    @Resource(name = "policyServiceClient")
    private PolicyDataService policyDataService;

    @Resource(name = "userServiceClient")
    protected UserDataWebService userDataWebService;

    @Resource(name = "passwordServiceClient")
    protected PasswordWebService passwordService;

    @Autowired
    @Resource(name = "loginServiceClient")
    protected LoginDataWebService loginServiceClient;

    public OpenIAMAuthenticationDetailsSource() {
        super();
    }

    /* no worries - this will be called once - on login */
    @Override
    public WebAuthenticationDetails buildDetails(HttpServletRequest request) {
        boolean isQuestionsAnswered = false;
        boolean isUsePolicyConfirmed = true;
        boolean isResetPasswordTypeSelected = true;

        final String userId = cookieProvider.getUserIdFromCookie(request);
        if (userId != null) {
            try {
                isQuestionsAnswered = challengeResponseServiceClient.isUserAnsweredSecurityQuestions(userId);
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }

            ITPolicy itPolicy = policyDataService.findITPolicy();
            final User user = userDataWebService.getUserWithDependent(userId, null, false);
            if (itPolicy != null && user != null) {
                Boolean status = UsePolicyHelper.getUsePolicyStatus(itPolicy, user);
                if (status != null) {
                    isUsePolicyConfirmed = UsePolicyHelper.getUsePolicyStatus(itPolicy, user);
                }
            }

            LoginResponse loginResponse = loginServiceClient.getPrimaryIdentity(userId);
            if (loginResponse.isSuccess()) {
                Policy policy = passwordService.getPasswordPolicy(loginResponse.getPrincipal().getLogin(),
                        loginResponse.getPrincipal().getManagedSysId());
                if (policy != null) {
                    PolicyAttribute policyAttribute = policy.getAttribute("USER_DRIVEN_RESET");
                    if (policyAttribute != null && StringUtils.isNotBlank(policyAttribute.getValue1())
                            && "true".equalsIgnoreCase(policyAttribute.getValue1()) && user.getResetPasswordType() == null) {
                        isResetPasswordTypeSelected = false;
                    }
                }
            }
        }

        final OpeniamWebAuthenticationDetails details = new OpeniamWebAuthenticationDetails(request);
        details.setIdentityQuestionsAnswered(isQuestionsAnswered);
        details.setUsePolicyConfirmed(isUsePolicyConfirmed);
        details.setResetPasswordTypeSelected(isResetPasswordTypeSelected);
        LOG.debug(String.format("Authentication Details After Building: %s", details));
        return details;
    }
}
