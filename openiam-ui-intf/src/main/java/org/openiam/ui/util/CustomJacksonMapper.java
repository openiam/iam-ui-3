package org.openiam.ui.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.web.util.HtmlUtils;

import java.io.IOException;

public class CustomJacksonMapper extends ObjectMapper {

	public CustomJacksonMapper() {
		super();
		this.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true)
		 .configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false)
		 .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		final SimpleModule stringModule = new SimpleModule("MyModule", new Version(1, 0, 0, null)).addDeserializer(String.class, new StringDeserializer());
		this.registerModule(stringModule);
	}
	
	private class StringDeserializer extends JsonDeserializer<String> {

		@Override
		public String deserialize(JsonParser parser, DeserializationContext ctx) throws IOException, JsonProcessingException {
			String str = StringUtils.trimToNull(parser.getText());
			//IDMAPPS-2393
			if(str != null) {
				str = Jsoup.clean(str, Whitelist.relaxed());
			}
			
			return HtmlUtils.htmlUnescape(XSSUtils.stripXSS(str, false));
		}
	}
}
