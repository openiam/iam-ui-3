package org.openiam.ui.util;

import org.apache.commons.lang.StringUtils;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

public class HeaderUtils {


	public static String getClientIP(final HttpServletRequest httpRequest) {
		String xForwardedFor = httpRequest.getHeader("X-Forwarded-For");
		return StringUtils.isBlank(xForwardedFor) ? httpRequest.getRemoteAddr() : xForwardedFor;
	}

	public static String getCaseInsensitiveHeader(final HttpServletRequest request, final String lowerCaseHeaderName) {
		String retVal = null;
		if(request.getHeaderNames() != null) {
			final Enumeration<String> names = request.getHeaderNames();
			while(names.hasMoreElements()) {
				final String name = names.nextElement();
				if(name != null) {
					if(name.toLowerCase().equals(lowerCaseHeaderName)) {
						retVal = request.getHeader(name);
						break;
					}
				}
			}
		}
		return retVal;
	}
}
