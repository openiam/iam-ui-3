<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.openiam.com/tags/openiam" prefix="openiam" %>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${titleOrganizatioName} - <fmt:message key="openiam.idp.reset.type.title"/></title>
    <link href="/openiam-ui-static/css/common/style.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/openiam-ui-static/css/common/style.client.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/jquery/css/smoothness/jquery-ui-1.12.1.custom.min.css" rel="stylesheet"
          type="text/css"/>
    <openiam:overrideCSS/>
    <script type="text/javascript" src="/openiam-ui-static/_dynamic/openiamResourceBundle.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-3.3.1.min.js"></script><script src="/openiam-ui-static/js/common/jquery/jquery.browser.min.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-ui-1.12.1.custom.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery.corner.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/selfservice/reset.password.type.js"></script>
    <script type="text/javascript">
        OPENIAM = window.OPENIAM || {};
        OPENIAM.ENV = window.OPENIAM.ENV || {};
        OPENIAM.ENV.AjaxResponse = <c:choose><c:when test="${! empty requestScope.ajaxResponse}">${requestScope.ajaxResponse}</c:when><c:otherwise>null</c:otherwise></c:choose>;
    </script>
</head>
<body>
<div>
    <div id="title" class="title">
        <fmt:message key="openiam.idp.reset.type.desc"/>
    </div>
    <div class="center">
        <form id="login-form`" action="javascript: void(0);">
            <input type="hidden" name="userId" value="${requestScope.currentUserId}"/>

            <select class="rounded margin-bottom-10" name="resetTypeSelect"
                    <c:if test="${(requestScope.disabledSelectResetPassword)}"> disabled="disabled" </c:if>>
                <option value="ADMIN_RESET"
                        <c:if test="${(not empty  requestScope.resetPasswordType)&& ('ADMIN_RESET' eq requestScope.resetPasswordType)}">selected="selected" </c:if> >
                    <fmt:message key="openiam.ui.user.password.fill.pass.type"/>
                </option>
                <option value="ACTIVATION_LINK"
                        <c:if test="${(not empty  requestScope.resetPasswordType)&& ('ACTIVATION_LINK' eq requestScope.resetPasswordType)}">selected="selected" </c:if>>
                    <fmt:message key="openiam.ui.user.password.send.link.type"/>
                </option>
            </select>

            <div class="lrow">
                <a href="javascript:void(0)"><input id="submit" type="submit" class="redBtn"
                                                    value="<fmt:message key='openiam.ui.common.select' />"/></a>
            </div>
        </form>
    </div>
</div>
</body>
</html>