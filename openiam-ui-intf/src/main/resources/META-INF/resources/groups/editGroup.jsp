<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.openiam.com/tags/openiam" prefix="openiam" %>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>

<c:set var="pageTitle">
    <c:choose>
        <c:when test="${! empty requestScope.group.id}">
            <fmt:message key="openiam.ui.shared.group.edit"/>: ${requestScope.group.name}
        </c:when>
        <c:otherwise>
            <fmt:message key="openiam.ui.shared.group.new"/>
        </c:otherwise>
    </c:choose>
</c:set>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${titleOrganizatioName} - ${pageTitle}</title>
    <link href="/openiam-ui-static/css/common/style.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/openiam-ui-static/css/common/style.client.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/jquery/css/smoothness/jquery-ui-1.12.1.custom.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/plugins/tiptip/tipTip.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/plugins/multiselect/jquery.multiselect.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/plugins/tablesorter/themes/yui/style.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/plugins/entitlementstable/entitlements.table.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/js/common/plugins/modalsearch/modal.search.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/webconsole/plugins/usersearch/user.search.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/css/selfservice/edit.profile.css" rel="stylesheet" type="text/css"/>
    <openiam:overrideCSS/>
    <script type="text/javascript" src="/openiam-ui-static/_dynamic/openiamResourceBundle.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-3.3.1.min.js"></script><script src="/openiam-ui-static/js/common/jquery/jquery.browser.min.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-ui-1.12.1.custom.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery.cookies.2.2.0.min.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/json/json.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter-2.0.3.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter.filer.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter.pager.js"></script>

    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tiptip/jquery.tipTip.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/js/common/plugins/multiselect/jquery.multiselect.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/plugins/uiTemplate/ui.template.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/plugins/modalEdit/modalEdit.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/js/common/plugins/persistentTable/persistent.table.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/js/webconsole/plugins/usersearch/user.search.form.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/js/webconsole/plugins/usersearch/user.search.results.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/js/common/plugins/orghierarchy/organization.hierarchy.js"></script>

    <script type="text/javascript" src="/openiam-ui-static/js/common/group/edit.group.bootstrap.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/group/group.edit.js"></script>

    <script type="text/javascript" src="/openiam-ui-static/js/common/search/search.result.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/js/common/plugins/entitlementstable/entitlements.table.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/search/group.search.js"></script>

    <script type="text/javascript">
        OPENIAM = window.OPENIAM || {};
        OPENIAM.ENV = window.OPENIAM.ENV || {};
        OPENIAM.ENV.MenuTree = <c:choose><c:when test="${! empty requestScope.menuTree}">${requestScope.menuTree}</c:when><c:otherwise>null</c:otherwise></c:choose>;
        OPENIAM.ENV.MenuTreeAppendURL = <c:choose><c:when test="${! empty requestScope.group.id}">"id=${requestScope.group.id}"
        </c:when><c:otherwise>null</c:otherwise></c:choose>;
        OPENIAM.ENV.UITEMPLATE = <c:choose><c:when test="${! empty requestScope.template}">${requestScope.template}</c:when><c:otherwise>null</c:otherwise></c:choose>;

        OPENIAM.ENV.supportsGroupParent = ${! empty requestScope.pageTemplate.uiFields['GROUP_PARENT']};
        OPENIAM.ENV.supportsGroupOwner = ${! empty requestScope.pageTemplate.uiFields['GROUP_OWNER']};
        OPENIAM.ENV.supportsGroupOrgs = ${! empty requestScope.pageTemplate.uiFields['GROUP_ORGANIZATION']};
        OPENIAM.ENV.OrganizationHierarchy = <c:choose><c:when test="${! empty requestScope.orgHierarchy}">${requestScope.orgHierarchy}</c:when><c:otherwise>null</c:otherwise></c:choose>;
        OPENIAM.ENV.GroupId = "${requestScope.group.id}";
        OPENIAM.ENV.GroupTypeId = "${requestScope.group.mdTypeId}";
        OPENIAM.ENV.Group =${requestScope.groupAsJSON};
        OPENIAM.ENV.OrgParamList = <c:choose><c:when test="${! empty requestScope.orgList}">${requestScope.orgList}</c:when><c:otherwise>[]</c:otherwise></c:choose>;
        OPENIAM.ENV.GroupParentRequired = <c:choose><c:when test="${! empty requestScope.pageTemplate.uiFields['GROUP_PARENT']}">${requestScope.pageTemplate.uiFields['GROUP_PARENT'].required}</c:when><c:otherwise>false</c:otherwise></c:choose>;
        OPENIAM.ENV.GroupOwnerRequired = <c:choose><c:when test="${! empty requestScope.pageTemplate.uiFields['GROUP_OWNER']}">${requestScope.pageTemplate.uiFields['GROUP_OWNER'].required}</c:when><c:otherwise>false</c:otherwise></c:choose>;
        OPENIAM.ENV.GroupOrgRequired = <c:choose><c:when test="${! empty requestScope.pageTemplate.uiFields['GROUP_ORGANIZATION']}">${requestScope.pageTemplate.uiFields['GROUP_ORGANIZATION'].required}</c:when><c:otherwise>false</c:otherwise></c:choose>;
    </script>
</head>
<body>
<div id="title" class="title">${pageTitle}</div>
<div id="basicInfoElements">
    <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_NAME']}">
        <c:set var="required" value="${requestScope.pageTemplate.uiFields['GROUP_NAME'].required eq true}"/>
        <c:set var="readOnly" value="${requestScope.pageTemplate.uiFields['GROUP_NAME'].editable eq false}"/>
        <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_NAME'].displayOrder}">
            <label for="name"
                   <c:if test="${required eq true}">class="required"</c:if> >
                    ${requestScope.pageTemplate.uiFields['GROUP_NAME'].name}
            </label>
            <input id="name" type="text" value="${requestScope.group.name}"
                   class="full rounded <c:if test="${readOnly eq true}">disabled</c:if>"
                   <c:if test="${readOnly eq true}">disabled="disabled"</c:if> />
        </div>
    </c:if>

    <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_DESCRIPTION']}">
        <c:set var="required" value="${requestScope.pageTemplate.uiFields['GROUP_DESCRIPTION'].required eq true}"/>
        <c:set var="readOnly" value="${requestScope.pageTemplate.uiFields['GROUP_DESCRIPTION'].editable eq false}"/>
        <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_DESCRIPTION'].displayOrder}">
            <label for="description"
                   <c:if test="${required eq true}">class="required"</c:if> >
                    ${requestScope.pageTemplate.uiFields['GROUP_DESCRIPTION'].name}
            </label>
            <textarea id="description" class="full rounded <c:if test="${readOnly eq true}">disabled</c:if>"
                      <c:if test="${readOnly eq true}">disabled="disabled"</c:if>>${requestScope.group.description}</textarea>
        </div>
    </c:if>

    <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_MANAGED_SYS']}">
        <c:set var="required" value="${requestScope.pageTemplate.uiFields['GROUP_MANAGED_SYS'].required eq true}"/>
        <c:set var="readOnly" value="${requestScope.pageTemplate.uiFields['GROUP_MANAGED_SYS'].editable eq false}"/>
        <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_MANAGED_SYS'].displayOrder}">
            <label for="managedSysId"
                   <c:if test="${required eq true}">class="required"</c:if> >
                    ${requestScope.pageTemplate.uiFields['GROUP_MANAGED_SYS'].name}
            </label>
            <select id="managedSysId" class="select rounded <c:if test="${readOnly eq true}">disabled</c:if>"
                    <c:if test="${readOnly eq true}">disabled="disabled"</c:if> >
                <option value=""><fmt:message key="openiam.ui.common.value.pleaseselect"/></option>
                <c:forEach var="mngSys" items="${requestScope.managedSystems}">
                    <option value="${mngSys.id}"
                            <c:if test="${requestScope.group.managedSysId eq mngSys.id}">selected="selected"</c:if> >${mngSys.name}</option>
                </c:forEach>
            </select>
        </div>
    </c:if>
    <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_CLASSIFICATION']}">
        <c:set var="required" value="${requestScope.pageTemplate.uiFields['GROUP_CLASSIFICATION'].required eq true}"/>
        <c:set var="readOnly" value="${requestScope.pageTemplate.uiFields['GROUP_CLASSIFICATION'].editable eq false}"/>
        <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_CLASSIFICATION'].displayOrder}">
            <label for="classificationId"
                   <c:if test="${required eq true}">class="required"</c:if> >
                    ${requestScope.pageTemplate.uiFields['GROUP_CLASSIFICATION'].name}
            </label>
            <select id="classificationId" class="select rounded <c:if test="${readOnly eq true}">disabled</c:if>"
                    <c:if test="${readOnly eq true}">disabled="disabled"</c:if> >
                <option value=""><fmt:message key="openiam.ui.common.value.pleaseselect"/></option>
                <c:forEach var="classification" items="${requestScope.groupClassification}">
                    <option value="${classification.id}"
                            <c:if test="${requestScope.group.classificationId eq classification.id}">selected="selected"</c:if> >${classification.name}</option>
                </c:forEach>
            </select>
        </div>
    </c:if>

    <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_RISK']}">
        <c:set var="required" value="${requestScope.pageTemplate.uiFields['GROUP_RISK'].required eq true}"/>
        <c:set var="readOnly" value="${requestScope.pageTemplate.uiFields['GROUP_RISK'].editable eq false}"/>
        <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_RISK'].displayOrder}">
            <label for="riskId"
                   <c:if test="${required eq true}">class="required"</c:if> >
                    ${requestScope.pageTemplate.uiFields['GROUP_RISK'].name}
            </label>
            <select id="riskId" class="select rounded <c:if test="${readOnly eq true}">disabled</c:if>"
                    <c:if test="${readOnly eq true}">disabled="disabled"</c:if> >
                <option value=""><fmt:message key="openiam.ui.common.value.pleaseselect"/></option>
                <c:forEach var="r" items="${requestScope.risk}">
                    <option value="${r.id}"
                            <c:if test="${requestScope.group.riskId eq r.id}">selected="selected"</c:if> >${r.name}</option>
                </c:forEach>
            </select>
        </div>
    </c:if>

    <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_MAX_USERS']}">
        <c:set var="required" value="${requestScope.pageTemplate.uiFields['GROUP_MAX_USERS'].required eq true}"/>
        <c:set var="readOnly" value="${requestScope.pageTemplate.uiFields['GROUP_MAX_USERS'].editable eq false}"/>
        <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_MAX_USERS'].displayOrder}">
            <label for="maxUserNumber"
                   <c:if test="${required eq true}">class="required"</c:if> >
                    ${requestScope.pageTemplate.uiFields['GROUP_MAX_USERS'].name}
            </label>
            <input id="maxUserNumber" type="text" value="${requestScope.group.maxUserNumber}"
                   class="full rounded <c:if test="${readOnly eq true}">disabled</c:if>"
                   <c:if test="${readOnly eq true}">disabled="disabled"</c:if> />
        </div>
    </c:if>
    <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_MEMBERSHIP_DURATION']}">
        <c:set var="required"
               value="${requestScope.pageTemplate.uiFields['GROUP_MEMBERSHIP_DURATION'].required eq true}"/>
        <c:set var="readOnly"
               value="${requestScope.pageTemplate.uiFields['GROUP_MEMBERSHIP_DURATION'].editable eq false}"/>
        <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_MEMBERSHIP_DURATION'].displayOrder}">
            <label for="membershipDuration"
                   <c:if test="${required eq true}">class="required"</c:if> >
                    ${requestScope.pageTemplate.uiFields['GROUP_MEMBERSHIP_DURATION'].name}
            </label>
            <input id="membershipDuration" type="text" value="${requestScope.group.membershipDuration}"
                   class="full rounded <c:if test="${readOnly eq true}">disabled</c:if>"
                   <c:if test="${readOnly eq true}">disabled="disabled"</c:if> />
        </div>
    </c:if>

    <c:if test="${requestScope.group.mdTypeId eq 'AD_GROUP'}">
        <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_AD_GROUP_TYPE']}">
            <c:set var="required"
                   value="${requestScope.pageTemplate.uiFields['GROUP_AD_GROUP_TYPE'].required eq true}"/>
            <c:set var="readOnly"
                   value="${requestScope.pageTemplate.uiFields['GROUP_AD_GROUP_TYPE'].editable eq false}"/>
            <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_AD_GROUP_TYPE'].displayOrder}">
                <label for="adGroupTypeId"
                       <c:if test="${required eq true}">class="required"</c:if> >
                        ${requestScope.pageTemplate.uiFields['GROUP_AD_GROUP_TYPE'].name}
                </label>
                <select id="adGroupTypeId" class="select rounded <c:if test="${readOnly eq true}">disabled</c:if>"
                        <c:if test="${readOnly eq true}">disabled="disabled"</c:if> >
                    <option value=""><fmt:message key="openiam.ui.common.value.pleaseselect"/></option>
                    <c:forEach var="adGrpTp" items="${requestScope.adGroupType}">
                        <option value="${adGrpTp.id}"
                                <c:if test="${requestScope.group.adGroupTypeId eq adGrpTp.id}">selected="selected"</c:if> >${adGrpTp.name}</option>
                    </c:forEach>
                </select>
            </div>
        </c:if>
        <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_AD_SCOPE']}">
            <c:set var="required" value="${requestScope.pageTemplate.uiFields['GROUP_AD_SCOPE'].required eq true}"/>
            <c:set var="readOnly" value="${requestScope.pageTemplate.uiFields['GROUP_AD_SCOPE'].editable eq false}"/>
            <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_AD_SCOPE'].displayOrder}">
                <label for="adGroupScopeId"
                       <c:if test="${required eq true}">class="required"</c:if> >
                        ${requestScope.pageTemplate.uiFields['GROUP_AD_SCOPE'].name}
                </label>
                <select id="adGroupScopeId" class="select rounded <c:if test="${readOnly eq true}">disabled</c:if>"
                        <c:if test="${readOnly eq true}">disabled="disabled"</c:if> >
                    <option value=""><fmt:message key="openiam.ui.common.value.pleaseselect"/></option>
                    <c:forEach var="adGrpSc" items="${requestScope.adGroupScope}">
                        <option value="${adGrpSc.id}"
                                <c:if test="${requestScope.group.adGroupScopeId eq adGrpSc.id}">selected="selected"</c:if> >${adGrpSc.name}</option>
                    </c:forEach>
                </select>
            </div>
        </c:if>
    </c:if>

    <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_PARENT']}">
        <c:set var="required" value="${requestScope.pageTemplate.uiFields['GROUP_PARENT'].required eq true}"/>
        <c:set var="readOnly" value="${requestScope.pageTemplate.uiFields['GROUP_PARENT'].editable eq false}"/>
        <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_PARENT'].displayOrder}">
            <label for="groupParent"
                   <c:if test="${required eq true}">class="required"</c:if> >
                <fmt:message key="openiam.ui.group.parent"/>
            </label>
            <div id="groupParent"></div>
        </div>
    </c:if>

    <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_OWNER']}">
        <c:set var="required" value="${requestScope.pageTemplate.uiFields['GROUP_OWNER'].required eq true}"/>
        <c:set var="readOnly" value="${requestScope.pageTemplate.uiFields['GROUP_OWNER'].editable eq false}"/>
        <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_OWNER'].displayOrder}">
            <label for="groupOwner"
                   <c:if test="${required eq true}">class="required"</c:if> >
                <fmt:message key="openiam.ui.group.owner"/>
            </label>
            <div>
                <input id="groupOwner" type="text" class="full rounded" readonly="readonly"
                       <c:if test="${! empty requestScope.groupOwner and ! empty requestScope.groupOwner.id}">value="${requestScope.groupOwner.type} - ${requestScope.groupOwner.name}"</c:if>/>
                <input id="groupOwnerId" type="hidden"
                       <c:if test="${! empty requestScope.groupOwner and ! empty requestScope.groupOwner.id}">value="${requestScope.groupOwner.id}"</c:if>
                       name="groupOwnerId">
                <input id="groupOwnerType" name="groupOwnerType" type="hidden" class="full rounded"/>
                <c:choose>
                    <c:when test="${'Y' == requestScope.notPermitToChangeGroupOwner }">
                        <div style="display: inline-block;"><p style="color: red"><fmt:message
                                key="openiam.ui.group.owner.not.permit"/></p></div>
                    </c:when>
                    <c:otherwise>
                        <a id="selectGroupOwner" href="javascript:void(0);"><fmt:message
                                key="openiam.ui.group.owner.select"/></a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </c:if>

    <c:if test="${! empty requestScope.group.adminResourceId}">
        <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_ADMIN_RESOURCE']}">
            <div displayOrder="${requestScope.pageTemplate.uiFields['GROUP_ADMIN_RESOURCE'].displayOrder}">
                <label>
                    <fmt:message key="openiam.ui.shared.protected.by.resource"/>
                </label>
                <a href="editResource.html?id=${requestScope.group.adminResourceId}">${requestScope.group.adminResourceCoorelatedName}</a>
            </div>
        </c:if>
    </c:if>

</div>

<div class="frameContentDivider">
    <div id="editGroupForm">
        <table id="basicInfoTable" class="profileTable fieldset" cellpadding="8px" align="center">
            <thead>
            <%--<tr>--%>
            <%--<th colspan="3">--%>
            <%--<label>--%>
            <%--<fmt:message key="openiam.ui.selfservice.ui.template.basic.information" />--%>
            <%--</label>--%>
            <%--</th>--%>
            <%--</tr>--%>
            </thead>
            <tbody>
            <tr>
                <td>
                    <label>
                        <fmt:message key="openiam.ui.group.type"/>
                    </label>
                </td>
                <td>
                    <input id="groupTypeId" name="groupTypeId" type="hidden" value="${requestScope.group.mdTypeId}"/>
                    <input id="groupType" name="groupType" type="text" readonly class="full rounded"
                           value="${requestScope.group.metadataTypeName}"/>
                </td>
            </tr>
            </tbody>
        </table>

        <table id="uiTemplateTable" class="profileTable fieldset" cellpadding="8px" align="center">
            <thead>
            <%--<tr>--%>
            <%--<th colspan="3">--%>
            <%--<label>--%>
            <%--<fmt:message key='openiam.ui.selfservice.ui.template.additional.information' />--%>
            <%--</label>--%>
            <%--</th>--%>
            <%--</tr>--%>
            </thead>
            <tbody id="uiTemplate">

            </tbody>
        </table>

        <c:if test="${! empty requestScope.pageTemplate.uiFields['GROUP_ORGANIZATION'] or requestScope.newUser eq true}">
            <c:set var="required" value="${requestScope.pageTemplate.uiFields['GROUP_ORGANIZATION'].required eq true}"/>
            <div id="organizationsTable">
                <label
                        <c:if test="${required eq true}">class="required"</c:if> >
                    <fmt:message key="openiam.ui.selfservice.ui.template.organization.structure"/>
                </label>
            </div>
        </c:if>

        <c:if test="${! empty requestScope.pageTemplate}">
            <div>
                <ul class="formControls">
                    <li class="leftBtn">
                        <a id="saveGroup" href="javascript:void(0)" class="redBtn">
                            <fmt:message key='openiam.ui.button.save'/>
                        </a>
                    </li>
                    <li class="leftBtn">
                        <a href="groups.html" class="whiteBtn">
                            <fmt:message key="openiam.ui.button.cancel"/>
                        </a>
                    </li>
                    <c:if test="${! requestScope.isNew}">
                        <li class="rightBtn">
                            <a id="deleteGroup" href="javascript:void(0);" class="redBtn">
                                <fmt:message key="openiam.ui.button.delete"/>
                            </a>
                        </li>
                    </c:if>
                </ul>
            </div>
        </c:if>

    </div>
</div>


<div id="editDialog"></div>
<div id="dialog"></div>
</body>
</html>