package org.openiam.ui.webconsole.am.web.model;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class AuthProviderFormRequest {

	private String providerId;
    private String providerType;
    private String managedSysId;
    private String resourceId;
    private String name;
    private String description;
    private String applicationURL;
    private String iconURL;
    private boolean isSignRequest=false;
	private CommonsMultipartFile publicKey;
	private CommonsMultipartFile privateKey;
	private CommonsMultipartFile caCert;
	private boolean clearPrivateKey;
	private boolean clearPublicKey;
	private boolean clearCaCert;
	private boolean chained;
	private String nextAuthProviderId;
	private boolean supportsCertAuth;
	private String certRegex;
	private String certGroovyScript;
	private String caValidateGroovyScript;
	private String derPath;
	
	/* maps attribute ID to value, so as to set the attribute of the AuthProvider correctly */
	private Map<String, String> attributeMap = new HashMap<String, String>();
	
	public String getProviderId() {
		return providerId;
	}
	
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
	
	public String getProviderType() {
		return providerType;
	}
	
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}
	
	public String getManagedSysId() {
		return managedSysId;
	}
	
	public void setManagedSysId(String managedSysId) {
		this.managedSysId = managedSysId;
	}
	
	public String getResourceId() {
		return resourceId;
	}
	
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isSignRequest() {
		return isSignRequest;
	}
	
	public void setSignRequest(boolean isSignRequest) {
		this.isSignRequest = isSignRequest;
	}
	
	public CommonsMultipartFile getPublicKey() {
		return publicKey;
	}
	
	public void setPublicKey(CommonsMultipartFile publicKey) {
		this.publicKey = publicKey;
	}
	
	public CommonsMultipartFile getPrivateKey() {
		return privateKey;
	}
	
	public void setPrivateKey(CommonsMultipartFile privateKey) {
		this.privateKey = privateKey;
	}

	public CommonsMultipartFile getCaCert() {
		return caCert;
	}

	public void setCaCert(CommonsMultipartFile caCert) {
		this.caCert = caCert;
	}

	public String getApplicationURL() {
		return applicationURL;
	}

	public void setApplicationURL(String applicationURL) {
		this.applicationURL = applicationURL;
	}

	public String getIconURL() {
		return iconURL;
	}

	public void setIconURL(String iconURL) {
		this.iconURL = iconURL;
	}

	public Map<String, String> getAttributeMap() {
		return attributeMap;
	}

	public void setAttributeMap(Map<String, String> attributeMap) {
		this.attributeMap = attributeMap;
	}

	public boolean isClearPrivateKey() {
		return clearPrivateKey;
	}

	public void setClearPrivateKey(boolean clearPrivateKey) {
		this.clearPrivateKey = clearPrivateKey;
	}

	public boolean isClearPublicKey() {
		return clearPublicKey;
	}

	public void setClearPublicKey(boolean clearPublicKey) {
		this.clearPublicKey = clearPublicKey;
	}

	public boolean isClearCaCert() {
		return clearCaCert;
	}

	public void setClearCaCert(boolean clearCaCert) {
		this.clearCaCert = clearCaCert;
	}


	public boolean isChained() {
		return chained;
	}

	public void setChained(boolean chained) {
		this.chained = chained;
	}

	public String getNextAuthProviderId() {
		return nextAuthProviderId;
	}

	public void setNextAuthProviderId(String nextAuthProviderId) {
		this.nextAuthProviderId = nextAuthProviderId;
	}

	public boolean isSupportsCertAuth() {
		return supportsCertAuth;
	}

	public void setSupportsCertAuth(boolean supportsCertAuth) {
		this.supportsCertAuth = supportsCertAuth;
	}

	public String getCertRegex() {
		return certRegex;
	}

	public void setCertRegex(String certRegex) {
		this.certRegex = certRegex;
	}

	public String getCertGroovyScript() {
		return certGroovyScript;
	}

	public void setCertGroovyScript(String certGroovyScript) {
		this.certGroovyScript = certGroovyScript;
	}

	public String getCaValidateGroovyScript() {
		return caValidateGroovyScript;
	}

	public void setCaValidateGroovyScript(String caValidateGroovyScript) {
		this.caValidateGroovyScript = caValidateGroovyScript;
	}

	public String getDerPath() {
		return derPath;
	}

	public void setDerPath(String derPath) {
		this.derPath = derPath;
	}
}
