<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.openiam.com/tags/openiam" prefix="openiam" %>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${titleOrganizatioName} - <fmt:message key="openiam.ui.sourceadapter.log.viewer"/></title>
    <link href="/openiam-ui-static/css/common/style.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/openiam-ui-static/css/common/style.client.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/jquery/css/smoothness/jquery-ui-1.12.1.custom.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/js/common/plugins/multiselect/jquery.multiselect.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/js/webconsole/plugins/usersearch/user.search.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/plugins/tablesorter/themes/yui/style.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/plugins/entitlementstable/entitlements.table.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/plugins/datetimepicker/jquery-ui-timepicker-addon.css" rel="stylesheet"
          type="text/css"/>
    <openiam:overrideCSS/>
    <script type="text/javascript" src="/openiam-ui-static/_dynamic/openiamResourceBundle.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-3.3.1.min.js"></script><script src="/openiam-ui-static/js/common/jquery/jquery.browser.min.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-ui-1.12.1.custom.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/json/json.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/js/common/plugins/multiselect/jquery.multiselect.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter-2.0.3.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter.filer.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter.pager.js"></script>

    <script type="text/javascript" src="/openiam-ui-static/js/common/plugins/modalEdit/modalEdit.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/js/common/plugins/entitlementstable/entitlements.table.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/plugins/datetimepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/log/sourceadapterlog.search.js"></script>
    <script type="text/javascript">
        OPENIAM = window.OPENIAM || {};
        OPENIAM.ENV = window.OPENIAM.ENV || {};
        OPENIAM.ENV.MenuTree = <c:choose><c:when test="${! empty requestScope.menuTree}">${requestScope.menuTree}</c:when><c:otherwise>null</c:otherwise></c:choose>;
    </script>
</head>
<body>
<div id="title" class="title">
    <fmt:message key="openiam.ui.audit.log.records"/>
</div>
<div class="frameContentDivider">
    <form method="post" action="" id="userSearchForm">
        <table width="100%" cellspacing="1" id="userSearchFormTable" class="yui">
            <thead>
            <tr>
                <td colspan="2">
                    <div style="position:relative;" class="info text-center"><fmt:message
                            key="openiam.ui.search.dialog.searchfilter.request"/></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="hidden" id="userid" value="${userId}"/>
                    <div style="width: 100%;">
                        <div id="login-div" class="userSearchCell">
                            <label for="login"><fmt:message key="openiam.idp.login"/></label>
                            <input id="login" name="login" type="text" class="full rounded">
                        </div>
                        <div id="text-div" class="userSearchCell">
                            <label fxr="text"><fmt:message key="openiam.ui.sourceadapter.xml.text"/></label>
                            <input id="text" name="text" class="full rounded">
                        </div>
                        <div id="action-div" class="userSearchCell">
                            <label for="result"><fmt:message key="openiam.ui.audit.log.result"/></label>
                            <select name="result" id="result" class="full rounded">
                                <option value=""><fmt:message key="openiam.ui.common.please.select"/></option>
                                <c:forEach items="${metaData.auditTargetStatus}" var="item">
                                    <option value="${item.id}">${item.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </td>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td class="filter" colspan="2">
                    <input type="submit" class="redBtn" id="searchLogs" autocomplete="off"
                           value="<fmt:message key='openiam.ui.common.search' />">
                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>
<div id="entitlementsContainer"></div>
</body>
</html>