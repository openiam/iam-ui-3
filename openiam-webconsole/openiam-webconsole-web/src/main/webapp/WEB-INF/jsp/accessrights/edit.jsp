<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.openiam.com/tags/openiam" prefix="openiam" %>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>

<c:set var="pageTitle">
    <c:choose>
        <c:when test="${! empty requestScope.dto.id}">
            <fmt:message key="openiam.ui.access.edit.title"/>: ${requestScope.dto.name}
        </c:when>
        <c:otherwise>
            <fmt:message key="openiam.ui.access.new.title"/>
        </c:otherwise>
    </c:choose>
</c:set>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${titleOrganizatioName} - ${pageTitle}</title>
    <link href="/openiam-ui-static/css/common/style.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/openiam-ui-static/css/common/style.client.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/jquery/css/smoothness/jquery-ui-1.12.1.custom.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/plugins/multiselect/css/multiselect.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/plugins/tiptip/tipTip.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/plugins/tablesorter/themes/yui/style.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/plugins/entitlementstable/entitlements.table.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/js/common/plugins/modalsearch/modal.search.css" rel="stylesheet" type="text/css"/>
    <openiam:overrideCSS/>

    <script type="text/javascript" src="/openiam-ui-static/_dynamic/openiamResourceBundle.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-3.3.1.min.js"></script><script src="/openiam-ui-static/js/common/jquery/jquery.browser.min.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-ui-1.12.1.custom.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/json/json.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js"></script>

    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter-2.0.3.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/multiselect/js/multiselect.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter.filer.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter.pager.js"></script>

    <script type="text/javascript" src="/openiam-ui-static/js/common/plugins/language/language.admin.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/webconsole/accessrights/access.right.edit.js"></script>
    <script type="text/javascript">
        OPENIAM = window.OPENIAM || {};
        OPENIAM.ENV = window.OPENIAM.ENV || {};
        OPENIAM.ENV.MenuTree = <c:choose><c:when test="${! empty requestScope.menuTree}">${requestScope.menuTree}</c:when><c:otherwise>null</c:otherwise></c:choose>;
        OPENIAM.ENV.PK = <c:choose><c:when test="${! empty requestScope.dto.id}">"${requestScope.dto.id}"
        </c:when><c:otherwise>null</c:otherwise></c:choose>;
        OPENIAM.ENV.MenuTreeAppendURL = <c:choose><c:when test="${! empty requestScope.dto.id}">"id=${requestScope.dto.id}"
        </c:when><c:otherwise>null</c:otherwise></c:choose>;
    </script>
</head>
<body>
<div id="title" class="title">${pageTitle}</div>
<div class="frameContentDivider">
    <form id="resourceForm" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <table cellpadding="8px" align="center" class="fieldset">
            <tbody>
            <tr>
                <td colspan="2">
                    <h3><spring:message code='openiam.ui.access.right.warning'/></h3>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="languageMap" class="required">

                    </label>
                </td>
                <td>
                    <div id="languageMap"></div>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="name" class="required">
                        <spring:message code='openiam.ui.common.display.name.cur'/>
                    </label>
                </td>
                <td>
                    <input id="name" autocomplete="off" type="text" class="full rounded"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="mdType1">
                        <spring:message code='openiam.ui.user.object.class'/> 1
                    </label>
                </td>
                <td>
                    <select id="mdType1" name="mdType1">
                        <option value=""><spring:message code='openiam.ui.selfservice.text.pleaseselect'/></option>
                        <c:forEach var="type" items="${requestScope.types}">
                            <option value="${type.id}">${type.name}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="mdType2">
                        <spring:message code='openiam.ui.user.object.class'/> 2
                    </label>
                </td>
                <td>


                    <select id="mdType2" name="mdType2">
                        <option value=""><spring:message code='openiam.ui.selfservice.text.pleaseselect'/></option>
                        <c:forEach var="type" items="${requestScope.types}">
                            <option value="${type.id}">${type.name}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
        <table width="100%">
            <tfoot>
            <tr>
                <td colspan="2">
                    <ul class="formControls">
                        <li class="leftBtn">
                            <a href="javascript:void(0)">
                                <input id="save" type="submit" class="redBtn"
                                       value="<fmt:message key="openiam.ui.common.save" />"/>
                            </a>
                        </li>
                        <li class="leftBtn">
                            <a href="accessRights.html" class="whiteBtn">
                                <fmt:message key="openiam.ui.common.cancel"/>
                            </a>
                        </li>
                        <c:if test="${! requestScope.dto.id}">
                            <li class="rightBtn">
                                <a id="deleteAccessRight" href="javascript:void(0);" class="redBtn">
                                    <fmt:message key="openiam.ui.common.delete"/>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>
<div id="editDialog"></div>
</body>
</html>