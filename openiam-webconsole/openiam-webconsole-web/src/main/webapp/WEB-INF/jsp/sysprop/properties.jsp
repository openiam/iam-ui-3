<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.openiam.com/tags/openiam" prefix="openiam" %>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${titleOrganizatioName} - <fmt:message key="openiam.ui.sourceadapter.log.viewer"/></title>
    <link href="/openiam-ui-static/css/common/style.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/openiam-ui-static/css/common/style.client.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/jquery/css/smoothness/jquery-ui-1.12.1.custom.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/js/common/plugins/multiselect/jquery.multiselect.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/js/webconsole/plugins/usersearch/user.search.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/plugins/tablesorter/themes/yui/style.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/plugins/entitlementstable/entitlements.table.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/plugins/datetimepicker/jquery-ui-timepicker-addon.css" rel="stylesheet"
          type="text/css"/>
    <openiam:overrideCSS/>
    <script type="text/javascript" src="/openiam-ui-static/_dynamic/openiamResourceBundle.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-3.3.1.min.js"></script><script src="/openiam-ui-static/js/common/jquery/jquery.browser.min.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-ui-1.12.1.custom.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/json/json.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/js/common/plugins/multiselect/jquery.multiselect.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter-2.0.3.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter.filer.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter.pager.js"></script>

    <script type="text/javascript" src="/openiam-ui-static/js/common/plugins/modalEdit/modalEdit.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/js/common/plugins/entitlementstable/entitlements.table.js"></script>
    <script type="text/javascript"
            src="/openiam-ui-static/plugins/datetimepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript">
        OPENIAM = window.OPENIAM || {};
        OPENIAM.ENV = window.OPENIAM.ENV || {};
        OPENIAM.ENV.MenuTree = <c:choose><c:when test="${! empty requestScope.menuTree}">${requestScope.menuTree}</c:when><c:otherwise>null</c:otherwise></c:choose>;
    </script>
    <script>
        $(document).ready(function () {
            $('.div-edit').hide();
            $('.save-button').hide();
            $('.edit-button').each(function (ind, item) {
                $(item).on('click', function () {
                    $(item).next().show();
                    $(item).hide();
                    var parent = $(item).parent().parent().children('.value-td');
                    parent.children('.div-edit').show();
                    parent.children('.value-view').hide();
                });
            });
            $('.save-button').each(function (ind, item) {
                $(item).on('click', function () {
                    $(item).prev().show();
                    $(item).hide();

                    var parent = $(item).parent().parent().children('.value-td');
                    parent.children('.div-edit').hide();
                    parent.children('.value-view').show();
                    var name = parent.prev().prev().text();
                    var val = parent.children('.div-edit').children('.value-edit').val();
                    saveSysProp({name: name, value: val});
                    parent.children('.value-view').text(val);
                    //TODO CALL SAVE method
                });
            });
            function saveSysProp(data) {
                $.ajax({
                    url: "property.html",
                    data: JSON.stringify(data),
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json",
                    success: function (data, textStatus, jqXHR) {
                        if (data.status == 200) {
                            OPENIAM.Modal.Success({
                                message: data.successMessage, showInterval: 2000, onIntervalClose: function () {

                                }
                            });
                        } else {
                            OPENIAM.Modal.Error({errorList: data.errorList});
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
                    }
                });
            }
        });
    </script>
</head>
<body>
<div id="title" class="title">
    <fmt:message key="openiam.ui.system.properties"/>
</div>
<div class="frameContentDivider">
    <form method="post" action="" id="userSearchForm">
        <table width="100%" cellspacing="1" id="userSearchFormTable" class="yui">
            <thead>
            <tbody>
            <c:forEach items="${properties}" var="property">
                <tr class="property" beanId="${property.name}">
                    <td>${property.name}</td>
                    <td>${property.mdTypeName}</td>
                    <td class="value-td">
                        <div class="value-view">${property.value}</div>
                        <div class="div-edit">
                            <c:choose>
                                <c:when test="${('SOURCE_ADAPTER_PROP' eq property.mdTypeId) and ('MODE' eq property.name)}">
                                    <select class="value-edit full rounded">
                                        <c:forEach items="${sourceAdapterValues}" var="val">
                                            <option value="${val}">${val}</option>
                                        </c:forEach>
                                    </select>
                                </c:when>
                                <c:when test="${('USER_SEARCH_PROP' eq property.mdTypeId) and ('DEFAULT_MATCH_TYPE' eq property.name)}">
                                    <select class="value-edit full rounded">
                                        <c:forEach items="${matchTypeVlues}" var="val">
                                            <option value="${val}">${val}</option>
                                        </c:forEach>
                                    </select>
                                </c:when>
                                <c:otherwise>
                                    <input class="value-edit full rounded" type="text" value="${property.value}">
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </td>
                    <td><a href="#" class="edit-button redBtn">Edit</a>
                        <a href="#" class="save-button redBtn">Save</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </form>
</div>
</body>
</html>