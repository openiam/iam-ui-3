package org.openiam.ui.rest.api.mvc;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.base.ws.Response;
import org.openiam.base.ws.ResponseCode;
import org.openiam.idm.searchbeans.AccessRightSearchBean;
import org.openiam.idm.srvc.access.dto.AccessRight;
import org.openiam.idm.srvc.access.service.AccessRightDataService;
import org.openiam.ui.rest.api.model.AccessRightBean;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.model.BeanResponse;
import org.openiam.ui.web.mvc.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/accessrights")
public class AccessRightRestController extends AbstractController {

    @Resource(name = "accessRightServiceClient")
    AccessRightDataService accessRightServiceClient;

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public
    @ResponseBody
    AccessRight getNew() {
        return new AccessRight();
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    AccessRight get(final HttpServletRequest request,
                    final HttpServletResponse response,
                    final @PathVariable("id") String id) throws IOException {
        final AccessRight dto = accessRightServiceClient.get(id);
        if (dto == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        } else {
            return dto;
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public
    @ResponseBody
    BasicAjaxResponse save(final HttpServletRequest request, final @RequestBody AccessRight dto) {
        final BasicAjaxResponse response = new BasicAjaxResponse();
        Errors error = null;
        String requesterId = getRequesterId(request);
        try {
            if (StringUtils.isBlank(dto.getMetadataType1())) {
                dto.setMetadataType1(null);
            }
            if (StringUtils.isBlank(dto.getMetadataType2())) {
                dto.setMetadataType2(null);
            }
            AccessRightSearchBean arsb = new AccessRightSearchBean();
            arsb.setName(dto.getName());
            List<AccessRight> lar = accessRightServiceClient.findBeans(arsb, 0, 2, null);
            if ((lar == null) || (lar.size() == 0) || (lar.get(0).getId().equals(dto.getId()) && lar.size() == 1)) {
                final Response wsResponse = accessRightServiceClient.save(dto, requesterId);
                if (wsResponse == null || !wsResponse.isSuccess()) {
                    if (ResponseCode.INVALID_ARGUMENTS.equals(wsResponse.getErrorCode())) {
                        error = Errors.COULD_NOT_SAVE_ACCESS_RIGHT_MDTYPES;
                    } else {
                        error = Errors.COULD_NOT_SAVE_ACCESS_RIGHT;
                    }

                }
                response.setObjectId((String) wsResponse.getResponseValue());
            } else {
                error = Errors.COULD_NOT_SAVE_ACCESS_RIGHT_NAME_EXIST;
            }
        } catch (Throwable e) {
            error = Errors.COULD_NOT_SAVE_ACCESS_RIGHT;
            log.error("Can't save object", e);
        } finally {
            if (error != null) {
                response.addError(new ErrorToken(error));
                response.setStatus(500);
            } else {
                response.setSuccessToken(new SuccessToken(SuccessMessage.ACCESS_RIGHT_SAVED));
                response.setStatus(200);
            }
            response.process(localeResolver, messageSource, request);
        }
        return response;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public
    @ResponseBody
    BasicAjaxResponse save(final HttpServletRequest request, final @PathVariable("id") String id) {
        final BasicAjaxResponse response = new BasicAjaxResponse();
        Errors error = null;
        String requesterId = getRequesterId(request);
        try {
            final Response wsResponse = accessRightServiceClient.delete(id, requesterId);
            if (wsResponse == null || !wsResponse.isSuccess()) {
                error = Errors.COULD_NOT_DELETE_ACCESS_RIGHT;
            }
        } catch (Throwable e) {
            error = Errors.COULD_NOT_DELETE_ACCESS_RIGHT;
            log.error("Can't save object", e);
        } finally {
            if (error != null) {
                response.addError(new ErrorToken(error));
                response.setStatus(500);
            } else {
                response.setRedirectURL("accessRights.html");
                response.setSuccessToken(new SuccessToken(SuccessMessage.ACCESS_RIGHT_DELETED));
                response.setStatus(200);
            }
            response.process(localeResolver, messageSource, request);
        }
        return response;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public
    @ResponseBody
    BeanResponse search(final HttpServletRequest request,
                        final HttpServletResponse response,
                        final @RequestParam(required = false, value = "mdType1") String mdType1,
                        final @RequestParam(required = false, value = "mdType2") String mdType2,
                        final @RequestParam(required = true, value = "size") int size,
                        final @RequestParam(required = true, value = "from") int from) {
        final AccessRightSearchBean searchBean = new AccessRightSearchBean();
        searchBean.setMetadataTypeId1(mdType1);
        searchBean.setMetadataTypeId2(mdType2);
        final List<AccessRight> dtos = accessRightServiceClient.findBeans(searchBean, from, size, getCurrentLanguage());
        final List<AccessRightBean> beans = mapper.mapToList(dtos, AccessRightBean.class);
        if (CollectionUtils.isNotEmpty(beans)) {
            final int count = accessRightServiceClient.count(searchBean);
            return new BeanResponse(beans, count);
        } else {
            return BeanResponse.EMPTY_RESPONSE;
        }
    }


}
