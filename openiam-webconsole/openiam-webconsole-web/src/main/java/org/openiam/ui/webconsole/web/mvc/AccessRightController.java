package org.openiam.ui.webconsole.web.mvc;

import org.openiam.idm.srvc.access.dto.AccessRight;
import org.openiam.idm.srvc.access.service.AccessRightDataService;
import org.openiam.idm.srvc.meta.domain.MetadataTypeGrouping;
import org.openiam.ui.rest.api.model.KeyNameBean;
import org.openiam.ui.web.mvc.AbstractController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AccessRightController extends AbstractController {
    @Resource(name = "accessRightServiceClient")
    AccessRightDataService accessRightServiceClient;

    @Value("${org.openiam.ui.access.right.menu.id}")
    private String rootMenu;

    @RequestMapping(value = "/accessRights", method = RequestMethod.GET)
    public String accessRights(final HttpServletRequest request) {
        setMenuTree(request, rootMenu);
        request.setAttribute("types", getPossibleMetadataTypes());
        return "accessrights/search";
    }

    private List<KeyNameBean> getPossibleMetadataTypes() {
        List<KeyNameBean> retVal = new ArrayList<>();
        retVal.addAll(this.getMetadataTypesByGroupingAsKeyNameBeans(MetadataTypeGrouping.RESOURCE_TYPE));
        retVal.addAll(this.getMetadataTypesByGroupingAsKeyNameBeans(MetadataTypeGrouping.ROLE_TYPE));
        return retVal;
    }

    @RequestMapping(value = "/editAccessRight", method = RequestMethod.GET)
    public String edit(final HttpServletRequest request,
                       final HttpServletResponse response,
                       final @RequestParam(required = false, value = "id") String id) throws IOException {
        AccessRight dto = new AccessRight();
        if (id != null) {
            dto = accessRightServiceClient.get(id);
            if (dto == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            }
        }
        setMenuTree(request, rootMenu);
        request.setAttribute("dto", dto);
        request.setAttribute("types", getPossibleMetadataTypes());
        return "accessrights/edit";
    }

    @RequestMapping(value = "/newAccessRight", method = RequestMethod.GET)
    public String newAccessRight(final HttpServletRequest request,
                       final HttpServletResponse response,
                       final @RequestParam(required = false, value = "id") String id) throws IOException {
        AccessRight dto = new AccessRight();
        setMenuTree(request, rootMenu);
        request.setAttribute("dto", dto);
        request.setAttribute("types", getPossibleMetadataTypes());
        return "accessrights/edit";
    }
}
