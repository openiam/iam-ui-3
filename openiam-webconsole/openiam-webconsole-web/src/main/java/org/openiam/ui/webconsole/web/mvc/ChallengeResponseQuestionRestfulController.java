package org.openiam.ui.webconsole.web.mvc;

import org.apache.commons.lang.StringUtils;
import org.openiam.base.OrderConstants;
import org.openiam.base.ws.SortParam;
import org.openiam.idm.searchbeans.IdentityQuestionSearchBean;
import org.openiam.idm.srvc.pswd.dto.IdentityQuestion;
import org.openiam.idm.srvc.pswd.service.ChallengeResponseWebService;
import org.openiam.ui.rest.api.model.IdentityQuestionBean;
import org.openiam.ui.web.mvc.AbstractController;
import org.openiam.ui.webconsole.web.model.IdentityQuestionSearchResultBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

@Controller
public class ChallengeResponseQuestionRestfulController extends AbstractController {

	@Resource(name = "challengeResponseServiceClient")
	private ChallengeResponseWebService challengeResponseServiceClient;
	
	@RequestMapping(value="/searchChallengeResponseQuestions", method=RequestMethod.GET)
	public @ResponseBody IdentityQuestionSearchResultBean searchChallengeResponseQuestions(final HttpServletRequest request,
												   final @RequestParam(required=true, value="from") int from,
												   final @RequestParam(required=true, value="size") int size,
													final @RequestParam(required = false, value = "sortBy") String sortBy,
													final @RequestParam(required = false, value = "orderBy") String orderBy) {
		final IdentityQuestionSearchBean searchBean = new IdentityQuestionSearchBean();
		searchBean.setDeepCopy(false);
		if (StringUtils.isNotBlank(sortBy))
			searchBean.setSortBy(Arrays.asList(new SortParam(OrderConstants.valueOf(orderBy), sortBy)));
		searchBean.setLanguageId(getCurrentLanguage().getId());

		final int count = challengeResponseServiceClient.count(searchBean);
		final List<IdentityQuestion> identityQuestionList = challengeResponseServiceClient.findQuestionBeans(searchBean, from, size, getCurrentLanguage());
		final List<IdentityQuestionBean> identityQuestionBeanList = mapper.mapToList(identityQuestionList, IdentityQuestionBean.class);
		return new IdentityQuestionSearchResultBean(count, identityQuestionBeanList);
	}
}
