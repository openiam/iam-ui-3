package org.openiam.ui.webconsole.web.model;

/**
 * Created by zaporozhec on 6/16/16.
 */
public enum SourceAdapterType {
    SIMULATION, DISABLE, PRODUCTION
}
