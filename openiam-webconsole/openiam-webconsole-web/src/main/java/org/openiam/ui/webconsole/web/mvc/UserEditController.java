package org.openiam.ui.webconsole.web.mvc;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.openiam.base.ws.Response;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.idm.srvc.continfo.domain.EmailEntity;
import org.openiam.idm.srvc.key.ws.KeyManagementWS;
import org.openiam.idm.srvc.msg.service.MailService;
import org.openiam.idm.srvc.org.dto.Organization;
import org.openiam.idm.srvc.org.dto.OrganizationType;
import org.openiam.idm.srvc.user.domain.UserAttributeEntity;
import org.openiam.idm.srvc.user.dto.ProfilePicture;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.idm.srvc.user.dto.UserAttribute;
import org.openiam.provision.dto.ProvisionActionEnum;
import org.openiam.provision.dto.ProvisionActionEvent;
import org.openiam.provision.dto.ProvisionActionTypeEnum;
import org.openiam.provision.service.ActionEventBuilder;
import org.openiam.provision.service.ProvisionServiceEventProcessor;
import org.openiam.ui.rest.api.model.EditUserModel;
import org.openiam.ui.rest.api.model.KeyNameBean;
import org.openiam.ui.rest.api.model.KeyNameOperationBean;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.util.DateFormatStr;
import org.openiam.ui.webconsole.web.model.UserEmailFormBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserEditController extends BaseUserController {

    @Value("${org.openiam.ui.user.manager.resend.email.button}")
    private boolean isResendButton;

    @Resource(name = "mailServiceClient")
    private MailService mailService;

    @RequestMapping(value = "/resetSecurityQuestions", method = RequestMethod.POST)
    public
    @ResponseBody
    BasicAjaxResponse resetSecurityQuestions(final HttpServletRequest request, final @RequestParam(value = "id", required = true) String userId) {
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();

        ErrorToken errorToken = null;
        SuccessToken successToken = null;
        Response wsResponse = null;

        ActionEventBuilder eventBuilder = new ActionEventBuilder(
                userId,
                getRequesterId(request),
                ProvisionActionEnum.RESET_SECURITY_QUESTIONS);
        ProvisionActionEvent actionEvent = eventBuilder.build();
        Response response = provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.PRE);
        if (ProvisionServiceEventProcessor.BREAK.equalsIgnoreCase((String) response.getResponseValue())) {
            return genAbortAjaxResponse(response, localeResolver.resolveLocale(request));
        }

        try {
            wsResponse = challengeResponseService.resetQuestionsForUser(userId);
            successToken = new SuccessToken(SuccessMessage.SECURITY_QUESTIONS_RESET);
        } catch (Throwable e) {
            errorToken = new ErrorToken(Errors.INTERNAL_ERROR);
            log.error("Exception while resetting security questions", e);
        } finally {
            if (errorToken != null) {
                ajaxResponse.setStatus(500);
                errorToken.setMessage(messageSource.getMessage(
                        errorToken.getError().getMessageName(),
                        errorToken.getParams(),
                        localeResolver.resolveLocale(request)));
                ajaxResponse.addError(errorToken);

            } else {
                ajaxResponse.setStatus(200);
                if (successToken != null) {
                    provisionService.addEvent(actionEvent, ProvisionActionTypeEnum.POST);
                    ajaxResponse.setSuccessToken(successToken);
                    ajaxResponse.setSuccessMessage(messageSource.getMessage(
                            successToken.getMessage().getMessageName(),
                            null,
                            localeResolver.resolveLocale(request)));
                }
            }
        }
        return ajaxResponse;
    }

    @RequestMapping(value = "/newUser", method = RequestMethod.GET)
    public String getNewUserPage(final HttpServletRequest request,
                                 final HttpServletResponse response,
                                 final Model model) throws IOException {
        return getEditPage(request, response, model, null);
    }

    @RequestMapping(value = "/resendEmail", method = RequestMethod.POST)
    public
    @ResponseBody
    BasicAjaxResponse resendEmail(final HttpServletRequest request, final @RequestParam(value = "id", required = true) String userId,
                                  final @RequestParam(value = "cc", required = false) String cc,
                                  final @RequestParam(value = "attrCnt", required = true) String emailId) {
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        SuccessToken successToken = new SuccessToken(SuccessMessage.RESEND_EMAIL_SUCCESS);
        ajaxResponse.setSuccessToken(successToken);
        ajaxResponse.setSuccessMessage(messageSource.getMessage(
                successToken.getMessage().getMessageName(),
                null,
                localeResolver.resolveLocale(request)));
        ajaxResponse.setStatus(200);

        Response resp = userDataWebService.resendEmail(emailId, cc);
        if (resp.getStatus() == ResponseStatus.FAILURE) {
            ajaxResponse.setStatus(500);
            ErrorToken errorToken = new ErrorToken(Errors.COULD_NOT_SEND_EMAIL);
            errorToken.setMessage(messageSource.getMessage(
                    errorToken.getError().getMessageName(),
                    errorToken.getParams(), localeResolver.resolveLocale(request)));
            ajaxResponse.addError(errorToken);
        }

        return ajaxResponse;
    }

    @RequestMapping(value = "/editUser", method = RequestMethod.GET)
    public String getEditPage(final HttpServletRequest request,
                              final HttpServletResponse response, Model model,
                              final @RequestParam(required = true, value = "id") String userId)
            throws IOException {
        String requesterId = getRequesterId(request);
        EditUserModel userModel = new EditUserModel();
        final List<List<OrganizationType>> orgTypes = getOrgTypeList();
        if (StringUtils.isNotBlank(userId)) {

            final User user = userDataWebService.getUserWithDependent(userId,
                    getRequesterId(request), false);
            if (user == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, String
                        .format("User with id '%s' does not exist", userId));
                return null;
            }
            ProfilePicture profilePic = userDataWebService.getProfilePictureByUserId(user.getId(), requesterId);
            if (profilePic != null && profilePic.getPicture() != null) {
                ImageInputStream iis = ImageIO.createImageInputStream(new ByteArrayInputStream(profilePic.getPicture()));
                Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);
                if (iter.hasNext()) {
                    ImageReader ir = iter.next();
                    String format = ir.getFormatName();
                    String imgSrc = profilePic.getUser().getId() + "." + format.toLowerCase().replace("jpeg", "jpg");
                    model.addAttribute("profilePicSrc", imgSrc);
                }
            }
            userModel = mapper.mapToObject(user, EditUserModel.class);
            setSupervisorInfo(userModel);
            setAlternateContactInfo(userModel);
            setUserModelOrg(userModel.getId(), userModel);
            userModel.formatDates();
            setMenuTree(request, userEditRootMenuId);
        } else {
            setMenuTree(request, userRootMenuId);
            model.addAttribute("provisionFlag", provisionServiceFlag);
        }

        model.addAttribute("orgHierarchy",
                (orgTypes != null) ? jacksonMapper.writeValueAsString(orgTypes)
                        : null);
        model.addAttribute("objClassList", getUserTypes());
        model.addAttribute("userSubTypeList", getUserSubTypes());
        model.addAttribute("emailTypeList", getEmailTypes());
        model.addAttribute("addressTypeList", getAddressTypes());
        model.addAttribute("phoneTypeList", getPhoneTypes());
        model.addAttribute("user", userModel);
        model.addAttribute("jobList", getJobCodeList());
        model.addAttribute("employeeTypeList", getUserTypeList());
        model.addAttribute("dateFormatDP", DateFormatStr.getDpDate());
        model.addAttribute("defaultAffiliationType", affiliationDefaultTypeId);
        model.addAttribute("primaryAffiliationType", affiliationPrimaryTypeId);
        model.addAttribute("userActivationLink", userActivationLink);
        model.addAttribute("orgList", (userModel.getOrganizationIds() != null) ?
                jacksonMapper.writeValueAsString(userModel.getOrganizationIds()) : null);

        List<EmailEntity> eeList = mailService.getEmailsForUser(userModel.getId(), 0, Integer.MAX_VALUE);
        List<UserEmailFormBean> emailList = new ArrayList<UserEmailFormBean>();
        boolean hasEmailForm = false;
        if (eeList != null) {
            for (EmailEntity em : eeList) {
                hasEmailForm = true;
                if (em.getEmailBody().length() > 0) {
                    emailList.add(new UserEmailFormBean(em.getEmailId(), em.getSubject(), em.getAddress()));
                }
            }
        }
        model.addAttribute("emailList", emailList);
        model.addAttribute("isResendButton", (isResendButton && hasEmailForm));
        if (userModel.getLastDate() != null)
            model.addAttribute("isResetLastDate", true);
        if (userModel.getStartDate() != null)
            model.addAttribute("isResetStartDate", true);
        final String menuString = getInitialMenuAsJson(request, "USER_INFO_PAGE");
        model.addAttribute("initialMenu", menuString);
        model.addAttribute("singleSearchGroup", singleSearchGroup);
        model.addAttribute("singleSearchRole", singleSearchRole);
        return "users/editUser";
    }

    private void setSupervisorInfo(EditUserModel userModel) {
        // get supervisor information
        User supVisor = userDataWebService.getPrimarySupervisor(userModel
                .getId());
        if (supVisor != null) {
            KeyNameOperationBean supVis = new KeyNameOperationBean();
            supVis.setId(supVisor.getId());
            supVis.setName(supVisor.getFirstName() + " "
                    + supVisor.getLastName());
            userModel.getSupervisors().add(supVis);
            userModel.setSupervisorId(supVisor.getId());
            userModel.setSupervisorName(supVisor.getFirstName() + " "
                    + supVisor.getLastName());
        }
    }

    private void setAlternateContactInfo(EditUserModel userModel) {
        // get the alternate contact name:
        if (userModel.getAlternateContactId() != null
                && userModel.getAlternateContactId().length() > 0) {
            User altUser = userDataWebService.getUserWithDependent(
                    userModel.getAlternateContactId(), null, false);
            if (altUser != null) {
                userModel.setAlternateContactName(altUser.getFirstName() + " "
                        + altUser.getLastName());
            }
        }
    }

    private BasicAjaxResponse genAbortAjaxResponse(Response response, Locale locale) {
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        if (response.isSuccess()) {
            SuccessToken successToken = new SuccessToken(SuccessMessage.OPERATION_ABORTED);
            ajaxResponse.setStatus(200);
            ajaxResponse.setSuccessToken(successToken);
            ajaxResponse.setSuccessMessage(messageSource.getMessage(
                    successToken.getMessage().getMessageName(),
                    null, locale));
        } else {
            ajaxResponse.setStatus(500);
            ErrorToken errorToken = new ErrorToken(Errors.OPERATION_ABORTED);
            errorToken.setMessage(messageSource.getMessage(
                    errorToken.getError().getMessageName(),
                    errorToken.getParams(), locale));
            ajaxResponse.addError(errorToken);
        }
        return ajaxResponse;
    }

    private void setUserModelOrg(String userId, EditUserModel userModel) {
        List<Organization> organizations = organizationDataService.getOrganizationsForUserLocalized(userId, null, -1, -1, getCurrentLanguage());
        if (organizations != null && organizations.size() > 0) {
            for (Organization org : organizations) {
                userModel.getOrganizationIds().add(new KeyNameBean(org.getId(), org.getName()));
            }
        }
    }
}
