package org.openiam.ui.webconsole.web.model;

import org.openiam.idm.srvc.auth.dto.LoginStatusEnum;
import org.openiam.idm.srvc.auth.dto.ProvLoginStatusEnum;

import java.io.Serializable;

public class MngSysViewerCommand implements Serializable {

    String managedSysId;
    String managedSysName;
    String managedSysStatus;
    String principalId;
    String principalName;
    int isPrincipalLocked;
    ProvLoginStatusEnum principalStatus;

    public String getManagedSysId() {
        return managedSysId;
    }

    public void setManagedSysId(String managedSysId) {
        this.managedSysId = managedSysId;
    }

    public String getManagedSysName() {
        return managedSysName;
    }

    public void setManagedSysName(String managedSysName) {
        this.managedSysName = managedSysName;
    }

    public String getManagedSysStatus() {
        return managedSysStatus;
    }

    public void setManagedSysStatus(String managedSysStatus) {
        this.managedSysStatus = managedSysStatus;
    }

    public String getPrincipalId() {
        return principalId;
    }

    public void setPrincipalId(String principalId) {
        this.principalId = principalId;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public int getIsPrincipalLocked() {
        return isPrincipalLocked;
    }

    public void setIsPrincipalLocked(int isPrincipalLocked) {
        this.isPrincipalLocked = isPrincipalLocked;
    }

    public ProvLoginStatusEnum getPrincipalStatus() {
        return principalStatus;
    }

    public void setPrincipalStatus(ProvLoginStatusEnum principalStatus) {
        this.principalStatus = principalStatus;
    }
}
