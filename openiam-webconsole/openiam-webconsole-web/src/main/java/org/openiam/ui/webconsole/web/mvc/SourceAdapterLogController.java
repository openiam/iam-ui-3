package org.openiam.ui.webconsole.web.mvc;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.authmanager.common.model.AuthorizationMenu;
import org.openiam.base.OrderConstants;
import org.openiam.base.ws.SortParam;
import org.openiam.idm.searchbeans.AuditLogSearchBean;
import org.openiam.idm.srvc.audit.constant.AuditAction;
import org.openiam.idm.srvc.audit.dto.IdmAuditLog;
import org.openiam.idm.srvc.audit.dto.IdmAuditLogCustom;
import org.openiam.provision.dto.srcadapter.SourceAdapterRequest;
import org.openiam.provision.service.SourceAdapter;
import org.openiam.ui.rest.api.model.AuditLogBean;
import org.openiam.ui.rest.api.model.AuditLogSearchFilter;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.model.BeanResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Controller
public class SourceAdapterLogController extends AbstractWebconsoleController {

    @Resource(name = "sourceAdapterClient")
    protected SourceAdapter sourceAdapter;

    @Value("${org.openiam.ui.landingpage.user.edit.root.id}")
    protected String userEditRootMenuId;

    @RequestMapping("/viewAuditSourceAdapter")
    public String list(Model model, final HttpServletRequest request, @RequestParam(required = false, value = "id") String userId) {

        final AuditLogSearchFilter metaData = new AuditLogSearchFilter();
        metaData.setAuditTargetStatus(getStatusCodeTypeKeyNames());
        model.addAttribute("metaData", metaData);
        model.addAttribute("userId", userId);
        putMenuTree(request, userId);
        return "log/sourceAdapterViewer";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/sourceAdapterSearch")
    public
    @ResponseBody
    BeanResponse search(Model model,
                        @RequestParam(required = false, value = "from") int from,
                        @RequestParam(required = false, value = "size") int size,
                        @RequestParam(required = false, value = "result") String res,
                        @RequestParam(required = false, value = "id") String userId,
                        @RequestParam(required = false, value = "login") String login,
                        @RequestParam(required = false, value = "text") String text) throws Exception {


        //first Call
        AuditLogSearchBean auditLogSearchBean = new AuditLogSearchBean();
        auditLogSearchBean.setDeepCopy(false);
        auditLogSearchBean.setFindInCache(true);
        auditLogSearchBean.setAction(AuditAction.SOURCE_ADAPTER_CALL.value());
        //FIXME
        //auditLogSearchBean.setSortBy(Arrays.asList(new SortParam()));

        if (from == 0 && size == 0) {
            size = 10;
        }

        if (StringUtils.isNotBlank(userId)) {
            auditLogSearchBean.setUserId(userId);
        }

        if (StringUtils.isNotBlank(login)) {
            auditLogSearchBean.setLogin(login);
        }

        if (StringUtils.isNotBlank(text)) {
            auditLogSearchBean.setAttributeName("Request XML");
            auditLogSearchBean.setAttributeValue(text);
        }

        if (StringUtils.isNotBlank(res)) {
            auditLogSearchBean.setResult(res);
        }

        auditLogSearchBean.addSortParam(new SortParam(OrderConstants.DESC, "timestamp"));
        final int count = auditLogService.count(auditLogSearchBean);
        List<IdmAuditLog> result = auditLogService.findBeans(auditLogSearchBean, from, size);
        List<AuditLogBean> auditLogBeans = new LinkedList<>();
        if (CollectionUtils.isNotEmpty(result)) {
            for (IdmAuditLog logId : result) {
                AuditLogBean logBean = new AuditLogBean(logId);
                auditLogBeans.add(logBean);
            }
        }
        return new BeanResponse(auditLogBeans, (count));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/sourceAdapterResend")
    public String post(final HttpServletRequest request, @RequestBody String text) throws Exception {
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        if (StringUtils.isNotBlank(text)) {
            try {
                SourceAdapterRequest sourceAdapterRequest = this.read(text);
                sourceAdapter.perform(sourceAdapterRequest);
                ajaxResponse.setStatus(HttpServletResponse.SC_OK);
                ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.SOURCE_ADAPTER_RESEND));
            } catch (Exception e) {
                ajaxResponse.addError(new ErrorToken(Errors.UNMARSHALLER_ERROR, e.getMessage()));
                ajaxResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        } else {
            ajaxResponse.addError(new ErrorToken(Errors.EMPTY_REQUEST, null));
            ajaxResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

        request.setAttribute("response", ajaxResponse);
        return "common/basic.ajax.response";
    }


    @RequestMapping("/viewSourceAdapterRecord")
    public String get(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final @RequestParam(required = true, value = "id") String id) throws IOException {

        return getForUserCommon(request, response, id, false);

    }

    @RequestMapping("/viewSourceAdapterRecordUser")
    public String getForUser(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final @RequestParam(required = true, value = "id") String id) throws IOException {

        return getForUserCommon(request, response, id, true);

    }

    public String getForUserCommon(
            final HttpServletRequest request,
            final HttpServletResponse response,
            String id, boolean forUser) throws IOException {

        IdmAuditLog log = auditLogService.getLogRecord(id);

        if (log == null) {
            throw new IOException("Wrong Log Id");
        }

        if (CollectionUtils.isNotEmpty(log.getCustomRecords())) {
            Iterator<IdmAuditLogCustom> idmAuditLogCustomIterator = log.getCustomRecords().iterator();
            while (idmAuditLogCustomIterator.hasNext()) {
                IdmAuditLogCustom custom = idmAuditLogCustomIterator.next();
                if ("Request XML".equalsIgnoreCase(custom.getKey())) {
                    request.setAttribute("xml", custom.getValue());
                    idmAuditLogCustomIterator.remove();
                }
            }
        }

        if (forUser) {
            String userId = log.getUserId();
            putMenuTree(request, userId);
        }

        request.setAttribute("log", log);
        return "log/sourceAdapterResender";

    }

    public static SourceAdapterRequest read(String request) throws Exception {
        JAXBContext ctx = JAXBContext.newInstance(SourceAdapterRequest.class);
        Unmarshaller m = ctx.createUnmarshaller();
        return (SourceAdapterRequest) m.unmarshal(new StringReader(soapMessageEscaping(request)));
    }

    private static String soapMessageEscaping(String input) {
        if (input == null)
            return null;

        StringBuilder sb = new StringBuilder(input.length());
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == '&') {
                sb.append("&amp;");
            }
//            else if (c == '"') {
//                sb.append("&quot;");
//            } else if (c == '\'') {
//                sb.append("&apos;");
//            }
            else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    private void putMenuTree(HttpServletRequest request, String userId) {
        if (StringUtils.isNotBlank(userId)) {
            log.info("GetMenu (putMenuTree) for userId:" + userId);
            AuthorizationMenu root = getMenuTree(request, userEditRootMenuId);
            AuthorizationMenu menu = root.getFirstChild();
            menu.setUrlParams("id=" + userId);
            while (menu.getNextSibling() != null) {
                menu = menu.getNextSibling();
                menu.setUrlParams("id=" + userId);
            }
            String menuTreeString = null;
            try {
                menuTreeString = (root != null) ? jacksonMapper.writeValueAsString(root) : null;
                log.info("Menu is:" + menuTreeString);
            } catch (Throwable e) {
                log.error("Can't serialize menu tree", e);
            }
            request.setAttribute("menuTree", menuTreeString);
        }
    }
}
