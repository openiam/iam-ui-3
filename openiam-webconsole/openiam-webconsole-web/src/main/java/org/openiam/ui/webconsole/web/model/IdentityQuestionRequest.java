package org.openiam.ui.webconsole.web.model;


import com.fasterxml.jackson.annotation.JsonProperty;

public class IdentityQuestionRequest {
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("questionText")
	private String questionText;
	
	
	@JsonProperty("active")
	private String active;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getQuestionText() {
		return questionText;
	}


	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}


	public String getActive() {
		return active;
	}


	public void setActive(String active) {
		this.active = active;
	}
	
	
}
