package org.openiam.ui.webconsole.web.mvc;


import org.apache.commons.lang.StringUtils;
import org.openiam.idm.srvc.audit.constant.AuditAction;
import org.openiam.idm.srvc.audit.dto.IdmAuditLog;
import org.openiam.idm.srvc.auth.ws.LoginResponse;
import org.openiam.ui.web.mvc.AbstractChallengeResponseController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ChallengeResponseController extends AbstractChallengeResponseController {

    @Value("${org.openiam.ui.landingpage.user.edit.root.id}")
    protected String userEditRootMenuId;


    @RequestMapping(value = "/challengeQuestRes", method = RequestMethod.GET)
    public String challengeQuestResUnlock(final HttpServletRequest request,
                                          final HttpServletResponse response,
                                          @RequestParam(value = "id", required = true) String userId) throws Exception {
        setMenuTree(request, userEditRootMenuId);
        request.setAttribute("userId", userId);
        return "challengeResponse/unlock";
    }

    @RequestMapping(value = "/challengeQuestResUnlock", method = RequestMethod.POST)
    public String challengeQuestResUnlockPost(final HttpServletRequest request,
                                              final HttpServletResponse response) throws Exception {
        String userId = request.getParameter("userId");
        String description = request.getParameter("description");
        if (StringUtils.isBlank(description)) {
          return  challengeQuestResUnlock(request, response, userId);
        }
        final IdmAuditLog auditLog = new IdmAuditLog();
        auditLog.succeed();
        auditLog.setAction(AuditAction.VIEW_CHALLENGE_ANSWERS.value());
        auditLog.setRequestorPrincipal(getRequesterPrincipal(request));
        auditLog.setAuditDescription(description);
        LoginResponse loginResponse = loginServiceClient.getPrimaryIdentity(userId);
        auditLog.setTargetUser(userId, loginResponse.getPrincipal().getLogin());
        auditWS.addLog(auditLog);
        return challengeQuestRes(request, response, userId);
    }

    public String challengeQuestRes(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    String userId) throws Exception {
        request.setAttribute("userId", userId);
        setMenuTree(request, userEditRootMenuId);
        request.setAttribute("readOnly", true);
        return challengeResponse(request, userId, false);
    }
}
