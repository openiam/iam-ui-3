package org.openiam.ui.webconsole.web.model;

public class UserEmailFormBean {
    private String userEmailFormId;
    private String emailBody;
    private String emailSubject;
    private String emailAddress;


    public UserEmailFormBean(String userEmailFormId, String emailSubject, String emailAddress) {
        this.userEmailFormId = userEmailFormId;
        this.emailSubject = emailSubject;
        this.emailAddress = emailAddress;
    }

    public UserEmailFormBean() {
    }

    public String getUserEmailFormId() {
        return userEmailFormId;
    }

    public void setUserEmailFormId(String userEmailFormId) {
        this.userEmailFormId = userEmailFormId;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
