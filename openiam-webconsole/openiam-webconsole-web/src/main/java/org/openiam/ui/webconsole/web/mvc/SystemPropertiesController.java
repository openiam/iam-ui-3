package org.openiam.ui.webconsole.web.mvc;

import org.apache.commons.lang.StringUtils;
import org.openiam.base.ws.MatchType;
import org.openiam.base.ws.Response;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.idm.srvc.continfo.domain.EmailEntity;
import org.openiam.idm.srvc.msg.service.MailService;
import org.openiam.idm.srvc.org.dto.OrganizationType;
import org.openiam.idm.srvc.sysprop.dto.SystemPropertyDto;
import org.openiam.idm.srvc.sysprop.ws.SystemPropertyWebService;
import org.openiam.idm.srvc.user.dto.ProfilePicture;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.idm.srvc.user.ws.UserDataWebService;
import org.openiam.provision.dto.ProvisionActionEnum;
import org.openiam.provision.dto.ProvisionActionEvent;
import org.openiam.provision.dto.ProvisionActionTypeEnum;
import org.openiam.provision.service.ActionEventBuilder;
import org.openiam.provision.service.ProvisionServiceEventProcessor;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.util.DateFormatStr;
import org.openiam.ui.webconsole.web.model.SourceAdapterType;
import org.openiam.ui.webconsole.web.model.UserEmailFormBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

@Controller
public class SystemPropertiesController extends AbstractWebconsoleController{

    @Resource(name = "systemPropertyClient")
    protected SystemPropertyWebService systemPropertyClient;

    @RequestMapping(value = "/property", method = RequestMethod.POST)
    public
    @ResponseBody
    BasicAjaxResponse save(final HttpServletRequest request, @RequestBody SystemPropertyDto propertyDto) {
        final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        try {
            systemPropertyClient.save(propertyDto);
            ajaxResponse.setStatus(200);
            ajaxResponse.setSuccessToken(new SuccessToken(SuccessMessage.ATTRIBUTE_SAVED));
        } catch (Exception e) {
            ajaxResponse.setStatus(500);
            ajaxResponse.addError(new ErrorToken(Errors.ATTRIBUTE_COULD_NOT_SAVE));
        }

        return ajaxResponse;
    }

    @RequestMapping(value = "/properties", method = RequestMethod.GET)
    public String get(final HttpServletRequest request,
                      final HttpServletResponse response,
                      final Model model) throws IOException {
        model.addAttribute("properties", systemPropertyClient.getAll());
        model.addAttribute("sourceAdapterValues", SourceAdapterType.values());
        model.addAttribute("matchTypeVlues", MatchType.values());
        return "sysprop/properties";
    }
}
