<%
    response.setHeader("Cache-Control","no-cache");
    response.setHeader("Pragma","no-cache");
    response.setDateHeader ("Expires", -1);
%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-3.3.1.min.js"></script><script src="/openiam-ui-static/js/common/jquery/jquery.browser.min.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-ui-1.12.1.custom.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery.corner.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery.boxshadow.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/placeholder/jquery.placeholder.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tooltipster/js/jquery.tooltipster.min.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/account.claim.js"></script>
    <script type="text/javascript">
        OPENIAM = window.OPENIAM || {};
        OPENIAM.ENV = window.OPENIAM.ENV || {};
        OPENIAM.ENV.AjaxResponse = <c:choose><c:when test="${! empty requestScope.ajaxResponse}">${requestScope.ajaxResponse}</c:when><c:otherwise>null</c:otherwise></c:choose>;
        OPENIAM.ENV.ErrorType = <c:choose><c:when test="${! empty requestScope.changeMessage.error}">"${requestScope.changeMessage.error}"</c:when><c:otherwise>null</c:otherwise></c:choose>;

    </script>
</head>

<c:set var="pageTitle" scope="request">
    <spring:message code="accountClaim.employee.page.title" />
</c:set>

<t:pubpage>
    <jsp:body>

        <div class="frameContentDivider">
            <form action="${flowExecutionUrl}" method="post">
                <table cellpadding="8px" align="center">
                    <tbody>
                    <c:if test="${flowRequestContext.messageContext.hasErrorMessages()}">
                        <tr>
                            <td colspan="2">
                                <div class="alert alert-error">
                                    <c:forEach items="${flowRequestContext.messageContext.allMessages}" var="message">
                                        <p class="error">${message.text}</p>
                                    </c:forEach>
                                </div>
                            </td>
                        </tr>
                    </c:if>
                    <tr>
                        <td><label for="firstName" class="required"><fmt:message key="accountClaim.employee.firstName.label"/></label></td>
                        <td><input type="text" id="firstName" name="firstName" value="${firstName}" class="full rounded" autocomplete="off" /></td>
                    </tr>
                    <tr>
                        <td><label for="lastName" class="required"><fmt:message key="accountClaim.employee.lastName.label"/></label></td>
                        <td><input type="text" id="lastName" name="lastName"  value="${lastName}" class="full rounded" autocomplete="off" /></td>
                    </tr>
                    <tr>
                        <td><label for="dateOfBirth" class="required"><fmt:message key="accountClaim.employee.dateOfBirth.label"/></label></td>
                        <td><input type="text" id="dateOfBirth" name="dateOfBirth" value="${dateOfBirth}" class="full rounded" autocomplete="off" /></td>
                    </tr>
                    <tr>
                        <td><label for="nationalID" class="required"><fmt:message key="accountClaim.employee.nationalID.label"/></label></td>
                        <td><input type="text" id="nationalID" name="nationalID" value="${nationalID}" class="full rounded" autocomplete="off" /></td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2">
                            <ul class="formControls">
                                <li class="leftBtn">
                                    <input id="cancel" class="whiteBtn" name="_eventId_cancel" type="submit" value='<spring:message code="accountClaim.common.cancel"/>'>
                                </li>
                                <li class="rightBtn">
                                    <input id="submit" class="redBtn" name="_eventId_next" type="submit" value='<spring:message code="accountClaim.common.next"/>'>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </form>
        </div>

    </jsp:body>
</t:pubpage>