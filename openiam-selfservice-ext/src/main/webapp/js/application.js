angular.module('euronextRESTApplication', ['angular-loading-bar', 'ngRoute']);

angular.module('euronextRESTApplication').config(function ($routeProvider) {
    $routeProvider.otherwise({
        redirectTo: '/'
    });
});

angular.module('euronextRESTApplication').factory('CommonService', ['$http', function ($http) {
    var PrivateMethods = {
        doGet: function (url, callBackFunction) {
            $http.get(url).then(function (response) {
            	if (302 == response.status){
            		window.location.href = response.headers("Location");
            	}
                              callBackFunction(response);
                }, function (response) {
                PrivateMethods.processInternalError(response);
            });
        }
    };
    return {
        doGet: function (url, callback) {
            PrivateMethods.doGet(url, callback);
        }
    }
}]);

angular.module('euronextRESTApplication').filter('strReplace', function () {
  return function (input, from, to) {
    input = input || '';
    from = from || '';
    to = to || '';
    return input.replace(new RegExp(from, 'g'), to);
  };
});

angular.module('euronextRESTApplication').controller('euronextRESTCtrl', ['$scope', 'CommonService', function ($scope, CommonService) {
   
    $scope.getEntitlementsTree=function(){
    	 CommonService.doGet("/selfservice-ext/euronext/planb/"+ $scope.version+".do", function (data) {
          	//$scope.userData = data.data;
                $scope.model = data.data;

            });

    }

       $scope.init = function () {
            CommonService.doGet("/selfservice-ext/euronext/getVersions.do", function (data) {
          	//$scope.userData = data.data;
                $scope.versions = data.data.bean.entitlements;
            });
        
    }
}]);

