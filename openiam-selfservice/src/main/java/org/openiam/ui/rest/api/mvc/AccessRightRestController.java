package org.openiam.ui.rest.api.mvc;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.base.ws.Response;
import org.openiam.base.ws.ResponseCode;
import org.openiam.idm.searchbeans.AccessRightSearchBean;
import org.openiam.idm.srvc.access.dto.AccessRight;
import org.openiam.idm.srvc.access.service.AccessRightDataService;
import org.openiam.ui.rest.api.model.AccessRightBean;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.util.messages.SuccessToken;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.model.BeanResponse;
import org.openiam.ui.web.mvc.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/accessrights")
public class AccessRightRestController extends AbstractController {

    @Resource(name = "accessRightServiceClient")
    AccessRightDataService accessRightServiceClient;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public
    @ResponseBody
    BeanResponse search(final HttpServletRequest request,
                        final HttpServletResponse response,
                        final @RequestParam(required = false, value = "mdType1") String mdType1,
                        final @RequestParam(required = false, value = "mdType2") String mdType2,
                        final @RequestParam(required = true, value = "size") int size,
                        final @RequestParam(required = true, value = "from") int from) {
        final AccessRightSearchBean searchBean = new AccessRightSearchBean();
        searchBean.setMetadataTypeId1(mdType1);
        searchBean.setMetadataTypeId2(mdType2);
        final List<AccessRight> dtos = accessRightServiceClient.findBeans(searchBean, from, size, getCurrentLanguage());
        final List<AccessRightBean> beans = mapper.mapToList(dtos, AccessRightBean.class);
        if (CollectionUtils.isNotEmpty(beans)) {
            final int count = accessRightServiceClient.count(searchBean);
            return new BeanResponse(beans, count);
        } else {
            return BeanResponse.EMPTY_RESPONSE;
        }
    }


}
