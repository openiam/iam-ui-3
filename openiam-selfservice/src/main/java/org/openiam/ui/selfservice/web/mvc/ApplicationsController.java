package org.openiam.ui.selfservice.web.mvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.openiam.ui.rest.api.model.Application;
import org.openiam.ui.security.OpenIAMCookieProvider;
import org.openiam.ui.web.util.ApplicationsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ApplicationsController extends AbstractSelfServiceController {
	
	@Autowired
	private OpenIAMCookieProvider cookieProvider;
	
	@Autowired
	private ApplicationsProvider applicationProvider;

	@Autowired
	@Qualifier("jacksonMapper")
	protected ObjectMapper jacksonMapper;

	@RequestMapping("/myApplications")
	public String myApplications(final HttpServletRequest request) {
		final List<Application> applications = applicationProvider.getAppliationsForUser(getRequesterId(request), request);
		
		request.setAttribute("applications", applications);
		request.setAttribute("iconsMap", getIconsMapAsJson(applications));
		return "sso/userApps";
	}

	private String getIconsMapAsJson(List<Application> applications) {
		Map<String, String> map = new HashMap<>(applications.size());
		for (Application app : applications) {
			if (StringUtils.isNotBlank(app.getIconURL())) {
				map.put(app.getId(), app.getIconURL());
			}
		}
		try {
			if (!map.isEmpty()) {
				return jacksonMapper.writeValueAsString(map);
			}
		} catch (Throwable e) {
			log.error("Can't serialize icons map", e);
		}
		return null;
	}
}
