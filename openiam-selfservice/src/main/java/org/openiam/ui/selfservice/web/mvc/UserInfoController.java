package org.openiam.ui.selfservice.web.mvc;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.Email;
import org.openiam.authmanager.ws.request.UserRequest;
import org.openiam.authmanager.ws.response.GroupsForUserResponse;
import org.openiam.authmanager.ws.response.RolesForUserResponse;
import org.openiam.idm.searchbeans.EmailSearchBean;
import org.openiam.idm.searchbeans.LoginSearchBean;
import org.openiam.idm.searchbeans.MetadataTypeSearchBean;
import org.openiam.idm.srvc.auth.dto.Login;
import org.openiam.idm.srvc.continfo.dto.Address;
import org.openiam.idm.srvc.continfo.dto.EmailAddress;
import org.openiam.idm.srvc.continfo.dto.Phone;
import org.openiam.idm.srvc.lang.dto.Language;
import org.openiam.idm.srvc.meta.dto.MetadataType;
import org.openiam.idm.srvc.meta.ws.MetadataWebService;
import org.openiam.idm.srvc.policy.dto.ITPolicy;
import org.openiam.idm.srvc.policy.dto.Policy;
import org.openiam.idm.srvc.policy.dto.PolicyAttribute;
import org.openiam.idm.srvc.policy.service.PolicyDataService;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.ui.web.util.UsePolicyHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class UserInfoController extends AbstractSelfServiceController {

    @Resource(name = "policyServiceClient")
    private PolicyDataService policyDataService;

    @Resource(name = "metadataServiceClient")
    private MetadataWebService metadataServiceClient;

    @Value("${org.openiam.ui.selfservice.show.lite.displayname}")
    private boolean showLiteDisplayName = false;

    @Value("${org.openiam.auth.policy.id}")
    private String authentificationPolicyId;

    @Value("${org.openiam.ui.mngsys.info.page}")
    private String mngSysInfoPage;

    @Value("${org.openiam.ui.selfservice.showNickname}")
    private boolean showNickname;

    @Value("${org.openiam.ui.selfservice.showPwdExp}")
    private boolean showPwdExp;

    @Value("${org.openiam.ui.selfservice.showLastAuth}")
    private boolean showLastAuth;

    @Value("${org.openiam.ui.selfservice.showEmail}")
    private boolean showEmail;


    @RequestMapping("/myInfo")
    public String myInfo(final HttpServletRequest request, final HttpServletResponse response) {
        final String userId = cookieProvider.getUserId(request);
        final User user = userDataWebService.getUserWithDependent(userId, null, false);
        final List<User> supervisorList = userDataWebService.getSuperiors(user.getId(), 0, Integer.MAX_VALUE);
        Language language = getCurrentLanguage();


        final UserRequest authRequest = new UserRequest();
        authRequest.setUserId(userId);
        final GroupsForUserResponse groupsForUserResponse = authorizationManager.getGroupsFor(authRequest);
        final RolesForUserResponse rolesForUserResponse = authorizationManager.getRolesFor(authRequest);

        String managed_sys = mngSysInfoPage;
        final Policy policy = policyDataService.getPolicy(authentificationPolicyId);
        if (policy != null) {
            PolicyAttribute policyAttribute = policy.getAttribute("MANAGED_SYS_ID");
            if (policyAttribute != null && StringUtils.isNotBlank(policyAttribute.getValue1())) {
                managed_sys = policyAttribute.getValue1();
            }
        }

        final LoginSearchBean searchBean = new LoginSearchBean();
        searchBean.setUserId(userId);
        searchBean.setManagedSysId(managed_sys);
        final List<Login> loginList = loginServiceClient.findBeans(searchBean, 0, Integer.MAX_VALUE);
        final Login openiamLogin = (CollectionUtils.isNotEmpty(loginList)) ? loginList.get(0) : null;

        ITPolicy itPolicy = policyDataService.findITPolicy();
        Boolean status = UsePolicyHelper.getUsePolicyStatus(itPolicy, user);
        if (status != null) {
            request.setAttribute("itPolicyStatus", status);
        }

        String profilePicture = this.getProfilePicture(userId);
        request.setAttribute("login", openiamLogin);
        request.setAttribute("roles", rolesForUserResponse.getRoles());
        request.setAttribute("groups", groupsForUserResponse.getGroups());
        request.setAttribute("supervisorList", supervisorList);
        request.setAttribute("showLiteDisplayName", showLiteDisplayName);
        request.setAttribute("showNickname", showNickname);
        request.setAttribute("user", user);
        request.setAttribute("showPwdExp", showPwdExp);
        request.setAttribute("showLastAuth", showLastAuth);
        //request.setAttribute("showEmail", showEmail);
        Phone defaultPhone = user.getDefaultPhone();
        if(defaultPhone != null) {
            MetadataType metadataType = metadataServiceClient.getMetadataTypeById(defaultPhone.getMetadataTypeId());
            String phoneLabel =  metadataType.getDisplayNameMap().get(language.getId()).getValue();
            request.setAttribute("defaultPhone", defaultPhone);
            request.setAttribute("phoneLabel", phoneLabel);
        }

        EmailAddress defaultEmail = user.getDefaultEmail();
        if(defaultEmail != null) {
            MetadataType metadataType = metadataServiceClient.getMetadataTypeById(defaultEmail.getMetadataTypeId());
            String emailLabel = metadataType.getDescription();
            request.setAttribute("defaultEmail", defaultEmail);
            request.setAttribute("emailLabel", emailLabel);
        }
        Address defaultAddress = user.getDefaultAddress();
        if(defaultAddress != null) {
            MetadataType metadataType = metadataServiceClient.getMetadataTypeById(defaultAddress.getMetadataTypeId());
            String defaultAddressLabel =  metadataType.getDisplayNameMap().get(language.getId()).getValue();
            request.setAttribute("defaultAddress", defaultAddress);
            request.setAttribute("defaultAddressLabel", defaultAddressLabel);
        }
        if (StringUtils.isNotBlank(profilePicture)) {
            request.setAttribute("profilePicture", profilePicture);
        }

        return "user/myInfo";
    }
}
