package org.openiam.ui.selfservice.web.mvc;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.CollectionUtils;
import org.openiam.authmanager.service.AuthorizationManagerWebService;
import org.openiam.idm.srvc.grp.ws.GroupDataWebService;
import org.openiam.idm.srvc.meta.dto.PageTempate;
import org.openiam.idm.srvc.mngsys.ws.ManagedSystemWebService;
import org.openiam.idm.srvc.org.dto.OrganizationType;
import org.openiam.idm.srvc.org.dto.OrganizationUserDTO;
import org.openiam.idm.srvc.role.ws.RoleDataWebService;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.idm.srvc.user.dto.UserStatusEnum;
import org.openiam.script.ScriptIntegration;
import org.openiam.ui.rest.api.model.KeyNameBean;
import org.openiam.ui.selfservice.web.groovy.AuthenticatedContactInfoPreprocessor;
import org.openiam.ui.web.util.DateFormatStr;
import org.openiam.ui.web.mvc.provider.TemplateProvider;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public abstract class AbstractProfileController extends AbstractSelfServiceController implements ApplicationContextAware {

    @Autowired
    @Resource(name = "authorizationManagerServiceClient")
    protected AuthorizationManagerWebService authorizationManager;

    @Resource(name = "managedSysServiceClient")
    protected ManagedSystemWebService managedSysServiceClient;

    @Autowired
    @Qualifier("configurableGroovyScriptEngine")
    private ScriptIntegration scriptRunner;

    @Value("${openiam.ui.loginurl}")
    protected String loginURL;

    private ApplicationContext applicationContext;

    @Resource(name = "roleServiceClient")
    private RoleDataWebService roleDataService;

    @Resource(name = "groupServiceClient")
    private GroupDataWebService groupServiceClient;

    @Autowired
    protected TemplateProvider templateProvider;

    @Value("${org.openiam.workflow.new.user.redirect.url}")
    protected String redirectUrlAfterUserWorkflowInitialization;

    @Value("${openiam.show.phone.area.code}")
    protected String showPhoneAreaCode;

    protected String processProfileScreenGetRequest(final HttpServletRequest request, final User user, final boolean newUser, final String targetUserId) throws Exception {
        final List<List<OrganizationType>> orgTypes = getOrgTypeList();

    	/*
         * IDMAPPS-1062
		 * Self Registration - we need an enhancement where we can pass an orgid or 
		 * orgname on the URL and the drop down that shows the org list will only 
		 * show the org that is passed in.
    	 */
        String[] orgArray = request.getParameterValues("orgList");
        orgArray = (orgArray != null) ? orgArray : new String[0];
        final List<String> orgList = new LinkedList<String>();
        for (final String org : orgArray) {
            orgList.add(org);
        }
        request.setAttribute("orgList", jacksonMapper.writeValueAsString(orgList));

        //final String userId = cookieProvider.getUserId(request);
        request.setAttribute("target", request.getRequestURI());
        request.setAttribute("orgHierarchy", (orgTypes != null) ? jacksonMapper.writeValueAsString(orgTypes) : null);
        request.setAttribute("user", user);
        request.setAttribute("userStatuses", UserStatusEnum.values());
        request.setAttribute("objClassList", getUserTypes());

        PageTempate template = templateProvider.getPageTemplate(targetUserId, getRequesterId(request), request);
        if(template != null && template.getJsonDataModel()!=null){
            template.setObjectDataModel(jacksonMapper.readValue(template.getJsonDataModel(), Map.class));
            template.setJsonDataModel(null);
        }
        request.setAttribute("template", (template != null) ? jacksonMapper.writeValueAsString(template) : null);

        request.setAttribute("currentUserId", getRequesterId(request));
        request.setAttribute("pageTemplate", (template != null) ? template : new PageTempate());
        request.setAttribute("employeeTypeList", getUserTypeList());
        request.setAttribute("jobList", getJobCodeList());
        request.setAttribute("newUser", newUser);

        if (CollectionUtils.isNotEmpty(user.getOrganizationUserDTOs())) {
            List<OrganizationUserDTO> userOrgs = new ArrayList<OrganizationUserDTO>(user.getOrganizationUserDTOs());

            Collections.sort(userOrgs, new Comparator<OrganizationUserDTO>() {
                @Override
                public int compare(OrganizationUserDTO o1, OrganizationUserDTO o2) {
                    int result = o1.getOrganization().getOrganizationTypeId().compareTo(o2.getOrganization().getOrganizationTypeId());
                    if (result != 0) {
                        return result;
                    }
                    return o1.getOrganization().getName().compareTo(o2.getOrganization().getName());
                }
            });
            if ((userOrgs != null) && (userOrgs.size() > 0)) {
                request.setAttribute("currentOrgId", userOrgs.get(0).getOrganization().getId());
            } else {
                request.setAttribute("currentOrgId", null);
            }

        }

        final List<KeyNameBean> emailTypes = getEmailTypes();
        final List<KeyNameBean> phoneTypes = getPhoneTypes();
        final List<KeyNameBean> addressTypes = getAddressTypes();
        final Map<String, KeyNameBean> emailTypeMap = new HashMap<String, KeyNameBean>();
        if (CollectionUtils.isNotEmpty(emailTypes)) {
            for (final KeyNameBean bean : emailTypes) {
                emailTypeMap.put(bean.getId(), bean);
            }
        }
        final Map<String, KeyNameBean> phoneTypeMap = new HashMap<String, KeyNameBean>();
        if (CollectionUtils.isNotEmpty(phoneTypes)) {
            for (final KeyNameBean bean : phoneTypes) {
                phoneTypeMap.put(bean.getId(), bean);
            }
        }

        final Map<String, KeyNameBean> addressTypeMap = new HashMap<String, KeyNameBean>();
        if (CollectionUtils.isNotEmpty(addressTypes)) {
            for (final KeyNameBean bean : addressTypes) {
                addressTypeMap.put(bean.getId(), bean);
            }
        }

        request.setAttribute("emailTypes", (emailTypes != null) ? jacksonMapper.writeValueAsString(emailTypes) : null);
        request.setAttribute("phoneTypes", (phoneTypes != null) ? jacksonMapper.writeValueAsString(phoneTypes) : null);
        request.setAttribute("addressTypes", (addressTypes != null) ? jacksonMapper.writeValueAsString(addressTypes) : null);
        request.setAttribute("emailTypeMap", jacksonMapper.writeValueAsString(emailTypeMap));
        request.setAttribute("phoneTypeMap", jacksonMapper.writeValueAsString(phoneTypeMap));
        request.setAttribute("addressTypeMap", jacksonMapper.writeValueAsString(addressTypeMap));
        request.setAttribute("dateFormatDP", DateFormatStr.getDpDate());
        request.setAttribute("showPhoneAreaCode", showPhoneAreaCode);

        return "user/profileScreen";
    }

    protected AuthenticatedContactInfoPreprocessor getAuthenticatedContactInfoPreProcessor(final User user, final HttpServletRequest request, final String script) {
        AuthenticatedContactInfoPreprocessor preProcessor = null;
        try {
            preProcessor = (AuthenticatedContactInfoPreprocessor) scriptRunner.instantiateClass(null, script);
        } catch (Throwable e) {
            log.info(String.format("Can't inialize script %s.  This is not an error", script), e);
            preProcessor = new AuthenticatedContactInfoPreprocessor();
        }

        final Map<String, Object> bindingMap = new HashMap<String, Object>();
        bindingMap.put("context", applicationContext);
        bindingMap.put("user", user);
        bindingMap.put("request", request);
        preProcessor.init(bindingMap);
        return preProcessor;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        this.applicationContext = applicationContext;
    }
}
