<%
    response.setHeader("Cache-Control","no-cache");
    response.setHeader("Pragma","no-cache");
    response.setDateHeader ("Expires", -1);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.openiam.com/tags/openiam" prefix="openiam" %>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${titleOrganizatioName} - <fmt:message key="openiam.ui.selfservice.my.tasks.text" /></title>
    <link href="/openiam-ui-static/css/common/style.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/openiam-ui-static/css/common/style.client.css" rel="stylesheet" type="text/css" />
    <link href="/openiam-ui-static/js/common/jquery/css/smoothness/jquery-ui-1.12.1.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/openiam-ui-static/plugins/tiptip/tipTip.css" rel="stylesheet" type="text/css" />
    <link href="/openiam-ui-static/plugins/tablesorter/themes/yui/style.css" rel="stylesheet" type="text/css" />
    <link href="/openiam-ui-static/css/common/entitlements.css" rel="stylesheet" type="text/css" />
    <link href="/openiam-ui-static/js/common/plugins/entitlementstable/entitlements.table.css" rel="stylesheet" type="text/css" />
    <link href="/openiam-ui-static/js/common/plugins/modalsearch/modal.search.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/webconsole/plugins/usersearch/user.search.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/plugins/modalEdit/modalEdit.css" rel="stylesheet" type="text/css"/>
    <openiam:overrideCSS />

    <script type="text/javascript" src="/openiam-ui-static/_dynamic/openiamResourceBundle.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-3.3.1.min.js"></script><script src="/openiam-ui-static/js/common/jquery/jquery.browser.min.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-ui-1.12.1.custom.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/json/json.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js"></script>

    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter-2.0.3.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter.filer.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tablesorter/js/jquery.tablesorter.pager.js"></script>

    <script type="text/javascript" src="/openiam-ui-static/js/common/plugins/entitlementstable/entitlements.table.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/selfservice/activiti/my.tasks.js"></script>

    <script type="text/javascript" src="/openiam-ui-static/js/webconsole/plugins/usersearch/user.search.form.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/webconsole/plugins/usersearch/user.search.results.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/plugins/modalEdit/modalEdit.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/plugins/modalsearch/modal.search.js"></script>



    <script type="text/javascript">
        OPENIAM = window.OPENIAM || {};
        OPENIAM.ENV = window.OPENIAM.ENV || {};
        OPENIAM.ENV.MenuTree = <c:choose><c:when test="${! empty requestScope.menuTree}">${requestScope.menuTree}</c:when><c:otherwise>null</c:otherwise></c:choose>;
        OPENIAM.ENV.MenuTreeAppendURL = null;

    </script>
    <style>
        #taskSearchForm label {
            float: left;
            width: 130px;
            padding-top: 6px;
            text-align: left;
        }
    </style>
</head>
<body>
<div id="title" class="title">
    <fmt:message key="openiam.ui.selfservice.my.tasks.text" />
</div>
<div id="formContainer" class="frameContentDivider">
    <form name="assignedTaskSearchForm" id="assignedTaskSearchForm">
        <table id="assignedTaskSearchFormTable" class="yui" cellspacing="1" width="100%">
            <thead>
            <tr>
                <td class="filter" colspan="3">
                    <label for="fromDate"><fmt:message key="openiam.ui.common.from.date.lable" />:</label>
                    <input id="fromDate" name="fromDate" autocomplete="off" class="date" size="23" type="text">
                </td>
                <td class="filter" colspan="3">
                    <label for="toDate"><fmt:message key="openiam.ui.common.to.date.lable" />:</label>
                    <input id="toDate" name="toDate" autocomplete="off" class="date" size="23" type="text">
                </td>
            </tr>
            <tr>
                <td class="filter" colspan="3">
                    <label for="description"><fmt:message key="openiam.ui.common.description.requester" />:</label> <br>
                    <select id="searchby" name="searchby">
                        <option value=""><fmt:message key="openiam.ui.common.please.select" /></option>
                        <option value="bydescription"><fmt:message key="openiam.ui.common.description" /></option>
                        <option value="byrequester"><fmt:message key="openiam.ui.common.requester" /></option>
                    </select>
                </td>
                <td class="filter" colspan="3">
                </td>
            </tr>
            <tr>
                <td class="filter" colspan="3">
                    <input id="description" name="description" autocomplete="off" class="full rounded" type="text" style="display: none;">

                    <input id="requesterName" type="text" class="full rounded" value="" readonly="readonly" style="display: none;">

                    <input id="requesterId" type="hidden" value="" name="requesterId">
                    <br>
                    <a id="selectRequester" href="javascript:void(0);" style="display: none;"><fmt:message
                            key="openiam.ui.user.requester.select"/></a>
                </td>
                <td class="filter" colspan="3">
                </td>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td class="filter" colspan="6">
                    <div id="humanReadable"></div>
                </td>
            </tr>
            <tr>
                <td class="filter" colspan="6">
                    <input value="Search" class="redBtn" type="submit">
                    <a id="cleanAssignedTaskSearchForm" class="whiteBtn" href="javascript:void(0);">Clear</a>
                </td>
            </tr>
            </tfoot>
        </table>
        <div id="assignedTaskSearchFormResultsArea"></div>
    </form>
</div>
<div class="frameContentDivider">
    <div id="assignedTasks"></div>
    <table cellpadding="8px">
        <tr>
            <td>
                <ul class="formControls">
                    <li>
                        <a href="javascript:void(0)">
                            <input id="accept" type="submit" class="redBtn" value="<fmt:message key='openiam.ui.selfservice.task.accept.selected.request' />" />
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <input id="reject" type="submit" class="whiteBtn" value="<fmt:message key='openiam.ui.selfservice.task.reject.selected.request' />" />
                        </a>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div id="candidateFormContainer" class="frameContentDivider">
    <form name="candidateTaskSearchForm" id="candidateTaskSearchForm">
        <table id="candidateTaskSearchFormTable" class="yui" cellspacing="1" width="100%">
            <thead>
            <tr>
                <td class="filter" colspan="3">
                    <label for="fromDate"><fmt:message key="openiam.ui.common.from.date.lable" />:</label>
                    <input id="candidateFromDate" name="candidateFromDate" autocomplete="off" class="date" size="23" type="text">
                </td>
                <td class="filter" colspan="3">
                    <label for="toDate"><fmt:message key="openiam.ui.common.to.date.lable" />:</label>
                    <input id="candidateToDate" name="candidateToDate" autocomplete="off" class="date" size="23" type="text">
                </td>
            </tr>
            <tr>
                <td class="filter" colspan="3">
                    <label for="candidateDescription"><fmt:message key="openiam.ui.common.description" />:</label>
                    <input id="candidateDescription" name="candidateDescription" autocomplete="off" class="date" size="23" type="text">
                </td>
                <td class="filter" colspan="3">
                </td>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td class="filter" colspan="6">
                    <div id="candidateHumanReadable"></div>
                </td>
            </tr>
            <tr>
                <td class="filter" colspan="6">
                    <input value="Search" class="redBtn" type="submit">
                    <a id="cleanCandidateTaskSearchForm" class="whiteBtn" href="javascript:void(0);">Clear</a>
                </td>
            </tr>
            </tfoot>
        </table>
        <div id="candidateTaskSearchFormResultsArea"></div>
    </form>
</div>
<div class="frameContentDivider">
    <div id="candidateTasks"></div>
    <a href="javascript:void(0)">
        <input id="acceptClaim" type="submit" class="redBtn" value="<fmt:message key='openiam.ui.selfservice.task.accept.selected.request' />" />
    </a>
</div>

<div id="userResultsArea"></div>
<div id="dialog"></div>
<div id="editDialog"></div>
</body>
</html>