<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.openiam.com/tags/openiam" prefix="openiam" %>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${titleOrganizatioName} - <fmt:message key="openiam.ui.user.my.info"/></title>
    <link href="/openiam-ui-static/css/common/style.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/openiam-ui-static/css/common/style.client.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/css/selfservice/selfservice.my.info.css" rel="stylesheet" type="text/css"/>
    <link href="/openiam-ui-static/js/common/jquery/css/smoothness/jquery-ui-1.12.1.custom.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/openiam-ui-static/plugins/tiptip/tipTip.css" rel="stylesheet" type="text/css"/>
    <openiam:overrideCSS/>
    <script type="text/javascript" src="/openiam-ui-static/_dynamic/openiamResourceBundle.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-3.3.1.min.js"></script><script src="/openiam-ui-static/js/common/jquery/jquery.browser.min.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-ui-1.12.1.custom.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/json/json.js"></script>

    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/tiptip/jquery.tipTip.js"></script>
    <script type="text/javascript">
        OPENIAM = window.OPENIAM || {};
        OPENIAM.ENV = window.OPENIAM.ENV || {};
        OPENIAM.ENV.MenuTree = <c:choose><c:when test="${! empty requestScope.menuTree}">${requestScope.menuTree}</c:when><c:otherwise>null</c:otherwise></c:choose>;
        OPENIAM.ENV.MenuTreeAppendURL = null;
    </script>
</head>
<body>
<div id="title" class="title">
    <fmt:message
            key="openiam.ui.common.welcome"/>
            <c:if test="${requestScope.showNickname}">${empty requestScope.user.nickname ? requestScope.user.displayName: requestScope.user.nickname}</c:if>
            <c:if test="${!requestScope.showNickname}">${requestScope.user.displayName}</c:if>

</div>
<div class="frameContentDivider">
    <div>
        <c:if test="${! empty requestScope.profilePicture}">
            <p>
                <img src='<c:url context="${pageContext.request.contextPath}" value="/rest/api/prov/images/${requestScope.profilePicture}" />'/>
            </p>
        </c:if>

        <div style="display: inline-block; margin-right:10px;">
        <c:if test="${requestScope.showLastAuth}">
            <p><b><fmt:message key="openiam.ui.user.identities.last.login"/>: </b><c:choose><c:when
                    test="${! empty requestScope.login}"><fmt:formatDate value="${requestScope.login.lastLogin}"
                                                                         pattern="yyyy-MM-dd HH:mm:ss"/>
            </c:when><c:otherwise>NA</c:otherwise> </c:choose></p>
        </c:if>
        <c:if test="${requestScope.showLastAuth}">
            <p><b><fmt:message key="openiam.ui.user.identities.last.login.ip"/>: </b><c:choose><c:when
                    test="${! empty requestScope.login}">${requestScope.login.lastLoginIP}
            </c:when><c:otherwise>NA</c:otherwise> </c:choose></p>
        </c:if>
            <c:if test="${requestScope.showPwdExp}">
                <p><b><fmt:message key="openiam.ui.user.my.info.password.expiration"/>: </b><c:choose><c:when
                        test="${! empty requestScope.login}"><fmt:formatDate value="${requestScope.login.pwdExp}"
                                                                             pattern="yyyy-MM-dd HH:mm:ss"/>
                </c:when><c:otherwise>NA</c:otherwise> </c:choose></p>
            </c:if>
            <%--<c:if test = "${requestScope.showEmail}">
                <p><b><fmt:message key="openiam.ui.user.my.info.showEmail"/> </b><c:choose><c:when
                    test="${! empty requestScope.login}">${requestScope.user.email}
                </c:when><c:otherwise>NA</c:otherwise></c:choose></p>
            </c:if>--%>
        </div>

        <div style="display: inline-block; margin-left:10px;">
        <c:if test="${requestScope.showLastAuth}">
            <p><b><fmt:message key="openiam.ui.user.identities.prev.login"/>: </b><c:choose><c:when
                    test="${! empty requestScope.login}"><fmt:formatDate value="${requestScope.login.prevLogin}"
                                                                         pattern="yyyy-MM-dd HH:mm:ss"/>
            </c:when><c:otherwise>NA</c:otherwise> </c:choose></p>
        </c:if>

        <c:if test="${requestScope.showLastAuth}">
            <p><b><fmt:message key="openiam.ui.user.identities.prev.login.ip"/>: </b><c:choose><c:when
                    test="${! empty requestScope.login}">${requestScope.login.prevLoginIP}
            </c:when><c:otherwise>NA</c:otherwise> </c:choose></p>
        </c:if>
            <c:if test="${requestScope.showPwdExp}">
                <p><b><fmt:message key="openiam.ui.user.identities.last.password.change"/>: </b><c:choose><c:when
                        test="${! empty requestScope.login}"><fmt:formatDate value="${requestScope.login.pwdChanged}"
                                                                             pattern="yyyy-MM-dd HH:mm:ss"/>
                </c:when><c:otherwise>NA</c:otherwise> </c:choose></p>
            </c:if>
        </div>

        <c:if test="${not empty requestScope.itPolicyStatus}">
            <p><b><a href="/selfservice/usePolicy.html"><u><fmt:message key="openiam.ui.webconsole.it.policy.status"/></u>: </a></b><c:choose><c:when
                    test="${requestScope.itPolicyStatus}"><fmt:message key="openiam.ui.user.accepted"/> <fmt:formatDate
                    value="${requestScope.user.dateITPolicyApproved}" pattern="yyyy-MM-dd HH:mm:ss"/>
            </c:when><c:otherwise><a href="/selfservice/usePolicy.html" class="blue"><fmt:message
                    key="openiam.ui.user.my.info.review.accept.policy"/></a></c:otherwise> </c:choose></p>
        </c:if>

        <p>
            <span>
                <c:if test="${! empty requestScope.defaultPhone}">
                        <c:if test="${! empty  requestScope.phoneLabel}"><b>${ requestScope.phoneLabel}</b></c:if>:&nbsp;
                    <c:if test="${! empty  requestScope.defaultPhone.countryCd}">${ requestScope.defaultPhone.countryCd}</c:if>
                    <c:if test="${! empty  requestScope.defaultPhone.areaCd}">(${ requestScope.defaultPhone.areaCd})</c:if>
                    <c:if test="${ empty  requestScope.defaultPhone.areaCd}"> </c:if>${ requestScope.defaultPhone.phoneNbr},&nbsp;
                </c:if>
                <c:if test="${! empty requestScope.defaultEmail}">
                    <c:if test="${! empty requestScope.emailLabel}"><b>${requestScope.emailLabel}</b></c:if>:&nbsp;
                    <c:if test="${! empty requestScope.defaultEmail.emailAddress}">${requestScope.defaultEmail.emailAddress}</c:if>
                </c:if>
                <c:if test="${! empty requestScope.defaultAddress}">
                    <c:if test="${! empty requestScope.defaultAddressLabel}"><b>${requestScope.defaultAddressLabel}</b></c:if>:
                    <c:if test="${! empty requestScope.defaultAddress.postalCd}">${requestScope.defaultAddress.postalCd},</c:if>
                    <c:if test="${! empty requestScope.defaultAddress.address1}">${requestScope.defaultAddress.address1}</c:if>
                    <c:if test="${! empty requestScope.defaultAddress.address2}">,${requestScope.defaultAddress.address2}</c:if>
                    <c:if test="${! empty requestScope.defaultAddress.bldgNumber}">,${requestScope.defaultAddress.bldgNumber}</c:if>
                    <c:if test="${! empty requestScope.defaultAddress.city}">,${requestScope.defaultAddress.city}</c:if>
                    <c:if test="${! empty requestScope.defaultAddress.state}">,${requestScope.defaultAddress.state}</c:if>
                    <c:if test="${! empty requestScope.defaultAddress.country}">,${requestScope.defaultAddress.country}</c:if>
                </c:if>
            </span>
        </p>
        <c:choose>
            <c:when test="${! empty requestScope.supervisorList and fn:length(requestScope.supervisorList) > 0}">
                <span><b><fmt:message key="openiam.ui.user.supervisor.myinfo"/>:</b>
                   <c:forEach var="supervisor" items="${requestScope.supervisorList}">
                       <c:if test="${requestScope.showLiteDisplayName}">${supervisor.firstName}&nbsp;${supervisor.prefixLastName}&nbsp;${supervisor.lastName}&nbsp;</c:if>
                       <c:if test="${!requestScope.showLiteDisplayName && requestScope.showNickname}">${empty supervisor.nickname ? supervisor.displayName : supervisor.nickname}&nbsp;</c:if>
                       <c:if test="${!requestScope.showLiteDisplayName && !requestScope.showNickname}">${supervisor.displayName}&nbsp;</c:if>
                   </c:forEach>
                </span>

            </c:when>
            <c:otherwise>
                <p><b><fmt:message key="openiam.ui.user.supervisor.myinfo"/>:</b> <fmt:message
                        key="openiam.ui.common.na"/></p>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test="${! empty requestScope.roles and fn:length(requestScope.roles) > 0}">
                <p><b><fmt:message key="openiam.ui.user.entitlement.group.title"/>:</b>
                <span>
                    <c:forEach var="group" items="${requestScope.groups}" varStatus="status">
                        ${group.name}<c:if test="${! status.last}">,</c:if><c:if test="${status.last}"></c:if>
                    </c:forEach>
                </span>
                </p>
            </c:when>
            <c:otherwise>
                <p><b><fmt:message key="openiam.ui.user.entitlement.group.title"/>:</b> <fmt:message
                        key="openiam.ui.common.na"/></p>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test="${! empty requestScope.roles and fn:length(requestScope.roles) > 0}">
                <p><b><fmt:message key="openiam.ui.user.entitlement.role.title"/>: </b>

                <span>
                    <c:forEach var="role" items="${requestScope.roles}" varStatus="status">
                        ${role.name}<c:if test="${! status.last}">,</c:if><c:if test="${status.last}"></c:if>
                    </c:forEach>
                </span>
                </p>
            </c:when>
            <c:otherwise>
                <p><b><fmt:message key="openiam.ui.user.entitlement.role.title"/>: </b><fmt:message
                        key="openiam.ui.common.na"/></p>
            </c:otherwise>
        </c:choose>

    </div>
</div>
</body>
</html>