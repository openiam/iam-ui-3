package org.openiam.ui.idp.saml.model.sp;

import org.openiam.ui.idp.saml.model.AbstractSignableMessageToken;
import org.openiam.ui.idp.saml.provider.SAMLAuthenticationProvider;
import org.opensaml.saml2.core.LogoutRequest;

public class SAMLLogoutRequestToken<P extends SAMLAuthenticationProvider> extends AbstractSignableMessageToken<LogoutRequest, P> {

    private boolean isConfiguredForSLO = true;

    public boolean isConfiguredForSLO() {
        return isConfiguredForSLO;
    }

    public void setConfiguredForSLO(boolean isConfiguredForSLO) {
        this.isConfiguredForSLO = isConfiguredForSLO;
    }

    @Override
    public String toString() {
        return "SAMLLogoutRequestToken{" +
                "isConfiguredForSLO=" + isConfiguredForSLO +
                '}';
    }
}
