package org.openiam.ui.idp.web.idp.mvc;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openiam.ui.idp.saml.model.SAMLResponseToken;
import org.openiam.ui.idp.saml.model.idp.SAMLIDPMetadataResponse;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutRequestToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutResponseToken;
import org.openiam.ui.idp.saml.provider.SAMLIdentityProvider;
import org.openiam.ui.idp.saml.service.SAMLIdpServiceProvider;
import org.openiam.ui.idp.saml.service.SAMLService;
import org.openiam.ui.idp.saml.util.SAMLDigestUtils;
import org.openiam.ui.idp.saml.util.SAMLSessionUtils;
import org.openiam.ui.idp.web.mvc.AbstractSAMLController;
import org.openiam.ui.util.URIUtils;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.web.util.SAMLConstants;
import org.opensaml.common.SignableSAMLObject;
import org.opensaml.saml2.core.*;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.util.XMLHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.Set;

@Controller
@RequestMapping("/idp")
public class SAMLIdentityProviderController extends AbstractSAMLController<AuthnRequest, Response, SAMLIdentityProvider> {
	
	private static Logger log = Logger.getLogger(SAMLIdentityProviderController.class);
	
	@Autowired
	private SAMLIdpServiceProvider samlIdpProvider;

	@Value("${org.openiam.idp.saml.skip.uri.check}")
	protected boolean skipURICheck;

	@RequestMapping(value="/metadata/{identityProviderId}", method=RequestMethod.GET, produces="text/xml")
	public @ResponseBody String SAMLMetadata(final HttpServletRequest request,
							   				 final HttpServletResponse response,
							   				 final @PathVariable("identityProviderId") String id) throws IOException {
		final SAMLIDPMetadataResponse token = samlIdpProvider.getMetadata(request, id);
		if(token.isError()) {
			//request.setAttribute("error", new ErrorToken(token.getError()));
			//return "common/message";
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, token.getError().toString());
			return null;
		} else {
			return XMLHelper.prettyPrintXML(token.getEntityDescriptorElement());
		}
	}
	
	/*
	 * this will be called when the SP sends a LogoutRequest.
	 * We will either respond directly with a LogoutResponse, or contact
	 * every SP that the user has been logged into with additional LogoutRequests
	 * 
	 * Input:  LogoutRequest sent by the SP
	 * Output:  either send a LogoutResponse back to the SP, or for every *other* SP, send a LogoutRequest to that
	 */
	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public String processLogoutRequest(final HttpServletRequest request, 
			   		   				 final HttpServletResponse response) throws MessageEncodingException, IOException, URISyntaxException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {

		boolean isLogoutRequest = false;
		try {
			SAMLDigestUtils.getLogoutRequest(request, skipURICheck);
			isLogoutRequest = true;
		} catch (Exception ex) {
			log.debug("Can't get logoutRequest from HTTP GET Request");
		}
		if (isLogoutRequest) {
			final SAMLLogoutRequestToken<SAMLIdentityProvider> token = validateAndExtractLogoutRequest(request);

			final Set<String> identityProviderIds = (Set<String>) request.getSession(true).getAttribute(SAMLConstants.SAML_IDENETITY_PROVIDERS_FOR_SESSION);
			log.debug(">>>>> token.getSamlObject() :" + (token != null ? token.getSamlObject() : null));
			if (token.isError()) {
				request.setAttribute("error", new ErrorToken(token.getError()));
				return "common/message";
			}
		
			/* remove the current service provider from the session list */
			if (CollectionUtils.isNotEmpty(identityProviderIds)) {
				boolean hasId = false;
				for (String id : identityProviderIds) {
					if (id.equals(token.getAuthProvider().getId())) {
						hasId = true;
					}
				}
				if (hasId) {
					identityProviderIds.remove(token.getAuthProvider().getId());
					log.debug(">>>>> identityProviderIds remove id:" + token.getAuthProvider().getId());
					request.getSession(true).setAttribute(SAMLConstants.SAML_IDENETITY_PROVIDERS_FOR_SESSION, identityProviderIds);
				}
			}
		
			/*
			 * if there are no service providers associated with this session (except the requesting one), then
			 * send a LogoutResponse back to the SP
			 */
			if (CollectionUtils.isNotEmpty(identityProviderIds)) {
				log.debug(">>>>> processLogoutRequest >>>  token.getSamlObject() :" + (token != null ? token.getSamlObject() : null));
				SAMLSessionUtils.setLastLogoutReqeust(request, token.getSamlObject());
				return sendNextLogoutRequest(request, response, identityProviderIds.iterator().next());
			}

			//logout
			if (!loginProvider.doLogout(request, response, false)) {
				log.error("Error occurred during logout.  This is an internal error");
				request.setAttribute("error", new ErrorToken(Errors.INTERNAL_ERROR));
				return "common/message";
			}

			//send LogoutResponse back to SP
			log.debug(">>>>> processLogoutRequest -> send LogoutResponse back to SP");
			return sendLogoutResponse(token.getSamlObject(), request, response, StatusCode.SUCCESS_URI);
		} else {
			log.debug("Can't get logoutRequest from HttpRequest - Try get logoutResponse from HttpRequest");
			return prLogoutResponse(request, response);
		}
	}
	
	@RequestMapping(value="/globalLogout", method=RequestMethod.GET)
	public String globalLogout(final HttpServletRequest request, final HttpServletResponse response) throws MessageEncodingException, IOException, URISyntaxException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		final Set<String> identityProviderIds = (Set<String>)request.getSession(true).getAttribute(SAMLConstants.SAML_IDENETITY_PROVIDERS_FOR_SESSION);
		log.debug(">>>>> globalLogout  -> identityProviderIds:");

		if(CollectionUtils.isNotEmpty(identityProviderIds)) {
			for (String id : identityProviderIds) {
				log.debug(">>>>> --- id :" + id);
			}
			return sendNextLogoutRequest(request, response, identityProviderIds.iterator().next());
		} else {
			log.debug(">>>>> globalLogout  -> response.sendRedirect:");
			response.sendRedirect(new StringBuilder(URIUtils.getBaseURI(request)).append(logoutURL).toString());
			return null;
		}
	}
	
	/* 
	 * this will be called in case the user has been logged into more than one SP
	 * and we sent a LogoutRequest to each ot those SPs (the SP in turn sends a 
	 * LogoutResponse to this endpoint)
	 * 
	 * Input:  LogoutResponse
	 * Output:  either send a LogoutRequest to the next SP, or send a LogoutResponse back to the original SP
	 */
	@RequestMapping(value="/logout", method=RequestMethod.POST)
	public String processLogoutResponse(final HttpServletRequest request, 
	   		   		   				  final HttpServletResponse response) throws MessageEncodingException, IOException, URISyntaxException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		return prLogoutResponse(request, response);
	}

	public String prLogoutResponse(final HttpServletRequest request,
										final HttpServletResponse response) throws MessageEncodingException, IOException, URISyntaxException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		final SAMLLogoutResponseToken<SAMLIdentityProvider> token = samlIdpProvider.getSAMLLogoutResponse(request);
		if(token.isError()) {
			request.setAttribute("error", new ErrorToken(token.getError()));
			return "common/message";
		}

		final Set<String> identityProviderIds = (Set<String>)request.getSession(true).getAttribute(SAMLConstants.SAML_IDENETITY_PROVIDERS_FOR_SESSION);
		if(CollectionUtils.isNotEmpty(identityProviderIds)) {
			boolean hasId = false;
			for (String id : identityProviderIds) {
				if (id.equals(token.getAuthProvider().getId())) {
					hasId = true;
				}
			}
			if (hasId) {
				identityProviderIds.remove(token.getAuthProvider().getId());
				log.debug(">>>>> identityProviderIds remove id :" + token.getAuthProvider().getId());
				request.getSession(true).setAttribute(SAMLConstants.SAML_IDENETITY_PROVIDERS_FOR_SESSION, identityProviderIds);
			}
		}

		/*
		 * if there are no more service providers associated with the current session, send a LogoutResponse back
		 * to the original SP
		 */
		if(CollectionUtils.isEmpty(identityProviderIds)) {
			/* get the LogoutRequest sent by the SP */
			final LogoutRequest logoutRequest = SAMLSessionUtils.getLastLogoutRequest(request);
			if(logoutRequest != null) {
				log.debug(">>>>> processLogoutResponse >>> sendLogoutResponse)");
				return sendLogoutResponse(logoutRequest, request, response, StatusCode.SUCCESS_URI);
			} else {
				log.debug(">>>>> processLogoutResponse >>> sendRedirect)");
				response.sendRedirect(new StringBuilder(URIUtils.getBaseURI(request)).append(logoutURL).toString());
				return null;
			}
		} else {
			return sendNextLogoutRequest(request, response, identityProviderIds.iterator().next());
		}
	}


	
	private String sendNextLogoutRequest(final HttpServletRequest request, final HttpServletResponse response, final String identityProviderId) throws MessageEncodingException, IOException, URISyntaxException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		final SAMLLogoutRequestToken<SAMLIdentityProvider> logoutRequestToken = samlIdpProvider.generateLogoutRequest(request, identityProviderId, LogoutResponse.ADMIN_LOGOUT_URI);
		if(logoutRequestToken.isError()) {
			request.setAttribute("error", new ErrorToken(logoutRequestToken.getError()));
			return "common/message";
		} else if(!logoutRequestToken.isConfiguredForSLO()) {
			/* 
			 * remove it, if you can't send an SLO Request
			 * it's a partial success at this point
			 */
			final Set<String> identityProviderIds = (Set<String>)request.getSession(true).getAttribute(SAMLConstants.SAML_IDENETITY_PROVIDERS_FOR_SESSION);
			boolean hasId = false;
			if(CollectionUtils.isNotEmpty(identityProviderIds)) {
				for (String id : identityProviderIds) {
					if (id.equals(identityProviderId)) {
						hasId = true;
					}
				}
				if (hasId) {
					identityProviderIds.remove(identityProviderId);
					log.debug(">>>>> identityProviderIds remove id :" + identityProviderId);
					request.getSession(true).setAttribute(SAMLConstants.SAML_IDENETITY_PROVIDERS_FOR_SESSION, identityProviderIds);
				}
			}

			if(CollectionUtils.isNotEmpty(identityProviderIds)) {
				log.debug(">>>>> sendNextLogoutRequest  >>> sendNextLogoutRequest");
				return sendNextLogoutRequest(request, response, identityProviderIds.iterator().next());
			} else {
				final LogoutRequest logoutRequest = SAMLSessionUtils.getLastLogoutRequest(request);
				log.debug(">>>>> sendNextLogoutRequest  >>> sendLogoutResponse");
				return sendLogoutResponse(logoutRequest, request, response, StatusCode.SUCCESS_URI);
			}
		} else {
			log.debug(">>>>> sendNextLogoutRequest  >>> samlIdpProvider.redirectAndSend");
			samlIdpProvider.redirectAndSend(logoutRequestToken, response, request);
			return null;
		}
	}
	
	/* 
	 * this URL is NOT protected by Spring Security, because we need to support SAML POST requests
	 * Standard SAML URL parameters are expected (SAMLRequest, RelayState)
	 */
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String samlLoginGet(final HttpServletRequest request, 
							   final HttpServletResponse response,
							   final @RequestParam(required=false, value="X_OPENIAM_POST") String wasPost,
							   final @RequestParam(required=true, value="SAMLRequest") String samlRequest,
							   final @RequestParam(required=false, value="RelayState") String relayState) throws IOException, ServletException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		log.info(String.format("SAMLLogin: %s", request.getMethod()));
		log.info(String.format("SAMLRequest: %s", samlRequest));
		log.info(String.format("RelayState: %s", relayState));
		if(StringUtils.equalsIgnoreCase("true", wasPost)) {
			request.setAttribute("SAMLRequest", samlRequest);
			request.setAttribute("RelayState", relayState);
			return "auth/doSAMLPostBack";
		} else {
			return doSamlLogin(request, response);
		}
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String samlLoginPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		return doSamlLogin(request, response);
	}
	
	public String doSamlLogin(final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		final SAMLResponseToken token = samlIdpProvider.generateSAMLResponse(request, response);
		return handleSAMLResponse(token, request);
	}

	@Override
	protected SAMLService<? extends SignableSAMLObject, ? extends SignableSAMLObject, SAMLIdentityProvider> getSAMLService() {
		return samlIdpProvider;
	}
}
