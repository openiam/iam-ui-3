package org.openiam.ui.idp.saml.model;

import org.openiam.ui.util.messages.Errors;

public class AbstractSAMLToken {

	private String customErrorMessage;
	private Errors error;
	private Throwable exception;

	public boolean isError() {
		return (error != null);
	}

	public Errors getError() {
		return error;
	}

	public Throwable getException() {
		return exception;
	}

	public String getCustomErrorMessage() {
		return customErrorMessage;
	}

	public void setCustomErrorMessage(String customErrorMessage) {
		this.customErrorMessage = customErrorMessage;
	}

	public void setError(final Errors error, final Throwable exception) {
		this.error = error;
		this.exception = exception;
	}
}
