package org.openiam.ui.idp.saml.service;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.openiam.am.srvc.dto.AuthResourceAttributeMap;
import org.openiam.idm.srvc.audit.constant.AuditAction;
import org.openiam.idm.srvc.audit.constant.AuditAttributeName;
import org.openiam.idm.srvc.audit.constant.AuditSource;
import org.openiam.idm.srvc.audit.dto.IdmAuditLog;
import org.openiam.idm.srvc.auth.dto.Login;
import org.openiam.idm.srvc.auth.service.AuthenticationService;
import org.openiam.ui.idp.saml.exception.AuthenticationException;
import org.openiam.ui.idp.saml.model.SAMLResponseToken;
import org.openiam.ui.idp.saml.model.idp.SAMLIDPMetadataResponse;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutRequestToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutResponseToken;
import org.openiam.ui.idp.saml.provider.SAMLIdentityProvider;
import org.openiam.ui.idp.saml.util.SAMLAttributeBuilder;
import org.openiam.ui.idp.saml.util.SAMLDigestUtils;
import org.openiam.ui.idp.saml.util.SAMLSessionUtils;
import org.openiam.ui.util.URIUtils;
import org.openiam.ui.util.messages.Errors;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.binding.encoding.HTTPRedirectDeflateEncoder;
import org.opensaml.saml2.core.*;
import org.opensaml.saml2.core.impl.NameIDBuilder;
import org.opensaml.saml2.core.impl.SessionIndexBuilder;
import org.opensaml.saml2.metadata.*;
import org.opensaml.saml2.metadata.impl.*;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.ws.transport.http.HTTPOutTransport;
import org.opensaml.ws.transport.http.HttpServletResponseAdapter;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.credential.UsageType;
import org.opensaml.xml.security.keyinfo.KeyInfoGenerator;
import org.opensaml.xml.security.x509.X509KeyInfoGeneratorFactory;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.Signer;
import org.opensaml.xml.util.XMLHelper;
import org.opensaml.xml.validation.ValidationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;

@Component("samlService")
public class SAMLIdpServiceProviderImpl extends AbstractSAMLService<AuthnRequest, Response, SAMLIdentityProvider> implements SAMLIdpServiceProvider, InitializingBean {
	
	@Value("${openiam.ui.logouturl}")
	private String samlLogoutURL;
	
	@Resource(name="authServiceClient")
	private AuthenticationService authServiceClient;
	
	@Override
	public SAMLIDPMetadataResponse getMetadata(final HttpServletRequest request, final String samlProviderId) {
		final SAMLIDPMetadataResponse token = new SAMLIDPMetadataResponse();
		try {
			final MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
			
			final SAMLIdentityProvider provider = getSAMLProviderById(samlProviderId);
			
			if(provider == null) {
				throw new AuthenticationException(String.format("SAMLProvider with ID '%s' not configured or doesn't exist", samlProviderId), Errors.SAML_NOT_CONFIGURED_FOR_ACS);
			}
			
			if(!provider.isMetadataExposed()) {
				throw new AuthenticationException(String.format("SAMLProvider with ID '%s' does not expose metdata", samlProviderId), Errors.SAML_METADATA_NOT_EXPOSED);
			}
			
			final XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
			
			final EntityDescriptor entityDescriptor = ((EntityDescriptorBuilder)builderFactory.getBuilder(EntityDescriptor.DEFAULT_ELEMENT_NAME)).buildObject();

			entityDescriptor.setEntityID(getResponseIssuer(request, provider));
			entityDescriptor.setCacheDuration(providerSweepTime);
			entityDescriptor.setID(generateId());

			final IDPSSODescriptor idpSSODescriptor = ((IDPSSODescriptorBuilder)builderFactory.getBuilder(IDPSSODescriptor.DEFAULT_ELEMENT_NAME)).buildObject();
			idpSSODescriptor.setWantAuthnRequestsSigned(false);
			//idpSSODescriptor.addSupportedProtocol(SAMLConstants.SAML20_NS);
			
			final KeyDescriptor keyDescriptor = ((KeyDescriptorBuilder)builderFactory.getBuilder(KeyDescriptor.DEFAULT_ELEMENT_NAME)).buildObject();
			if(provider.isSign()) {
				keyDescriptor.setUse(UsageType.SIGNING);
				final Credential signingCredential = getSigningCredential(provider);
				final X509KeyInfoGeneratorFactory keyInfoGeneratorFactory = new X509KeyInfoGeneratorFactory();  
				keyInfoGeneratorFactory.setEmitEntityCertificate(true);
				final KeyInfoGenerator keyInfoGenerator = keyInfoGeneratorFactory.newInstance();
				keyDescriptor.setKeyInfo(keyInfoGenerator.generate(signingCredential));
			} else {
				keyDescriptor.setUse(UsageType.UNSPECIFIED);
			}
			idpSSODescriptor.getKeyDescriptors().add(keyDescriptor);
			
			final NameIDFormat nameIdFormat = ((NameIDFormatBuilder)builderFactory.getBuilder(NameIDFormat.DEFAULT_ELEMENT_NAME)).buildObject();
			nameIdFormat.setFormat(getNameIdFormat(provider));
			idpSSODescriptor.getNameIDFormats().add(nameIdFormat);
			
			final String baseURI = URIUtils.getBaseURI(request);
			final String loginURI = new StringBuilder(baseURI).append(idpLoginURL).toString();
			final String logoutURI = new StringBuilder(baseURI).append(idpLogoutURL).toString();
			for(final String binding : new String[] {SAMLConstants.SAML2_REDIRECT_BINDING_URI, SAMLConstants.SAML2_POST_BINDING_URI}) {
				final SingleSignOnService ssoService = ((SingleSignOnServiceBuilder)builderFactory.getBuilder(SingleSignOnService.DEFAULT_ELEMENT_NAME)).buildObject();
				ssoService.setBinding(binding);
				ssoService.setLocation(loginURI);
				ssoService.setResponseLocation(loginURI);
				idpSSODescriptor.getSingleSignOnServices().add(ssoService);
				
				final SingleLogoutService ssoLogoutService = ((SingleLogoutServiceBuilder)builderFactory.getBuilder(SingleLogoutService.DEFAULT_ELEMENT_NAME)).buildObject();
				ssoLogoutService.setBinding(binding);
				ssoLogoutService.setLocation(logoutURI);
				ssoLogoutService.setResponseLocation(logoutURI);
				idpSSODescriptor.getSingleLogoutServices().add(ssoLogoutService);
			}
			
			if(CollectionUtils.isNotEmpty(provider.getAmAttributes())) {
				for(final AuthResourceAttributeMap attribute : provider.getAmAttributes()) {
					idpSSODescriptor.getAttributes().add(SAMLAttributeBuilder.buildMetadataAttribute(attribute, builderFactory));
				}
			}
			
			idpSSODescriptor.addSupportedProtocol(SAMLConstants.SAML20_NS);
			entityDescriptor.getRoleDescriptors().add(idpSSODescriptor);

			/* sign the response.  this MUST BE THE LAST BLOCK, otheriwse you'll get no signature!!!!! */
			if(provider.isSign()) {
				final Signature signature = getSignature(provider, builderFactory);
				entityDescriptor.setSignature(signature);
				marshallerFactory.getMarshaller(entityDescriptor).marshall(entityDescriptor);
				Signer.signObject(signature);
			}
			final Element entityDescriptorElement = marshallerFactory.getMarshaller(entityDescriptor).marshall(entityDescriptor);
			token.setEntityDescriptor(entityDescriptor);
			token.setEntityDescriptorElement(entityDescriptorElement);
		} catch(SecurityException e) {
			token.setError(Errors.SAML_SECURITY_EXCEPTION, e);
			log.warn(String.format("Authencation Exception: %s", e.getMessage()));
		} catch(InvalidKeySpecException e) {
			token.setError(Errors.AUTH_PROVIDER_SECURITY_KEYS_INVALID, e);
			log.warn(String.format("Invalid Key Exception: %s", e.getMessage()), e);
		} catch(AuthenticationException e) {
			token.setError(e.getError(), e);
			log.warn(String.format("Authencation Exception: %s", e.getMessage()));
		} catch(Throwable e) {
			log.error("SAML Exception", e);
			token.setError(Errors.INTERNAL_ERROR, e);
		}
		return token;
	}

	
	/**
	 * This function is called after the user has logged into our system.  At this point, he has a SAML Token, as well as a Session
	 * within the IdP.  Since he has already authenticated, we need to:
	 * 1) Check if the User has a login with the requested Managed System (part of the SAMLProvider)
	 * 2) Generate a SAML Response to be sent back to the ACS
	 * 
	 * The following links were used to reverse engineer SAML, generates the keys, etc
	 * https://developers.google.com/google-apps/sso/saml_reference_implementation
	 * https://developers.google.com/google-apps/sso/saml_reference_implementation_web#samlReferenceImplementationWebCodeStructure
	 * https://developers.google.com/google-apps/help/faq/saml-sso
	 * https://developers.google.com/google-apps/help/articles/sso-keygen
	 * http://docs.oasis-open.org/security/saml/v2.0/saml-core-2.0-os.pdf
	 * http://na14.salesforce.com/help/doc/en/sso_saml_assertion_examples.htm
	 */
	@Override
	public SAMLResponseToken generateSAMLResponse(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) {
		SAMLResponseToken token = new SAMLResponseToken();
		final String relayState = httpRequest.getParameter("RelayState");
		final String userId = cookieProvider.getUserId(httpRequest);
		final String principal = cookieProvider.getPrincipal(httpRequest);
		try {
			final AuthnRequest authnRequest = SAMLDigestUtils.getAuthNRequest(httpRequest, skipURICheck);
			final String requestIssuer = authnRequest.getIssuer().getValue();
			final SAMLIdentityProvider provider = getSAMLIdentityProviderByRequestIssuer(requestIssuer);
	
			SAMLSessionUtils.setLastAuthnRequest(httpRequest, authnRequest);
			token = getSAMLResponeToken(provider, authnRequest, relayState, userId, principal, httpRequest, httpResponse, null);
		} catch(SecurityException e) {
			token.setError(Errors.SAML_SECURITY_EXCEPTION, e);
			log.warn(String.format("Authencation Exception: %s", e.getMessage()));
		} catch(Throwable e) {
			log.error("SAML Exception", e);
			token.setError(Errors.INTERNAL_ERROR, e);
		}
		return token;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			sweep();
		} catch(Throwable e) {
			/* in case ESB not started */
			log.warn("Could not load SAML Providers - will retry on next hit, or refesh interval...");
		}
	}

	@Override
	public SAMLLogoutResponseToken<SAMLIdentityProvider> getSAMLLogoutResponse(HttpServletRequest request) {
		final IdmAuditLog auditLog = auditLogProvider.newInstance(request);
		auditLog.setAction(AuditAction.SAML_SP_LOGOUT_READ.value());
		
		final SAMLLogoutResponseToken<SAMLIdentityProvider> token = new SAMLLogoutResponseToken<>();
		final DateTime now = new DateTime(DateTimeZone.UTC);
		try {
			final MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
			final LogoutResponse xmlRequest = SAMLDigestUtils.getLogoutResponse(request, skipURICheck);
			
			final Element xmlRequestElmt = marshallerFactory.getMarshaller(xmlRequest).marshall(xmlRequest);
			final String prettyXML = XMLHelper.prettyPrintXML(xmlRequestElmt);
			auditLog.addAttribute(AuditAttributeName.SAML_XML, prettyXML);
			if(log.isDebugEnabled()) {
				log.debug(String.format("Logout Response: %s", prettyXML));
			}
			
			validateTimestamps(xmlRequest, now, request);
			
			if(xmlRequest.getIssuer() == null || StringUtils.isBlank(xmlRequest.getIssuer().getValue())) {
				throw new AuthenticationException(String.format("response.issuer is missing or empty"), Errors.SAML_SP_EXCEPTION);
			}
			
			final String issuer = xmlRequest.getIssuer().getValue();
			
			final SAMLIdentityProvider authProvider = getSAMLIdentityProviderByRequestIssuer(issuer);
			if(authProvider == null) {
				throw new AuthenticationException(String.format("SAMLIdentityProvider with Request Issuer '%s' not configured or doesn't exist", issuer), Errors.SAML_SERVICE_PROVIDER_NOT_FOUND);
			}
			
			if(authProvider.isSign()) {
				validateSAMLLogoutResponseSignature(xmlRequest, authProvider);
			}
			
			token.setSamlObject(xmlRequest);
			token.setAuthProvider(authProvider);
			auditLog.succeed();
		} catch(AuthenticationException e) {
			auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
			token.setError(e.getError(), e);
			log.warn(String.format("Authencation Exception: %s", e.getMessage()));
		} catch(Throwable e) {
			auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
			log.error("SAML Exception", e);
			token.setError(Errors.INTERNAL_ERROR, e);
		} finally {
			auditLogProvider.add(AuditSource.SP, request, auditLog);
		}
		return token;
	}

	@Override
	public void redirectAndSend(SAMLLogoutResponseToken<SAMLIdentityProvider> token, HttpServletRequest request,
			HttpServletResponse response) throws MessageEncodingException {
		final LogoutResponse xmlRequest = token.getSamlObject();
		final Endpoint endpoint = token.getEndpoint();
		final BasicSAMLMessageContext<SAMLObject, LogoutResponse, SAMLObject> messageContext = new BasicSAMLMessageContext<SAMLObject, LogoutResponse, SAMLObject>();
		final HTTPOutTransport outTransport = new HttpServletResponseAdapter(response, URIUtils.isSecure(request));
		
		messageContext.setOutboundMessageTransport(outTransport);
		messageContext.setOutboundMessage(xmlRequest);
		messageContext.setOutboundSAMLMessage(xmlRequest);
		messageContext.setPeerEntityEndpoint(endpoint);
		new HTTPRedirectDeflateEncoder().encode(messageContext);
	}


	@Override
	protected Class<SAMLIdentityProvider> getAuthProviderClass() {
		return SAMLIdentityProvider.class;
	}


	@Override
	protected void modifyLogoutRequestBeforeSigning(final HttpServletRequest request, 
													final SAMLIdentityProvider authProvider,
			   										final XMLObjectBuilderFactory builderFactory,
			   										final LogoutRequest xmlRequest) throws AuthenticationException {
		final Login providerLogin = getLoginByUserIdNamdManagedSystemId(cookieProvider.getUserId(request), authProvider.getManagedSysId());
		if(providerLogin == null) {
			throw new AuthenticationException("User does not have a login with this managed system", Errors.AUTH_USER_NOT_PROVISIONED);
		}
		
		final NameID nameId = ((NameIDBuilder)builderFactory.getBuilder(NameID.DEFAULT_ELEMENT_NAME)).buildObject();
		nameId.setValue(providerLogin.getLogin());
		nameId.setFormat(getNameIdFormat(authProvider));
		nameId.setSPNameQualifier(getSPNameQualifier(authProvider));
		nameId.setNameQualifier(getNameQualifier(authProvider));
		xmlRequest.setNameID(nameId);
		if (request != null) {
			if (StringUtils.isNotBlank(request.getRequestedSessionId())) {
				final SessionIndex sessionIndex = ((SessionIndexBuilder) builderFactory.getBuilder(SessionIndex.DEFAULT_ELEMENT_NAME)).buildObject();
				sessionIndex.setSessionIndex(getSessionIndexForAuthProviderFromSession(request, authProvider.getId()));
				xmlRequest.getSessionIndexes().add(sessionIndex);
			}
		}
	}


	@Override
	public SAMLLogoutRequestToken<SAMLIdentityProvider> generateLogoutRequest(final HttpServletRequest request, final String id, final String reason) {
		final SAMLIdentityProvider authProvider = getSAMLProviderById(id);
		return super.generateLogoutRequest(request, authProvider, reason);
	}


	@Override
	protected Issuer buildIssuer(final HttpServletRequest request, final XMLObjectBuilderFactory builderFactory, final SAMLIdentityProvider authProvider) {
		try {
			return getResponseIssuer(request, builderFactory, (SAMLIdentityProvider)authProvider);
		} catch(Throwable e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public SAMLLogoutResponseToken<SAMLIdentityProvider> generateSAMLLogoutResponse(final HttpServletRequest request, final LogoutRequest logoutRequest, final String statusCode) {
		log.debug(">>>>> logoutRequest:" + logoutRequest);
		log.debug(">>>>> logoutRequest.getIssuer():" + ((logoutRequest != null) ? logoutRequest.getIssuer() : null));
		log.debug(">>>>> statusCode:" + statusCode);
         SAMLIdentityProvider authProvider =null;
		if (logoutRequest!=null){
            authProvider = getSAMLIdentityProviderByRequestIssuer(logoutRequest.getIssuer().getValue());
        }
		return super.generateSAMLLogoutResponse(request, logoutRequest, authProvider, statusCode);
	}


	@Override
	protected SAMLIdentityProvider getAuthProvider(LogoutRequest logoutRequest, HttpServletRequest request) {
		final String issuer = logoutRequest.getIssuer().getValue();
		final SAMLIdentityProvider samlProvider = getSAMLIdentityProviderByRequestIssuer(issuer);
		return samlProvider;
	}


	@Override
	protected Element signLogoutResponse(final LogoutResponse response, final SAMLIdentityProvider provider, final XMLObjectBuilderFactory builderFactory, final MarshallerFactory marshallerFactory) {
		return super.sign(response, provider, builderFactory, marshallerFactory);
	}


	@Override
	protected void validateSAMLLogoutResponseSignature(LogoutResponse logoutResponse, SAMLIdentityProvider authProvider) throws CertificateException, IOException, ValidationException {
		log.info("Not validating signature for this LogoutResponse, as it was sent by the Service Provider, which cannot sign the Signature");
	}
}
