package org.openiam.ui.idp.saml.service;

import org.openiam.ui.idp.saml.model.sp.SAMLLogoutRequestToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutResponseToken;
import org.openiam.ui.idp.saml.provider.SAMLAuthenticationProvider;
import org.opensaml.common.SignableSAMLObject;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.ws.message.encoder.MessageEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SAMLService<RequestObject extends SignableSAMLObject, ResponseObject extends SignableSAMLObject, P extends SAMLAuthenticationProvider> {

	void redirectAndSend(SAMLLogoutRequestToken<P> token, HttpServletResponse response, HttpServletRequest request) throws MessageEncodingException;
	
	SAMLLogoutRequestToken<P> validateAndExtractLogoutRequest(HttpServletRequest request);
	
	SAMLLogoutResponseToken<P> generateSAMLLogoutResponse(HttpServletRequest request, LogoutRequest logoutRequest, String statusCode);
}
