package org.openiam.ui.idp.saml.model;

import org.openiam.ui.idp.saml.provider.SAMLAuthenticationProvider;
import org.opensaml.saml2.core.AuthnRequest;

public class SAMLRequestToken<P extends SAMLAuthenticationProvider> extends AbstractSignableMessageToken<AuthnRequest, P> {

}
