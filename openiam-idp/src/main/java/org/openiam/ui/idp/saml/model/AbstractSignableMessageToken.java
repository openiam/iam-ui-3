package org.openiam.ui.idp.saml.model;

import org.openiam.ui.idp.saml.provider.SAMLAuthenticationProvider;
import org.openiam.ui.idp.saml.provider.SAMLServiceProvider;
import org.opensaml.common.SignableSAMLObject;
import org.opensaml.saml2.metadata.Endpoint;

public abstract class AbstractSignableMessageToken<T extends SignableSAMLObject, P extends SAMLAuthenticationProvider> extends AbstractSAMLToken {
    private T samlObject;
    private P authProvider;
    private Endpoint endpoint;

    public Endpoint getEndpoint() {
        return endpoint;
    }
    public void setEndpoint(Endpoint endpoint) {
        this.endpoint = endpoint;
    }
    public T getSamlObject() {
        return samlObject;
    }
    public void setSamlObject(T samlObject) {
        this.samlObject = samlObject;
    }
    public P getAuthProvider() {
        return authProvider;
    }
    public void setAuthProvider(P authProvider) {
        this.authProvider = authProvider;
    }


}
