package org.openiam.ui.idp.saml.exception;

/**
 * A custom exception which can be thrown by the implementor
 * Used by SAML Groovy classes
 * @author lbornova
 *
 */
public class CustomSAMLException extends RuntimeException {

	private CustomSAMLException() {}

	/**
	 * The message will be displayed in the UI
	 * @param message - the message that describes the error
	 */
	public CustomSAMLException(final String message) {
		super(message);
	}
}
