package org.openiam.ui.idp.web.mvc;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openiam.base.ws.MatchType;
import org.openiam.base.ws.Response;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.base.ws.SearchParam;
import org.openiam.idm.searchbeans.*;
import org.openiam.idm.srvc.audit.constant.AuditAttributeName;
import org.openiam.idm.srvc.audit.dto.IdmAuditLog;
import org.openiam.idm.srvc.auth.dto.Login;
import org.openiam.idm.srvc.auth.dto.OTPServiceRequest;
import org.openiam.idm.srvc.auth.service.AuthenticationService;
import org.openiam.idm.srvc.auth.ws.LoginDataWebService;
import org.openiam.idm.srvc.auth.ws.LoginResponse;
import org.openiam.idm.srvc.continfo.dto.EmailAddress;
import org.openiam.idm.srvc.continfo.dto.Phone;
import org.openiam.idm.srvc.msg.dto.NotificationParam;
import org.openiam.idm.srvc.msg.dto.NotificationRequest;
import org.openiam.idm.srvc.msg.service.MailService;
import org.openiam.idm.srvc.policy.dto.Policy;
import org.openiam.idm.srvc.policy.dto.PolicyAttribute;
import org.openiam.idm.srvc.pswd.dto.*;
import org.openiam.idm.srvc.pswd.service.ChallengeResponseWebService;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.idm.srvc.user.dto.UserStatusEnum;
import org.openiam.idm.srvc.user.ws.UserDataWebService;
import org.openiam.provision.service.AsynchUserProvisionService;
import org.openiam.provision.service.ProvisionService;
import org.openiam.ui.exception.ErrorMessageException;
import org.openiam.ui.exception.ErrorTokenException;
import org.openiam.ui.exception.HttpErrorCodeException;
import org.openiam.ui.idp.web.model.*;
import org.openiam.ui.util.URIUtils;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.util.messages.SuccessMessage;
import org.openiam.ui.web.model.BasicAjaxResponse;
import org.openiam.ui.web.model.ChallengeResponseModel;
import org.openiam.ui.web.model.SetPasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.Phaser;

@Controller
public class UnlockUserController extends AbstractUserStatusController {

    private static final String REQUEST_PASSWORD_RESET_NOTIFICATION = "REQUEST_PASSWORD_RESET";
    private static final String REQUEST_LOGIN_REMINDER_NOTIFICATION = "REQUEST_LOGIN_REMINDER";

    @Value("${org.openiam.password.unlock.secure.enabled}")
    private boolean isUnlockSecure;

    @Value("${org.openiam.selfservice.challenge.response.group}")
    private String challengeResponseGroup;

    @Value("${org.openiam.idp.unlock.password.email.enabled}")
    private boolean isEmailUnlockEnabled;

    @Resource(name = "challengeResponseServiceClient")
    private ChallengeResponseWebService challengeResponseService;

    @Value("${org.openiam.idp.password.unlock.sms.token.enabled}")
    private boolean isUnlockSmsToken;
    @Value("${org.openiam.idp.password.unlock.email.enabled}")
    private boolean isUnlockEmail;


    @Autowired
    @Resource(name = "loginServiceClient")
    private LoginDataWebService loginServiceClient;

    @Resource(name = "provisionServiceClient")
    private ProvisionService provisionService;

    @Resource(name = "asynchProvisionServiceClient")
    private AsynchUserProvisionService asynchUserProvisionService;

    @Resource(name = "userServiceClient")
    private UserDataWebService userDataWebService;

    @Resource(name = "mailServiceClient")
    private MailService mailService;

    @Value("${org.openiam.ui.password.unlock.questions.url}")
    private String unlockQuestionsURL;

    @Value("${org.openiam.ui.challenge.answers.secured}")
    private Boolean secureAnswers;

    @Autowired
    private AuthenticationService authenticationService;

    private static Logger LOG = Logger.getLogger(UnlockUserController.class);


    @RequestMapping(value = "/unlockUserResetPasswordForm", method = RequestMethod.GET)
    public String unlockPasswordForm(final HttpServletRequest request,
                                     final HttpServletResponse response,
                                     final @RequestParam(required = true, value = "token") String token) {
        request.setAttribute("token", token);
        return passwordResetFormView;
    }

    @RequestMapping(value = "/unlockUserResetPasswordForm", method = RequestMethod.POST)
    public String unlockPasswordForm(final HttpServletRequest request,
                                     final HttpServletResponse response,
                                     final UnlockPasswordFormRequest formRequest) throws IOException {
        final String referer = request.getHeader("referer");
        final String redirectTo = (StringUtils.containsIgnoreCase(referer, "webconsole")) ? "/webconsole" : "/selfservice";

        List<ErrorToken> errorList = null;
        String view = null;
        SetPasswordToken token = null;
        try {
            Policy policy = this.getAuthentificationPolicy();
            final Login login = attemptToResetPassword(request, formRequest, policy);

            final Response unlockResponse = loginServiceClient.unLockLogin(login.getLogin(), this.getAuthentificationManagedSystem(policy));
            if (unlockResponse == null || unlockResponse.getStatus() != ResponseStatus.SUCCESS) {
                throw new ErrorMessageException(Errors.CHANGE_PASSWORD_FAILED);
            }
        } catch (ErrorTokenException e) {
            errorList = e.getTokenList();
        } catch (ErrorMessageException e) {
            errorList = new LinkedList<>();
            errorList.add(new ErrorToken(e.getError()));
        } catch (Throwable e) {
            LOG.error("Can't unlock user", e);
            errorList = new LinkedList<>();
            errorList.add(new ErrorToken(Errors.INTERNAL_ERROR));
        } finally {
            if (CollectionUtils.isNotEmpty(errorList)) {
                final BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
                ajaxResponse.addErrors(errorList);
                if (token != null) {
                    ajaxResponse.setPossibleErrors(token.getRules());
                }
                ajaxResponse.setStatus(500);
                ajaxResponse.process(localeResolver, messageSource, request);
                request.setAttribute("ajaxResponse", jacksonMapper.writeValueAsString(ajaxResponse));
                view = unlockPasswordForm(request, response, formRequest.getToken());
            } else {
                request.setAttribute("message", SuccessMessage.PASSWORD_CHANGED.getMessageName());
                request.setAttribute("loginTo", redirectTo);
                view = "common/message";
            }
        }
        return view;
    }

    @RequestMapping(value = "/sendSmsToken", method = RequestMethod.GET)
    public
    @ResponseBody
    BasicAjaxResponse sendSmsToken(final HttpServletRequest request,
                                   final HttpServletResponse response,
                                   @RequestParam("phoneId") String phoneId,
                                   @RequestParam("userId") String userId) {
        OTPServiceRequest otpServiceRequest = new OTPServiceRequest();
        otpServiceRequest.setUserId(userId);
        Phone phone = userDataWebService.getPhoneById(phoneId);
        otpServiceRequest.setPhone(phone);
        Response otpResponse = authenticationService.sendSMSOtp(otpServiceRequest);
        BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        if(otpResponse.isSuccess()) {
            ajaxResponse.setStatus(200);
            ajaxResponse.setRedirectURL("smsToken.html?userId=" + userId);
        } else {
            ajaxResponse.setStatus(500);
        }
        return ajaxResponse;
    }

    @RequestMapping(value = "/smsToken", method = RequestMethod.GET)
    public String smsToken(final HttpServletRequest request,
                           final HttpServletResponse response,
                           final @RequestParam("userId") String userId,
                           final Model model) {
        OTPServiceRequest otpServiceRequest = new OTPServiceRequest();
        otpServiceRequest.setUserId(userId);
        otpServiceRequest.setOtpCode("");
        model.addAttribute("smsTokenBean", otpServiceRequest);
        model.addAttribute("smsTokenPlaceholder", "SMS OTP..");
        return "core/user/smsToken";
    }

    @RequestMapping(value = "/validateSmsToken", method = RequestMethod.POST)
    @ResponseBody
    public BasicAjaxResponse validateSmsToken(@RequestParam("userId") final String userId,
                                              @RequestParam("otpCode") final String otpCode,
                                              final HttpServletRequest request,
                                              final HttpServletResponse response) {
        OTPServiceRequest otpServiceRequest = new OTPServiceRequest();
        otpServiceRequest.setOtpCode(otpCode);
        otpServiceRequest.setUserId(userId);
        Response otpResponse = authenticationService.validateSMSOtp(otpServiceRequest);
        BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        if(otpResponse.isSuccess()) {
            Policy policy = this.getAuthentificationPolicy();
            String managedSysId = this.getAuthentificationManagedSystem(policy);
            final Login login = getLoginByUserId(otpServiceRequest.getUserId(), managedSysId);
            final String passwordResetToken = getPasswordResetToken(login.getLogin(), managedSysId);
            final UnlockUserToken unlockUserToken;
            try {
                unlockUserToken = getToken(request, otpServiceRequest.getUserId(), passwordResetToken, true);
                setUnlockPasswordQuestionsParams(request, unlockUserToken);
                ajaxResponse.setRedirectURL("unlockUserResetPasswordForm.html?token=" + passwordResetToken);
                ajaxResponse.setStatus(200);
            } catch (HttpErrorCodeException e) {
                e.printStackTrace();
                ajaxResponse.setStatus(500);
            }
        } else {
            ajaxResponse.setStatus(500);
        }
        return ajaxResponse;
    }

    @RequestMapping(value = "/unlockPasswordQuestions", method = RequestMethod.POST)
    public
    @ResponseBody
    BasicAjaxResponse unlockPasswordQuestionsPOST(final HttpServletRequest request,
                                                  final HttpServletResponse response,
                                                  @RequestBody UnlockUserRequestModel requestModel) throws IOException {
        UnlockUserToken unlockUserToken = null;
        BasicAjaxResponse ajaxResponse = new BasicAjaxResponse();
        ajaxResponse.setStatus(200);
        final String userId;
        Policy policy = this.getAuthentificationPolicy();
        String managedSysId = this.getAuthentificationManagedSystem(policy);
        Login login = null;
        try {
            unlockUserToken = getToken(request, requestModel.getUserId(), requestModel.getToken(), false);
            userId = unlockUserToken.getUser().getId();
            login = getLoginByUserId(userId, managedSysId);
            Policy passwordPolicy = passwordService.getPasswordPolicy(login.getLogin(), managedSysId);
            PolicyAttribute policyAttribute = passwordPolicy.getAttribute("FAILED_QUESTION_COUNT");
            String failAnswersCounter = policyAttribute == null ? null : policyAttribute.getValue1();
            if (login == null) {
                throw new HttpErrorCodeException(HttpServletResponse.SC_NOT_FOUND);
            }
            if (policyAttribute != null && policyAttribute.isRequired())
                this.checkPossibilityToAnswer(userId, login, managedSysId, failAnswersCounter);

            final List<QuestionAnswerBean> answerMap = requestModel.getAnswers();
            if (CollectionUtils.isEmpty(answerMap)) {
                throw new HttpErrorCodeException(HttpServletResponse.SC_NOT_FOUND);
            }


            final IdentityAnswerSearchBean searchBean = new IdentityAnswerSearchBean();
            searchBean.setUserId(userId);
            final List<UserIdentityAnswer> searchResult = challengeResponseService.findAnswerBeans(searchBean, getRequesterId(request), 0, 1000);
            if (CollectionUtils.isNotEmpty(searchResult)) {
                for (final QuestionAnswerBean bean : answerMap) {
                    for (UserIdentityAnswer userIdentityAnswer : searchResult) {
                        if (StringUtils.equals(userIdentityAnswer.getId(), bean.getQuestionId())) {
                            userIdentityAnswer.setQuestionAnswer(StringUtils.trimToNull(bean.getAnswerValue()));
                            break;
                        }
                    }
                }
            }
            if (!challengeResponseService.isResponseValid(unlockUserToken.getUser().getId(), searchResult)) {
                if (policyAttribute.isRequired())
                    this.processFailAnswers(userId, login, managedSysId, failAnswersCounter);
                throw new ErrorMessageException(Errors.IDENTITY_QUESTIONS_INCORRECT);
            }

            login.setChallengeResponseFailCount(0);
            loginServiceClient.saveLogin(login);

            final String passwordResetToken = getPasswordResetToken(login.getLogin(), managedSysId);
            if (StringUtils.isBlank(passwordResetToken)) {
                throw new HttpErrorCodeException(HttpServletResponse.SC_UNAUTHORIZED);
            }

            ajaxResponse.setRedirectURL("unlockUserResetPasswordForm.html?token=" + passwordResetToken);
        } catch (ErrorMessageException e) {
            ajaxResponse.setStatus(500);
            ErrorToken tokenError = new ErrorToken();
            tokenError.setMessage(e.getError().getMessageName());
            ajaxResponse.addError(tokenError);
            loginServiceClient.saveLogin(login);
        } catch (HttpErrorCodeException e) {
            ajaxResponse.setStatus(e.getErrorCode());
        } catch (Throwable e) {
            ajaxResponse.setStatus(500);
            LOG.error("Can't unlock user", e);
        }
        return ajaxResponse;
    }

    private void processFailAnswers(String userId, Login login, String managedSysId, String amountOfWrongAnswers) throws ErrorMessageException {
        if (login == null || userId == null || managedSysId == null) return;
        login.setChallengeResponseFailCount(login.getChallengeResponseFailCount() == null ? 1 : login.getChallengeResponseFailCount() + 1);
        try {
            this.checkPossibilityToAnswer(userId, login, managedSysId, amountOfWrongAnswers);
        } catch (ErrorMessageException e) {
            try {
                Response response = userDataWebService.setSecondaryStatus(userId, UserStatusEnum.LOCKED);
                if (response.isFailure()) {
                    throw new ErrorMessageException(Errors.CANNOT_LOCK_USER);
                }
            } catch (Exception e1) {
                throw new ErrorMessageException(Errors.CANNOT_LOCK_USER);
            } finally {
                throw e;
            }
        }
    }


    private void checkPossibilityToAnswer(String userId, Login login, String managedSysId, String amountOfWrongAnswers) throws ErrorMessageException {
        if (login == null || userId == null || managedSysId == null) return;
        if (StringUtils.isNotBlank(amountOfWrongAnswers)) {
            int counter = 0;
            try {
                counter = Integer.parseInt(amountOfWrongAnswers);
                if (counter == login.getChallengeResponseFailCount() || counter < login.getChallengeResponseFailCount()) {
                    throw new ErrorMessageException(Errors.TOO_MANY_WRONG_ANSWERS);
                }
            } catch (NumberFormatException e) {
                log.warn(e.getMessage());
            }
        }
    }

    @RequestMapping(value = "/unlockPasswordQuestions", method = RequestMethod.GET)
    public String unlockPasswordQuestions(final HttpServletRequest request,
                                          final HttpServletResponse response,
                                          final @RequestParam(required = false, value = "userId") String unsecureUserId,
                                          final @RequestParam(required = false, value = "token") String token) throws IOException {
        String view = null;
        try {
            final UnlockUserToken unlockUserToken = getToken(request, unsecureUserId, token, false);
            setUnlockPasswordQuestionsParams(request, unlockUserToken);
            view = "core/user/unlockPasswordQuestions";
        } catch (HttpErrorCodeException e) {
            //response.sendError(e.getErrorCode(), e.getErrorMessage());
            request.setAttribute("error", new ErrorToken(Errors.UNLOCK_INVALID_TOKEN));
            view="common/message";
        } catch (Throwable e) {
            LOG.error("Can't unlock user", e);
            request.setAttribute("error", new ErrorToken(Errors.UNLOCK_ERROR));
            view="common/message";
        }
        return view;
    }

    private void setUnlockPasswordQuestionsParams(final HttpServletRequest request, final UnlockUserToken unlockUserToken) {
        request.setAttribute("unlockUserToken", unlockUserToken);
        request.setAttribute("currentURI", URIUtils.getRequestURL(request));
        request.setAttribute("secureAnswers", secureAnswers);
    }

    @RequestMapping(value = "/unlockPassword", method = RequestMethod.POST)
    public String resetPasswordPOST(final @Valid @ModelAttribute("passwordBean") UnlockPasswordBean passwordBean,
                                    BindingResult result,
                                    final HttpServletRequest request,
                                    final HttpServletResponse response) throws IOException {
        final IdmAuditLog auditLog = new IdmAuditLog();
        auditLog.setAction("Unlock password");
        auditLog.setPrincipal(passwordBean.getPrincipal());
        auditLog.addAttribute(AuditAttributeName.EMAIL, passwordBean.getEmail());
        if (!passwordUnlockEnabled) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }

        final String referer = request.getHeader("referer");
        final String redirectTo = (StringUtils.containsIgnoreCase(referer, "webconsole")) ? "/webconsole" : "/selfservice";

        Errors error = null;
        String view = "core/user/unlockUser";
        try {
            Policy policy = this.getAuthentificationPolicy();
            String internalLogin = this.buildLoginAccordingAuthPolicy(policy, passwordBean.getPrincipal());
            String managedSysId = this.getAuthentificationManagedSystem(policy);
            if (StringUtils.isNotBlank(passwordBean.getPrincipal())) {
                final User user = getUserByPrincipal(internalLogin, managedSysId);
                if (user == null) {
                    throw new ErrorMessageException(Errors.USER_NOT_FOUND);
                }
                if (CollectionUtils.isEmpty(getQuestionsForUser(request, user.getId()))) {
                    throw new ErrorMessageException(Errors.IDENTITY_QUESTIONS_NOT_ANSWERED);
                }

                Login l = loginServiceClient.getPrimaryIdentity(user.getId()).getPrincipal();
                Policy passwordPolicy = passwordService.getPasswordPolicy(l.getLogin(), managedSysId);
                PolicyAttribute policyAttribute = passwordPolicy.getAttribute("FAILED_QUESTION_COUNT");
                String failAnswersCounter = policyAttribute == null ? null : policyAttribute.getValue1();
                if (policyAttribute != null && policyAttribute.isRequired())
                    checkPossibilityToAnswer(user.getId(), l, managedSysId, failAnswersCounter);

                if (isUnlockSecure) {
                    if (!sendTokenEmail(request, user, internalLogin, managedSysId)) {
                        throw new ErrorMessageException(Errors.COULD_NOT_SEND_EMAIL);
                    }
                    request.setAttribute("message", SuccessMessage.UNLOCK_EMAIL_SENT.getMessageName());
                    request.setAttribute("loginTo", redirectTo);
                    view = "common/message";
                } else {
                    view = unlockPasswordQuestions(request, response, user.getId(), null);
                }

            } else if (isUnlockSecure && StringUtils.isNotBlank(passwordBean.getEmail())) {

                if (result.hasErrors()) {
                    return view;
                }

                final Set<User> users = getUserListByEmail(passwordBean.getEmail());
                if (CollectionUtils.isEmpty(users)) {
                    throw new ErrorMessageException(Errors.USER_NOT_FOUND);
                }
                for (final User user : users) {
                    final Login login = getLoginByUserId(user.getId(), managedSysId);
                    if (login != null) {
                        if (!sendTokenEmail(request, user, login.getLogin(), managedSysId)) {
                            throw new ErrorMessageException(Errors.COULD_NOT_SEND_EMAIL);
                        }
                        request.setAttribute("message", SuccessMessage.UNLOCK_EMAIL_SENT.getMessageName());
                        request.setAttribute("loginTo", redirectTo);
                        view = "common/message";
                    }
                    /*
                    if(!sendLoginReminderEmail(u)) {
                        throw new ErrorMessageException(Errors.COULD_NOT_SEND_EMAIL);
                    }
                    */
                }
                request.setAttribute("message", SuccessMessage.LOGIN_REMINDER_SENT.getMessageName());
                view = "common/message";

            } else {
                throw new ErrorMessageException(Errors.INVALID_REQUEST);
            }
            auditLog.succeed();
        } catch (ErrorMessageException e) {
            error = e.getError();
            auditLog.fail();
            auditLog.setFailureReason(getMessage(request, e.getError()));
            auditLog.setException(e);
            view = "core/user/unlockUser";
        } catch (Throwable e) {
            error = Errors.INTERNAL_ERROR;
            auditLog.fail();
            auditLog.setFailureReason(getMessage(request, error));
            auditLog.setException(e);
            LOG.error("Can't unlock user", e);
        } finally {
            if (error != null) {
                request.setAttribute("error", new ErrorToken(error));
            }
            auditLogService.addLog(auditLog);
        }
        return view;
    }

    @RequestMapping(value = "/unlockPassword", method = RequestMethod.GET)
    public String resetPassword(final HttpServletResponse response, final Model model) throws IOException {
        if (!passwordUnlockEnabled) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        model.addAttribute("passwordBean", new UnlockPasswordBean());
        return "core/user/unlockUser";
    }

    @RequestMapping(value = "/userByLogin", method = RequestMethod.GET)
    @ResponseBody
    public String findUserByLogin(@RequestParam(value = "username") String  username) {
        //LoginSearchBean bean = new LoginSearchBean();
        //bean.setLoginMatchToken(new SearchParam(username, MatchType.EXACT));
        //List<Login> logins = loginServiceClient.findBeans(bean, 0, 1);
        User user = userDataWebService.getUserByPrincipal(username.toLowerCase(), "0", false);
        if(user == null || user.getId() == null) {
            return "";
        }
        return user.getId();
    }

    @RequestMapping(value = "/forgetPasswordOptions", method = RequestMethod.GET)
    public String forgetPasswordOptions(final HttpServletRequest request, @RequestParam(value = "userId", required = true) String userId, final Model modelObj) throws Exception {
        final IdentityQuestionSearchBean questionSearchBean = new IdentityQuestionSearchBean();
        questionSearchBean.setDeepCopy(false);
        questionSearchBean.setGroupId(challengeResponseGroup);
        questionSearchBean.setActive(true);
        //all questions
        final List<IdentityQuestion> questionList = challengeResponseService.findQuestionBeans(questionSearchBean, 0, Integer.MAX_VALUE, getCurrentLanguage());
        // questions as a MAP
        Map<String, IdentityQuestion> questionMap = null;
        if (CollectionUtils.isNotEmpty(questionList)) {
            questionMap = new HashMap<String, IdentityQuestion>();
            for (IdentityQuestion identityQuestion : questionList) {
                questionMap.put(identityQuestion.getId(), identityQuestion);
            }
        }

        final IdentityAnswerSearchBean answerSearchBean = new IdentityAnswerSearchBean();
        answerSearchBean.setDeepCopy(true);
        answerSearchBean.setUserId(userId);
        //all answers
        List<UserIdentityAnswer> answerList ;
        try {
            answerList = challengeResponseService.findAnswerBeans(answerSearchBean, userId, 0, Integer.MAX_VALUE);
        } catch (Exception e) {
            answerList = new ArrayList<UserIdentityAnswer>();
        }
        //get number of Required Questions Enterprise
        Integer numOfRequiredQuestionsEnterprise = challengeResponseService.getNumOfRequiredQuestions(userId, true);
        numOfRequiredQuestionsEnterprise = (numOfRequiredQuestionsEnterprise == null) ? 0 : numOfRequiredQuestionsEnterprise;
        //get number of Required Questions Custom
        Integer numOfRequiredQuestionsCustom = challengeResponseService.getNumOfRequiredQuestions(userId, false);
        numOfRequiredQuestionsCustom = (numOfRequiredQuestionsCustom == null) ? 0 : numOfRequiredQuestionsCustom;
        final List<ChallengeResponseModel> modelListEnterprise = new ArrayList<ChallengeResponseModel>(numOfRequiredQuestionsEnterprise.intValue());
        final List<ChallengeResponseModel> modelListCustom = new ArrayList<ChallengeResponseModel>(numOfRequiredQuestionsCustom.intValue());

        if (CollectionUtils.isNotEmpty(answerList)) {
            int counterEnterprise = 0;
            int counterCustom = 0;
            ChallengeResponseModel model = null;
            for (UserIdentityAnswer userIdentityAnswer : answerList) {
                if (userIdentityAnswer.getQuestionId() != null) {
                    model = new ChallengeResponseModel(questionList, userIdentityAnswer);
                    if (counterEnterprise < numOfRequiredQuestionsEnterprise) {
                        if (questionMap.get(userIdentityAnswer.getQuestionId()) != null) {
                            counterEnterprise++;
                            model.getAnswer().setQuestionText(questionMap.get(userIdentityAnswer.getQuestionId()).getDisplayName());
                        }
                    } else {
                        model.setMarkedAsDeleted(true);
                    }
                    modelListEnterprise.add(model);
                } else {
                    model = new ChallengeResponseModel(null, userIdentityAnswer);
                    if (counterCustom < numOfRequiredQuestionsCustom) {
                        counterCustom++;
                    } else {
                        model.setMarkedAsDeleted(true);
                    }
                    modelListCustom.add(model);
                }
            }

        }
        UserSearchBean userSearchBean = new UserSearchBean();
        //userSearchBean.
        userSearchBean.setKey(userId);
        userSearchBean.setDeepCopy(true);
        List<User> users = userDataWebService.findBeans(userSearchBean, 0, 1);
        if (users != null && !users.isEmpty()) {
            User user = users.get(0);
            Set<Phone> phones = user.getPhones();
            List<Phone> phoneLists = new ArrayList<Phone>();
            for (Phone phone : phones) {
                if(phone.getMetadataTypeId() != null && phone.getMetadataTypeId().equals("CELL_PHONE")) {
                    if (phone.getPhoneNbr() != null && !phone.getPhoneNbr().isEmpty()) {
                        StringBuilder s = new StringBuilder();
                        for (int i = 0; i < phone.getPhoneNbr().length() - 2; i++) {
                            s.append("x");
                        }
                        s.append(phone.getPhoneNbr().substring(phone.getPhoneNbr().length() - 2));
                        phone.setPhoneNbr(s.toString());
                        phoneLists.add(phone);
                    }
                }
            }
            request.setAttribute("phones", phoneLists);
            Policy policy = this.getAuthentificationPolicy();
            String managedSysId = this.getAuthentificationManagedSystem(policy);
            final Login login = getLoginByUserId(user.getId(), managedSysId);
            final String passwordResetToken = getPasswordResetToken(login.getLogin(), managedSysId);
            UnlockUserToken unlockUserToken = null;
            try {
                unlockUserToken = getToken(request, userId, passwordResetToken, false);
                setUnlockPasswordQuestionsParams(request, unlockUserToken);
                request.setAttribute("token", passwordResetToken);
            } catch(HttpErrorCodeException e) {
            }
            UnlockPasswordBean unlockPasswordBean = new UnlockPasswordBean();
            unlockPasswordBean.setEmail(user.getEmail());
            unlockPasswordBean.setPrincipal(login.getLogin());
            modelObj.addAttribute("passwordBean", unlockPasswordBean);

            //Mask email addresses and give options of which to send to.
            Set<EmailAddress> maskedEmailAddresses = new HashSet<>();
            for(EmailAddress e : user.getEmailAddresses()){
                EmailAddress masked = new EmailAddress();
                masked.setEmailId(e.getEmailId());
                masked.setMetadataTypeId(e.getMetadataTypeId());
                masked.setEmailAddress(e.getEmailAddress().substring(0, 2) +
                        StringUtils.leftPad("", e.getEmailAddress().length()-e.getEmailAddress().substring(0, 2).length()-e.getEmailAddress().split("@")[1].length()-1, "*") +
                        "@"+ e.getEmailAddress().split("@")[1]
                        );
                maskedEmailAddresses.add(masked);
            }
            request.setAttribute("emails", maskedEmailAddresses);
            
        } else {
            modelObj.addAttribute("passwordBean", new UnlockPasswordBean());
        }
        modelListEnterprise.addAll(modelListCustom);
        request.setAttribute("modelList", modelListEnterprise);
        request.setAttribute("userId",userId);
        request.setAttribute("isSMSResetPassword", isUnlockSmsToken);
        request.setAttribute("isEmailResetPassword", isUnlockEmail);

        return "core/user/forgotOptions";
    }

    @ModelAttribute("isUnlockSecure")
    public boolean isUnlockSecure() {
        return isUnlockSecure;
    }

    @ModelAttribute("isEmailUnlockEnabled")
    public boolean isEmailUnlockEnabled() {
        return isEmailUnlockEnabled;
    }

    private UnlockUserToken getToken(final HttpServletRequest request, final String unsecureUserId, final String token, boolean unlockBySms) throws HttpErrorCodeException {
        if ((isUnlockSecure && StringUtils.isBlank(token)) || (!isUnlockSecure && StringUtils.isBlank(unsecureUserId))) {
            throw new HttpErrorCodeException(HttpServletResponse.SC_NOT_FOUND);
        }

        final Map<String, String> hiddenAttributes = new HashMap<String, String>();
        String userId = null;
        if (isUnlockSecure) {
            final ValidatePasswordResetTokenResponse validateResponse = passwordService.validatePasswordResetToken(token);
            if (validateResponse == null || !validateResponse.isSuccess() || validateResponse.getPrincipal() == null) {
                throw new HttpErrorCodeException(HttpServletResponse.SC_UNAUTHORIZED, "Token is either invalid, has expired, or can no longer be used");
            }
            userId = validateResponse.getPrincipal().getUserId();
            hiddenAttributes.put("token", token);
        } else {
            userId = unsecureUserId;
            hiddenAttributes.put("userId", userId);
        }

        final User user = getUserById(userId);
        if (user == null) {
            throw new HttpErrorCodeException(HttpServletResponse.SC_NOT_FOUND);
        }

        final List<UserIdentityAnswer> answerList = getQuestionsForUser(request, userId);
        if (CollectionUtils.isEmpty(answerList) && !unlockBySms) {
            LOG.warn(String.format("No security answers found for user %s - returning 404", userId));
            throw new HttpErrorCodeException(HttpServletResponse.SC_NOT_FOUND);
        }

        final List<QuestionAnswerBean> questionsEnterprise = new ArrayList<>();
        final List<QuestionAnswerBean> questionsCustom = new ArrayList<>();

        if(!unlockBySms) {

            // get number of Required Questions Enterprise
            Integer numOfRequiredQuestionsEnterprise = challengeResponseService.getNumOfRequiredQuestions(userId, true);
            numOfRequiredQuestionsEnterprise = (numOfRequiredQuestionsEnterprise == null) ? 0 : numOfRequiredQuestionsEnterprise;
            // get number of Required Questions Custom
            Integer numOfRequiredQuestionsCustom = challengeResponseService.getNumOfRequiredQuestions(userId, false);
            numOfRequiredQuestionsCustom = (numOfRequiredQuestionsCustom == null) ? 0 : numOfRequiredQuestionsCustom;

            final IdentityQuestionSearchBean searchBean = new IdentityQuestionSearchBean();
            searchBean.setActive(true);
            final List<IdentityQuestion> allQuestionList = challengeResponseService.findQuestionBeans(searchBean, 0, Integer.MAX_VALUE, getCurrentLanguage());
            if (CollectionUtils.isEmpty(allQuestionList)) {
                throw new HttpErrorCodeException(HttpServletResponse.SC_NOT_FOUND);
            }

            for (final UserIdentityAnswer answer : answerList) {
                if (StringUtils.isBlank(answer.getQuestionId()) && StringUtils.isNotBlank(answer.getQuestionText())
                        && questionsCustom.size() < numOfRequiredQuestionsCustom) {
                    questionsCustom.add(new QuestionAnswerBean(answer.getId(), answer.getQuestionText()));
                } else if (StringUtils.isNotBlank(answer.getQuestionId()) && StringUtils.isBlank(answer.getQuestionText())
                        && questionsEnterprise.size() < numOfRequiredQuestionsEnterprise) {
                    Iterator<IdentityQuestion> identityQuestionIterator = allQuestionList.iterator();
                    while (identityQuestionIterator.hasNext()) {
                        IdentityQuestion identityQuestion = identityQuestionIterator.next();
                        if (StringUtils.equals(identityQuestion.getId(), answer.getQuestionId())) {
                            questionsEnterprise.add(new QuestionAnswerBean(answer.getId(), identityQuestion.getDisplayName()));
                        }
                    }
                }

                if (questionsEnterprise.size() >= numOfRequiredQuestionsEnterprise && questionsCustom.size() >= numOfRequiredQuestionsCustom)
                    break;
            }

            questionsEnterprise.addAll(questionsCustom);
            if (CollectionUtils.isEmpty(questionsEnterprise)) {
                throw new HttpErrorCodeException(HttpServletResponse.SC_NOT_FOUND);
            }
        }

        return new UnlockUserToken(user, hiddenAttributes, questionsEnterprise);
    }

    private User getUserByPrincipal(final String principal, String managedSysId) {
        User user = null;
        final LoginResponse loginResponse = loginServiceClient.getLoginByManagedSys(principal, managedSysId);
        if (loginResponse != null && loginResponse.getStatus() == ResponseStatus.SUCCESS && loginResponse.getPrincipal() != null) {
            final UserSearchBean searchBean = new UserSearchBean();
            searchBean.setKey(loginResponse.getPrincipal().getUserId());
            searchBean.setDeepCopy(false);
            final List<User> userList = userDataWebService.findBeans(searchBean, 0, 1);
            user = (CollectionUtils.isNotEmpty(userList)) ? userList.get(0) : null;
        }
        return user;
    }

    private Set<User> getUserListByEmail(final String email) {
        final EmailSearchBean emailSearchBean = new EmailSearchBean();
        emailSearchBean.setEmailMatchToken(new SearchParam(email, MatchType.EXACT));
        emailSearchBean.setDeepCopy(false);
        List<EmailAddress> emailAddresses = userDataWebService.findEmailBeans(emailSearchBean, -1, -1);
        emailAddresses = (emailAddresses != null) ? emailAddresses : new LinkedList<EmailAddress>();

        final Set<User> userList = new HashSet<>();
        if (CollectionUtils.isNotEmpty(emailAddresses)) {
            for (final EmailAddress address : emailAddresses) {
                final User user = userDataWebService.getUserWithDependent(address.getParentId(), null, false);
                if (user != null) {
                    userList.add(user);
                }
            }
        }
        return userList;
    }

    private User getUserById(final String userId) {
        final UserSearchBean searchBean = new UserSearchBean();
        searchBean.setKey(userId);
        searchBean.setDeepCopy(false);
        final List<User> userList = userDataWebService.findBeans(searchBean, 0, 1);
        return (CollectionUtils.isNotEmpty(userList)) ? userList.get(0) : null;
    }

    private List<UserIdentityAnswer> getQuestionsForUser(final HttpServletRequest request, final String userId) {
        List<UserIdentityAnswer> retVal = null;
        if (StringUtils.isNotBlank(userId)) {
            final IdentityAnswerSearchBean searchBean = new IdentityAnswerSearchBean();
            searchBean.setDeepCopy(false);
            searchBean.setUserId(userId);
            try {
                retVal = challengeResponseService.findAnswerBeans(searchBean, getRequesterId(request), 0, Integer.MAX_VALUE);
//                CollectionUtil.quickSort(retVal, new Comparator<UserIdentityAnswer>() {
//                    @Override
//                    public int compare(UserIdentityAnswer o1, UserIdentityAnswer o2) {
//                        if (o1 == null && o2 == null)
//                            return 0;
//                        if (o1 == null && o2 != null)
//                            return 1;
//                        if (o1 != null && o2 == null) return -1;
//                        if (o1 != null && o2 != null) {
//                            if (o1.getQuestionId() == null && o2.getQuestionId() == null) {
//                                return 0;
//                            }
//                            if (o1.getQuestionId() == null && o2.getQuestionId() != null) {
//                                return 1;
//                            }
//                            if (o1.getQuestionId() != null && o2.getQuestionId() == null) {
//                                return -1;
//                            }
//                            if (o1.getQuestionId() != null && o2.getQuestionId() != null) {
//                                return o2.getQuestionId().compareTo(o1.getQuestionId());
//                            }
//                        }
//                        return 0;
//                    }
//                });
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return retVal;
    }

    private boolean sendTokenEmail(final HttpServletRequest request,
                                   final User user, final String principal, String managedSysId) throws URISyntaxException {
        boolean success = false;
        final String passwordResetToken = getPasswordResetToken(principal, managedSysId);
        if (StringUtils.isNotBlank(passwordResetToken)) {
            final NotificationRequest notificationRequest = new NotificationRequest();
            notificationRequest.setNotificationType(REQUEST_PASSWORD_RESET_NOTIFICATION);
            notificationRequest.setUserId(user.getId());

            final String baseURL = new StringBuilder(URIUtils.getBaseURI(request)).append(unlockQuestionsURL).toString();

            final List<NotificationParam> paramList = new LinkedList<NotificationParam>();
            paramList.add(new NotificationParam("TARGET_URL", baseURL));
            paramList.add(new NotificationParam("USER_ID", user.getId()));
            paramList.add(new NotificationParam("TOKEN", passwordResetToken));
            notificationRequest.setParamList(paramList);
            success = mailService.sendNotification(notificationRequest);
        } else {
            log.error(String.format("Password reset token was invalid (blank) for user: %s:%s", principal, (user != null) ? user.getId() : null));
        }
        return success;
    }

    private boolean sendLoginReminderEmail(final User user)
            throws URISyntaxException {
        boolean success = false;
        final LoginResponse response = loginServiceClient.getPrimaryIdentity(user.getId());
        if (response.isSuccess() && response.getPrincipal() != null) {
            final NotificationRequest notificationRequest = new NotificationRequest();
            notificationRequest.setNotificationType(REQUEST_LOGIN_REMINDER_NOTIFICATION);
            notificationRequest.setUserId(user.getId());

            final List<NotificationParam> paramList = new LinkedList<NotificationParam>();
            paramList.add(new NotificationParam("USER_ID", user.getId()));
            paramList.add(new NotificationParam("LOGIN", response.getPrincipal().getLogin()));
            notificationRequest.setParamList(paramList);
            success = mailService.sendNotification(notificationRequest);
        }

        return success;
    }

    private String getPasswordResetToken(final String principal, String managedSysId) {
        String retVal = null;
        final PasswordResetTokenRequest tokenRequest = new PasswordResetTokenRequest(principal, managedSysId);
        final PasswordResetTokenResponse tokenResponse = passwordService.generatePasswordResetToken(tokenRequest);
        if (tokenResponse != null && tokenResponse.isSuccess() && tokenResponse.getPasswordResetToken() != null) {
            retVal = tokenResponse.getPasswordResetToken();
        }
        return retVal;
    }

    private Login getLoginByUserId(final String userId, final String managedSysId) {
        LoginSearchBean lsb = new LoginSearchBean();
        lsb.setManagedSysId(managedSysId);
        lsb.setUserId(userId);
        List<Login> logins = loginServiceClient.findBeans(lsb, 0, 1);
        if (CollectionUtils.isNotEmpty(logins)) {
            return logins.get(0);
        } else return null;
    }
}
