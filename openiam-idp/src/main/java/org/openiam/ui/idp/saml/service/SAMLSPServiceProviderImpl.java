package org.openiam.ui.idp.saml.service;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.openiam.idm.srvc.audit.constant.AuditAction;
import org.openiam.idm.srvc.audit.constant.AuditAttributeName;
import org.openiam.idm.srvc.audit.constant.AuditSource;
import org.openiam.idm.srvc.audit.dto.IdmAuditLog;
import org.openiam.idm.srvc.auth.dto.Login;
import org.openiam.idm.srvc.auth.ws.LoginResponse;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.idm.srvc.user.ws.UserResponse;
import org.openiam.provision.dto.ProvisionUser;
import org.openiam.provision.resp.ProvisionUserResponse;
import org.openiam.ui.idp.saml.exception.AuthenticationException;
import org.openiam.ui.idp.saml.groovy.AbstractJustInTimeSAMLAuthenticator;
import org.openiam.ui.idp.saml.groovy.DefaultNameIDResolver;
import org.openiam.ui.idp.saml.groovy.DefaultServiceProviderRelayStateGenerator;
import org.openiam.ui.idp.saml.model.SAMLRequestToken;
import org.openiam.ui.idp.saml.model.SAMLResponseToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutRequestToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutResponseToken;
import org.openiam.ui.idp.saml.model.sp.SAMLSPMetadataResponse;
import org.openiam.ui.idp.saml.provider.SAMLServiceProvider;
import org.openiam.ui.idp.saml.util.SAMLDigestUtils;
import org.openiam.ui.idp.saml.util.SAMLSessionUtils;
import org.openiam.ui.login.LoginActionToken;
import org.openiam.ui.util.URIUtils;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.web.util.LoginProvider;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.binding.encoding.HTTPRedirectDeflateEncoder;
import org.opensaml.saml2.core.*;
import org.opensaml.saml2.core.impl.IssuerBuilder;
import org.opensaml.saml2.core.impl.NameIDBuilder;
import org.opensaml.saml2.core.impl.SessionIndexBuilder;
import org.opensaml.saml2.metadata.*;
import org.opensaml.saml2.metadata.impl.*;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.ws.transport.http.HTTPOutTransport;
import org.opensaml.ws.transport.http.HttpServletResponseAdapter;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.credential.UsageType;
import org.opensaml.xml.security.keyinfo.KeyInfoGenerator;
import org.opensaml.xml.security.x509.X509KeyInfoGeneratorFactory;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.util.XMLHelper;
import org.opensaml.xml.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

@Service
public class SAMLSPServiceProviderImpl extends AbstractSAMLService<LogoutRequest, LogoutResponse, SAMLServiceProvider> implements SAMLSPServiceProvider {


	@Autowired
	private LoginProvider loginProvider;
	
	@Override
	public SAMLSPMetadataResponse getMetadata(final HttpServletRequest request, final String id) {
		final SAMLSPMetadataResponse token = new SAMLSPMetadataResponse();
		
		try {
			final MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
			
			final SAMLServiceProvider provider = getSAMLServiceProvierById(id);
			if(provider == null) {
				throw new AuthenticationException(String.format("SAMLServiceProvider with ID '%s' not configured or doesn't exist", id), Errors.SAML_SERVICE_PROVIDER_NOT_FOUND);
			}
			
			final XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
			final EntityDescriptor entityDescriptor = ((EntityDescriptorBuilder)builderFactory.getBuilder(EntityDescriptor.DEFAULT_ELEMENT_NAME)).buildObject();
			entityDescriptor.setID(generateId());
			entityDescriptor.setCacheDuration(providerSweepTime);
			entityDescriptor.setEntityID(provider.getIssuer());
			
			final SPSSODescriptor descriptor = ((SPSSODescriptorBuilder)builderFactory.getBuilder(SPSSODescriptor.DEFAULT_ELEMENT_NAME)).buildObject();
			descriptor.setAuthnRequestsSigned(provider.isSign());
			descriptor.setWantAssertionsSigned(false); /* we only expect the Response to be signed */
			descriptor.addSupportedProtocol("urn:oasis:names:tc:SAML:2.0:protocol"); /* looks like Foregrock reads the metadata as invalid w/o this */
			
			final KeyDescriptor keyDescriptor = ((KeyDescriptorBuilder)builderFactory.getBuilder(KeyDescriptor.DEFAULT_ELEMENT_NAME)).buildObject();
			if(provider.isSign()) {
				keyDescriptor.setUse(UsageType.SIGNING);
				final Credential signingCredential = getSigningCredential(provider);
				final X509KeyInfoGeneratorFactory keyInfoGeneratorFactory = new X509KeyInfoGeneratorFactory();  
				keyInfoGeneratorFactory.setEmitEntityCertificate(true);
				final KeyInfoGenerator keyInfoGenerator = keyInfoGeneratorFactory.newInstance();
				keyDescriptor.setKeyInfo(keyInfoGenerator.generate(signingCredential));
			} else {
				keyDescriptor.setUse(UsageType.UNSPECIFIED);
			}
			descriptor.getKeyDescriptors().add(keyDescriptor);
			
			/* NameID would go here, but we have no name ID */
			for(final String format : new String[] {NameIDType.EMAIL, NameIDType.TRANSIENT}) {
				final NameIDFormat nameIdFormat = ((NameIDFormatBuilder)builderFactory.getBuilder(NameIDFormat.DEFAULT_ELEMENT_NAME)).buildObject();
				nameIdFormat.setFormat(format);
				descriptor.getNameIDFormats().add(nameIdFormat);
			}
			
			final String baseURI = URIUtils.getBaseURI(request);
			final String ssoURL = new StringBuilder(baseURI).append(spLoginURL).toString();
			final String sloURL = new StringBuilder(baseURI).append(spLogoutURL).toString();
			
			descriptor.getAssertionConsumerServices();
			//descriptor.getSingleLogoutServices();
			
			for(final String binding : new String[] {SAMLConstants.SAML2_POST_BINDING_URI}) {
				final AssertionConsumerService ssoService = ((AssertionConsumerServiceBuilder)builderFactory.getBuilder(AssertionConsumerService.DEFAULT_ELEMENT_NAME)).buildObject();
				ssoService.setBinding(binding);
				ssoService.setLocation(ssoURL);
				//ssoService.setResponseLocation(applicationLoginURL);
				ssoService.setIndex(0);
				ssoService.setIsDefault(true);
				descriptor.getAssertionConsumerServices().add(ssoService);
				
				final SingleLogoutService sloService = ((SingleLogoutServiceBuilder)builderFactory.getBuilder(SingleLogoutService.DEFAULT_ELEMENT_NAME)).buildObject();
				sloService.setBinding(binding);
				sloService.setLocation(sloURL);
				
				descriptor.getSingleLogoutServices().add(sloService);
			}
			
			descriptor.addSupportedProtocol(SAMLConstants.SAML20_NS);
			entityDescriptor.getRoleDescriptors().add(descriptor);
			
			final Element entityDescriptorElement = marshallerFactory.getMarshaller(entityDescriptor).marshall(entityDescriptor);
			token.setEntityDescriptor(entityDescriptor);
			token.setEntityDescriptorElement(entityDescriptorElement);
		} catch(SecurityException e) {
			token.setError(Errors.SAML_SECURITY_EXCEPTION, e);
			log.warn(String.format("Authencation Exception: %s", e.getMessage()));
		} catch(InvalidKeySpecException e) {
			token.setError(Errors.AUTH_PROVIDER_SECURITY_KEYS_INVALID, e);
			log.warn(String.format("Invalid Key Exception: %s", e.getMessage()), e);
		} catch(AuthenticationException e) {
			token.setError(e.getError(), e);
			log.warn(String.format("Authencation Exception: %s", e.getMessage()));
		} catch(Throwable e) {
			log.error("SAML Exception", e);
			token.setError(Errors.INTERNAL_ERROR, e);
		}
		return token;
	}

	@Override
	public SAMLLogoutRequestToken<SAMLServiceProvider> generateLogoutRequest(final HttpServletRequest request, final String id, final String reason) {
		final SAMLServiceProvider serviceProvider = getSAMLServiceProvierById(id);
		return generateLogoutRequest(request, serviceProvider, reason);
	}

	@Override
	public SAMLRequestToken<SAMLServiceProvider> generateSAMLRequest(HttpServletRequest request, String issuerName) {
		return generateSAMLRequest(getSAMLServiceProviderByIssuer(issuerName), URIUtils.getRequestURL(request), request);
	}
	
	@Override
	public void redirectAndSend(final SAMLRequestToken<SAMLServiceProvider> token, final HttpServletResponse response, final HttpServletRequest request, final String relayState) throws MessageEncodingException {
		super.process(token, response, request, relayState);
	}

	@Override
	public void redirectAndSend(final SAMLLogoutRequestToken<SAMLServiceProvider> token, final HttpServletResponse response, final HttpServletRequest request)
			throws MessageEncodingException {
		final LogoutRequest logoutRequest = token.getSamlObject();
		final Endpoint endpoint = token.getEndpoint();
		final BasicSAMLMessageContext<SAMLObject, LogoutRequest, SAMLObject> messageContext = new BasicSAMLMessageContext<SAMLObject, LogoutRequest, SAMLObject>();
		final HTTPOutTransport outTransport = new HttpServletResponseAdapter(response, URIUtils.isSecure(request));
		
		messageContext.setOutboundMessageTransport(outTransport);
		messageContext.setOutboundMessage(logoutRequest);
		messageContext.setOutboundSAMLMessage(logoutRequest);
		messageContext.setPeerEntityEndpoint(endpoint);
		new HTTPRedirectDeflateEncoder().encode(messageContext);
		
	}
	
	@Override
	public SAMLLogoutResponseToken<SAMLServiceProvider> processLogoutResponse(final HttpServletRequest request, final boolean isDebugRequest) {
		final DateTime now = new DateTime(DateTimeZone.UTC);
		final SAMLLogoutResponseToken<SAMLServiceProvider> token = new SAMLLogoutResponseToken<>();
		final IdmAuditLog auditLog = auditLogProvider.newInstance(request);
		auditLog.setAction(AuditAction.SAML_LOGOUT_RESPONSE_PROCESS.value());
		try {
			
			final LogoutRequest logoutRequest = SAMLSessionUtils.getLastLogoutRequest(request);
			if(logoutRequest == null) {
				throw new SecurityException("No LogoutRequest found in session");
			}

			final MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
			
			final LogoutResponse response = SAMLDigestUtils.getLogoutResponse(request, skipURICheck);
			if(response == null) {
				throw new SecurityException("No LogoutResponse, or could not decode");
			}
			
			if(response.getStatus() == null || response.getStatus().getStatusCode() == null) {
				throw new SecurityException("No LogoutResponse.Status or LogoutResponse.Status.StatusCode present");
			}
			
			final Element marshalledResponse = marshallerFactory.getMarshaller(response).marshall(response);
			final String xmlString = XMLHelper.prettyPrintXML(marshalledResponse);
			auditLog.addAttribute(AuditAttributeName.SAML_XML, xmlString);
			log.info(String.format("LogoutResponse: %s", xmlString));
			

			if(!StatusCode.SUCCESS_URI.equals(response.getStatus().getStatusCode().getValue())) {
				throw new SecurityException(String.format("LogoutResponse has an unsuccesful status code: %s", response.getStatus().getStatusCode().getValue()));
			}
			
			if(!isDebugRequest) {
				validateTimestamps(response, now, request);
			}
			

			final String serviceProviderIssuer = logoutRequest.getIssuer().getValue();
			final SAMLServiceProvider serviceProvider = getSAMLServiceProviderByIssuer(serviceProviderIssuer);
			
			if(serviceProvider == null) {
				throw new AuthenticationException(String.format("SAMLServiceProvider with Issuer '%s' not configured or doesn't exist", serviceProviderIssuer), Errors.SAML_SERVICE_PROVIDER_NOT_FOUND);
			}
			
			if(serviceProvider.isSign() && !isDebugRequest) {
				final Signature signature = response.getSignature();
				validateSignature(signature, serviceProvider);
			}
			
			token.setAuthProvider(serviceProvider);
			token.setSamlObject(response);
			auditLog.succeed();
		} catch(SecurityException e) {
			auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
			token.setError(Errors.SAML_SECURITY_EXCEPTION, e);
			log.warn(String.format("Authencation Exception: %s", e.getMessage()));
		} catch(AuthenticationException e) {
			auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
			token.setError(e.getError(), e);
			log.warn(String.format("Authencation Exception: %s", e.getMessage()));
		} catch(Throwable e) {
			auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
			log.error("SAML Exception", e);
			token.setError(Errors.INTERNAL_ERROR, e);
		} finally {
			auditLogProvider.add(AuditSource.SP, request, auditLog);
		}
		return token;
	}
	
	private SAMLServiceProvider getSAMLServiceProviderFromSAMLResponse(final HttpServletRequest request, final Response response) {
		SAMLServiceProvider serviceProvider = null;
		if(serviceProvider == null && CollectionUtils.isNotEmpty(response.getAssertions())) {
			final Assertion assertion = response.getAssertions().get(0);
			final Conditions conditions = assertion.getConditions();
			if(CollectionUtils.isNotEmpty(conditions.getAudienceRestrictions())) {
				for(final AudienceRestriction restriction : conditions.getAudienceRestrictions()) {
					if(CollectionUtils.isNotEmpty(restriction.getAudiences())) {
						for(final Audience audience : restriction.getAudiences()) {
							if(StringUtils.isNotBlank(audience.getAudienceURI())) {
								serviceProvider = getSAMLServiceProviderByIssuer(audience.getAudienceURI());
							}
							if(serviceProvider != null) {
								log.info(String.format("Found Service Provider from Audience '%s'", audience.getAudienceURI()));
								break;
							}
						}
					}
					if(serviceProvider != null) {
						break;
					}
				}
			}
		}
		return serviceProvider;
	}

	@Override
	public SAMLResponseToken processSAMLResponse(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse, final boolean isDebugRequest) {
		final int samlIssueThresholdInMillis = samlThresholdMinutes * 60 * 1000;
		
		final SAMLResponseToken token = new SAMLResponseToken();
		final String relayState = httpRequest.getParameter("RelayState");
		final DateTime now = new DateTime(DateTimeZone.UTC);
		
		if(log.isDebugEnabled()) {
			log.debug(String.format("Relay State", relayState));
		}
		
		final IdmAuditLog auditLog = auditLogProvider.newInstance(httpRequest);
		auditLog.setAction(AuditAction.SAML_SP_RESPONSE_PROCESS.value());
		auditLog.addAttribute(AuditAttributeName.RELAY_STATE, relayState);
		auditLog.addAttribute(AuditAttributeName.DEBUG_REQUEST, Boolean.valueOf(isDebugRequest).toString());
		try {
			final MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
			final Response response = SAMLDigestUtils.getSAMLResponse(httpRequest, skipURICheck);
			if(response == null) {
				throw new SecurityException("No SAMLResponse, or could not decode");
			}
			final Element marshalledResponse = marshallerFactory.getMarshaller(response).marshall(response);
			final String samlResponseString = XMLHelper.prettyPrintXML(marshalledResponse);
			auditLog.addAttribute(AuditAttributeName.SAML_RESPONSE_XML, samlResponseString);
			log.info(String.format("SAML Request: %s", samlResponseString));
			
			
			//TODO:  check ID
			if(!isDebugRequest) {
				validateTimestamps(response, now, httpRequest);
			}
			
			if(response.getIssuer() == null) {
				throw new AuthenticationException(String.format("response.issuer is null"), Errors.SAML_SP_EXCEPTION);
			}
			
			if(CollectionUtils.isEmpty(response.getAssertions())) {
				throw new AuthenticationException(String.format("response.assertions is null"), Errors.SAML_SP_EXCEPTION);
			}
			
			final Assertion assertion = response.getAssertions().get(0);
			
			final Conditions conditions = assertion.getConditions();
			if(conditions == null) {
				throw new AuthenticationException(String.format("response.assertion.conditions is null"), Errors.SAML_SP_EXCEPTION);
			}
			
			final SAMLServiceProvider serviceProvider = getSAMLServiceProviderFromSAMLResponse(httpRequest, response);
			
			if(serviceProvider == null) {
				throw new AuthenticationException(String.format("SAMLServiceProvider with Issuer or Audience not configured or doesn't exist"), Errors.SAML_SERVICE_PROVIDER_NOT_FOUND);
			}
			
			if(conditions.getNotOnOrAfter() == null) {
				throw new AuthenticationException(String.format("response.assertion.conditions.notOnOrAfter is null"), Errors.SAML_SP_EXCEPTION);
			}
			
			if(conditions.getNotBefore() == null) {
				throw new AuthenticationException(String.format("response.assertion.conditions.notBefore is null"), Errors.SAML_SP_EXCEPTION);
			}
			
			if(!isDebugRequest) {
				if(now.isAfter(conditions.getNotOnOrAfter().toDateTime(DateTimeZone.UTC).getMillis())) {
					throw new AuthenticationException(String.format("response.assertion.conditions.notOnOrAfter threshhold %s", samlIssueThresholdInMillis), Errors.SAML_SP_EXCEPTION);
				}
			
				if(now.isBefore(conditions.getNotBefore().toDateTime(DateTimeZone.UTC))) {
					throw new AuthenticationException(String.format("response.assertion.conditions.notBefore threshhold %s", samlIssueThresholdInMillis), Errors.SAML_SP_EXCEPTION);
				}
			}
			
			final Subject subject = assertion.getSubject();
			if(subject == null) {
				throw new AuthenticationException(String.format("response.assertions.subject is null"), Errors.SAML_SP_EXCEPTION);
			}
			
			final NameID nameId = subject.getNameID();
			if(nameId == null) {
				throw new AuthenticationException(String.format("response.assertions.subject.nameId is null"), Errors.SAML_SP_EXCEPTION);
			}
			
			if(nameId.getFormat() == null) {
				final String message = "Name ID did not have a format.";
				log.warn(message);
				auditLog.addWarning(message);
				//throw new AuthenticationException(String.format("response.assertions.subject.nameId.format is null"), Errors.SAML_SP_EXCEPTION);
			}
			
			if(serviceProvider.isSign() && !isDebugRequest) {
				Signature signature = response.getSignature();
				if(signature == null) {
					log.warn("SAML Response isn't signed - looking at assertion");
					signature = assertion.getSignature();
				}
				validateSignature(signature, serviceProvider);
			}
			
			final String nameIdFormat = nameId.getFormat();
			final String nameIdValue = nameId.getValue();
			final String managedSysId = serviceProvider.getManagedSysId();
			
			final boolean justInTimeAuthEnabled = StringUtils.isNotBlank(serviceProvider.getJustInTimeSAMLAuthenticatorScript());
			
			String principal = null;
			final DefaultNameIDResolver nameIdResolver = getNameIDResolver(serviceProvider);
			nameIdResolver.init(httpRequest, response, nameId);
			principal = nameIdResolver.getPrincipal(managedSysId, justInTimeAuthEnabled);
			auditLog.setPrincipal(principal);
			
			/* if a principal is sent, check to see if it maps to a user */
			LoginResponse loginResponse = null;
			if(principal != null) {
				loginResponse = loginServiceClient.getLoginByManagedSys(principal, managedSysId);
				if(!justInTimeAuthEnabled && loginResponse.isFailure()) {
					throw new SecurityException(String.format("Could not find principal '%s' using managed System '%s'", principal, managedSysId));
				}
			}
			
			auditLog.addAttribute(AuditAttributeName.JUST_IN_TIME_AUTH, Boolean.valueOf(justInTimeAuthEnabled).toString());
			
			/* try just-in-time auth */
			if(justInTimeAuthEnabled) {
				if(loginResponse == null || loginResponse.isFailure()) {
					final AbstractJustInTimeSAMLAuthenticator authenticator = (AbstractJustInTimeSAMLAuthenticator)scriptRunner.instantiateClass(null, serviceProvider.getJustInTimeSAMLAuthenticatorScript());
					authenticator.init(response, nameId, serviceProvider);
					User user = authenticator.createUser();
					if (provisionServiceFlag) {
						final ProvisionUser pUser = new ProvisionUser(user);
						final ProvisionUserResponse userResponse = provisionService.addUser(pUser);
						if(userResponse.isFailure()) {
							throw new AuthenticationException("Just in time provisioning failed due to the provisioning service returning a failure",
									Errors.SAML_SECURITY_EXCEPTION);
						}
						user = userResponse.getUser();
					} else {
						final UserResponse userResponse = userDataWebService.saveUserInfo(user, null);
						if(userResponse.isFailure()) {
							throw new AuthenticationException("Just in time provisioning failed due to the provisioning service returning a failure",
									Errors.SAML_SECURITY_EXCEPTION);
						}
						user = userResponse.getUser();
					}
					
					if(CollectionUtils.isEmpty(user.getPrincipalList())) {
						throw new SecurityException("The return value from the provisioning service did not have any principals.");
					}
					
					for(final Login login : user.getPrincipalList()) {
						if(StringUtils.equals(login.getManagedSysId(), managedSysId)) {
							principal = login.getLogin();
							break;
						}
					}
					
					loginResponse = loginServiceClient.getLoginByManagedSys(principal, managedSysId);
					if(loginResponse.isFailure()) {
						throw new SecurityException(String.format("Could not find principal '%s' using managed System '%s' AFTER Just-in-time provisioning", principal, managedSysId));
					}
				}
			}
			
			final Login login = loginResponse.getPrincipal();
			auditLog.setRequestorUserId(login.getUserId());
			if(!isUserEntitledTo(login.getUserId(), serviceProvider)) {
				final String message = String.format("User with ID '%s' not entitled to SSO with SAML SP Provider '%s", login.getUserId(), serviceProvider.getName());
				throw new AuthenticationException(message, Errors.SAML_USER_NOT_AUTHORIZED_TO_SSO);
			}
			
			
			AuthnRequest authnRequest = null;
			
			if(serviceProvider.getNextInChain() != null) {
				if(httpRequest.getSession(false) == null) {
					throw new IllegalStateException("No Session found for the current request");
				} else if(!SAMLSessionUtils.hasLastAuthnRequest(httpRequest)) {
					throw new IllegalStateException("No attribute named 'lastSAMLMessageId' found for the current session");
				} else {
					authnRequest = SAMLSessionUtils.getLastAuthnRequest(httpRequest);
				}
			}
			
			if(!isDebugRequest) {
				
				/* 
				 * log out only if the current user is different than the one in the request 
				 * Note that in 3.5 you will have to read the userid and principal directly from the cookie,
				 * due to differences in Spring Security b/w V3.5 and V4 
				 */
				final String currentUserId = cookieProvider.getUserId(httpRequest);
				final String currentPrincipal = cookieProvider.getPrincipal(httpRequest);
				if(!StringUtils.equals(currentUserId, login.getUserId()) || !StringUtils.equals(currentPrincipal, login.getLogin())) {
					if(!loginProvider.doLogout(httpRequest, httpResponse, false)) {
						return null;
					}
					LoginActionToken loginActionToken = loginProvider.getLoginActionToken(httpRequest, httpResponse, login.getLogin());
					if(!loginActionToken.isSuccess()) {
						throw new SecurityException(String.format("Exception From Login token: %s", loginActionToken.getErrCode()));
					}
					
					/* 
					 * Lev Bornovalov . March 15 2017
					 * we have a dillema here.
					 * The user logged in using a managed system/principal, which may or not not be '0' (i.e. OpenIAM).
					 * We need to set the auth cookie.  But the auth cookie will only properly renew for managed system '0', unless we do a lot of
					 * work to fix that issue. 
					 * 
					 * For now, I am going to just convert the login auth response to the OpenIAM auth response.
					 */
					final LoginResponse defaultLogin = loginServiceClient.getPrimaryIdentity(login.getUserId());
					if(!defaultLogin.isSuccess()) {
						throw new SecurityException(String.format("Exception From Login token: %s", defaultLogin));
					}
					
					loginActionToken = loginProvider.getLoginActionToken(httpRequest, httpResponse, defaultLogin.getPrincipal().getLogin());
					if(!loginActionToken.isSuccess()) {
						throw new SecurityException(String.format("Exception From Login token: %s", loginActionToken.getErrCode()));
					}
					
					cookieProvider.setAuthInfo(httpRequest, httpResponse, defaultLogin.getPrincipal().getLogin(), loginActionToken.getAuthResponse());
				}
			}
			
			if(serviceProvider.getNextInChain() != null) {
				final SAMLResponseToken responseToken = getSAMLResponeToken(serviceProvider.getNextInChain(), authnRequest, relayState, login.getUserId(), principal, httpRequest, httpResponse, response);
				token.setChainedResponseToken(responseToken);
			}
			
			DefaultServiceProviderRelayStateGenerator relayStateGenerator = null;
			if(StringUtils.isNotBlank(serviceProvider.getRelayStateGeneratorGroovyScript())) {
				if(scriptRunner.scriptExists(serviceProvider.getRelayStateGeneratorGroovyScript())) {
					relayStateGenerator = (DefaultServiceProviderRelayStateGenerator)scriptRunner.instantiateClass(null, serviceProvider.getRelayStateGeneratorGroovyScript());
				}
			}
			if(relayStateGenerator == null) {
				relayStateGenerator = new DefaultServiceProviderRelayStateGenerator();
			}
			relayStateGenerator.init(httpRequest, response, userDataWebService.getUserWithDependent(login.getUserId(), null, true));
			token.setRelayState(relayStateGenerator.getRelayState());
			
			addProviderToSession(serviceProvider, httpRequest);
			
			SAMLSessionUtils.setLastSAMLResponse(httpRequest, response);
			auditLog.succeed();
		} catch(SecurityException e) {
			auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
			token.setError(Errors.SAML_SECURITY_EXCEPTION, e);
			log.warn(String.format("Authencation Exception: %s", e.getMessage()));
		} catch(AuthenticationException e) {
			auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
			token.setError(e.getError(), e);
			log.warn(String.format("Authencation Exception: %s", e.getMessage()));
		} catch(Throwable e) {
			auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
			log.error("SAML Exception", e);
			token.setError(Errors.INTERNAL_ERROR, e);
		} finally {
			auditLogProvider.add(AuditSource.SP, httpRequest, auditLog);
		}
		return token;
	}
	
	private void addProviderToSession(final SAMLServiceProvider provider, final HttpServletRequest request) {
		addProvierToSession(provider, request, org.openiam.ui.web.util.SAMLConstants.SAML_SERVICE_PROVIDERS_FOR_SESSION);
	}
	
	
	private DefaultNameIDResolver getNameIDResolver(final SAMLServiceProvider serviceProvider) {
		DefaultNameIDResolver nameIdResolver = null;
		if(StringUtils.isNotBlank(serviceProvider.getNameIdResolverGroovyScript())) {
			try {
				nameIdResolver = (DefaultNameIDResolver)scriptRunner.instantiateClass(null, serviceProvider.getNameIdResolverGroovyScript());
			} catch(Throwable e) {
				log.error(String.format("Can't instantiate '%s'", serviceProvider.getNameIdResolverGroovyScript()), e);
			}
		} else {
			nameIdResolver = new DefaultNameIDResolver();
		}
		return nameIdResolver;
	}

	@Override
	protected Class<SAMLServiceProvider> getAuthProviderClass() {
		return SAMLServiceProvider.class;
	}

	@Override
	protected void modifyLogoutRequestBeforeSigning(final HttpServletRequest request, 
													final SAMLServiceProvider authProvider,
													final XMLObjectBuilderFactory builderFactory,
													final LogoutRequest xmlRequest) throws AuthenticationException {
		final Response lastSAMLResponse = SAMLSessionUtils.getLastSAMLResponse(request);
		if(lastSAMLResponse == null) {
			throw new AuthenticationException(String.format("Last SAML Response does not exist in session"), Errors.SAML_SECURITY_EXCEPTION);
		}
		if(CollectionUtils.isEmpty(lastSAMLResponse.getAssertions())) {
			throw new AuthenticationException(String.format("Last SAML Response had no assertions"), Errors.SAML_SECURITY_EXCEPTION);
		}
		
		final Assertion assertion = lastSAMLResponse.getAssertions().get(0);
		if(assertion.getSubject() == null) {
			throw new AuthenticationException(String.format("Last SAML Response had no subject"), Errors.SAML_SECURITY_EXCEPTION);
		}
		if(assertion.getSubject().getNameID() == null) {
			throw new AuthenticationException(String.format("Last SAML Response had no subject.nameId"), Errors.SAML_SECURITY_EXCEPTION);
		}

		final NameID nameId = ((NameIDBuilder)builderFactory.getBuilder(NameID.DEFAULT_ELEMENT_NAME)).buildObject();
		nameId.setValue(assertion.getSubject().getNameID().getValue());
		nameId.setFormat(assertion.getSubject().getNameID().getFormat());		
		xmlRequest.setNameID(nameId);
		
		if(CollectionUtils.isNotEmpty(assertion.getAuthnStatements())) {
			List<SessionIndex> indecies = new ArrayList<>();
			for (AuthnStatement authSt : assertion.getAuthnStatements()) {
				if (StringUtils.isNotBlank(authSt.getSessionIndex())) {
					final SessionIndex sessionIndex = ((SessionIndexBuilder)builderFactory.getBuilder(SessionIndex.DEFAULT_ELEMENT_NAME)).buildObject();
					sessionIndex.setSessionIndex(authSt.getSessionIndex());
					indecies.add(sessionIndex);
				}
			}
			if(CollectionUtils.isNotEmpty(indecies)) {
				xmlRequest.getSessionIndexes().addAll(indecies);
			}
		}
	}
	
	@Override
	protected Issuer buildIssuer(HttpServletRequest request, XMLObjectBuilderFactory builderFactory,
			SAMLServiceProvider authProvider) {
		final SAMLServiceProvider sp = (SAMLServiceProvider)authProvider;
		final Issuer issuer = ((IssuerBuilder)builderFactory.getBuilder(Issuer.DEFAULT_ELEMENT_NAME)).buildObject();
		issuer.setValue(sp.getIssuer());
		issuer.setFormat(sp.getSamlIssuerFormat());
		return issuer;
	}

	@Override
	public SAMLLogoutResponseToken<SAMLServiceProvider> generateSAMLLogoutResponse(final HttpServletRequest request, final LogoutRequest logoutRequest, final String statusCode) {
		SAMLLogoutResponseToken<SAMLServiceProvider> retval = null;
		final Response samlResponse = SAMLSessionUtils.getLastSAMLResponse(request);
		if(samlResponse == null) {
			final String errorMessage = "No SAMLResponse found in session - cannot determine Service Provider to send";
			log.error(errorMessage);
			retval = new SAMLLogoutResponseToken<SAMLServiceProvider>();
			retval.setError(Errors.SAML_SERVICE_PROVIDER_NOT_FOUND, new RuntimeException(errorMessage));
		} else {
			final SAMLServiceProvider authProvider = getSAMLServiceProviderFromSAMLResponse(request, samlResponse);
			log.debug(">>>>> logoutRequest:" + logoutRequest);
			log.debug(">>>>> logoutRequest.getIssuer():" + ((logoutRequest != null) ? logoutRequest.getIssuer() : null));
			log.debug(">>>>> statusCode:" + statusCode);
			retval = super.generateSAMLLogoutResponse(request, logoutRequest, authProvider, statusCode);
		}
		return retval;
	}

	@Override
	protected SAMLServiceProvider getAuthProvider(final LogoutRequest logoutRequest, final HttpServletRequest request) {
		SAMLServiceProvider authProvider = null;
		final Response samlResponse = SAMLSessionUtils.getLastSAMLResponse(request);
		if(samlResponse != null) {
			authProvider = getSAMLServiceProviderFromSAMLResponse(request, samlResponse);
		}
		return authProvider;
	}

	@Override
	protected Element signLogoutResponse(final LogoutResponse logoutResponse, final SAMLServiceProvider authProvider, final XMLObjectBuilderFactory builderFactory, final MarshallerFactory marshallFactory) {
		try {
			addX509Certificate(logoutResponse, authProvider, builderFactory);
			return marshallFactory.getMarshaller(logoutResponse).marshall(logoutResponse);
		} catch(Throwable e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void validateSAMLLogoutResponseSignature(final LogoutResponse logoutResponse, final SAMLServiceProvider authProvider) throws CertificateException, IOException, ValidationException {
		final Signature signature = logoutResponse.getSignature();
		validateSignature(signature, authProvider);
	}

}
