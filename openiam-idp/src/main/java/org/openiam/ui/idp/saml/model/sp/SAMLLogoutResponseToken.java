package org.openiam.ui.idp.saml.model.sp;

import org.openiam.ui.idp.saml.model.AbstractSignableMessageToken;
import org.openiam.ui.idp.saml.provider.SAMLAuthenticationProvider;
import org.opensaml.saml2.core.LogoutResponse;

public class SAMLLogoutResponseToken<P extends SAMLAuthenticationProvider> extends AbstractSignableMessageToken<LogoutResponse, P> {

    private boolean isConfiguredForSLO = true;

    public boolean isConfiguredForSLO() {
        return isConfiguredForSLO;
    }

    public void setConfiguredForSLO(boolean isConfiguredForSLO) {
        this.isConfiguredForSLO = isConfiguredForSLO;
    }


}
