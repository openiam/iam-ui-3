package org.openiam.ui.idp.saml.model;

import org.openiam.ui.idp.saml.provider.SAMLIdentityProvider;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.Response;

public class SAMLResponseToken extends AbstractSignableMessageToken<Response, SAMLIdentityProvider> {

	private String relayState = null;
	public String assertionConsumerURL;
	private boolean authenticationCommenced;
	private SAMLResponseToken chainedResponseToken;

	public String getAssertionConsumerURL() {
		return assertionConsumerURL;
	}

	public void setAssertionConsumerURL(String assertionConsumerURL) {
		this.assertionConsumerURL = assertionConsumerURL;
	}

	public String getRelayState() {
		return relayState;
	}

	public void setRelayState(String relayState) {
		this.relayState = relayState;
	}

	public boolean isAuthenticationCommenced() {
		return authenticationCommenced;
	}

	public void setAuthenticationCommenced(boolean authenticationCommenced) {
		this.authenticationCommenced = authenticationCommenced;
	}

	public SAMLResponseToken getChainedResponseToken() {
		return chainedResponseToken;
	}

	public void setChainedResponseToken(SAMLResponseToken chainedResponseToken) {
		this.chainedResponseToken = chainedResponseToken;
	}


}
