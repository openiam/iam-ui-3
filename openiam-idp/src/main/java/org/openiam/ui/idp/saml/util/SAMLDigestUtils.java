package org.openiam.ui.idp.saml.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openiam.ui.idp.saml.decoder.OpenIAMHTTPPostDecoder;
import org.openiam.ui.idp.saml.decoder.OpenIAMHTTPRedirectDeflateDecoder;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.saml2.binding.decoding.BaseSAML2MessageDecoder;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.saml2.core.LogoutResponse;
import org.opensaml.saml2.core.Response;
import org.opensaml.ws.message.decoder.MessageDecodingException;
import org.opensaml.ws.transport.http.HttpServletRequestAdapter;
import org.opensaml.xml.security.SecurityException;

import javax.servlet.http.HttpServletRequest;

public class SAMLDigestUtils {
	
	private static final Log LOG = LogFactory.getLog(SAMLDigestUtils.class);
	
	public static AuthnRequest getAuthNRequest(final HttpServletRequest request, final boolean skipcheckEndpointURI) throws MessageDecodingException, SecurityException {
		final BasicSAMLMessageContext<AuthnRequest, Response, SAMLObject> msgContext = new BasicSAMLMessageContext<AuthnRequest, Response, SAMLObject>();
		msgContext.setInboundMessageTransport(new HttpServletRequestAdapter(request));
		BaseSAML2MessageDecoder decoder = null;
		
		try {
			decoder = new OpenIAMHTTPPostDecoder(skipcheckEndpointURI);
			decoder.decode(msgContext);
		} catch(Throwable e) {
			LOG.error(String.format("Could not decode message using HTTPPostDecoder Deflater - using HTTPRedirectDeflateDecoder.  Printing Exception", e));
			decoder = new OpenIAMHTTPRedirectDeflateDecoder(skipcheckEndpointURI);
			decoder.decode(msgContext);
		}
		final AuthnRequest authnRequest = msgContext.getInboundSAMLMessage();
		return authnRequest;
	}
	
	public static Response getSAMLResponse(final HttpServletRequest request, final boolean skipcheckEndpointURI) throws SecurityException, MessageDecodingException {
		final BasicSAMLMessageContext<Response, Response, SAMLObject> msgContext = new BasicSAMLMessageContext<Response, Response, SAMLObject>();
		msgContext.setInboundMessageTransport(new HttpServletRequestAdapter(request));
		BaseSAML2MessageDecoder decoder = null;
		
		try {
			decoder = new OpenIAMHTTPPostDecoder(skipcheckEndpointURI);
			decoder.decode(msgContext);
		} catch(Throwable e) {
			LOG.error(String.format("Could not decode message using HTTPPostDecoder Deflater - using HTTPRedirectDeflateDecoder.  Printing Exception", e));
			decoder = new OpenIAMHTTPRedirectDeflateDecoder(skipcheckEndpointURI);
			decoder.decode(msgContext);
		}
		final Response response = msgContext.getInboundSAMLMessage();
		return response;
	}

	public static LogoutResponse getLogoutResponse(final HttpServletRequest request, final boolean skipcheckEndpointURI) throws SecurityException, MessageDecodingException {
		final BasicSAMLMessageContext<LogoutResponse, LogoutResponse, SAMLObject> msgContext = new BasicSAMLMessageContext<LogoutResponse, LogoutResponse, SAMLObject>();
		msgContext.setInboundMessageTransport(new HttpServletRequestAdapter(request));
		BaseSAML2MessageDecoder decoder = null;

		try {
			decoder = new OpenIAMHTTPPostDecoder(skipcheckEndpointURI);
			decoder.decode(msgContext);
		} catch(Throwable e) {
			LOG.warn("Could not decode message using HTTPPostDecoder Deflater - using HTTPRedirectDeflateDecoder.");
			decoder = new OpenIAMHTTPRedirectDeflateDecoder(skipcheckEndpointURI);
			decoder.decode(msgContext);
		}
		return msgContext.getInboundSAMLMessage();

	}



	public static LogoutRequest getLogoutRequest(final HttpServletRequest request, final boolean skipcheckEndpointURI) throws MessageDecodingException, SecurityException {
		final BasicSAMLMessageContext<LogoutRequest, LogoutRequest, SAMLObject> msgContext = new BasicSAMLMessageContext<LogoutRequest, LogoutRequest, SAMLObject>();
		msgContext.setInboundMessageTransport(new HttpServletRequestAdapter(request));
		BaseSAML2MessageDecoder decoder = null;

		try {
			decoder = new OpenIAMHTTPPostDecoder(skipcheckEndpointURI);
			decoder.decode(msgContext);
		} catch(Throwable e) {
			LOG.warn("Could not decode message using HTTPPostDecoder Deflater - using HTTPRedirectDeflateDecoder.");
			decoder = new OpenIAMHTTPRedirectDeflateDecoder(skipcheckEndpointURI);
			decoder.decode(msgContext);
		}
		return msgContext.getInboundSAMLMessage();
	}
}
