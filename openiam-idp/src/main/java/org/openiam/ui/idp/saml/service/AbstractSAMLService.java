package org.openiam.ui.idp.saml.service;

import net.sf.ehcache.Ehcache;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.openiam.am.srvc.dto.SSOAttribute;
import org.openiam.am.srvc.ws.AuthResourceAttributeWebService;
import org.openiam.authmanager.service.AuthorizationManagerWebService;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.idm.srvc.audit.constant.AuditAction;
import org.openiam.idm.srvc.audit.constant.AuditAttributeName;
import org.openiam.idm.srvc.audit.constant.AuditSource;
import org.openiam.idm.srvc.audit.dto.IdmAuditLog;
import org.openiam.idm.srvc.auth.dto.Login;
import org.openiam.idm.srvc.auth.ws.LoginDataWebService;
import org.openiam.idm.srvc.auth.ws.LoginListResponse;
import org.openiam.idm.srvc.continfo.dto.EmailAddress;
import org.openiam.idm.srvc.user.ws.UserDataWebService;
import org.openiam.provision.service.ProvisionService;
import org.openiam.script.ScriptIntegration;
import org.openiam.thread.Sweepable;
import org.openiam.ui.audit.AuditLogProvider;
import org.openiam.ui.idp.common.service.AuthenticationSweeper;
import org.openiam.ui.idp.saml.encoder.OpenIAMHTTPRedirectDeflateEncoder;
import org.openiam.ui.idp.saml.exception.AuthenticationException;
import org.openiam.ui.idp.saml.exception.CustomSAMLException;
import org.openiam.ui.idp.saml.groovy.AbsractSAMLSPChainHandler;
import org.openiam.ui.idp.saml.model.SAMLRequestToken;
import org.openiam.ui.idp.saml.model.SAMLResponseToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutRequestToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutResponseToken;
import org.openiam.ui.idp.saml.provider.AuthenticationProvider;
import org.openiam.ui.idp.saml.provider.SAMLAuthenticationProvider;
import org.openiam.ui.idp.saml.provider.SAMLIdentityProvider;
import org.openiam.ui.idp.saml.provider.SAMLServiceProvider;
import org.openiam.ui.idp.saml.util.SAMLAttributeBuilder;
import org.openiam.ui.idp.saml.util.SAMLDigestUtils;
import org.openiam.ui.security.OpenIAMAuthenticationEntryPoint;
import org.openiam.ui.security.OpenIAMCookieProvider;
import org.openiam.ui.util.URIUtils;
import org.openiam.ui.util.messages.Errors;
import org.opensaml.common.SAMLException;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.SignableSAMLObject;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.common.impl.SAMLObjectContentReference;
import org.opensaml.common.impl.SecureRandomIdentifierGenerator;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.binding.encoding.HTTPRedirectDeflateEncoder;
import org.opensaml.saml2.core.*;
import org.opensaml.saml2.core.impl.*;
import org.opensaml.saml2.metadata.AssertionConsumerService;
import org.opensaml.saml2.metadata.Endpoint;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.ws.transport.http.HTTPOutTransport;
import org.opensaml.ws.transport.http.HttpServletResponseAdapter;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.security.SecurityConfiguration;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.credential.UsageType;
import org.opensaml.xml.security.keyinfo.KeyInfoGenerator;
import org.opensaml.xml.security.keyinfo.KeyInfoGeneratorFactory;
import org.opensaml.xml.security.keyinfo.KeyInfoGeneratorManager;
import org.opensaml.xml.security.keyinfo.NamedKeyInfoGeneratorManager;
import org.opensaml.xml.security.x509.BasicX509Credential;
import org.opensaml.xml.signature.*;
import org.opensaml.xml.signature.impl.KeyInfoBuilder;
import org.opensaml.xml.signature.impl.SignatureBuilder;
import org.opensaml.xml.signature.impl.X509CertificateBuilder;
import org.opensaml.xml.signature.impl.X509DataBuilder;
import org.opensaml.xml.util.XMLHelper;
import org.opensaml.xml.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.w3c.dom.Element;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.*;

public abstract class AbstractSAMLService<RequestObject extends SignableSAMLObject, ResponseObject extends SignableSAMLObject, P extends SAMLAuthenticationProvider> implements SAMLService<RequestObject, ResponseObject, P>, Sweepable {

    protected final Log log = LogFactory.getLog(this.getClass());

    private boolean cacheInitialized = false;

    @Value("${org.openiam.provision.service.flag}")
    protected Boolean provisionServiceFlag;

    @Value("${org.openiam.saml.time.threshold.minutes}")
    protected Integer samlThresholdMinutes;


    @Autowired
    @Qualifier("authAttributeServiceClient")
    private AuthResourceAttributeWebService authAttributeServiceClient;

    @Autowired
    @Qualifier("samlAuthenticationSweeper")
    protected AuthenticationSweeper authSweeper;

    @Autowired
    @Qualifier("loginServiceClient")
    protected LoginDataWebService loginServiceClient;

    @Autowired
    @Qualifier("userServiceClient")
    protected UserDataWebService userDataWebService;

    @Autowired
    @Qualifier("configurableGroovyScriptEngine")
    protected ScriptIntegration scriptRunner;

    @Autowired
    @Qualifier("provisionServiceClient")
    protected ProvisionService provisionService;

    @Autowired
    @Qualifier("authorizationManagerServiceClient")
    private AuthorizationManagerWebService authorizationManager;

    @Autowired
    @Qualifier("samlSPCache")
    private Ehcache samlSPCache;

    private Map<String, SAMLServiceProvider> spIdCache;

    @Autowired
    protected OpenIAMCookieProvider cookieProvider;

    @Autowired
    protected AuditLogProvider auditLogProvider;

    @Value("${org.openiam.saml.provider.sweeptime}")
    protected long providerSweepTime;

    @Value("${org.openiam.idp.saml.skip.uri.check}")
    protected boolean skipURICheck;

    @Autowired
    @Qualifier("samlProviderCache")
    private Ehcache samlIdpCache;

    @Autowired
    @Qualifier("samlProviderIdCache")
    private Ehcache samlIdCache;


    @Value("${org.openiam.idp.provider.saml.idp.login}")
    protected String idpLoginURL;

    @Value("${org.openiam.idp.provider.saml.idp.logout}")
    protected String idpLogoutURL;

    @Value("${org.openiam.idp.provider.saml.sp.login}")
    protected String spLoginURL;

    @Value("${org.openiam.idp.provider.saml.sp.logout}")
    protected String spLogoutURL;

    @Autowired
    private OpenIAMAuthenticationEntryPoint authEntryPoint;

    public void afterPropertiesSet() throws Exception {
        try {
            sweep();
        } catch (Throwable e) {
            log.warn("Could not load SAML Providers - will retry on next hit, or refesh interval...");
        }
    }

    @Scheduled(fixedRate = 7200)
    public void sweep() {
        log.debug("Attempting to Sweep SAML Provider cache...");
        final Map<String, SAMLIdentityProvider> tempIdpIdCache = new HashMap<String, SAMLIdentityProvider>();
        final List<SAMLIdentityProvider> samlProviders = authSweeper.getSAMLIdpProviders();
        if (CollectionUtils.isNotEmpty(samlProviders)) {
            final Map<String, SAMLIdentityProvider> providerCache = new HashMap<String, SAMLIdentityProvider>();
            final Map<String, SAMLIdentityProvider> providerIdCache = new HashMap<String, SAMLIdentityProvider>();
            for (final SAMLIdentityProvider provider : samlProviders) {
                if (StringUtils.isNotBlank(provider.getRequestIssuer())) {
                    providerCache.put(StringUtils.lowerCase(provider.getRequestIssuer()), provider);
                }
                providerIdCache.put(provider.getId(), provider);
            }

			/* purge deleted items */
            final List<String> cacheKeys = (List<String>) samlIdpCache.getKeys();
            for (final String key : cacheKeys) {
                if (!providerCache.containsKey(key)) {
                    samlIdpCache.remove(key);
                }
            }

            final List<String> cacheIdKeys = (List<String>) samlIdCache.getKeys();
            for (final String key : cacheIdKeys) {
                if (!providerIdCache.containsKey(key)) {
                    samlIdCache.remove(key);
                }
            }

			/* update/add existing/new items */
            for (final SAMLIdentityProvider provider : samlProviders) {
                final String issuer = provider.getRequestIssuer();
                if (StringUtils.isNotEmpty(issuer)) {
                    samlIdpCache.put(new net.sf.ehcache.Element(StringUtils.lowerCase(issuer), provider));
                }
                samlIdCache.put(new net.sf.ehcache.Element(provider.getId(), provider));
            }
        } else {
            samlIdpCache.flush();
            samlIdCache.flush();
        }

        final Map<String, SAMLServiceProvider> tempSpIdCache = new HashMap<>();
        final List<SAMLServiceProvider> samlServiceProviders = authSweeper.getSAMLServiceProviders();
        if (CollectionUtils.isNotEmpty(samlServiceProviders)) {
            final Map<String, SAMLServiceProvider> providerCache = new HashMap<String, SAMLServiceProvider>();
            for (final SAMLServiceProvider provider : samlServiceProviders) {
                if (StringUtils.isNotBlank(provider.getName())) {
                    providerCache.put(StringUtils.lowerCase(provider.getIssuer()), provider);
                }
            }

			/* purge deleted items */
            final List<String> cacheKeys = (List<String>) samlSPCache.getKeys();
            for (final String key : cacheKeys) {
                if (!providerCache.containsKey(key)) {
                    samlSPCache.remove(key);
                }
            }
			
			/* update/add existing/new items */
            for (final SAMLServiceProvider provider : samlServiceProviders) {
                final String issuer = provider.getIssuer();
                if (StringUtils.isNotEmpty(issuer)) {
                    samlSPCache.put(new net.sf.ehcache.Element(StringUtils.lowerCase(issuer), provider));
                    tempSpIdCache.put(provider.getId(), provider);
                }
            }
        } else {
            samlSPCache.flush();
        }

        for (final SAMLServiceProvider sp : tempSpIdCache.values()) {
            if (sp.getNextInChain() != null && StringUtils.isNotBlank(sp.getNextInChain().getId())) {
                sp.setNextInChain(tempIdpIdCache.get(sp.getNextInChain().getId()));
            } else {
                sp.setNextInChain(null);
            }
        }

        for (final SAMLIdentityProvider idp : tempIdpIdCache.values()) {
            if (idp.getNextInChain() != null && StringUtils.isNotBlank(idp.getNextInChain().getId())) {
                idp.setNextInChain(tempSpIdCache.get(idp.getNextInChain().getId()));
            } else {
                idp.setNextInChain(null);
            }
        }

        spIdCache = tempSpIdCache;
        cacheInitialized = true;
        log.debug("Done sweeping SAML Cache...");
    }

    protected SAMLIdentityProvider getSAMLIdentityProviderByRequestIssuer(final String issuer) {
        try {
            if (!cacheInitialized) { /* in case the ESB was down at startup, retry */
                sweep();
            }

            SAMLIdentityProvider provider = null;
            if (issuer != null) {
                String lowerIssuer = issuer.toLowerCase();
                final net.sf.ehcache.Element cacheElement = samlIdpCache.get(lowerIssuer);
                if (cacheElement != null) {
                    provider = (SAMLIdentityProvider) cacheElement.getObjectValue();
                }
            }
            return provider;
        } catch (Throwable e) { /* would happen if the ESB is down - just return null */
            return null;
        }
    }

    public SAMLServiceProvider getSAMLServiceProvierById(final String id) {
        try {
            if (!cacheInitialized) { /* in case the ESB was down at startup, retry */
                sweep();
            }
            return spIdCache.get(id);
        } catch (Throwable e) { /* would happen if the ESB is down - just return null */
            return null;
        }
    }

    protected SAMLServiceProvider getSAMLServiceProviderByIssuer(final String issuer) {
        try {
            if (!cacheInitialized) { /* in case the ESB was down at startup, retry */
                sweep();
            }

            SAMLServiceProvider provider = null;
            if (issuer != null) {
                final net.sf.ehcache.Element cacheElement = samlSPCache.get(issuer.toLowerCase());
                if (cacheElement != null) {
                    provider = (SAMLServiceProvider) cacheElement.getObjectValue();
                }
            }
            return provider;
        } catch (Throwable e) {
            return null;
        }
    }

    protected SAMLIdentityProvider getSAMLProviderById(final String id) {
        try {
            if (!cacheInitialized) { /* in case the ESB was down at startup, retry */
                sweep();
            }
            SAMLIdentityProvider provider = null;
            if (id != null) {
                final net.sf.ehcache.Element cacheElement = samlIdCache.get(id);
                if (cacheElement != null) {
                    provider = (SAMLIdentityProvider) cacheElement.getObjectValue();
                }
            }
            return provider;
        } catch (Throwable e) { /* would happen if the ESB is down - just return null */
            return null;
        }
    }

    protected String generateId() {
        try {
            return new SecureRandomIdentifierGenerator().generateIdentifier();
        } catch (Throwable e) {
            log.error("Can't generate ID", e);
            return RandomStringUtils.randomAlphanumeric(16);
        }
    }

    protected BasicX509Credential getSigningCredential(final SAMLAuthenticationProvider provider) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, CertificateException {
        final BasicX509Credential credential = new BasicX509Credential();

        credential.setUsageType(UsageType.SIGNING);

        final InputStream inStream = new ByteArrayInputStream(provider.getPublicKey());
        final CertificateFactory cf = CertificateFactory.getInstance("X.509");
        final X509Certificate cert = (X509Certificate) cf.generateCertificate(inStream);
        inStream.close();
        credential.setEntityCertificate(cert);
        credential.setPublicKey(cert.getPublicKey());

        if (provider instanceof SAMLIdentityProvider) {
            final PKCS8EncodedKeySpec kspec = new PKCS8EncodedKeySpec(((SAMLIdentityProvider) provider).getPrivateKey());
            final KeyFactory kf = KeyFactory.getInstance("RSA");
            final PrivateKey privKey = kf.generatePrivate(kspec);
            credential.setPrivateKey(privKey);
        }

        return credential;
    }

    protected String getDigestAlgorithm(final SAMLIdentityProvider provider) {
        String algorithm = SignatureConstants.ALGO_ID_DIGEST_SHA384;
        if (StringUtils.isNotBlank(provider.getDigestAlgorithm())) {
            algorithm = provider.getDigestAlgorithm();
        }
        return algorithm;
    }

    protected String getSignatureAlgorithm(final SAMLAuthenticationProvider provider) {
        String algorithm = SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA384;
        if (StringUtils.isNotBlank(provider.getSigningAlgorithm())) {
            algorithm = provider.getSigningAlgorithm();
        }
        return algorithm;
    }

    protected String getCanonicalizationAlgorithm(final SAMLAuthenticationProvider provider) {
        String algorithm = SignatureConstants.ALGO_ID_C14N_EXCL_OMIT_COMMENTS;
        if (StringUtils.isNotBlank(provider.getCanonicalizationAlgorithm())) {
            algorithm = provider.getCanonicalizationAlgorithm();
        }
        return algorithm;
    }

    protected SAMLRequestToken<SAMLServiceProvider> generateSAMLRequest(final SAMLServiceProvider serviceProvider, final String acs, final HttpServletRequest request) {
        final SAMLRequestToken<SAMLServiceProvider> token = new SAMLRequestToken<>();
        final DateTime now = new DateTime();


        final IdmAuditLog auditLog = auditLogProvider.newInstance(request);
        auditLog.setAction(AuditAction.SAML_SAML_REQUEST_GENERATE.value());

        try {
            final MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
            //final SAMLServiceProvider serviceProvider = getSAMLServiceProviderByIssuer(issuerName);
            if (serviceProvider == null) {
                throw new AuthenticationException(String.format("SAMLServiceProvider not configured or doesn't exist"), Errors.SAML_SERVICE_PROVIDER_NOT_FOUND);
            }

            final XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
            final AuthnRequest xmlRequest = ((AuthnRequestBuilder) builderFactory.getBuilder(AuthnRequest.DEFAULT_ELEMENT_NAME)).buildObject();
            xmlRequest.setAssertionConsumerServiceURL(acs);
            xmlRequest.setID(generateId());
            xmlRequest.setIssueInstant(now);
            xmlRequest.setProtocolBinding(SAMLConstants.SAML2_POST_BINDING_URI);
            xmlRequest.setProviderName(serviceProvider.getName());
			/* IDMAPPS-2807 */
            if (serviceProvider.isIncludeDestinationInAuthnRequest()) {
                xmlRequest.setDestination(serviceProvider.getLoginURL());
            }

            final Issuer issuer = ((IssuerBuilder) builderFactory.getBuilder(Issuer.DEFAULT_ELEMENT_NAME)).buildObject();
            issuer.setValue(serviceProvider.getIssuer());
            issuer.setFormat(serviceProvider.getSamlIssuerFormat());
            xmlRequest.setIssuer(issuer);

            final Endpoint endpoint = ((SAMLObjectBuilder<Endpoint>) builderFactory.getBuilder(AssertionConsumerService.DEFAULT_ELEMENT_NAME)).buildObject();
            endpoint.setLocation(serviceProvider.getLoginURL());
            token.setEndpoint(endpoint);

			/* IDMAPPS-2807 */
            if (StringUtils.isNotBlank(serviceProvider.getNameIdFormatInSAMLRequest())) {
                final NameIDPolicy nameIdPolicy = ((NameIDPolicyBuilder) builderFactory.getBuilder(NameIDPolicy.DEFAULT_ELEMENT_NAME)).buildObject();
                nameIdPolicy.setAllowCreate(serviceProvider.isAllowCreateOnNameIdPolicy());
                nameIdPolicy.setSPNameQualifier(serviceProvider.getSpNameQualifier());
                nameIdPolicy.setFormat(serviceProvider.getNameIdFormatInSAMLRequest());
                xmlRequest.setNameIDPolicy(nameIdPolicy);
            }

            if (StringUtils.isNotBlank(serviceProvider.getAuthnContextClassRef())) {
                final RequestedAuthnContext context = ((RequestedAuthnContextBuilder) builderFactory.getBuilder(RequestedAuthnContext.DEFAULT_ELEMENT_NAME)).buildObject();
                context.setComparison(AuthnContextComparisonTypeEnumeration.EXACT);
                final AuthnContextClassRef classRef = ((AuthnContextClassRefBuilder) builderFactory.getBuilder(AuthnContextClassRef.DEFAULT_ELEMENT_NAME)).buildObject();
                classRef.setAuthnContextClassRef(serviceProvider.getAuthnContextClassRef());
                context.getAuthnContextClassRefs().add(classRef);
                xmlRequest.setRequestedAuthnContext(context);
            }

            Element authnRequestElmt = null;
            if (serviceProvider.isSign()) {
                addX509Certificate(xmlRequest, serviceProvider, builderFactory);
            }

            authnRequestElmt = marshallerFactory.getMarshaller(xmlRequest).marshall(xmlRequest);


            final String prettyXML = XMLHelper.prettyPrintXML(authnRequestElmt);
            auditLog.addAttribute(AuditAttributeName.SAML_REQUEST_XML, prettyXML);
            if (log.isDebugEnabled()) {
                log.debug(String.format("SAML XML: %s", prettyXML));
            }

            token.setAuthProvider(serviceProvider);
            token.setSamlObject(xmlRequest);
            auditLog.succeed();
        } catch (AuthenticationException e) {
            auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
            token.setError(e.getError(), e);
            log.warn(String.format("Authencation Exception: %s", e.getMessage()));
        } catch (Throwable e) {
            auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
            log.error("SAML Exception", e);
            token.setError(Errors.INTERNAL_ERROR, e);
        } finally {
            auditLogProvider.add(AuditSource.SP, request, auditLog);
        }
        return token;
    }

    protected final Element sign(final StatusResponseType response, final SAMLIdentityProvider provider, final XMLObjectBuilderFactory builderFactory, final MarshallerFactory marshallerFactory) {
        try {
            final Signature signature = getSignature(provider, builderFactory);
            response.setSignature(signature);
			

			/* 
			 * The DigestAlgorithm ALGO_ID_SIGNATURE_RSA_SHA384, and it does not change even if the Signature algorithm is modified.  Seems like a bug in OpenSAML,
			 * But this fixes the issue by explicitly setting the Digest.Algorithm attribute
			 */
            if (CollectionUtils.isNotEmpty(signature.getContentReferences())) {
                ((SAMLObjectContentReference) signature.getContentReferences().get(0)).setDigestAlgorithm(getDigestAlgorithm(provider));
            }

            final Element responseElmt = marshallerFactory.getMarshaller(response).marshall(response);
            Signer.signObject(signature);
            return responseElmt;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    protected final void addX509Certificate(final RequestAbstractType xmlObject, final SAMLAuthenticationProvider serviceProvider, final XMLObjectBuilderFactory builderFactory) throws InvalidKeySpecException, NoSuchAlgorithmException, CertificateException, IOException {
        final BasicX509Credential credential = getSigningCredential(serviceProvider);
        final Signature signature = ((SignatureBuilder) builderFactory.getBuilder(Signature.DEFAULT_ELEMENT_NAME)).buildObject();
        signature.setSigningCredential(credential);
        signature.setSignatureAlgorithm(getSignatureAlgorithm(serviceProvider));
        signature.setCanonicalizationAlgorithm(getCanonicalizationAlgorithm(serviceProvider));
        final KeyInfo keyInfo = ((KeyInfoBuilder) builderFactory.getBuilder(KeyInfo.DEFAULT_ELEMENT_NAME)).buildObject();
        final X509Data data = ((X509DataBuilder) builderFactory.getBuilder(X509Data.DEFAULT_ELEMENT_NAME)).buildObject();
        final org.opensaml.xml.signature.X509Certificate cert = ((X509CertificateBuilder)
                builderFactory.getBuilder(org.opensaml.xml.signature.X509Certificate.DEFAULT_ELEMENT_NAME)).buildObject();
        final String value = org.apache.xml.security.utils.Base64.encode(credential.getEntityCertificate().getEncoded());
        cert.setValue(value);
        data.getX509Certificates().add(cert);
        keyInfo.getX509Datas().add(data);
        signature.setKeyInfo(keyInfo);

        xmlObject.setSignature(signature);
    }

    protected final Element addX509Certificate(final RequestAbstractType xmlObject, final SAMLAuthenticationProvider serviceProvider, final XMLObjectBuilderFactory builderFactory, final MarshallerFactory marshallerFactory) throws InvalidKeySpecException, NoSuchAlgorithmException, CertificateException, IOException {

        final BasicX509Credential credential = getSigningCredential(serviceProvider);
        final Signature signature = ((SignatureBuilder) builderFactory.getBuilder(Signature.DEFAULT_ELEMENT_NAME)).buildObject();
        signature.setSigningCredential(credential);
        signature.setSignatureAlgorithm(getSignatureAlgorithm(serviceProvider));
        signature.setCanonicalizationAlgorithm(getCanonicalizationAlgorithm(serviceProvider));
        final KeyInfo keyInfo = ((KeyInfoBuilder) builderFactory.getBuilder(KeyInfo.DEFAULT_ELEMENT_NAME)).buildObject();
        final X509Data data = ((X509DataBuilder) builderFactory.getBuilder(X509Data.DEFAULT_ELEMENT_NAME)).buildObject();
        final org.opensaml.xml.signature.X509Certificate cert = ((X509CertificateBuilder)
                builderFactory.getBuilder(org.opensaml.xml.signature.X509Certificate.DEFAULT_ELEMENT_NAME)).buildObject();
        final String value = org.apache.xml.security.utils.Base64.encode(credential.getEntityCertificate().getEncoded());
        cert.setValue(value);
        data.getX509Certificates().add(cert);
        keyInfo.getX509Datas().add(data);
        signature.setKeyInfo(keyInfo);

        xmlObject.setSignature(signature);
        Element responseElmt = null;
        try {
            responseElmt = marshallerFactory.getMarshaller(xmlObject).marshall(xmlObject);
            Signer.signObject(signature);
        } catch (SignatureException ex) {
            log.error(ex.getMessage());
        } catch (MarshallingException e) {
            throw new RuntimeException(e);
        }
        return responseElmt;
    }

    protected final void addX509Certificate(final StatusResponseType xmlObject, final SAMLAuthenticationProvider serviceProvider, final XMLObjectBuilderFactory builderFactory) throws InvalidKeySpecException, NoSuchAlgorithmException, CertificateException, IOException {
        final BasicX509Credential credential = getSigningCredential(serviceProvider);
        final Signature signature = ((SignatureBuilder) builderFactory.getBuilder(Signature.DEFAULT_ELEMENT_NAME)).buildObject();
        signature.setSigningCredential(credential);
        signature.setSignatureAlgorithm(getSignatureAlgorithm(serviceProvider));
        signature.setCanonicalizationAlgorithm(getCanonicalizationAlgorithm(serviceProvider));
        final KeyInfo keyInfo = ((KeyInfoBuilder) builderFactory.getBuilder(KeyInfo.DEFAULT_ELEMENT_NAME)).buildObject();
        final X509Data data = ((X509DataBuilder) builderFactory.getBuilder(X509Data.DEFAULT_ELEMENT_NAME)).buildObject();
        final org.opensaml.xml.signature.X509Certificate cert = ((X509CertificateBuilder)
                builderFactory.getBuilder(org.opensaml.xml.signature.X509Certificate.DEFAULT_ELEMENT_NAME)).buildObject();
        final String value = org.apache.xml.security.utils.Base64.encode(credential.getEntityCertificate().getEncoded());
        cert.setValue(value);
        data.getX509Certificates().add(cert);
        keyInfo.getX509Datas().add(data);
        signature.setKeyInfo(keyInfo);

        xmlObject.setSignature(signature);
    }

    protected void process(final SAMLRequestToken<? extends SAMLAuthenticationProvider> token, final HttpServletResponse response, final HttpServletRequest request, final String relayState) throws MessageEncodingException {
        final AuthnRequest authnRequest = token.getSamlObject();
        final Endpoint endpoint = token.getEndpoint();
        final BasicSAMLMessageContext<SAMLObject, AuthnRequest, SAMLObject> messageContext = new BasicSAMLMessageContext<SAMLObject, AuthnRequest, SAMLObject>();
        final HTTPOutTransport outTransport = new HttpServletResponseAdapter(response, URIUtils.isSecure(request));

        messageContext.setOutboundMessageTransport(outTransport);
        messageContext.setOutboundMessage(authnRequest);
        messageContext.setOutboundSAMLMessage(authnRequest);
        messageContext.setRelayState(relayState);
        messageContext.setPeerEntityEndpoint(endpoint);
        new HTTPRedirectDeflateEncoder().encode(messageContext);
    }

    protected boolean isUserEntitledTo(final String userId, final AuthenticationProvider provider) {
        return authorizationManager.isUserEntitledToResource(userId, provider.getResource().getId());
    }

    protected String getNameIdFormat(final SAMLIdentityProvider provider) {
        String retval = null;
        if (StringUtils.isNotBlank(provider.getNameIdFormat())) {
            retval = provider.getNameIdFormat();
        } else {
            retval = NameIDType.EMAIL;
        }
        return retval;
    }

    protected String getNameQualifier(final SAMLIdentityProvider provider) {
        return StringUtils.trimToNull(provider.getNameQualifier());
    }

    protected String getSPNameQualifier(final SAMLIdentityProvider provider) {
        return StringUtils.trimToNull(provider.getSpNameQualifier());
    }

    protected SAMLResponseToken getSAMLResponeToken(final SAMLIdentityProvider provider,
                                                    final AuthnRequest authnRequest,
                                                    final String relayState,
                                                    final String userId,
                                                    final String principal,
                                                    final HttpServletRequest httpRequest,
                                                    final HttpServletResponse httpResponse,
                                                    final Response previousChainedResponse) {
        final int samlIssueThresholdInMillis = samlThresholdMinutes * 60 * 1000;

        final SAMLResponseToken token = new SAMLResponseToken();
        final DateTime now = new DateTime(DateTimeZone.UTC);

        if (log.isDebugEnabled()) {
            log.debug(String.format("Relay State", relayState));
            log.debug(String.format("userId: %s", userId));
        }

        final IdmAuditLog auditLog = auditLogProvider.newInstance(httpRequest);
        auditLog.setAction(AuditAction.SAML_GENERATE_SAML_RESPONSE.value());
        auditLog.setRequestorPrincipal(principal);
        auditLog.setRequestorUserId(userId);
        auditLog.addAttribute(AuditAttributeName.RELAY_STATE, relayState);
        try {
            final MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
            final Element marshalledRequest = marshallerFactory.getMarshaller(authnRequest).marshall(authnRequest);
            final String samlReqeustString = XMLHelper.prettyPrintXML(marshalledRequest);
            auditLog.addAttribute(AuditAttributeName.SAML_REQUEST_XML, samlReqeustString);
            log.info(String.format("SAML Request: %s", samlReqeustString));

            final String requestIssuer = authnRequest.getIssuer().getValue();

            if (provider == null) {
                throw new AuthenticationException(String.format("SAMLProvider with Issuer '%s' not configured or doesn't exist", requestIssuer), Errors.SAML_NOT_CONFIGURED_FOR_ACS);
            }
            auditLog.addAttribute(AuditAttributeName.IDENTITY_PROVIDER_ID, provider.getId());

            final boolean isAnonymousAccessAllowed = (provider.getNextInChain() != null);
            if (userId == null && !isAnonymousAccessAllowed) {
                token.setAuthenticationCommenced(true);
                auditLog.addWarning("User not authenticated - commencing authentication");
                authEntryPoint.commence(httpRequest, httpResponse, null);
            } else if (provider.getNextInChain() != null) {
                final SAMLRequestToken<SAMLServiceProvider> samlRequestToken = generateSAMLRequest(provider.getNextInChain(), String.format("%s/idp/sp/login", URIUtils.getBaseURI(httpRequest)), httpRequest);
                process(samlRequestToken, httpResponse, httpRequest, relayState);
                token.setAuthenticationCommenced(true);
            } else {

                auditLog.addAttribute(AuditAttributeName.SAML_PROVIDER, provider.getId());

                if (!isUserEntitledTo(userId, provider)) {
                    final String message = String.format("User with ID '%s' not entitled to SSO with SAML Provider '%s", userId, provider.getName());
                    throw new AuthenticationException(message, Errors.SAML_USER_NOT_AUTHORIZED_TO_SSO);
                }

                final String acs = provider.getAssertionConsumerURL();

                validateAuthnRequest(authnRequest, provider);

				/* before even trying to generate the SAML Response, check to see if the User has a login with the SAML Provider */
                final Login providerLogin = getLoginByUserIdNamdManagedSystemId(userId, provider.getManagedSysId());
                if (providerLogin == null) {
                    throw new AuthenticationException("User does not have a login with this managed system", Errors.AUTH_USER_NOT_PROVISIONED);
                }

                final XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
                final Response response = ((ResponseBuilder) builderFactory.getBuilder(Response.DEFAULT_ELEMENT_NAME)).buildObject();
                response.setDestination(acs);
                response.setIssueInstant(now);
                response.setInResponseTo(authnRequest.getID());
                response.setID(generateId());


				/* set issuer on response */
                response.setIssuer(getResponseIssuer(httpRequest, builderFactory, provider));

				/* set status on response */
                final Status status = ((StatusBuilder) builderFactory.getBuilder(Status.DEFAULT_ELEMENT_NAME)).buildObject();
                final StatusCode statuscode = ((StatusCodeBuilder) builderFactory.getBuilder(StatusCode.DEFAULT_ELEMENT_NAME)).buildObject();
                statuscode.setValue(StatusCode.SUCCESS_URI);
                status.setStatusCode(statuscode);
                response.setStatus(status);

				/* create assertion on response */
                final Assertion assertion = ((AssertionBuilder) builderFactory.getBuilder(Assertion.DEFAULT_ELEMENT_NAME)).buildObject();
                assertion.setID(generateId());
                assertion.setIssueInstant(now);
                assertion.setIssuer(getResponseIssuer(httpRequest, builderFactory, provider));
                response.getAssertions().add(assertion);

				/* create subject */
                final Subject subject = ((SubjectBuilder) builderFactory.getBuilder(Subject.DEFAULT_ELEMENT_NAME)).buildObject();
                assertion.setSubject(subject);

				/* set subject confirmation data */
                final SubjectConfirmation bearerConfirmation = ((SubjectConfirmationBuilder) builderFactory.getBuilder(SubjectConfirmation.DEFAULT_ELEMENT_NAME)).buildObject();
                bearerConfirmation.setMethod(SubjectConfirmation.METHOD_BEARER);
                subject.getSubjectConfirmations().add(bearerConfirmation);

                final SubjectConfirmationData confirmationData = ((SubjectConfirmationDataBuilder) builderFactory.getBuilder(SubjectConfirmationData.DEFAULT_ELEMENT_NAME)).buildObject();
                bearerConfirmation.setSubjectConfirmationData(confirmationData);
                confirmationData.setInResponseTo(authnRequest.getID());
                confirmationData.setRecipient(acs);
                confirmationData.setNotOnOrAfter(now.plus(samlIssueThresholdInMillis));

				/* set nameId on subject */
                log.debug(String.format("Sending %s as the login ID", providerLogin.getLogin()));
                final NameID nameId = ((NameIDBuilder) builderFactory.getBuilder(NameID.DEFAULT_ELEMENT_NAME)).buildObject();
                final EmailAddress emailAddress = getDefaultEmailByUserId(userId);
                if (getNameIdFormat(provider).endsWith("emailAddress") && emailAddress != null) {
                    nameId.setValue(emailAddress.getEmailAddress());
                } else {
                    nameId.setValue(providerLogin.getLogin());
                }
                nameId.setFormat(getNameIdFormat(provider));
                nameId.setSPNameQualifier(getSPNameQualifier(provider));
                nameId.setNameQualifier(getNameQualifier(provider));
                subject.setNameID(nameId);

				/* set audience conditions */
                final List<Audience> audienceList = getAudiences(acs, provider, builderFactory);
                final Conditions condition = ((ConditionsBuilder) builderFactory.getBuilder(Conditions.DEFAULT_ELEMENT_NAME)).buildObject();
                condition.setNotBefore(now.minus(samlIssueThresholdInMillis));
                condition.setNotOnOrAfter(now.plus(samlIssueThresholdInMillis));
                if (CollectionUtils.isNotEmpty(audienceList)) {
                    final AudienceRestriction audienceRestriction = ((AudienceRestrictionBuilder) builderFactory.getBuilder(AudienceRestriction.DEFAULT_ELEMENT_NAME)).buildObject();
                    condition.getAudienceRestrictions().add(audienceRestriction);
                    audienceRestriction.getAudiences().addAll(audienceList);
                }
                assertion.setConditions(condition);

				/* set authentication statement */
                final AuthnStatement authnStatement = ((AuthnStatementBuilder) builderFactory.getBuilder(AuthnStatement.DEFAULT_ELEMENT_NAME)).buildObject();
                authnStatement.setAuthnInstant(now);
                final AuthnContext authnContext = ((AuthnContextBuilder) builderFactory.getBuilder(AuthnContext.DEFAULT_ELEMENT_NAME)).buildObject();
                final AuthnContextClassRef authnContextClassRef = ((AuthnContextClassRefBuilder) builderFactory.getBuilder(AuthnContextClassRef.DEFAULT_ELEMENT_NAME)).buildObject();
                authnContextClassRef.setAuthnContextClassRef(getAuthnContextClassRef(provider));
                authnStatement.setAuthnContext(authnContext);
                authnContext.setAuthnContextClassRef(authnContextClassRef);

                String sessionInd = generateId();
                addSessionIndexForAuthProviderToSession(httpRequest, provider.getId(), sessionInd);
                authnStatement.setSessionIndex(sessionInd);
                assertion.getAuthnStatements().add(authnStatement);

                if (provider.isHasAMAttributes()) {
                    final List<SSOAttribute> ssoAttributes =
                            authAttributeServiceClient.getSSOAttributes(provider.getId(), userId);
                    if (CollectionUtils.isNotEmpty(ssoAttributes)) {
                        final AttributeStatement attrStatement = ((AttributeStatementBuilder) builderFactory.getBuilder(AttributeStatement.DEFAULT_ELEMENT_NAME)).buildObject();
                        for (final SSOAttribute ssoAttribute : ssoAttributes) {
                            final Attribute attr = SAMLAttributeBuilder.buildAttribute(ssoAttribute, builderFactory);
                            if (attr != null) {
                                attrStatement.getAttributes().add(attr);
                            }
                        }
                        assertion.getAttributeStatements().add(attrStatement);
                    }
                }

                if (StringUtils.isNotBlank(provider.getPostProcessGroovyScript())) {
                    final AbsractSAMLSPChainHandler chainHandler = (AbsractSAMLSPChainHandler) scriptRunner.instantiateClass(null, provider.getPostProcessGroovyScript());
                    chainHandler.init(previousChainedResponse, httpRequest, httpResponse);
                    chainHandler.process(response);
                }

				/*
				 * TODO in 4.0 - validate signature.
				if(authnRequest.isSigned()) {
					final BasicX509Credential credential = new BasicX509Credential();

					credential.setUsageType(UsageType.SIGNING);

				    final InputStream inStream = new ByteArrayInputStream(provider.getPublicKey());
				    final CertificateFactory cf = CertificateFactory.getInstance("X.509");
				    final X509Certificate cert = (X509Certificate)cf.generateCertificate(inStream);
				    inStream.close();
				    credential.setEntityCertificate(cert);
				    credential.setPublicKey(cert.getPublicKey());

					final Signature signature = authnRequest.getSignature();
					final SignatureValidator validator = new SignatureValidator(credential);
					validator.validate(signature);
				}
				*/

				/* sign the response.  this MUST BE THE LAST BLOCK, otheriwse you'll get no signature!!!!! */
                Element responseElmt = null;
                if (provider.isSignAsserion()) {
                    final Signature signature = getSignature(provider, builderFactory);
                    if (CollectionUtils.isNotEmpty(signature.getContentReferences())) {
                        ((SAMLObjectContentReference) signature.getContentReferences().get(0)).setDigestAlgorithm(getDigestAlgorithm(provider));
                    }
                    assertion.setSignature(signature);
                    Configuration.getMarshallerFactory().getMarshaller(assertion).marshall(assertion);
                    Signer.signObject(signature);
                }
                if (provider.isSign()) {
                    responseElmt = sign(response, provider, builderFactory, marshallerFactory);
                } else {
                    responseElmt = marshallerFactory.getMarshaller(response).marshall(response);
                }

                token.setAssertionConsumerURL(acs);
                token.setSamlObject(response);
                token.setRelayState(relayState);
                token.setAuthProvider(provider);

                final String prettyXML = XMLHelper.prettyPrintXML(responseElmt);

                log.info(String.format("SAML XML: %s", prettyXML));
//				auditLog.addAttribute(AuditAttributeName.SAML_REQUEST_XML, samlReqeustString);
                auditLog.addAttribute(AuditAttributeName.SAML_RESPONSE_XML, prettyXML);

                addProviderToSession(provider, httpRequest);
            }
            auditLog.succeed();
        } catch (CustomSAMLException e) {
            auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
            token.setCustomErrorMessage(e.getMessage());
            log.warn(String.format("Authencation Exception: %s", e.getMessage()));
        } catch (AuthenticationException e) {
            auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
            token.setError(e.getError(), e);
            log.warn(String.format("Authencation Exception: %s", e.getMessage()));
        } catch (Throwable e) {
            auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
            log.error("SAML Exception", e);
            token.setError(Errors.INTERNAL_ERROR, e);
        } finally {
            auditLogProvider.add(AuditSource.IDP, httpRequest, auditLog);
        }
        return token;
    }

    protected Login getLoginByUserIdNamdManagedSystemId(final String userId, final String managedSystemId) {
		/* before even trying to generate the SAML Response, check to see if the User has a login with the SAML Provider */
        Login providerLogin = null;
        final LoginListResponse loginListResponse = loginServiceClient.getLoginByUser(userId);
        if (loginListResponse.getStatus() == ResponseStatus.SUCCESS) {
            if (CollectionUtils.isNotEmpty(loginListResponse.getPrincipalList())) {
                for (final Login login : loginListResponse.getPrincipalList()) {
                    if (StringUtils.equals(login.getManagedSysId(), managedSystemId)) {
                        providerLogin = login;
                        break;
                    }
                }
            }
        }

        return providerLogin;
    }

    protected SAMLLogoutRequestToken<P> generateLogoutRequest(final HttpServletRequest request, final P authProvider, final String reason) {
        final SAMLLogoutRequestToken<P> token = new SAMLLogoutRequestToken<>();

        final IdmAuditLog auditLog = auditLogProvider.newInstance(request);
        auditLog.setAction(AuditAction.SAML_SP_LOGOUT_REQUEST_GENERATE.value());
        final DateTime now = new DateTime();
        final int samlIssueThresholdInMillis = samlThresholdMinutes * 60 * 1000;

        try {
            final MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
            if (authProvider == null) {
                throw new AuthenticationException(String.format("%s not configured or doesn't exist", getAuthProviderClass()), Errors.SAML_SERVICE_PROVIDER_NOT_FOUND);
            }

            if (StringUtils.isBlank(authProvider.getLogoutURL())) {
                token.setConfiguredForSLO(false);
            } else {

                final XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();

                final LogoutRequest xmlRequest = ((LogoutRequestBuilder) builderFactory.getBuilder(LogoutRequest.DEFAULT_ELEMENT_NAME)).buildObject();
                xmlRequest.setDestination(authProvider.getLogoutURL());
                xmlRequest.setID(generateId());
                xmlRequest.setIssueInstant(now);
                xmlRequest.setReason(reason);
                xmlRequest.setNotOnOrAfter(now.plus(samlIssueThresholdInMillis));
                xmlRequest.setIssueInstant(now);

                xmlRequest.setIssuer(buildIssuer(request, builderFactory, authProvider));

                modifyLogoutRequestBeforeSigning(request, authProvider, builderFactory, xmlRequest);

                final Endpoint endpoint = ((SAMLObjectBuilder<Endpoint>) builderFactory.getBuilder(AssertionConsumerService.DEFAULT_ELEMENT_NAME)).buildObject();
                endpoint.setLocation(authProvider.getLogoutURL());
                token.setEndpoint(endpoint);

                Element xmlRequestElmt = null;
                if (authProvider.isSign()) {
                    xmlRequestElmt = addX509Certificate(xmlRequest, authProvider, builderFactory, marshallerFactory);
                } else {
                    xmlRequestElmt = marshallerFactory.getMarshaller(xmlRequest).marshall(xmlRequest);
                }

                final String prettyXML = XMLHelper.prettyPrintXML(xmlRequestElmt);
                auditLog.addAttribute(AuditAttributeName.SAML_REQUEST_XML, prettyXML);
                token.setSamlObject(xmlRequest);
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Logout Request to be sent: %s", prettyXML));
                }
            }
            token.setAuthProvider(authProvider);
            auditLog.succeed();
        } catch (AuthenticationException e) {
            auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
            token.setError(e.getError(), e);
            log.warn(String.format("Authencation Exception: %s", e.getMessage()));
        } catch (Throwable e) {
            auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
            log.error("SAML Exception", e);
            token.setError(Errors.INTERNAL_ERROR, e);
        } finally {
            auditLogProvider.add(AuditSource.SP, request, auditLog);
        }
        return token;
    }

    private String getAuthnContextClassRef(final SAMLIdentityProvider provider) {
        String retVal = null;
        if (StringUtils.isNotBlank(provider.getAuthContextClassRef())) {
            retVal = provider.getAuthContextClassRef();
        } else {
            retVal = AuthnContext.PASSWORD_AUTHN_CTX;
        }
        return retVal;
    }

    private List<Audience> getAudiences(final String assertionConsumerURL, final SAMLIdentityProvider provider, final XMLObjectBuilderFactory builderFactory) {
        final List<Audience> audienceList = new LinkedList<Audience>();
        if (CollectionUtils.isNotEmpty(provider.getAudiences())) {
            for (final String audienceURI : provider.getAudiences()) {
                final Audience audience = ((AudienceBuilder) builderFactory.getBuilder(Audience.DEFAULT_ELEMENT_NAME)).buildObject();
                audience.setAudienceURI(audienceURI);
                audienceList.add(audience);
            }
        }
        return audienceList;
    }

    protected Issuer getResponseIssuer(final HttpServletRequest request, final XMLObjectBuilderFactory builderFactory, final SAMLIdentityProvider provider) throws URISyntaxException {
        final Issuer issuer = ((IssuerBuilder) builderFactory.getBuilder(Issuer.DEFAULT_ELEMENT_NAME)).buildObject();
        issuer.setValue(getResponseIssuer(request, provider));
        return issuer;
    }

    protected String getResponseIssuer(final HttpServletRequest request, final SAMLIdentityProvider provider) throws URISyntaxException {
        String issuerURL = null;
        if (StringUtils.isNotBlank(provider.getResponseIssuer())) {
            issuerURL = provider.getResponseIssuer();
        } else { /* default to current URL*/
            issuerURL = new StringBuilder(URIUtils.getBaseURI(request)).append(idpLoginURL).toString();
        }
        return issuerURL;
    }

    protected Signature getSignature(final SAMLIdentityProvider provider, final XMLObjectBuilderFactory builderFactory) throws SecurityException, InvalidKeySpecException, NoSuchAlgorithmException, CertificateException, IOException {
        final Credential signingCredential = getSigningCredential(provider);
        final Signature signature = ((SignatureBuilder) builderFactory.getBuilder(Signature.DEFAULT_ELEMENT_NAME)).buildObject();
        signature.setSigningCredential(signingCredential);
        signature.setSignatureAlgorithm(getSignatureAlgorithm(provider));
        signature.setCanonicalizationAlgorithm(getCanonicalizationAlgorithm(provider));
        signature.setKeyInfo(getKeyInfo(signingCredential));
        return signature;
    }

    protected KeyInfo getKeyInfo(final Credential credential) throws SecurityException {
        final SecurityConfiguration secConfiguration = Configuration.getGlobalSecurityConfiguration();
        final NamedKeyInfoGeneratorManager namedKeyInfoGeneratorManager = secConfiguration.getKeyInfoGeneratorManager();
        final KeyInfoGeneratorManager keyInfoGeneratorManager = namedKeyInfoGeneratorManager.getDefaultManager();
        final KeyInfoGeneratorFactory keyInfoGeneratorFactory = keyInfoGeneratorManager.getFactory(credential);
        final KeyInfoGenerator keyInfoGenerator = keyInfoGeneratorFactory.newInstance();
        final KeyInfo keyInfo = keyInfoGenerator.generate(credential);
        return keyInfo;
    }

    private void addProviderToSession(final SAMLIdentityProvider provider, final HttpServletRequest request) {
        addProvierToSession(provider, request, org.openiam.ui.web.util.SAMLConstants.SAML_IDENETITY_PROVIDERS_FOR_SESSION);
    }


    protected void addProvierToSession(final SAMLAuthenticationProvider provider, final HttpServletRequest request, final String variableName) {
        Set<String> providers = (Set<String>) request.getSession(true).getAttribute(variableName);
        if (CollectionUtils.isEmpty(providers)) {
            providers = new HashSet<>();
        }
        providers.add(provider.getId());
        request.getSession(true).setAttribute(variableName, providers);
    }

    protected void addSessionIndexForAuthProviderToSession(final HttpServletRequest request, String providerId, String sessionIndex) {
        Map<String, String> indexes = (Map<String, String>) request.getSession(true).getAttribute(org.openiam.ui.web.util.SAMLConstants.SAML_SESSION_INDEX_FOR_PROVIDERS);
        if (indexes == null) {
            indexes = new HashMap<>();
        }
        indexes.put(providerId, sessionIndex);
        request.getSession(true).setAttribute(org.openiam.ui.web.util.SAMLConstants.SAML_SESSION_INDEX_FOR_PROVIDERS, indexes);
    }

    protected Map<String, String> getSessionIndexsForAuthProvidersFromSession(final HttpServletRequest request) {
        return (Map<String, String>) request.getSession(true).getAttribute(org.openiam.ui.web.util.SAMLConstants.SAML_SESSION_INDEX_FOR_PROVIDERS);
    }

    protected String getSessionIndexForAuthProviderFromSession(final HttpServletRequest request, String providerId) {
        Map<String, String> indexes = (Map<String, String>) request.getSession(true).getAttribute(org.openiam.ui.web.util.SAMLConstants.SAML_SESSION_INDEX_FOR_PROVIDERS);
        String ind = null;
        if (indexes != null) {
            ind = indexes.get(providerId);
        }
        return ind;
    }

    public void redirectAndSend(SAMLLogoutRequestToken<P> token, HttpServletResponse response, HttpServletRequest request) throws MessageEncodingException {
        log.debug(">>>>> AbstractSAMLService -> redirectAndSend");
        final LogoutRequest logoutRequest = token.getSamlObject();
        final Endpoint endpoint = token.getEndpoint();
        final BasicSAMLMessageContext<SAMLObject, LogoutRequest, SAMLObject> messageContext = new BasicSAMLMessageContext<SAMLObject, LogoutRequest, SAMLObject>();
        final HTTPOutTransport outTransport = new HttpServletResponseAdapter(response, URIUtils.isSecure(request));

        messageContext.setOutboundMessageTransport(outTransport);
        messageContext.setOutboundMessage(logoutRequest);
        messageContext.setOutboundSAMLMessage(logoutRequest);
        messageContext.setPeerEntityEndpoint(endpoint);
        messageContext.setOutboundSAMLMessageSigningCredential(token.getSamlObject().getSignature().getSigningCredential());
        new OpenIAMHTTPRedirectDeflateEncoder().encode(messageContext);
    }

    /*
     * Does basic validation on the SAML Request
     */
    private void validateAuthnRequest(final AuthnRequest authnRequest, final SAMLIdentityProvider provider) throws SAMLException {
        //TODO: timestamp validation

    }

    protected void validateTimestamps(final StatusResponseType xmlObject, final DateTime now, final HttpServletRequest request) throws AuthenticationException {
        final int samlIssueThresholdInMillis = samlThresholdMinutes * 60 * 1000;
        if (xmlObject.getIssueInstant() != null) {
            final DateTime issueInstant = xmlObject.getIssueInstant().toDateTime(DateTimeZone.UTC);
            if (issueInstant.isAfter(now.plusMillis((int) samlIssueThresholdInMillis))) {
                throw new AuthenticationException(String.format("response.issueInstant.isAfter threshhold %s", samlIssueThresholdInMillis), Errors.SAML_SP_EXCEPTION);
            }
            if (issueInstant.isBefore(now.minusMillis((int) samlIssueThresholdInMillis))) {
                throw new AuthenticationException(String.format("response.issueInstant.isBefore threshhold %s", samlIssueThresholdInMillis), Errors.SAML_SP_EXCEPTION);
            }
        } else {
            throw new AuthenticationException(String.format("response.issueInstant is null"), Errors.SAML_SP_EXCEPTION);
        }
    }

    protected void validateTimestamps(final RequestAbstractType xmlObject, final DateTime now, final HttpServletRequest request) throws AuthenticationException {
        final int samlIssueThresholdInMillis = samlThresholdMinutes * 60 * 1000;
        if (xmlObject.getIssueInstant() != null) {
            final DateTime issueInstant = xmlObject.getIssueInstant().toDateTime(DateTimeZone.UTC);
            if (issueInstant.isAfter(now.plusMillis((int) samlIssueThresholdInMillis))) {
                throw new AuthenticationException(String.format("response.issueInstant.isAfter threshhold %s", samlIssueThresholdInMillis), Errors.SAML_SP_EXCEPTION);
            }
            if (issueInstant.isBefore(now.minusMillis((int) samlIssueThresholdInMillis))) {
                throw new AuthenticationException(String.format("response.issueInstant.isBefore threshhold %s", samlIssueThresholdInMillis), Errors.SAML_SP_EXCEPTION);
            }
        } else {
            throw new AuthenticationException(String.format("response.issueInstant is null"), Errors.SAML_SP_EXCEPTION);
        }
    }

    protected void validateSignature(final Signature signature, final P serviceProvider) throws CertificateException, IOException, ValidationException {
        final BasicX509Credential credential = new BasicX509Credential();

        credential.setUsageType(UsageType.SIGNING);

        final InputStream inStream = new ByteArrayInputStream(serviceProvider.getPublicKey());
        final CertificateFactory cf = CertificateFactory.getInstance("X.509");
        final X509Certificate cert = (X509Certificate) cf.generateCertificate(inStream);
        inStream.close();
        credential.setEntityCertificate(cert);
        credential.setPublicKey(cert.getPublicKey());

        final SignatureValidator validator = new SignatureValidator(credential);
        validator.validate(signature);
    }

    protected SAMLLogoutResponseToken<P> generateSAMLLogoutResponse(final HttpServletRequest request, final LogoutRequest logoutRequest, final P authProvider, final String statusCode) {
        final SAMLLogoutResponseToken<P> token = new SAMLLogoutResponseToken<>();
        final IdmAuditLog auditLog = auditLogProvider.newInstance(request);
        auditLog.setAction(AuditAction.SAML_SP_LOGOUT_RESPONSE_GENERATE.value());
        final DateTime now = new DateTime();
        final int samlIssueThresholdInMillis = samlThresholdMinutes * 60 * 1000;

        try {
            final MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
            if (authProvider == null) {
                throw new AuthenticationException(String.format("SAMLServiceProvider not configured or doesn't exist"), Errors.SAML_SERVICE_PROVIDER_NOT_FOUND);
            }

            if (StringUtils.isBlank(authProvider.getLogoutURL())) {
                final String warning = String.format(String.format("SLO is not configured for IDP %s - it has no SP Logout URL defined", logoutRequest.getIssuer().getValue()));
                log.warn(warning);
                auditLog.addWarning(warning);
                token.setConfiguredForSLO(false);
            } else {
                final XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();

                final LogoutResponse xmlRequest = ((LogoutResponseBuilder) builderFactory.getBuilder(LogoutResponse.DEFAULT_ELEMENT_NAME)).buildObject();
                xmlRequest.setDestination(authProvider.getLogoutURL());
                xmlRequest.setID(generateId());
                xmlRequest.setIssueInstant(now);
                xmlRequest.setInResponseTo(logoutRequest.getID());

                xmlRequest.setIssuer(buildIssuer(request, builderFactory, authProvider));

                final StatusCode newStatusCode = ((StatusCodeBuilder) builderFactory.getBuilder(StatusCode.DEFAULT_ELEMENT_NAME)).buildObject();
                newStatusCode.setValue(statusCode);

                final Status newStatus = ((StatusBuilder) builderFactory.getBuilder(Status.DEFAULT_ELEMENT_NAME)).buildObject();
                newStatus.setStatusCode(newStatusCode);
                xmlRequest.setStatus(newStatus);

                final Endpoint endpoint = ((SAMLObjectBuilder<Endpoint>) builderFactory.getBuilder(AssertionConsumerService.DEFAULT_ELEMENT_NAME)).buildObject();
                endpoint.setLocation(authProvider.getLogoutURL());
                token.setEndpoint(endpoint);


                Element xmlRequestElmt = null;

                if (authProvider.isSign()) {
                    xmlRequestElmt = signLogoutResponse(xmlRequest, authProvider, builderFactory, marshallerFactory);
                } else {
                    xmlRequestElmt = marshallerFactory.getMarshaller(xmlRequest).marshall(xmlRequest);
                }

                final String prettyXML = XMLHelper.prettyPrintXML(xmlRequestElmt);
                auditLog.addAttribute(AuditAttributeName.SAML_XML, prettyXML);
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Logout Response (Before Signature): %s", prettyXML));
                }
                token.setSamlObject(xmlRequest);
            }
            token.setAuthProvider(authProvider);
            auditLog.succeed();
        } catch (AuthenticationException e) {
            auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
            token.setError(e.getError(), e);
            log.warn(String.format("Authencation Exception: %s", e.getMessage()));
        } catch (Throwable e) {
            auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
            log.error("SAML Exception", e);
            token.setError(Errors.INTERNAL_ERROR, e);
        } finally {
            auditLogProvider.add(AuditSource.SP, request, auditLog);
        }
        return token;
    }

    @Override
    public SAMLLogoutRequestToken<P> validateAndExtractLogoutRequest(HttpServletRequest request) {
        final IdmAuditLog auditLog = auditLogProvider.newInstance(request);
        auditLog.setAction(AuditAction.SAML_GET_LOGOUT_REQUEST.value());

        final SAMLLogoutRequestToken<P> token = new SAMLLogoutRequestToken<>();
        final DateTime now = new DateTime(DateTimeZone.UTC);
        try {
            final MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
            final LogoutRequest xmlRequest = SAMLDigestUtils.getLogoutRequest(request, skipURICheck);

            final Element xmlRequestElmt = marshallerFactory.getMarshaller(xmlRequest).marshall(xmlRequest);
            final String prettyXML = XMLHelper.prettyPrintXML(xmlRequestElmt);
            auditLog.addAttribute(AuditAttributeName.SAML_XML, prettyXML);
            if (log.isDebugEnabled()) {
                log.debug(String.format("Logout Response: %s", prettyXML));
            }

            validateTimestamps(xmlRequest, now, request);

            if (xmlRequest.getIssuer() == null || StringUtils.isBlank(xmlRequest.getIssuer().getValue())) {
                throw new AuthenticationException(String.format("response.issuer is missing or empty"), Errors.SAML_SP_EXCEPTION);
            }

            final P samlProvider = getAuthProvider(xmlRequest, request);
            if (samlProvider == null) {
                throw new AuthenticationException(String.format("%s not configured or doesn't exist for this request", getAuthProviderClass()), Errors.SAML_SERVICE_PROVIDER_NOT_FOUND);
            }

            if (samlProvider.isSign()) {
                final Signature signature = xmlRequest.getSignature();
                if (signature != null) {
                    validateSignature(signature, samlProvider);
                }
            }

            isUserEntitledTo(cookieProvider.getUserId(request), samlProvider);

            token.setSamlObject(xmlRequest);
            token.setAuthProvider(samlProvider);
            auditLog.addAttribute(AuditAttributeName.LOGOUT_REASON, xmlRequest.getReason());
            auditLog.succeed();
        } catch (AuthenticationException e) {
            auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
            token.setError(e.getError(), e);
            log.warn(String.format("Authencation Exception: %s", e.getMessage()));
        } catch (Throwable e) {
            auditLog.setFailureReason(e.getMessage());
            auditLog.setException(e);
            auditLog.fail();
            log.error("SAML Exception", e);
            token.setError(Errors.INTERNAL_ERROR, e);
        } finally {
            auditLogProvider.add(AuditSource.SP, request, auditLog);
        }
        return token;
    }


    protected abstract P getAuthProvider(LogoutRequest logoutRequest, HttpServletRequest request);

    protected abstract Issuer buildIssuer(HttpServletRequest request, XMLObjectBuilderFactory builderFactory, P authProvider);

    protected abstract Class<P> getAuthProviderClass();

    protected abstract void modifyLogoutRequestBeforeSigning(HttpServletRequest request,
                                                             P authProvider,
                                                             XMLObjectBuilderFactory builderFactory,
                                                             LogoutRequest xmlRequest) throws AuthenticationException;

    protected abstract Element signLogoutResponse(LogoutResponse logoutResponse, P authProvider, XMLObjectBuilderFactory builderFactory, MarshallerFactory marhsallFactory);

    protected abstract void validateSAMLLogoutResponseSignature(LogoutResponse logoutResponse, P authProvider) throws CertificateException, IOException, ValidationException;

    protected EmailAddress getDefaultEmailByUserId(final String userId) {
        EmailAddress email = null;
        final List<EmailAddress> emailAddresses = userDataWebService.getEmailAddressList(userId);
        if (CollectionUtils.isNotEmpty(emailAddresses)) {
            for (final EmailAddress addr : emailAddresses) {
                if (addr.getIsDefault()) {
                    email = addr;
                    break;
                }
            }
            if (email == null) {
                email = emailAddresses.get(0);
            }
        }
        return email;
    }
}
