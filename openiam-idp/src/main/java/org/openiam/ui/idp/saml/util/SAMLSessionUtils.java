package org.openiam.ui.idp.saml.util;

import org.opensaml.common.SAMLObject;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.saml2.core.Response;
import org.opensaml.xml.XMLObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.saml.storage.HttpSessionStorage;

public class SAMLSessionUtils {
	
	private static final String LAST_SAML_MESSAGE_ID = "lastSAMLMessageId";
	private static final String LAST_LOGOUT_REQUEST_ID = "lastSAMLLogoutRequestId";
	private static final String LAST_SAML_RESPONSE = "lastSAMLResponse";

	public static void setLastSAMLResponse(final HttpServletRequest request, final Response object) {
		set(request, object, object.getID(), LAST_SAML_RESPONSE);
	}
	
	public static Response getLastSAMLResponse(final HttpServletRequest request) {
		return get(request, LAST_SAML_RESPONSE, Response.class);
	}
	
	public static void setLastAuthnRequest(final HttpServletRequest request, final AuthnRequest object) {
		set(request, object, object.getID(), LAST_SAML_MESSAGE_ID);
	}
	
	public static boolean hasLastAuthnRequest(final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);
		return (session != null) ? (session.getAttribute(LAST_SAML_MESSAGE_ID) != null) : false;
	}
	
	public static AuthnRequest getLastAuthnRequest(final HttpServletRequest request) {
		return get(request, LAST_SAML_MESSAGE_ID, AuthnRequest.class);
	}
	
	public static LogoutRequest getLastLogoutRequest(final HttpServletRequest request) {
		return get(request, LAST_LOGOUT_REQUEST_ID, LogoutRequest.class);
	}
	
	public static void setLastLogoutReqeust(final HttpServletRequest request, final LogoutRequest object) {
		set(request, object, object.getID(), LAST_LOGOUT_REQUEST_ID);
	}
	
	private static <T> T get(final HttpServletRequest request, final String variableName, final Class<T> clazz) {
		final HttpSession session = request.getSession(true);
		final HttpSessionStorage storage = new HttpSessionStorage(session);
		final String lastSAMLMessageId = (String)request.getSession(false).getAttribute(variableName);
		final T logoutRequest = (lastSAMLMessageId != null) ? (T)storage.retrieveMessage(lastSAMLMessageId) : null;
		if(logoutRequest != null) {
			/* store it back to the session after retreiving it */
			storage.storeMessage(lastSAMLMessageId, (XMLObject)logoutRequest);
		}
			
		return logoutRequest;
	}
	
	private static void set(final HttpServletRequest request, final SAMLObject object, final String id, final String variableName) {
		final HttpSession session = request.getSession(true);
		session.setAttribute(variableName, id);
		final HttpSessionStorage storage = new HttpSessionStorage(session);
		storage.storeMessage(id, object);
	}
}
