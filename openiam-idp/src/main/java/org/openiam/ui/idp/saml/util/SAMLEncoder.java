package org.openiam.ui.idp.saml.util;

import org.apache.commons.codec.binary.Base64;
import org.opensaml.saml2.core.StatusResponseType;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.io.MarshallingException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

public class SAMLEncoder {

	public static String encodeResponse(final StatusResponseType response) throws MarshallingException, TransformerFactoryConfigurationError, TransformerException, UnsupportedEncodingException {
		final MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
		final Marshaller marshaller = marshallerFactory.getMarshaller(response);
		final Element responseElmt = marshaller.marshall(response);
		final String xml = toString(responseElmt);
		final String encodedXML = new Base64().encodeAsString(xml.getBytes("UTF-8"));
		//System.out.println(xml);
		return encodedXML;
	}
	
	private static String toString(final Node node) throws TransformerFactoryConfigurationError, TransformerException {
		final StringWriter writer = new StringWriter();
		final Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.transform(new DOMSource(node), new StreamResult(writer));
		final String xml = writer.toString();
		return xml;
	}
}
