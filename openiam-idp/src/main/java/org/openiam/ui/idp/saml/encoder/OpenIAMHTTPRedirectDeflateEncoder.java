package org.openiam.ui.idp.saml.encoder;

import org.opensaml.common.binding.SAMLMessageContext;
import org.opensaml.saml2.binding.encoding.HTTPRedirectDeflateEncoder;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.ws.transport.http.HTTPOutTransport;
import org.opensaml.ws.transport.http.HTTPTransportUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpenIAMHTTPRedirectDeflateEncoder extends HTTPRedirectDeflateEncoder {

	/** Class logger. */
	private final Logger log = LoggerFactory.getLogger(OpenIAMHTTPRedirectDeflateEncoder.class);

	public OpenIAMHTTPRedirectDeflateEncoder() {
		super();
	}
/*
	@Override
	protected void doEncode(MessageContext messageContext) throws MessageEncodingException {
		if (!(messageContext instanceof SAMLMessageContext)) {
			log.error("Invalid message context type, this encoder only support SAMLMessageContext");
			throw new MessageEncodingException(
					"Invalid message context type, this encoder only support SAMLMessageContext");
		}

		if (!(messageContext.getOutboundMessageTransport() instanceof HTTPOutTransport)) {
			log.error("Invalid outbound message transport type, this encoder only support HTTPOutTransport");
			throw new MessageEncodingException(
					"Invalid outbound message transport type, this encoder only support HTTPOutTransport");
		}

		SAMLMessageContext samlMsgCtx = (SAMLMessageContext) messageContext;

		String endpointURL = getEndpointURL(samlMsgCtx).buildURL();

		setResponseDestination(samlMsgCtx.getOutboundSAMLMessage(), endpointURL);

		//removeSignature(samlMsgCtx);

		String encodedMessage = deflateAndBase64Encode(samlMsgCtx.getOutboundSAMLMessage());

		String redirectURL = buildRedirectURL(samlMsgCtx, endpointURL, encodedMessage);

		HTTPOutTransport out = (HTTPOutTransport) messageContext.getOutboundMessageTransport();
		HTTPTransportUtils.addNoCacheHeaders(out);
		HTTPTransportUtils.setUTF8Encoding(out);

		out.sendRedirect(redirectURL);
	}
	*/
}
