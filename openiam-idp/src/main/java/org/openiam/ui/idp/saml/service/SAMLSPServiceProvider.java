package org.openiam.ui.idp.saml.service;

import org.openiam.ui.idp.saml.model.SAMLRequestToken;
import org.openiam.ui.idp.saml.model.SAMLResponseToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutRequestToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutResponseToken;
import org.openiam.ui.idp.saml.model.sp.SAMLSPMetadataResponse;
import org.openiam.ui.idp.saml.provider.SAMLServiceProvider;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.saml2.core.LogoutResponse;
import org.opensaml.ws.message.encoder.MessageEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SAMLSPServiceProvider extends SAMLService<LogoutRequest, LogoutResponse, SAMLServiceProvider> {

	SAMLSPMetadataResponse getMetadata(HttpServletRequest request, String id);
	SAMLLogoutRequestToken<SAMLServiceProvider> generateLogoutRequest(HttpServletRequest request, String id, String reason);
	SAMLRequestToken<SAMLServiceProvider> generateSAMLRequest(HttpServletRequest request, String id);
	SAMLServiceProvider getSAMLServiceProvierById(String id);
	
	void redirectAndSend(SAMLRequestToken<SAMLServiceProvider> token, HttpServletResponse response, HttpServletRequest request, String relayState) throws MessageEncodingException;
	
	SAMLResponseToken processSAMLResponse(HttpServletRequest request, HttpServletResponse httpResponse, boolean isDebugRequest);
	
	SAMLLogoutResponseToken<SAMLServiceProvider> processLogoutResponse(HttpServletRequest request, boolean isDebugRequest);
}
