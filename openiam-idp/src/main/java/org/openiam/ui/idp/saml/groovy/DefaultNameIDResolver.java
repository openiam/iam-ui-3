package org.openiam.ui.idp.saml.groovy;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.base.ws.MatchType;
import org.openiam.base.ws.SearchParam;
import org.openiam.idm.searchbeans.EmailSearchBean;
import org.openiam.idm.srvc.auth.dto.Login;
import org.openiam.idm.srvc.auth.ws.LoginDataWebService;
import org.openiam.idm.srvc.auth.ws.LoginListResponse;
import org.openiam.idm.srvc.continfo.dto.EmailAddress;
import org.openiam.idm.srvc.user.ws.UserDataWebService;
import org.openiam.ui.util.SpringContextProvider;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.NameIDType;
import org.opensaml.saml2.core.Response;
import org.opensaml.xml.security.SecurityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class DefaultNameIDResolver {

	protected HttpServletRequest request;
	protected Response samlResponse;
	protected NameID nameId;

	@Autowired
	@Qualifier("loginServiceClient")
	private LoginDataWebService loginServiceClient;

	@Autowired
	@Qualifier("userServiceClient")
	protected UserDataWebService userDataWebService;

	public DefaultNameIDResolver() {
		SpringContextProvider.resolveProperties(this);
		SpringContextProvider.autowire(this);
	}

	public final void init(final HttpServletRequest request, final Response samlResponse, final NameID nameId) {
		this.request = request;
		this.samlResponse = samlResponse;
		this.nameId = nameId;
	}

	/**
	 * This method returns a principal, given the current managedSysId, NameID, SAML Response, and HttpServletRequest.
	 * If no principal is found, and justInTimeAuthEnabled is false, a SecurityException should be thrown.
	 * If no principal is found, but justInTimeAuthEnabled is true, null should be returned
	 *
	 * @param managedSysId - the Managed System of the current SAML Service Provider
	 * @param justInTimeAuthEnabled - is just-in-time authentication enabled for teh current SAML Service Provider
	 * @return
	 * @throws SecurityException
	 */
	public String getPrincipal(final String managedSysId, final boolean justInTimeAuthEnabled) throws SecurityException {
		String nameIdFormat = nameId.getFormat();
		final String nameIdValue = nameId.getValue();
		
		/* SAML Oasis Section 2.2.2 - use default NameID if none is specified */
		nameIdFormat = (StringUtils.isBlank(nameIdFormat)) ? NameIDType.UNSPECIFIED : nameIdFormat;

		String principal = null;
		switch(nameIdFormat) {
			case NameIDType.EMAIL:
				final EmailSearchBean searchBean = new EmailSearchBean();
				searchBean.setDeepCopy(false);
				searchBean.setEmailMatchToken(new SearchParam(nameIdValue, MatchType.EXACT));
				final List<EmailAddress> emailAddresses = userDataWebService.findEmailBeans(searchBean, 0, 10);
				
				/* if just-in-time auth is not enabled, and there is no match, throw an exception */
				if(!justInTimeAuthEnabled && CollectionUtils.isEmpty(emailAddresses)) {
					throw new SecurityException(String.format("Email address '%s' resulted in no results", nameIdValue));
				}
				
				/* 
				 * If any matching email address is found, try to find a login match 
				 */
				if(emailAddresses != null) {
					/* bad data check */
					if(emailAddresses.size() > 1) {
						throw new SecurityException(String.format("Email address '%s' maps to mroe than one record", nameIdValue));
					}
					
					/* bad data check */
					final LoginListResponse loginListResponse = loginServiceClient.getLoginByUser(emailAddresses.get(0).getParentId());
					if(CollectionUtils.isEmpty(loginListResponse.getPrincipalList())) {
						throw new SecurityException(String.format("Email address '%s' maps to a user with no principals", nameIdValue));
					}

					for(final Login login : loginListResponse.getPrincipalList()) {
						if(StringUtils.equals(login.getManagedSysId(), managedSysId)) {
							principal = login.getLogin();
							break;
						}
					}

					if(!justInTimeAuthEnabled && principal == null) {
						throw new SecurityException(String.format("Could not find principal '%s'", nameIdValue));
					}
				}
				break;
			default:
				principal = nameIdValue;
				
				/* none was sent */
				if(StringUtils.isBlank(principal)) {
					throw new SecurityException(String.format("No principal sent in AuthnResponse"));
				}
				break;
		}
		return principal;
	}
}
