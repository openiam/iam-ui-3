package org.openiam.ui.idp.saml.provider;

import java.util.Arrays;

public abstract class SAMLAuthenticationProvider extends AuthenticationProvider {

	private String signingAlgorithm;
	private String canonicalizationAlgorithm;
	private byte[] publicKey;
	private String logoutURL;


	public byte[] getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(byte[] publicKey) {
		this.publicKey = publicKey;
	}

	public String getSigningAlgorithm() {
		return signingAlgorithm;
	}

	public void setSigningAlgorithm(String signingAlgorithm) {
		this.signingAlgorithm = signingAlgorithm;
	}

	public String getCanonicalizationAlgorithm() {
		return canonicalizationAlgorithm;
	}

	public void setCanonicalizationAlgorithm(String canonicalizationAlgorithm) {
		this.canonicalizationAlgorithm = canonicalizationAlgorithm;
	}

	public String getLogoutURL() {
		return logoutURL;
	}

	public void setLogoutURL(String logoutURL) {
		this.logoutURL = logoutURL;
	}

	@Override
	public String toString() {
		return "SAMLAuthenticationProvider{" +
				"signingAlgorithm='" + signingAlgorithm + '\'' +
				", canonicalizationAlgorithm='" + canonicalizationAlgorithm + '\'' +
				", publicKey=" + Arrays.toString(publicKey) +
				", logoutURL='" + logoutURL + '\'' +
				", name=" + this.getName() + ", id=" + this.getId() + '\'' +
				", managedSysId=" + this.getManagedSysId() + '\'' +
				", description=" + this.getDescription()  + '\'' +
				", sign=" + this.getSigningAlgorithm() + '}';
	}
}
