package org.openiam.ui.idp.web.mvc;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.openiam.ui.idp.saml.model.SAMLResponseToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutRequestToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutResponseToken;
import org.openiam.ui.idp.saml.provider.SAMLAuthenticationProvider;
import org.openiam.ui.idp.saml.service.SAMLService;
import org.openiam.ui.idp.saml.util.SAMLEncoder;
import org.openiam.ui.util.URIUtils;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.util.messages.Errors;
import org.openiam.ui.web.util.LoginProvider;
import org.opensaml.common.SignableSAMLObject;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.xml.io.MarshallingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public abstract class AbstractSAMLController<RequestObject extends SignableSAMLObject, ResponseObject extends SignableSAMLObject, P extends SAMLAuthenticationProvider> {

	@Value("${openiam.ui.logouturl}")
	protected String logoutURL;

	@Autowired
	protected LoginProvider loginProvider;

	protected String handleSAMLResponse(final SAMLResponseToken token, final HttpServletRequest request) throws UnsupportedEncodingException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		if(token.isError()) {
			request.setAttribute("error", new ErrorToken(token.getError()));
			request.setAttribute("customMessage", token.getCustomErrorMessage());
			return "common/message";
		} else if(token.isAuthenticationCommenced()) {
			return null;
		} else {
			request.setAttribute("SAMLResponse", SAMLEncoder.encodeResponse(token.getSamlObject()));
			request.setAttribute("acs", token.getAssertionConsumerURL());
			request.setAttribute("RelayState", token.getRelayState());
			return "auth/samlPost";
		}
	}

	protected String handleSAMLResponse(final SAMLLogoutResponseToken<P> token, final HttpServletRequest request) throws UnsupportedEncodingException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		if(token.isError()) {
			request.setAttribute("error", new ErrorToken(token.getError()));
			request.setAttribute("customMessage", token.getCustomErrorMessage());
			return "common/message";
		} else {
			request.setAttribute("SAMLResponse", SAMLEncoder.encodeResponse(token.getSamlObject()));
			request.setAttribute("acs", token.getEndpoint().getLocation());
			return "auth/samlPost";
		}
	}

	protected String sendLogoutResponse(final LogoutRequest logoutRequest, final HttpServletRequest request, final HttpServletResponse response, final String statusCode) throws MessageEncodingException, IOException, URISyntaxException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		//send LogoutResponse back to SP
		final SAMLLogoutResponseToken<P> responseToken = getSAMLService().generateSAMLLogoutResponse(request, logoutRequest, statusCode);
		if(responseToken.isError()) {
			if (Errors.SAML_SERVICE_PROVIDER_NOT_FOUND.equals(responseToken.getError())){
				response.sendRedirect(new StringBuilder(URIUtils.getBaseURI(request)).append(logoutURL).toString());
				return null;
			}
			request.setAttribute("error", new ErrorToken(responseToken.getError()));
			return "common/message";
		} else if(!responseToken.isConfiguredForSLO()) {
			response.sendRedirect(new StringBuilder(URIUtils.getBaseURI(request)).append(logoutURL).toString());
			return null;
		} else {
			final String view = handleSAMLResponse(responseToken, request);
			loginProvider.doLogout(request, response, false);
			return view;
		}
	}

	protected SAMLLogoutRequestToken<P> validateAndExtractLogoutRequest(final HttpServletRequest request) {
		return getSAMLService().validateAndExtractLogoutRequest(request);
	}

	protected abstract SAMLService<? extends SignableSAMLObject, ? extends SignableSAMLObject, P> getSAMLService();
}
