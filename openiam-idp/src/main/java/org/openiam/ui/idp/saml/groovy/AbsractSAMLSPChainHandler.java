package org.openiam.ui.idp.saml.groovy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openiam.ui.idp.saml.exception.CustomSAMLException;
import org.openiam.ui.util.SpringContextProvider;
import org.opensaml.saml2.core.Response;

public abstract class AbsractSAMLSPChainHandler {

	/**
	 * The previous SAMLResponse, in case of chaining
	 * null if not chained
	 */
	protected Response previousSAMLResponse;

	/**
	 * Current HttpServletRequest
	 */
	protected HttpServletRequest request;

	/**
	 * current HttpServletResponse
	 */
	protected HttpServletResponse response;

	public AbsractSAMLSPChainHandler() {
		SpringContextProvider.resolveProperties(this);
		SpringContextProvider.autowire(this);
	}

	public final void init(final Response previousSAMLResponse,
						   final HttpServletRequest request,
						   final HttpServletResponse response) {
		this.previousSAMLResponse = previousSAMLResponse;
		this.request = request;
		this.response = response;
	}

	/**
	 * This method can decorate the current SAML Response as required by business rules
	 * @param currentSAMLResponse - the current SAML Response, which will be sent back to the Service Provider
	 * @throws CustomSAMLException - a custom Exception which can be thrown by the implementor.
	 */
	public abstract void process(final Response currentSAMLResponse) throws CustomSAMLException;
}
