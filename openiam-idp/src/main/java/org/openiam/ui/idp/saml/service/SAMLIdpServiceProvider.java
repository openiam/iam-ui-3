package org.openiam.ui.idp.saml.service;

import org.openiam.ui.idp.saml.model.SAMLResponseToken;
import org.openiam.ui.idp.saml.model.idp.SAMLIDPMetadataResponse;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutRequestToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutResponseToken;
import org.openiam.ui.idp.saml.provider.SAMLIdentityProvider;
import org.openiam.ui.idp.saml.provider.SAMLServiceProvider;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.Response;
import org.opensaml.ws.message.encoder.MessageEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SAMLIdpServiceProvider extends SAMLService<AuthnRequest, Response, SAMLIdentityProvider> {
	
	/**
	 * Logs the user in, and generates a SAML Response Object in the corresponding Token
	 * @param request - HttpServletRequest
	 * @return The SAMLResponseToken representing what this method did/did not do.
	 */
	public SAMLResponseToken generateSAMLResponse(final HttpServletRequest request, final HttpServletResponse httpResponse);
	
	SAMLIDPMetadataResponse getMetadata(HttpServletRequest request, String id);
	
	public SAMLServiceProvider getSAMLServiceProvierById(final String id);
	
	SAMLLogoutResponseToken<SAMLIdentityProvider> getSAMLLogoutResponse(final HttpServletRequest request);
	
	public SAMLLogoutRequestToken<SAMLIdentityProvider> generateLogoutRequest(final HttpServletRequest request, final String id, final String reason);
	
	void redirectAndSend(SAMLLogoutResponseToken<SAMLIdentityProvider> token, HttpServletRequest request, HttpServletResponse response) throws MessageEncodingException;
}
