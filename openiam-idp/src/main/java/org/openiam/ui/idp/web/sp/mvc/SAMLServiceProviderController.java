package org.openiam.ui.idp.web.sp.mvc;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openiam.ui.idp.saml.model.SAMLRequestToken;
import org.openiam.ui.idp.saml.model.SAMLResponseToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutRequestToken;
import org.openiam.ui.idp.saml.model.sp.SAMLLogoutResponseToken;
import org.openiam.ui.idp.saml.model.sp.SAMLSPMetadataResponse;
import org.openiam.ui.idp.saml.provider.SAMLServiceProvider;
import org.openiam.ui.idp.saml.service.SAMLSPServiceProvider;
import org.openiam.ui.idp.saml.service.SAMLService;
import org.openiam.ui.idp.saml.util.SAMLSessionUtils;
import org.openiam.ui.idp.web.mvc.AbstractSAMLController;
import org.openiam.ui.util.URIUtils;
import org.openiam.ui.util.messages.ErrorToken;
import org.openiam.ui.web.util.SAMLConstants;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.saml2.core.LogoutResponse;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.util.XMLHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import java.io.IOException;
import java.net.URISyntaxException;

@Controller
@RequestMapping("/sp")
public class SAMLServiceProviderController extends AbstractSAMLController<LogoutRequest, LogoutResponse, SAMLServiceProvider> {

	private static Logger log = Logger.getLogger(SAMLServiceProviderController.class);

	@Autowired
	private SAMLSPServiceProvider samlService;

	@RequestMapping(value="/metadata/{serviceProviderId}", method=RequestMethod.GET, produces="text/xml")
	public @ResponseBody String spMetadata(final HttpServletRequest request,
										   final HttpServletResponse response,
										   final @PathVariable(value="serviceProviderId") String serviceProviderId) throws IOException {
		final SAMLSPMetadataResponse token = samlService.getMetadata(request, serviceProviderId);
		if(token.isError()) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, token.getError().toString());
			return null;
		} else {
			return XMLHelper.prettyPrintXML(token.getEntityDescriptorElement());
		}
	}

	/**
	 * This method is called by the IDP
	 * Input:  LogoutRequest, send by the IDP
	 * Output:  LogoutResponse sent back to the IDP
	 * @param request
	 * @param response
	 * @return
	 * @throws TransformerException
	 * @throws TransformerFactoryConfigurationError
	 * @throws MarshallingException
	 * @throws URISyntaxException
	 * @throws IOException
	 * @throws MessageEncodingException
	 */
	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public String logoutRequestFromIDP(final HttpServletRequest request,
									   final HttpServletResponse response) throws MessageEncodingException, IOException, URISyntaxException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		final SAMLLogoutRequestToken<SAMLServiceProvider> token = validateAndExtractLogoutRequest(request);
		if(token.getSamlObject() != null) {
			log.debug(">>>>> logoutRequestFromIDP :" + token.getSamlObject());
			final String view = sendLogoutResponse(token.getSamlObject(), request, response, (!token.isError()) ? StatusCode.SUCCESS_URI : StatusCode.PARTIAL_LOGOUT_URI);
			
			/* clear the session now that you don't need it anymore */
			loginProvider.doLogout(request, response, false);
			return view;
		} else {
			request.setAttribute("error", new ErrorToken(token.getError()));
			return "common/message";
		}
	}

	@RequestMapping(value="/logout/{serviceProviderId}", method=RequestMethod.GET)
	public String generateSLORequest(final HttpServletRequest request,
									 final HttpServletResponse response,
									 final @PathVariable(value="serviceProviderId") String serviceProviderId) throws IOException, MessageEncodingException, URISyntaxException {
		String view = null;
		if(serviceProviderId != null) {
			final SAMLServiceProvider sp = samlService.getSAMLServiceProvierById(serviceProviderId);
			if(sp != null) {
				log.debug(">>>>> generateSLORequest >>> SAMLServiceProvider.id:" + sp.getId());
				request.getSession(true).setAttribute(SAMLConstants.SAML_SERVICE_PROVIDERS_FOR_SESSION, null);
				log.debug(">>>>> generateSLORequest >>> sp.getLogoutURL():" + sp.getLogoutURL());
				if(StringUtils.isNotBlank(sp.getLogoutURL())) {
					final SAMLLogoutRequestToken<SAMLServiceProvider> token = samlService.generateLogoutRequest(request, serviceProviderId, LogoutResponse.USER_LOGOUT_URI);
					if(token.isError()) {
						request.setAttribute("error", new ErrorToken(token.getError()));
						view = "common/message";
					} else {
						log.debug(">>>>> generateSLORequest >>> token.getSamlObject() :" + (token != null ? token.getSamlObject(): null));
						SAMLSessionUtils.setLastLogoutReqeust(request, token.getSamlObject());
						samlService.redirectAndSend(token, response, request);
					}
				} else { /* just logout, if there is no SLO endpoint */
					log.debug(">>>>> generateSLORequest : logout without SLO endpoint");
					loginProvider.doLogout(request, response, false);
				}
			} else {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, String.format("SP with ID '%s' not found", serviceProviderId));
			}
		}
		return view;
	}

	@RequestMapping(value="/logout", method=RequestMethod.POST)
	public String processSLOResponse(final HttpServletRequest request,
									 final HttpServletResponse response) throws IOException, URISyntaxException {
		final SAMLLogoutResponseToken<SAMLServiceProvider> token = samlService.processLogoutResponse(request, false);
		if(token.isError()) {
			request.setAttribute("error", new ErrorToken(token.getError()));
			return "common/message";
		} else {
			response.sendRedirect(new StringBuilder(URIUtils.getBaseURI(request)).append(logoutURL).toString());
			return null;
		}
	}

	@RequestMapping(value="/login", method=RequestMethod.GET)
	public void login(final HttpServletRequest request,
					  final HttpServletResponse response,
					  final @RequestParam(required=true, value="issuer") String issuer,
					  final @RequestParam(value = "postbackURL", required = false) String postbackURL) throws MessageEncodingException, IOException {
		final SAMLRequestToken<SAMLServiceProvider> token = samlService.generateSAMLRequest(request, issuer);
		if(token.isError()) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, token.getError().toString());
		} else {
			samlService.redirectAndSend(token, response, request, postbackURL);
		}
	}

	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String doSAMLLogin(final HttpServletRequest request, final HttpServletResponse response) throws IOException, MarshallingException, TransformerFactoryConfigurationError, TransformerException {
		final SAMLResponseToken token = samlService.processSAMLResponse(request, response, false);
		if(token.isError()) {
			request.setAttribute("error", new ErrorToken(token.getError()));
			return "common/message";
		} else if(token.getChainedResponseToken() != null) {
			return handleSAMLResponse(token.getChainedResponseToken(), request);
		} else {
			response.sendRedirect(token.getRelayState());
			return null;
		}
	}

	@Override
	protected SAMLService<LogoutRequest, LogoutResponse, SAMLServiceProvider> getSAMLService() {
		return samlService;
	}
}
