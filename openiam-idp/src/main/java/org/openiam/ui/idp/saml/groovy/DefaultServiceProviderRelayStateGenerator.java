package org.openiam.ui.idp.saml.groovy;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.ui.util.SpringContextProvider;
import org.opensaml.saml2.core.Response;
import org.springframework.beans.factory.annotation.Value;

public class DefaultServiceProviderRelayStateGenerator {

	protected HttpServletRequest request;
	protected Response samlResponse;
	protected User user;

	@Value("${org.openiam.sp.login.redirect.success.url}")
	protected String spSuccessURL;

	public DefaultServiceProviderRelayStateGenerator() {
		SpringContextProvider.autowire(this);
		SpringContextProvider.resolveProperties(this);
	}

	public final void init(final HttpServletRequest request, final Response samlResponse, final User user) {
		this.request = request;
		this.samlResponse = samlResponse;
		this.user = user;
	}

	/**
	 * This method can be overridden by a Groovy script.
	 * This method determines where the user gets redirected to after successful authentication into the SAML Service Provider.
	 *
	 * By default the RelayState parameter for this HTTP Request is going to be where the user gets redirected to.  If it's not empty,
	 * the value of ${org.openiam.sp.login.redirect.success.url} will be used, which can be overridden in 
	 * ${confpath}/openiam/conf/openiam.ui.properties
	 *
	 * @return - a relative or absolute URL to redirect the user to after successful authentication into the Service Provider
	 */
	public String getRelayState() {
		final String relayState = request.getParameter("RelayState");
		final String redirectURL = (StringUtils.isNotEmpty(relayState)) ? relayState : spSuccessURL;
		return redirectURL;
	}
}