<%
    response.setHeader("Cache-Control","no-cache");
    response.setHeader("Pragma","no-cache");
    response.setDateHeader ("Expires", -1);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.openiam.com/tags/openiam" prefix="openiam" %>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${titleOrganizatioName} - <fmt:message key="openiam.ui.common.unlock.user" /></title>
    <link href="/openiam-ui-static/css/common/style.css?3.4.0.RELEASE" rel="stylesheet" type="text/css" media="screen" />
    <link href="/openiam-ui-static/css/webconsole/login.css?3.4.0.RELEASE" rel="stylesheet" type="text/css" media="screen" />
    <openiam:overrideCSS />
    <script type="text/javascript" src="/openiam-ui-static/_dynamic/openiamResourceBundle.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-3.3.1.min.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery.corner.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery.boxshadow.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/placeholder/jquery.placeholder.js?3.4.0.RELEASE"></script>
    <script>
        $(function() {

            $("#validate-sms-form").submit(function(e) {
                e.preventDefault();
                var data = {userId:$("#userId").val(), otpCode : $("#otpCode").val()};
                if($("#otpCode").val()) {
                    $("p.error").hide();
                    $("p.error").empty();
                    $.ajax({
                        type:'post',
                        url : '/idp/validateSmsToken.html',
                        data: data
                    }).success(function(res) {
                        if(res.status == 200) {
                            window.location.href = res.redirectURL;
                        } else {
                            $("p.error").show();
                            $("p.error").html(localeManager["openiam.ui.invalid.sms.token"]);
                        }
                    }).error(function(xhr,status,error) {
                        OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
                    });
                }
            });
            $("p.error").hide();
        });
    </script>
    <style type="text/css">
        .fieldset {
            top: 35px;
            position: relative;
        }
        .fieldset .full {
            width: 340px;
            margin-right: 0px;
            margin-left: 9px;
        }
        .loginBtn {
            margin: 10px 25%;
            width: 50%;
        }
    </style>
</head>
<body>
<c:import url="/WEB-INF/jsp/core/login-header.jsp"/>
<div id="pagebg">
    <div id="login">
        <div id="llogo">
            <a href="javascript:void(0);" class="logo llogo">Openiam</a>
        </div>

        <form action="validateSmsToken.html" class="fieldset" id="validate-sms-form">
            <c:set var="loginPlaceholder">
                <fmt:message key='openiam.idp.enter.username' />
            </c:set>

            <input id="userId" type="hidden" name="userId" value="${smsTokenBean.userId}"/>
            <input type="text" id="otpCode" name="otpCode" class="full rounded" value="${smsTokenBean.otpCode}" placeholder="${smsTokenPlaceholder}" />
            <p class="error" style="width:65%;margin: 10px 9% 0px; display: none;">
            </p>
            <div>
                <input id="submit" type="submit" class="redBtn loginBtn" value="<fmt:message key='openiam.ui.common.submit' />" />
            </div>
        </form>

        <div id="lbuttons">

        </div>
    </div>
    <c:import url="/WEB-INF/jsp/core/login-footer.jsp" />
</div>
</body>
</html>