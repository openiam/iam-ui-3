<%
    response.setHeader("Cache-Control","no-cache");
    response.setHeader("Pragma","no-cache");
    response.setDateHeader ("Expires", -1);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.openiam.com/tags/openiam" prefix="openiam" %>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${titleOrganizatioName} - <fmt:message key="openiam.ui.common.unlock.user" /></title>
    <link href="/openiam-ui-static/css/common/style.css?3.4.0.RELEASE" rel="stylesheet" type="text/css" media="screen" />
    <link href="/openiam-ui-static/css/webconsole/login.css?3.4.0.RELEASE" rel="stylesheet" type="text/css" media="screen" />
    <openiam:overrideCSS />
    <script type="text/javascript" src="/openiam-ui-static/_dynamic/openiamResourceBundle.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-3.3.1.min.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery.corner.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery.boxshadow.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/login.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/placeholder/jquery.placeholder.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/hideShowPassword.min.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/json/json.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/user/unlock.password.questions.js"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/user/unlock.sms.js"></script>
    <script>
        $(function() {
            $("#type").change(function() {
                var val = $(this).val();
                if(val == 'challenge') {
                    $("#challenge-div").show();
                    $("#email-div").hide();
                    $("#sms-div").hide();
                } else if(val == 'email') {
                    $("#email-div").show();
                    $("#sms-div").hide();
                    $("#challenge-div").hide();
                } else {
                    $("#email-div").hide();
                    $("#sms-div").show();
                    $("#challenge-div").hide();
                }
            });
            $("#email-div").hide();
            $("#sms-div").hide();
        });
    </script>
<%--    <style type="text/css">
        .title {
            font-size: 10px;
            color: #000000;
        }
        .fieldset .full {
            width: 310px;
        }
        #type {
            width: 310px;
        }
        .mar-left-10 {
            margin-left:10px;
        }
    </style>--%>
</head>
<body>
<c:import url="/WEB-INF/jsp/core/login-header.jsp"/>
<div id="pagebg">
    <div id="login">
        <div id="llogo">
            <a href="javascript:void(0);" class="logo llogo">Openiam</a>
        </div>
        <div id="selection-div" style="margin-top: 125px;">
            <div id="title-selection-form" class="challenge-title challenge-mar-left-10" style="margin-left: 10px;">
                Please choose password retrieval method
            </div>
            <select name="type" id="type" class="full rounded challenge-mar-left-10">
                <option value="challenge"><spring:message code="openiam.ui.challenge.question"/></option>
                 <c:if test="${!empty requestScope.isEmailResetPassword and requestScope.isEmailResetPassword==true}">
                    <option value="email"><spring:message code="openiam.ui.send.email.token"/></option>
                </c:if>
                <c:if test="${!empty requestScope.isSMSResetPassword and requestScope.isSMSResetPassword==true}">
                    <option value="sms"><spring:message code="openiam.ui.send.sms.token"/></option>
                </c:if>
            </select>
        </div>
        <div id="challenge-div">
            <div id="title" class="challenge-title challenge-mar-left-10">
                <fmt:message key="openiam.idp.answer.response.questions.unlock.account"/>
            </div>
            <div class="frameContentDivider">
                <form id="unlockUserForm">
                    <input type="hidden" id="userId" value="${requestScope.userId}"/>
                    <input type="hidden" id="token" value="${requestScope.token}">
                    <c:if test="${! empty requestScope.unlockUserToken.hiddenAttributes}">
                        <c:forEach var="attribute" items="${requestScope.unlockUserToken.hiddenAttributes}">
                            <input type="hidden" id="${attribute.key}" name="${attribute.key}"
                                   value="${attribute.value}"/>
                        </c:forEach>
                    </c:if>
                    <c:if test="${! empty requestScope.modelList}">
                    <table cellpadding="8px" align="center" class="challenge-fieldset">
                        <thead>
                        <tr>
                            <td id="error">
                                <p class="error" style="width:265px;">
                                </p>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="questionModel" items="${requestScope.modelList}">
                            <tr class="question <c:if test="${questionModel.markedAsDeleted}">marked-as-deleted _input_tiptip</c:if>"
                                <c:if test="${questionModel.markedAsDeleted}">title="This Question will be marked as deleted"</c:if>
                                <c:if test="${! empty questionModel.answer}">answerId="${questionModel.answer.id}"</c:if> >
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <span>${questionModel.answer.questionText}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input name="identityAnswer" type="password" class="answer full rounded mainAnswer"
                                                       oiamQuestionId="${questionModel.answer.id}"
                                                       maxlength="255" answerId="${questionModel.answer.id}"
                                                       placeholder="<fmt:message key='openiam.idp.enter.answer' />"
                                                       autocomplete="off"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="2">
                                <ul class="formControls">
                                    <c:if test="${not requestScope.readOnly}">
                                        <li class="leftBtn">
                                            <c:set var="cancelUrl"
                                                   value="${requestScope.auth? '/idp/logout.html': '/selfservice/'}"/>
                                            <a href="${cancelUrl}" class="whiteBtn"><spring:message
                                                    code="openiam.ui.common.cancel"/></a>
                                        </li>
                                        <li class="rightBtn">
                                            <a href="javascript:void(0)">
                                                <input id="save" type="submit" class="redBtn"
                                                       value='<spring:message code="openiam.ui.common.save" />'/>
                                            </a>
                                        </li>
                                    </c:if>
                                </ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                    </c:if>
                    <c:if test="${empty requestScope.modelList}">
                        <div style="color:red;text-align: center"><fmt:message key="openiam.idp.no.question"/></div>
                    </c:if>
                </form>
            </div>
        </div>
        <div id="email-div" style="margin-top: 10px;">
            <c:if test="${!empty requestScope.modelList}">
                <form:form id="unlock-form" method="POST" action="unlockPassword.html" commandName="passwordBean">
                    <div style="margin: 0px 20%;">
                        <form:hidden path="principal" name="principal" initialized="${not empty requestScope.passwordBean.principal}"></form:hidden>
                        <form:hidden path="email" name="email" initialized="${not empty requestScope.passwordBean.email}"></form:hidden>
                        <c:forEach items="${requestScope.emails}" var="email">
                            <input type="radio" name="email" value="${email.emailId}"/> <label>${email.emailAddress}</label>
                            <br>
                            <br>
                        </c:forEach>
                        <input id="submit" type="submit" class="redBtn loginBtn" value="Get email link" />
                    </div>
                </form:form>
            </c:if>
            <c:if test="${empty requestScope.modelList}">
                <div style="color:red;text-align: center"><fmt:message key="openiam.idp.no.question"/></div>
            </c:if>

            <div id="lbuttons">

            </div>
        </div>
        <div id="sms-div" style="margin-top: 10px;">
            <form id="sms-form"  style="margin: 0px 25%;">
                <p class="error" style="display: none;" >
                <c:if test="${not empty requestScope.phones}">
                    <c:forEach items="${requestScope.phones}" var="phone">
                        <div style="padding: 5px 20px;"><input type="radio" name="phone" value="${phone.phoneId}"/> <label>${phone.phoneNbr}</label></div>
                    </c:forEach>
                    <div>
                        <input id="sms-form-submit" type="submit" class="redBtn loginBtn" value='<spring:message code="openiam.ui.send.text"/>'/>
                    </div>
                </c:if>
                <c:if test="${empty requestScope.phones}">
                    <div style="color:red;text-align: center"><fmt:message key="openiam.idp.no.phone"/></div>
                </c:if>
            </form>
        </div>
    </div>
    <c:import url="/WEB-INF/jsp/core/login-footer.jsp" />
</div>
</body>
</html>