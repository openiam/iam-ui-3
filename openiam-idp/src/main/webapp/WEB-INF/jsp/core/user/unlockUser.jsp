<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.openiam.com/tags/openiam" prefix="openiam" %>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${titleOrganizatioName} - <fmt:message key="openiam.ui.common.unlock.user" /></title>
    <link href="/openiam-ui-static/css/common/style.css?3.4.0.RELEASE" rel="stylesheet" type="text/css" media="screen" />
    <link href="/openiam-ui-static/css/webconsole/login.css?3.4.0.RELEASE" rel="stylesheet" type="text/css" media="screen" />
    <openiam:overrideCSS />
    <script type="text/javascript" src="/openiam-ui-static/_dynamic/openiamResourceBundle.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery/jquery-3.3.1.min.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery.corner.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/jquery.boxshadow.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/openiam.common.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/js/common/login.js?3.4.0.RELEASE"></script>
    <script type="text/javascript" src="/openiam-ui-static/plugins/placeholder/jquery.placeholder.js?3.4.0.RELEASE"></script>
    <script>
        $(function() {
            $("#unlock-form").submit(function(e) {
               if($("#username").val()) {
                   $.ajax({
                       method:'get',
                       url : '/idp/userByLogin.html',
                       data: {username:$("#username").val()}
                   }).success(function(data) {
                       if(data) {
                           window.location.href = '/idp/forgetPasswordOptions.html?userId='+data;
                       } else {
                           $("#error").empty().hide();
                           $("#error").show();
                           $("#error").append(
                                $(document.createElement("div")).addClass("error").text("<fmt:message key='openiam.ui.idp.password.unlock.user.not.found' />")
                           );
                       }
                   }).error(function(xhr,status,error) {
                       console.log(status);
                       console.log(xhr);
                       console.log(error);
                   });
               }
               e.preventDefault();
            });
        });
    </script>
</head>
<body>
<c:import url="/WEB-INF/jsp/core/login-header.jsp"/>
<div id="pagebg">
    <div id="login">
        <div id="llogo">
            <a href="javascript:void(0);" class="logo llogo">Openiam</a>
        </div>
        <div id="credentials">
            <fmt:message key="openiam.idp.enter.username.to.password.reset" />
        </div>

        <form:form id="unlock-form" commandName="passwordBean">
            <c:set var="loginPlaceholder">
                <fmt:message key='openiam.idp.enter.username' />
            </c:set>
            <form:input path="principal" id="username" name="username" cssClass="loginField" placeholder="${loginPlaceholder}" initialized="${not empty requestScope.passwordBean.principal}" />
            <div id="error" style="display:none"></div>
            <div>
                <input id="submit" type="submit" class="redBtn loginBtn" value="<fmt:message key='openiam.idp.unlock.account' />" />
            </div>
        </form:form>

        <div id="lbuttons">

        </div>
    </div>
    <c:import url="/WEB-INF/jsp/core/login-footer.jsp" />
</div>
</body>
</html>