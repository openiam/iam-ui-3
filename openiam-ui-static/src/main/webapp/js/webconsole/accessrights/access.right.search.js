OPENIAM = window.OPENIAM || {};

OPENIAM.Entity = {
    init: function () {
        $("#entitlementsContainer").entitlemetnsTable({
            columnHeaders: [
                localeManager["openiam.ui.access.right.name"],
                localeManager["openiam.ui.user.object.class"] + " 1",
                localeManager["openiam.ui.user.object.class"] + " 2",
                localeManager["openiam.ui.common.actions"]
            ],
            columnsMap: ["name", "metadataTypeDisplayName1", "metadataTypeDisplayName2"],
            onEdit: function (bean) {
                window.location.href = "editAccessRight.html?id=" + bean.id;
            },
            hasEditButton: true,
            ajaxURL: "rest/api/accessrights/search",
            pageSize: 20,
            emptyResultsText: localeManager["openiam.ui.access.right.search.empty"],
            showPageSizeSelector: true,
            getAdditionalDataRequestObject: function () {
                return {
                    mdType1: $("#mdType1").val(),
                    mdType2: $("#mdType2").val()
                };
            }
        });
    },
};

$(document).ready(function () {
    OPENIAM.Entity.init();
    $('#search').on('click', function () {
        OPENIAM.Entity.init();
    })
});