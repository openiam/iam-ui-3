OPENIAM = window.OPENIAM || {};
OPENIAM.Entity = {
    init: function () {
        var $this = this;
        $this.populate();
        $("#save").click(function () {
            $this.save();
            return false;
        });

        $("#deleteAccessRight").click(function () {
            $this.onDelete();
        });
    },
    populate: function () {
        $.ajax({
            url: (OPENIAM.ENV.PK != null) ? "rest/api/accessrights/get/" + OPENIAM.ENV.PK : "rest/api/accessrights/new",
            data: JSON.stringify(OPENIAM.ENV.Bean),
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: function (data, textStatus, jqXHR) {
                OPENIAM.ENV.Bean = data;
                $("#languageMap").languageAdmin({
                    bean: OPENIAM.ENV.Bean,
                    beanKey: "languageMap"
                });
                $("#name").val(OPENIAM.ENV.Bean.name);
                $("#mdType1").val(OPENIAM.ENV.Bean.metadataType1);
                $("#mdType2").val(OPENIAM.ENV.Bean.metadataType2);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
            }
        });
    },
    updateJSON: function () {
        var obj = OPENIAM.ENV.Bean;
        obj.languageMap = $("#languageMap").languageAdmin("getMap");
        obj.name = $("#name").val();
        obj.metadataType1 = $("#mdType1").val();
        obj.metadataType2 = $("#mdType2").val();
    },
    save: function () {
        this.updateJSON();
        $.ajax({
            url: "rest/api/accessrights/save",
            data: JSON.stringify(OPENIAM.ENV.Bean),
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            success: function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    OPENIAM.Modal.Success({
                        message: data.successMessage,
                        showInterval: 2000,
                        onIntervalClose: function () {
                            window.location.href = "editAccessRight.html?id=" + data.objectId;
                        }
                    });
                } else {
                    OPENIAM.Modal.Error({
                        errorList: data.errorList
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
            }
        });
    },
    onDelete: function () {
        $.ajax({
            url: "rest/api/accessrights/delete/" + OPENIAM.ENV.PK,
            data: JSON.stringify(OPENIAM.ENV.Bean),
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
            success: function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    OPENIAM.Modal.Success({
                        message: data.successMessage,
                        showInterval: 2000,
                        onIntervalClose: function () {
                            window.location.href = data.redirectURL;
                        }
                    });
                } else {
                    OPENIAM.Modal.Error({
                        errorList: data.errorList
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
            }
        });
    }
};

$(document).ready(function () {
    OPENIAM.Entity.init();
});