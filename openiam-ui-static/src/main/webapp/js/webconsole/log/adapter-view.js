OPENIAM = window.OPENIAM || {};

OPENIAM.SourceAdapterLogView = {
    init: function () {
        $("#resendAction").on('click', function () {
            var text = $('#xmlText').val();
            $.ajax({
                url: "sourceAdapterResend.html",
                data: text,
                type: "POST",
                dataType: "json",
                contentType: "application/json",
                success: function (data, textStatus, jqXHR) {
                    if (data.status == 200) {
                        OPENIAM.Modal.Success({
                            message: data.successMessage,
                            showInterval: 2000,
                            onIntervalClose: function () {
                                window.location.href = data.redirectURL;
                            }
                        });
                    } else {
                        OPENIAM.Modal.Error({
                            errorList: data.errorList
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
                }
            });
        });
    }
};

$(document).ready(function () {
    OPENIAM.SourceAdapterLogView.init();
});