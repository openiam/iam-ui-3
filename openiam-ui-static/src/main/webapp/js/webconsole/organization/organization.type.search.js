OPENIAM = window.OPENIAM || {};
OPENIAM.OrganizationType = {
	init : function() {
		$("#searchTable").entitlemetnsTable({
			ajaxURL : "organization/type/rest/search.html",
			emptyResultsText : OPENIAM.ENV.Text.EmptyResults,
			columnHeaders : [localeManager["openiam.ui.common.name"]],
			columnsMap : ["displayName"],
			hasEditButton : false,
			entityUrl : "organizationTypeEdit.html",
			sortEnable : true,
			initialSortColumn : "displayName"
		});
	}
};

$(document).ready(function() {
	OPENIAM.OrganizationType.init();
});