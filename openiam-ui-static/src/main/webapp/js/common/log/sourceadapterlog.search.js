OPENIAM = window.OPENIAM || {};
OPENIAM.AuditLog = window.OPENIAM.AuditLog || {};

OPENIAM.AuditLog.COLUMN_HEADERS = [
    localeManager["openiam.ui.audit.log.requestor"],
    localeManager["openiam.ui.audit.log.action"],
    localeManager["openiam.ui.audit.log.result"],
    localeManager["openiam.ui.common.date"],
    localeManager["openiam.ui.common.actions"]
];

OPENIAM.AuditLog.COLUMNS_MAP = ["principal", "action", "result", "formattedDate"];

OPENIAM.AuditLog.Form = {
    init: function () {
        var userId = $('#userid').val();
        OPENIAM.AuditLog.Form.draw(!($('#userid').val() == null || $('#userid').val() == ""));
    },
    search: function () {
        OPENIAM.AuditLog.Form.draw(true);
    },
    toJSON: function () {
        var login = $('#login').val();
        var text = $('#text').val();
        var result = $('#result').find(":selected");
        var resultRetVal = null
        if ($(result).val() != '') {
            resultRetVal = $(result).text();
        }
        var userId = $('#userid').val();
        return {id: userId, text: text, login: login, result: resultRetVal};
    },
    draw: function (doRequest) {
        var entityUrl = ($('#userid').val() == null || $('#userid').val() == "" ) ? "viewSourceAdapterRecord.html" : "viewSourceAdapterRecordUser.html"
        $("#entitlementsContainer").entitlemetnsTable({
            getData: doRequest ? null : function () {
                return {size: 0, beans: []};
            },
            columnHeaders: OPENIAM.AuditLog.COLUMN_HEADERS,
            columnsMap: OPENIAM.AuditLog.COLUMNS_MAP,
            hasEditButton: true,
            onEdit: function (bean, options) {
                var params = "";
                $.each(OPENIAM.AuditLog.Form.toJSON(), function (key, value) {
                    params += "&" + key + "=" + value;
                });

                window.location.href = entityUrl + "?id=" + bean.id;
            },
            ajaxURL: "sourceAdapterSearch.html",
            entityUrl: entityUrl,
            entityURLIdentifierParamName: "id",
            pageSize: 20,
            emptyResultsText: localeManager["openiam.ui.audit.log.search.empty"],
            getAdditionalDataRequestObject: function () {
                return OPENIAM.AuditLog.Form.toJSON();
            }
        });
    }
};


$(document).ready(function () {

    var dateCtrl = $("input.date");
    dateCtrl.datepicker({
        dateFormat: OPENIAM.ENV.DateFormatDP,
        showOn: "button",
        changeMonth: true,
        changeYear: true
    }).attr('readonly', 'readonly');
    // try to fix icon position. Temporary solution. Still under
    // investigation
    dateCtrl.each(function () {
        var element = $(this);
        var icon = element.next();
        var el_pos = element.position(), el_h = element.outerHeight(false), el_mt = parseInt(element.css('marginTop'), 10) || 0, el_w = element
            .outerWidth(false), el_ml = parseInt(element.css('marginLeft'), 10) || 0,

            i_w = icon.outerWidth(true), i_h = icon.outerHeight(true);

        var new_icon_top = el_pos.top + el_mt + ((el_h - i_h) / 2);
        var new_icon_left = el_pos.left + el_ml + el_w - i_w;

        var icon_pos = icon.position();

        if (icon_pos.top != new_icon_top) {
            icon.css('top', new_icon_top);
        }
        if (icon_pos.left != new_icon_left) {
            icon.css('left', new_icon_left);
        }
        icon.css('float', 'none');
    });


    $("#cleanSearchForm").click(function () {
        $("#userSearchForm").find("input, select").not(":input[type=submit]").val('');
    });

    $("#searchFilter").multiselect({
        header: false,
        click: function (event, ui) {
            var $el = $("#userSearchFormTable #" + ui.value);
            if (ui.checked) {
                $el.show();
            } else {
                $el.find("input, select").val('');
                $el.hide();
            }
        },
        noneSelectedText: localeManager["openiam.ui.user.add.more.search.criteria"]
    });

    $("#searchLogs").click(function () {
        OPENIAM.AuditLog.Form.search();
        return false;
    });

    OPENIAM.AuditLog.Form.init();

});