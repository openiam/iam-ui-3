$(document).ready(function() {
    $("#sms-form").submit(function() {
        var phone = $("input[name='phone']:checked").val();
        if(phone) {
            $("#sms-form p.error").hide();
            var data = {};
            data.phoneId = phone;
            data.userId = $("#userId").val();
            $.ajax({
                url: "sendSmsToken.html",
                "data": data,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                success: function (data, textStatus, jqXHR) {
                    if (data.status == 200) {
                        if (data.redirectURL != null && data.redirectURL != undefined && data.redirectURL.length > 0) {
                            window.location.href = data.redirectURL;
                        } else {
                            window.location.href = "/selfservice"
                        }
                    } else {
                        $("#sms-form p.error").show();
                        $("#sms-form p.error").empty();
                        $("#sms-form p.error").text(localeManager["openiam.ui.internal.error"]);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#sms-form p.error").show();
                    $("#sms-form p.error").empty();
                    $("#sms-form p.error").text(localeManager["openiam.ui.internal.error"]);
                }

            });
        }
        return false;
    });
});