OPENIAM = window.OPENIAM || {};
OPENIAM.ChangePassword = {
    init: function () {

        $("#currentPassword").keyup(function () {
            $("#currentPasswordHidden").val($(this).val());
        });

        $("#newPassword").keyup(function () {
            $("#newPasswordHidden").val($(this).val());
        });

        $("#newPasswordConfirm").keyup(function () {
            $("#newPasswordConfirmHidden").val($(this).val());
        });

        $("#newPassword").passwordRules();

        $("#submit").on('click', function () {
            $("#buttonPanel").hide();
            $("#statusInfo").show();
            OPENIAM.ChangePassword.post();
        })
    },
    post: function () {
        $.ajax({
            "url": "/idp/changePassword.html",
            "data": JSON.stringify(OPENIAM.ChangePassword.toJson()),
            contentType: "application/json",
            type: "POST",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    if (data.successMessage && !OPENIAM.ENV.changeReason) {
                        if (data.contextValues.windowType == "WARN") {
                            OPENIAM.Modal.Warn({
                                message: data.successMessage,
                                buttons : true,
                                OK : {
                                    text : localeManager["openiam.ui.button.ok"],
                                    onClick : function() {
                                        if (OPENIAM.ENV.postbackURL) {
                                            if (OPENIAM.ENV.postbackURL != null && OPENIAM.ENV.postbackURL != undefined && OPENIAM.ENV.postbackURL.length > 0) {
                                                window.location.href = OPENIAM.ENV.postbackURL;
                                            } else {
                                                window.location.reload(true);
                                            }
                                        }
                                    }
                                }
                            });
                        } else {
                            OPENIAM.Modal.Success({
                                message: data.successMessage, showInterval: 3000, onIntervalClose: function () {
                                    if (OPENIAM.ENV.postbackURL) {
                                        if (OPENIAM.ENV.postbackURL != null && OPENIAM.ENV.postbackURL != undefined && OPENIAM.ENV.postbackURL.length > 0) {
                                            window.location.href = OPENIAM.ENV.postbackURL;
                                        } else {
                                            window.location.reload(true);
                                        }
                                    }
                                }
                            });
                        }
                    } else {
                        if (OPENIAM.ENV.postbackURL != null && OPENIAM.ENV.postbackURL != undefined && OPENIAM.ENV.postbackURL.length > 0) {
                            window.location.href = OPENIAM.ENV.postbackURL;
                        } else {
                            window.location.reload(true);
                        }
                    }
                } else {
                    OPENIAM.Modal.Error({html: OPENIAM.PasswordPolicy.getFromAjaxResponse(data)});
                    $("#statusInfo").hide();
                    $("#buttonPanel").show();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
                $("#statusInfo").hide();
                $("#buttonPanel").show();
            }
        });
    },
    toJson: function () {
        var obj = {};
        obj.login = $('input[name=login]').val();
        obj.userId = $('input[name=userId]').val();
        obj.currentPassword = $('#currentPassword').val();
        obj.newPassword = $('#newPassword').val();
        obj.newPasswordConfirm = $('#newPasswordConfirm').val();
        return obj;
    },
    redirectToLogin: function (postbackURL) {
        setTimeout(function () {
            window.location.href = "/idp/login.html" + (postbackURL ? ("?postbackURL=" + postbackURL) : "");
        }, 3000);
    }
};

$(document).ready(function () {
    OPENIAM.ChangePassword.init();

});

$(window).on('load', function () {
    setTimeout(function () {
        if ('CHANGE_PASSWORD_SESSION_EXPIRED' != OPENIAM.ENV.ErrorType) {
            if (OPENIAM.ENV.AjaxResponse != null && OPENIAM.ENV.AjaxResponse.status == 500) {
                OPENIAM.Modal.Error({html: OPENIAM.PasswordPolicy.getFromAjaxResponse(OPENIAM.ENV.AjaxResponse)});
            }
        }
    }, 1000);
});