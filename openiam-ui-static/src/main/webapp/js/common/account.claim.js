OPENIAM = window.OPENIAM || {};
OPENIAM.ClaimAccount = {
    init: function() {

        $("#submit").click(function () {
            $("#cancel").hide();
            $(this).hide();
            if (OPENIAM.ENV.AjaxResponse == null && OPENIAM.ENV.AjaxResponse.status == 500){
                OPENIAM.Modal.Error(localeManager["openiam.ui.invalid.request"]);
                $(this).show();
            }
        });
    }
};

$(document).ready(function () {
    OPENIAM.ClaimAccount.init();

});