console = window.console || {};
console.log = window.console.log || function () {
    };

(function ($) {
    initialized = false;
    var privateMethods = {
        request: function () {
            var $this = this;
            var $options = $this.data("accessRightsGetterOpts");
            $.ajax({
                url: "rest/api/accessrights/search",
                data: {from: 0, size: 100},
                type: "GET",
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    $options.data = data.beans;
                    initialized = true;
                    $options.onLoaded.call($this);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
                }
            });
        }
    };

    var methods = {
        getAllAsCheckboxes: function (rightIds, readOnly, mdType1, mdType2) {
            if ("" == mdType2) {
                mdType2 = null;
            }
            if ("" == mdType1) {
                mdType1 = null;
            }
            var $this = this;
            var $options = $this.data("accessRightsGetterOpts");
            var retval = [];
            $.each($options.data, function (idx, right) {
                if ((right.metadataType1 == mdType1 && right.metadataType2 == mdType2) || (right.metadataType1 == mdType2 && right.metadataType2 == mdType1)) {
                    retval.push({
                        checkbox: $(document.createElement("input"))
                            .attr("type", "checkbox")
                            .data("right", right)
                            .attr("data-role", "access-right")
                            .val(right.id)
                            .prop("readonly", (readOnly === true))
                            .prop("checked", $.inArray(right.id, rightIds) != -1)
                            .click(function (e) {
                                /* for some reason, if the event propagates up, the checkbox gets automatically unchecked */
                                /* so we won't let it propagate to the rougue DOM */
                                e.stopPropagation();
                            }),
                        label: right.displayName
                    });
                }
            });
            return retval;
        },
        getAllAsCheckboxesInUnorderedList: function (rightIds, readOnly, mdType1, mdType2) {
            var $this = this;
            var $options = $this.data("accessRightsGetterOpts");
            var retval = $(document.createElement("ul"));
            $.each(methods.getAllAsCheckboxes.call($this, rightIds, readOnly, mdType1, mdType2), function (idx, checkbox) {
                retval.append(
                    $(document.createElement("li")).append(
                        checkbox.checkbox,
                        $(document.createElement("label")).text(checkbox.label)
                    )
                );
            });
            return retval;
        },
        getAll: function () {
            var $this = this;
            var $options = $this.data("accessRightsGetterOpts");
            var retval = [];
            $.each($options.data, function (idx, right) {
                retval.push({id: right.id, name: right.displayName});
            });
            return retval;
        },
        getAllAsUnorderedList: function (rightIds) {
            var $this = this;
            var $options = $this.data("accessRightsGetterOpts");
            var retval = $(document.createElement("ul"));
            $.each(methods.getAll.call($this), function (idx, right) {
                if ($.inArray(right.id, rightIds) > -1) {
                    retval.append($(document.createElement("li")).text(right.name));
                }
            });
            return retval;
        },
        init: function (args) {
            var $this = this;
            var options = $.extend({
                onLoaded: function () {
                }
            }, args);

            $this.data("accessRightsGetterOpts", options);
            privateMethods.request.call($this);
        }
    };

    $.fn.accessRightsGetter = function (method) {
        if (this.length > 0) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('Method ' + method + ' does not exist on jQuery.attributeTableEdit');
            }
        }
    };
})(jQuery);

$.accessRightsGetter = {};
$.accessRightsGetter.getAllAsCheckboxes = function (rightIds, readOnly, mdType1, mdType2) {
    return $(document).accessRightsGetter("getAllAsCheckboxes", rightIds, readOnly, mdType1, mdType2);
};
$.accessRightsGetter.getAll = function () {
    return $(document).accessRightsGetter("getAll")
};
$.accessRightsGetter.getAllAsUnorderedList = function (rightIds) {
    return $(document).accessRightsGetter("getAllAsUnorderedList", rightIds);
}
$.accessRightsGetter.getAllAsCheckboxesInUnorderedList = function (rightIds, readOnly, mdType1, mdType2) {
    return $(document).accessRightsGetter("getAllAsCheckboxesInUnorderedList", rightIds, readOnly, mdType1, mdType2);
}