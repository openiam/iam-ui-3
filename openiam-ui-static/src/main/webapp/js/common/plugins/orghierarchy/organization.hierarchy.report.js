console = window.console || {};
console.log = window.console.log || function() {
};

$(function() {
    var cssDependencies = [
        "/openiam-ui-static/js/common/plugins/modalsearch/modal.search.css"
    ];

    var javascriptDependencies = [
        "/openiam-ui-static/js/common/search/organization.search.js",
        "/openiam-ui-static/js/common/plugins/modalEdit/modalEdit.js"
    ];
    var privateMethods = {
        init : function() {
            var $this = this;
            var allowedParetnsType=$this.data("options").allowedParetnsType;
            var parentId = $this.data("options").parentId;
            var nextElement = $($this.data("options").elementToShow);
            $("#editDialog").organizationDialogSearch({
                searchTargetElmt : "#editDialog .emptyContainer",
                showResultsInDialog : false,
                showResultsInSameDialog: true,
                organizationTypes: allowedParetnsType,
                onAdd : function(bean) {
                    privateMethods.addData.call($this, nextElement, bean);
                },
                pageSize : 5,
                parentId : parentId,
                isUncoverParents : true
            });
        },
        addData : function(element, bean){
            var $this = this;
            var options = $this.data("options");

            if(bean!=null && bean.id!=null && bean.id!=undefined){
                privateMethods.addBean.call($this, bean);
                // hide result dialog
                privateMethods.addElement.call($this,bean);
                if($("#editDialog").is(':data(dialog)')) {
                    $("#editDialog").dialog("close");
                }
                // show next element
                if (element) {
                    element.parents("tr").show();
                }
            }
        },
        addBean : function(bean){
            this.data("entity",bean);
            this.data("options").onAdd(bean);
        },
        addElement : function(bean) {
            var $this = this;
            $this.hide();
            var sp = document.createElement("span");
            sp.innerHTML=bean.name;
            sp.style.float = "left";

            var a = document.createElement("a");
            a.className="choice-close ui-icon ui-icon-closethick";
            a.href="javascript:void(0)";

            $(a).click(function(){
                privateMethods.removeElement.call($this);
            });
            $this.parents("tr").children("td").eq(1).append(sp,a);
            $this.hide();
        },
        removeElement : function(){
            var $this = this;
            var org = $this.data("org");
            $("."+org.id.toLowerCase()+"-btn").show();
            $("."+org.id.toLowerCase()+"-row").children("td").eq(1).empty();
            while(org.child) {
                org = org.child;
                $("."+org.id.toLowerCase()+"-row").children("td").eq(1).empty();
                $("."+org.id.toLowerCase()+"-btn").show();
                $("."+org.id.toLowerCase()+"-row").hide();
            }
        }
    };

    var methods = {
        init : function (args) {
            var $this = this;
            $this.data("options", args);
            $.each(cssDependencies, function(idx, file) {
                OPENIAM.lazyLoadCSS(file);
            });

            OPENIAM.loadScripts(javascriptDependencies, function() {
                privateMethods.init.call($this);
            });
        }
    };


    $.fn.orgHierarchy = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.organizationHierarchy');
        }
    };

    OPENIAM.ORGHIERARCHY.orgHierarchy = function(args) {
        this.data("options",args);
        methods.init.call(this,args);
    };
})();