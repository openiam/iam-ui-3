OPENIAM = window.OPENIAM || {};

OPENIAM.EditGroupBootstrap = {
    onReady: function (readyArgs) {
        var $this = this;

        $("#uiTemplate").openiamUITemplate({
            templateObject: OPENIAM.ENV.UITEMPLATE,
            onTemplateObjectEmpty: function () {
                $("#uiTemplateTable").hide();
            },
            onAppend: function (label, element) {
                var tr = document.createElement("tr");
                var tdl = document.createElement("td");
                $(tdl).append(label);
                var tde = document.createElement("td");
                $(tde).append(element);
                $(tr).append(tdl, tde);

                $(this).append(tr);
            },
            onFinishedDrawing: function () {
            }
        });


        $("#saveGroup").click(function () {
            $this.onSubmit(readyArgs.editURL, JSON.stringify(readyArgs.editData()));
            return false;
        });

        $("#deleteGroup").click(function () {
            $this.showConfirmForDeleteGroup($this, readyArgs);
            return false;
        });
    },
    onSubmit: function (url, data) {
        var validationError = null;
        if (typeof validate != "undefined" && validate != null && validate != undefined && $.isFunction(validate)) {
            validationError = validate();
        }
        if (validationError) {
            OPENIAM.Modal.Error(validationError);
        } else {
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                dataType: "json",
                contentType: "application/json",
                success: function (data, textStatus, jqXHR) {
                    if (data.status == 200) {
                        OPENIAM.Modal.Success({
                            message: data.successMessage, showInterval: 3000, onIntervalClose: function () {
                                if (data.redirectURL != null && data.redirectURL != undefined && data.redirectURL.length > 0) {
                                    window.location.href = data.redirectURL;
                                } else {
                                    window.location.reload(true);
                                }
                            }
                        });
                    } else {
                        OPENIAM.Modal.Error({errorList: data.errorList});
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
                }
            });
        }
    },
    showConfirmForDeleteGroup: function ($this, readyArgs) {
        OPENIAM.Modal.Warn({
            message: localeManager["openiam.ui.shared.group.delete.warning"],
            buttons: true,
            OK: {
                text: localeManager["openiam.ui.shared.group.delete.confirmation"],
                onClick: function () {
                    OPENIAM.Modal.Close();
                    $this.onSubmit(readyArgs.deleteURL, readyArgs.deleteData());
                }
            },
            Cancel: {
                text: localeManager["openiam.ui.button.cancel"],
                onClick: function () {
                    OPENIAM.Modal.Close();
                }
            }
        });
    }

};