OPENIAM = window.OPENIAM || {};
OPENIAM.Group = window.OPENIAM.Group || {};

OPENIAM.Group.Form = {
	addOwner:function(typeName, bean){
		var type = localeManager["openiam.ui.common."+typeName]
		$("#groupOwnerId").val(bean.id);
		$("#groupOwnerType").val(typeName);
		$("#groupOwner").val(type +" - " + bean.name);
		//$("#selectGroupOwner").remove()
	},
}

$(document).ready(function() {
    OPENIAM.EditGroupBootstrap.onReady({
        editURL: "editGroup.html",
        editData: function () {
            var obj = {};

            var group = {
                id: OPENIAM.ENV.GroupId,
                mdTypeId: $("#groupTypeId").val(),
                metadataTypeName: $("#groupType").val(),
                name: $("#name").val(),
                description: $("#description").val(),
                managedSysId: $("#managedSysId").val(),
                classificationId: $("#classificationId").val(),
                adGroupTypeId: $("#adGroupTypeId").val(),
                adGroupScopeId: $("#adGroupScopeId").val(),
                riskId: $("#riskId").val(),
                maxUserNumber: $("#maxUserNumber").val(),
                membershipDuration: $("#membershipDuration").val(),

            };

            group.owner = {};
            group.owner.type = $("#groupOwnerType").val();
            group.owner.id = $("#groupOwnerId").val();

            group.organizations = [];
            if (OPENIAM.ENV.supportsGroupOrgs) {
                var orgids = $("#organizationsTable").organizationHierarchyWrapper("getValues");
                if (orgids != null && orgids != undefined && orgids.length > 0) {
                    for (var i = 0; i < orgids.length; i++) {
						group.organizations.push({id: orgids[i]});
                    }
                }
            }

            var parentId = $("#groupParent").selectableSearchResult("getId");
            if (parentId != null && parentId != undefined) {
                group.parentGroups = [];
                group.parentGroups.push({id: parentId});
            }


            obj.targetObject = group;
            obj.pageTemplate = $("#uiTemplate").openiamUITemplate("getObject");
            return obj;
        },
        deleteURL: "deleteGroup.html",
        deleteData: function () {
            return OPENIAM.ENV.Group.id;
        }
    });

	var elements = $("#basicInfoElements").remove();
	var children = elements.children();
	children.sort(function(a, b) {
		var aOrder = parseInt($(a).attr("displayorder"));
		var bOrder = parseInt($(b).attr("displayorder"));
		return aOrder - bOrder;
	});
	var tr = null;
	var tbody = $("#basicInfoTable").find("tbody");
	children.each(function(idx, elmt) {
		var chdrn =  $(elmt).children();

		tr = document.createElement("tr");
		var tdl = document.createElement("td");
		var tde= document.createElement("td");
		$(tdl).append(chdrn[0]);
		$(tde).append(chdrn[1]);
		$(tr).append(tdl,tde);
		tbody.append(tr);
	});

	var hierarchy = OPENIAM.ENV.OrganizationHierarchy;
	if (hierarchy == null || hierarchy == undefined || hierarchy.length == 0 || $("#organizationsTable").length == 0) {
		$("#organizationsTable").hide();
	} else {
		$("#organizationsTable").organizationHierarchyWrapper({
			hierarchy : OPENIAM.ENV.OrganizationHierarchy,
			selectedOrgs : OPENIAM.ENV.OrgParamList,
			draw : function(select, labelText) {
				if(this.idx == undefined) {
					this.idx = 0;
					this.tr = null;
				}
				if (this.idx == 0 || this.idx % 3 == 0) {
					this.tr = document.createElement("tr");
					this.append(this.tr);
				}
				var label = document.createElement("label");
				$(label).text(labelText);
				var td = document.createElement("td");
				$(td).append(label).append("<br/>");
				$(td).append(select);
				$(this.tr).append(td);
				this.idx++;
			},
			hide : function(select) {
				select.closest("td").hide();
			},
			show : function(select) {
				select.closest("td").show();
			}
		});
	}

	var initialParent = null;
	if(OPENIAM.ENV.Group.parentGroups && OPENIAM.ENV.Group.parentGroups.length>0){
		if(OPENIAM.ENV.Group.parentGroups[0] && OPENIAM.ENV.Group.parentGroups[0].id){
			initialParent={};
			initialParent.id=OPENIAM.ENV.Group.parentGroups[0].id;
			initialParent.name=OPENIAM.ENV.Group.parentGroups[0].name;
		}
	}

	$("#groupParent").selectableSearchResult({
		singleSearch : true,
		noneSelectedText : localeManager["openiam.ui.shared.group.search"],
		addMoreText : localeManager["openiam.ui.common.group.change"],
		initialBeans : (initialParent!=null) ? [initialParent]:null,
		onClick : function($that) {
			$("#editDialog").groupDialogSearch({
				closedDialogOnSelect : true,
				searchTargetElmt : "#editDialog",
				showResultsInDialog : true,
				onAdd : function(bean) {
					$("#groupParent").selectableSearchResult("add", bean);
				},
				pageSize : 5
			});
		}
	});

	$("#selectGroupOwner").click(function(){
		OPENIAM.Modal.Warn({
			title : localeManager["openiam.ui.group.owner.type.to.select"],
			buttons : true,
			OK : {
				text : localeManager["openiam.ui.common.user"],
				onClick : function() {
					OPENIAM.Modal.Close();
					$("#editDialog").userSearchForm(
						{
							afterFormAppended : function() {
								$("#editDialog").dialog({
									autoOpen : false,
									draggable : false,
									resizable : false,
									title : localeManager["openiam.ui.common.search.users"],
									width : "auto",
									position : "center"
								});
								$("#editDialog").dialog("open");
							},
							onSubmit : function(json) {
								$("#dialog").userSearchResults(
									{
										"jsonData" : json,
										"page" : 0,
										"size" : 20,
										initialSortColumn : "name",
										initialSortOrder : "ASC",
										url : "rest/api/users/search",
										emptyFormText : localeManager["openiam.ui.common.user.search.empty"],
										emptyResultsText : localeManager["openiam.ui.common.user.search.no.results"],
										onAppendDone : function() {
											$("#editDialog").dialog("close");
											$("#dialog").prepend("<div class=\"\">" + localeManager["openiam.ui.user.supervisor.table.description"] + "</div>")
												.dialog({
													autoOpen : true,
													draggable : false,
													resizable : false,
													title : localeManager["openiam.ui.user.search.result.title"],
													width : "auto"
												})
										},
										onEntityClick : function(bean) {
											$("#dialog").dialog("close");
											//$("#groupOwnerId").val(bean.id);
											//$("#groupOwner").val(localeManager["openiam.ui.common.user"] +" - " + bean.name);
											OPENIAM.Group.Form.addOwner("user", bean);
										}
									});
							}
						});
				}
			},
			No : {
				text : localeManager["openiam.ui.common.group"],
				className:"redBtn",
				onClick : function() {
					OPENIAM.Modal.Close();
					$("#editDialog").groupDialogSearch({
						searchTargetElmt : "#editDialog",
						showResultsInDialog : true,
						onSearchResultClick : function(bean) {
							//$("#groupOwnerId").val(bean.id);
							//$("#groupOwner").val(localeManager["openiam.ui.common.group"] +" - " + bean.name);
							OPENIAM.Group.Form.addOwner("group", bean);
							return false;
						}
					});
				}
			}
		});
	});
});