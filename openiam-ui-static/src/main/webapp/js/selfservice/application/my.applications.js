OPENIAM = window.OPENIAM || {};
OPENIAM.ENV = window.OPENIAM.ENV || {};
OPENIAM.Application = window.OPENIAM.Application || {};

OPENIAM.Application = {

    init: function () {

        var that = this;

        $(".app-list li").each(function() {
            var $li = $(this);
            var eid = $li.prop("id");
            if (eid != null && eid != 'undefined' && eid.indexOf("app-") === 0) {
                var id = eid.substr(4);
                if (id in OPENIAM.ENV.IconsMap) {
                    that.draw($li, OPENIAM.ENV.IconsMap[id]);
                }
            }
        });

    },

    draw : function($el, iconUrl) {
        $el.css("background", "url("+iconUrl+") 50% 0 no-repeat");
    }
};

$(document).ready(function() {
    OPENIAM.Application.init();
});