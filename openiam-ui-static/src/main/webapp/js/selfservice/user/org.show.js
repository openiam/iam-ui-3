OPENIAM = window.OPENIAM || {};
OPENIAM.Org = {
    Load: {
        onReady: function () {
            OPENIAM.Org.Organizations.load();
        }
    },
    Common: {
        load: function (args) {
            $("#orgContainer").entitlemetnsTable({
                columnHeaders: args.columns,
                columnsMap: args.columnsMap,
                ajaxURL: args.ajaxURL,
                entityURLIdentifierParamName: "id",
                requestParamIdName: "id",
                getAdditionalDataRequestObject: args.getAdditionalDataRequestObject,
                requestParamIdValue: OPENIAM.ENV.UserId,
                pageSize: 10,
                preventOnclickEvent: OPENIAM.ENV.PreventOnClick,
                sortEnable: false,
                emptyResultsText: args.emptyResultsText
            });
        }
    },
    Organizations: {
        load: function () {
            var $this = this;
            var _columns = [
                localeManager["openiam.ui.common.organization.name"],
                localeManager["openiam.ui.common.organization.type"]
            ];
            
            OPENIAM.Org.Common.load({
                modalAjaxURL: "rest/api/entitlements/searchOrganizations",
                columns: _columns,
                columnsMap: ["name", "organizationType"],
                ajaxURL: "rest/api/entitlements/getOrganizationsForUser",
                buttonTitle: localeManager["openiam.ui.shared.organization.search"],
                placeholder: localeManager["openiam.ui.shared.organization.type.name"],
                emptyResultsText: localeManager["openiam.ui.user.entitlement.organization.not.found"],
                dialogTitle: localeManager["openiam.ui.shared.organization.search"],
                emptySearchResultsText: localeManager["openiam.ui.shared.organization.search.empty"],
                target: $this
            });
        }
    }
};

$(document).ready(function () {
    if (!$("#orgContainer").length) {return true;}
    OPENIAM.Org.Load.onReady();
});