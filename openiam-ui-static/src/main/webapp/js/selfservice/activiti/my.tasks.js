OPENIAM = window.OPENIAM || {};
OPENIAM.MyTasks = {
    init : function() {
        var $this = this;
        $("#accept").click(function() {
            var selectedList = $("#assignedTasks").entitlemetnsTable("getSelectedItemsId");
            if (selectedList.length > 0) {
                OPENIAM.MyTasks.submitList(selectedList, true);
            } else {
                OPENIAM.Modal.Success({message : localeManager["openiam.ui.selfservice.assigned.tasks.selected.nothing"], showInterval : 3000});
            }
        });
        $("#reject").click(function(){
            var selectedList = $("#assignedTasks").entitlemetnsTable("getSelectedItemsId");
            if (selectedList.length > 0) {
                OPENIAM.MyTasks.submitList(selectedList, false);
            }else {
                OPENIAM.Modal.Success({message : localeManager["openiam.ui.selfservice.assigned.tasks.selected.nothing"], showInterval : 3000});
            }
        });

        $("#acceptClaim").click(function() {
            var selectedList = $("#candidateTasks").entitlemetnsTable("getSelectedItemsId");
            if (selectedList.length > 0) {
                OPENIAM.MyTasks.submitListClaim(selectedList);
            } else {
                OPENIAM.Modal.Success({message : localeManager["openiam.ui.selfservice.assigned.tasks.selected.nothing"], showInterval : 3000});
            }
        });

        $("#searchby").change(function() {
            var selectedVal = $("#searchby").children("option:selected").attr("value");

            if (selectedVal == ""){
                $("#description").val("").hide();
                $("#requesterName").val("").hide();
                $("#selectRequester").hide();
                $("#requesterId").val("");
            } else if (selectedVal == "bydescription"){
                $("#description").show();
                $("#requesterName").val("").hide();
                $("#selectRequester").hide();
                $("#requesterId").val("");
            } else if (selectedVal == "byrequester"){
                $("#description").val("").hide();
                $("#requesterName").show();
                $("#selectRequester").show();
                $("#requesterId").val("");

            }
        });

        $("#selectRequester").click(
            function () {
                $("#dialog").userSearchForm(
                    {
                        afterFormAppended: function () {
                            $("#dialog").dialog({
                                autoOpen: false,
                                draggable: false,
                                resizable: false,
                                title: localeManager["openiam.ui.common.search.users"],
                                width: "auto",
                                position: "center"
                            });
                            $("#dialog").dialog("open");
                        },
                        onSubmit: function (json) {
                            $("#userResultsArea").userSearchResults(
                                {
                                    "jsonData": json,
                                    "page": 0,
                                    "size": 20,
                                    initialSortColumn: "name",
                                    initialSortOrder: "ASC",
                                    url: "/webconsole/rest/api/users/search",
                                    emptyFormText: localeManager["openiam.ui.common.user.search.empty"],
                                    emptyResultsText: localeManager["openiam.ui.common.user.search.no.results"],
                                    isDialogPage: true,
                                    onAppendDone: function () {
                                        $("#dialog").dialog("close");
                                        $("#userResultsArea").prepend("<div class=\"\">" + localeManager["openiam.ui.user.requester.table.description"] + "</div>")
                                            .dialog({
                                                autoOpen: true,
                                                draggable: false,
                                                resizable: false,
                                                title: localeManager["openiam.ui.user.search.result.title"],
                                                width: "auto"
                                            })
                                    },
                                    onEntityClick: function (bean) {
                                        $("#userResultsArea").dialog("close");
                                        $("#requesterId").val(bean.id);
                                        $("#requesterName").val(bean.name);

                                    }
                                });
                        }
                    });
            });


        $("#assignedTasks").entitlemetnsTable({
            columnHeaders : [
                localeManager["openiam.ui.common.name"],
                localeManager["openiam.ui.common.description"],
                localeManager["openiam.ui.common.requested.date"],
                localeManager["openiam.ui.common.actions"]
            ],
            columnsMap : ["name", "description", "createdTime"],
            dateFields: ["createdTime"],
            hasEditButton : true,
            onEdit : function(bean) {
                window.location.href = "task.html?id=" + bean.id;
            },
            onRejectButton : function(bean) {
                OPENIAM.MyTasks.process(false, bean.id);
            },
            onAcceptButton : function(bean) {
                OPENIAM.MyTasks.process(true, bean.id);
            },
            hasAcceptButton : function(bean) {
                return !OPENIAM.MyTasks.isCertificateTask(bean);
            },
            hasRejectButton : function(bean) {
                return !OPENIAM.MyTasks.isCertificateTask(bean);
            },
            isSelectAllowed : true,
            canSelect : function(bean) {
                return !OPENIAM.MyTasks.isCertificateTask(bean);
            },
            ajaxURL : "rest/api/activiti/tasks/assigned",
            entityUrl : "task.html",
            entityURLIdentifierParamName : "id",
            // http://54.227.54.255:8080/browse/IDMAPPS-3073
            //,
            //
            //deleteOptions : {
            //	isDeletable : function(bean) {
            //		return (bean.deletable);
            //	},
            //	onDelete : function(bean) {
            //	$this.deleteTask(bean.id);
            //}
            emptyResultsText : localeManager["openiam.ui.selfservice.my.tasks.have.tasks.assigned"]
        });
        $("#candidateTasks").entitlemetnsTable({
            columnHeaders : [
                localeManager["openiam.ui.common.name"],
                localeManager["openiam.ui.common.description"],
                localeManager["openiam.ui.common.requested.date"],
                localeManager["openiam.ui.common.actions"]
            ],
            columnsMap : ["name", "description", "createdTime"],
            ajaxURL : "rest/api/activiti/tasks/candidate",
            dateFields: ["createdTime"],
            //entityUrl : "task.html",
            //entityURLIdentifierParamName : "id",
            emptyResultsText : localeManager["openiam.ui.selfservice.my.tasks.no.tasks.claim"],
            preventOnclickEvent : true,
            isSelectAllowed : true,
            onAdd : function(bean) {
                $this.claimTask(bean.id);
            }
        });
    },
    isCertificateTask : function(bean) {
        if ((bean.processDefinitionId.indexOf("attestationWorkflow") > -1)||
            (bean.processDefinitionId.indexOf("groupAttestationWorkflow") > -1))
            return true;
        return false;
    },
    process : function(accepted, elId) {
        var obj = this.toJSON(elId);
        obj.accepted = accepted;
        this.submit(obj);
    },
    toJSON : function(elId) {
        var obj = {
            taskId : elId
        };
        return obj;
    },
    submit : function(obj) {
        $.ajax({
            url : "rest/api/activiti/task/decision",
            data : JSON.stringify(obj),
            type: "POST",
            dataType : "json",
            contentType: "application/json",
            success : function(data, textStatus, jqXHR) {
                if(data.status == 200) {
                    OPENIAM.Modal.Success({message : data.successMessage, showInterval : 4000, onIntervalClose : function() {
                        if(data.redirectURL != null && data.redirectURL != undefined && data.redirectURL.length > 0) {
                            window.location.href = data.redirectURL;
                        } else {
                            window.location.reload(true);
                        }
                    }});
                } else {
                    OPENIAM.Modal.Error({errorList : data.errorList});
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
            }
        });

    },
    claimTask : function(taskId) {
        $.ajax({
            url : "rest/api/activiti/task/claim",
            data : { id : taskId},
            type: "POST",
            dataType : "json",
            success : function(data, textStatus, jqXHR) {
                if(data.status == 200) {
                    OPENIAM.Modal.Success({message : data.successMessage, showInterval : 4000, onIntervalClose : function() {
                        if(data.redirectURL != null && data.redirectURL != undefined && data.redirectURL.length > 0) {
                            window.location.href = data.redirectURL;
                        } else {
                            window.location.reload(true);
                        }
                    }});
                } else {
                    OPENIAM.Modal.Error({errorList : data.errorList});
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
            }
        });
    },
    submitList : function(obj, isAccept) {
        var ajaxUrl = "rest/api/activiti/task/" + ((isAccept) ? "decisionListAccept" : "decisionListReject");
        $.ajax({
            url: ajaxUrl,
            data: JSON.stringify(obj),
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            success: function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    OPENIAM.Modal.Success({
                        message: data.successMessage, showInterval: 4000, onIntervalClose: function () {
                            if (data.redirectURL != null && data.redirectURL != undefined && data.redirectURL.length > 0) {
                                window.location.href = data.redirectURL;
                            } else {
                                window.location.reload(true);
                            }
                        }
                    });
                } else {
                    OPENIAM.Modal.Error({errorList: data.errorList});
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
            }
        });
    },
    submitListClaim : function(obj) {
        var ajaxUrl = "rest/api/activiti/task/decisionClaimList";
        $.ajax({
            url: ajaxUrl,
            data: JSON.stringify(obj),
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            success: function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    OPENIAM.Modal.Success({
                        message: data.successMessage, showInterval: 4000, onIntervalClose: function () {
                            if (data.redirectURL != null && data.redirectURL != undefined && data.redirectURL.length > 0) {
                                window.location.href = data.redirectURL;
                            } else {
                                window.location.reload(true);
                            }
                        }
                    });
                } else {
                    OPENIAM.Modal.Error({errorList: data.errorList});
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
            }
        });
    },
    deleteTask : function(taskId) {
        $.ajax({
            url : "rest/api/activiti/task/delete",
            data : { id : taskId},
            type: "POST",
            dataType : "json",
            success : function(data, textStatus, jqXHR) {
                if(data.status == 200) {
                    OPENIAM.Modal.Success({message : data.successMessage, showInterval : 4000, onIntervalClose : function() {
                        if(data.redirectURL != null && data.redirectURL != undefined && data.redirectURL.length > 0) {
                            window.location.href = data.redirectURL;
                        } else {
                            window.location.reload(true);
                        }
                    }});
                } else {
                    OPENIAM.Modal.Error({errorList : data.errorList});
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
            }
        });
    }
};

$(document).ready(function() {
    OPENIAM.MyTasks.init();
    OPENIAM.ENV.DateFormatDP = "mm/dd/yy";
    var curYear = (new Date).getFullYear();
    $("#fromDate, #toDate, #candidateFromDate, #candidateToDate").datepicker({
        dateFormat: OPENIAM.ENV.DateFormatDP,
        showOn: "button",
        changeMonth: true,
        changeYear: true,
        yearRange: "".concat(curYear - 100, ":", curYear + 5)
    });
    $("#assignedTaskSearchForm").submit(function(e) {
        var params = "";
        if($("#fromDate").val()) {
            params = "&fromDate="+$("#fromDate").val();
        }
        if($("#toDate").val()) {
            params = params+"&toDate="+$("#toDate").val();
        }

        if($("#description").val()) {
            params = params+"&description="+$("#description").val();
        }
        if($("#requesterId").val()) {
            params = params+"&requesterId="+$("#requesterId").val();
        }
        $("#assignedTasks").empty();
        $("#assignedTasks").entitlemetnsTable({
            columnHeaders: [
                localeManager["openiam.ui.common.name"],
                localeManager["openiam.ui.common.description"],
                localeManager["openiam.ui.common.requested.date"],
                localeManager["openiam.ui.common.actions"]
            ],
            columnsMap: ["name", "description", "createdTime"],
            dateFields: ["createdTime"],
            hasEditButton: true,
            onEdit: function (bean) {
                window.location.href = "task.html?id=" + bean.id;
            },
            onRejectButton: function (bean) {
                OPENIAM.MyTasks.process(false, bean.id);
            },
            onAcceptButton: function (bean) {
                OPENIAM.MyTasks.process(true, bean.id);
            },
            hasAcceptButton: function (bean) {
                return !OPENIAM.MyTasks.isCertificateTask(bean);
            },
            hasRejectButton: function (bean) {
                return !OPENIAM.MyTasks.isCertificateTask(bean);
            },
            isSelectAllowed: true,
            canSelect: function (bean) {
                return !OPENIAM.MyTasks.isCertificateTask(bean);
            },
            ajaxURL: "rest/api/activiti/tasks/assigned?size=20&sortBy=name&orderBy=ASC" + params,
            entityUrl: "task.html",
            entityURLIdentifierParamName: "id",
            emptyResultsText: localeManager["openiam.ui.selfservice.my.tasks.have.tasks.assigned"]
        });
        e.preventDefault();
    });
    $("#cleanAssignedTaskSearchForm").click(function(e) {
        $("#fromDate").val("");
        $("#toDate").val("");

        $("#description").val("").hide();
        $("#requesterName").val("").hide();
        $("#requesterId").val("");
        $("#searchby").val("");
        $("#selectRequester").hide();
    });

    $("#cleanCandidateTaskSearchForm").click(function(e) {
        $("#candidateFromDate").val("");
        $("#candidateToDate").val("");
    });

    $("#candidateTaskSearchForm").submit(function(e) {
        var params = "";
        if($("#candidateFromDate").val()) {
            params = "&fromDate="+$("#candidateFromDate").val();
        }
        if($("#candidateToDate").val()) {
            params = params+"&toDate="+$("#candidateToDate").val();
        }
        if($("#candidateDescription").val()) {
            params = params+"&description="+$("#candidateDescription").val();
        }
        $("#candidateTasks").empty();
        $("#candidateTasks").entitlemetnsTable({
            columnHeaders : [
                localeManager["openiam.ui.common.name"],
                localeManager["openiam.ui.common.description"],
                localeManager["openiam.ui.common.requested.date"],
                localeManager["openiam.ui.common.actions"]
            ],
            columnsMap : ["name", "description", "createdTime"],
            ajaxURL : "rest/api/activiti/tasks/candidate?size=20&sortBy=name&orderBy=ASC"+params,
            dateFields: ["createdTime"],
            emptyResultsText : localeManager["openiam.ui.selfservice.my.tasks.no.tasks.claim"],
            preventOnclickEvent : true,
            isSelectAllowed : true,
            onAdd : function(bean) {
                $this.claimTask(bean.id);
            }
        });
        e.preventDefault();
    });
});