$(document).ready(function () {

    $('#submit').click(function () {
        var obj = {
            userId: $('input[name="userId"]').val(),
            resetTypeSelect: $('select[name="resetTypeSelect"]').val()
        };

        $.ajax({
            url: "selectResetPasswordType.html",
            data: JSON.stringify(obj),
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            success: function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    OPENIAM.Modal.Success({
                        message: data.successMessage, showInterval: 2000, onIntervalClose: function () {
                            window.location.href = data.redirectURL;
                        }
                    });
                } else {
                    OPENIAM.Modal.Error({errorList: data.errorList});
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                OPENIAM.Modal.Error(localeManager["openiam.ui.internal.error"]);
            }
        });

    });

});
